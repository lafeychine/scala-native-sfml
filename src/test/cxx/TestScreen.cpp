#include "GraphicalTest.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

TestScreen::TestScreen(char const * path)
    : _path(path), _id_screenshot(0)
{
}

void TestScreen::takeScreenshot(sf::Window & window)
{
    std::string filename = "screenshot_" + std::to_string(_id_screenshot) + ".bmp";
    std::string path = std::string(_path) + "/" + std::string(filename);

    sf::Texture texture = sf::Texture();
    texture.create(window.getSize().x, window.getSize().y);
    texture.update(window);

    texture.copyToImage().saveToFile(path);

    _id_screenshot = _id_screenshot + 1;
}
