#include "GraphicalTest.hpp"

#include <iostream>

std::unordered_map<std::string, void (*)(TestScreen &)> registrar;

#define err(msg)                   \
    std::cerr << msg << std::endl; \
    return EXIT_FAILURE;

int main(int argc, char * argv[])
{
    if (argc != 3) {
        err("Usage: " << argv[0] << " <screenshot_path> <test_name>");
    }

    auto search = registrar.find(argv[2]);

    if (search == registrar.end()) {
        err("Test named " << argv[2] << " is not found")
    }

    TestScreen snTestScreen(argv[1]);

    search->second(snTestScreen);

    return EXIT_SUCCESS;
}
