#ifndef __GRAPHICAL_TEST_H_
#define __GRAPHICAL_TEST_H_

#include <SFML/Window.hpp>
#include <unordered_map>

class TestScreen
{
  public:
    TestScreen(char const * path);
    void takeScreenshot(sf::Window & window);

  private:
    char const * _path;
    int _id_screenshot;
};

extern std::unordered_map<std::string, void (*)(TestScreen &)> registrar;

#define snGraphicalTest(name)                                        \
    static void name(TestScreen &);                                  \
    auto reg_##name = registrar.insert(std::make_pair(#name, name)); \
                                                                     \
    static void name(TestScreen & snTestScreen)

#endif /* __GRAPHICAL_TEST_H_ */
