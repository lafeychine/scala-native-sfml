import org.junit.Assert.*
import org.junit.Test

import sfml.window.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Window/ContextSettings.test.cpp */
/* Commit: f6dfc04 */
class ContextSettingsTest extends NativeTest:

    @Test def constructorDefault(): Unit =
        val contextSettings = ContextSettings()

        assertEquals(0, contextSettings.depthBits)
        assertEquals(0, contextSettings.stencilBits)
        assertEquals(0, contextSettings.antialiasingLevel)
        assertEquals(1, contextSettings.majorVersion)
        assertEquals(1, contextSettings.minorVersion)
        assertEquals(Option(ContextSettings.Attribute.Default), contextSettings.attributeFlags)
        assertEquals(false, contextSettings.sRgbCapable)

    @Test def constructorCombinedColorAlphaDefaultParameter(): Unit =
        val contextSettings = ContextSettings(1, 1, 2, 3, 5, ContextSettings.Attribute.Core, true)

        assertEquals(1, contextSettings.depthBits)
        assertEquals(1, contextSettings.stencilBits)
        assertEquals(2, contextSettings.antialiasingLevel)
        assertEquals(3, contextSettings.majorVersion)
        assertEquals(5, contextSettings.minorVersion)
        assertEquals(Option(ContextSettings.Attribute.Core), contextSettings.attributeFlags)
        assertEquals(true, contextSettings.sRgbCapable)
