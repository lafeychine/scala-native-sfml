import scala.scalanative.unsafe.*

import sfml.graphics.Texture
import sfml.window.Window

// NOTE: Currently, cannot use JUnit's rules to retrieve the test name -> Use of `testName` variable
class TestScreen(var folderPath: String = ".", var testName: String = ""):
    private var idScreenshot = 0

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    def takeScreenshot(window: Window): Unit =
        val path = s"${folderPath}/scala/screenshot_${idScreenshot}.bmp"

        val texture = Texture()
        texture.create(window.size.x, window.size.y)
        texture.update(window)

        val image = texture.copyToImage()
        image.saveToFile(path)

        idScreenshot = idScreenshot + 1

        flushMemory()

    def screenshots: Int = idScreenshot - 1
