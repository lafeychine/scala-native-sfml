import scala.scalanative.unsafe.*

package scala.scalanative.runtime: // scalafix:ok DirectoryAndPackageName
    def gc_collect(): Unit =
        GC.collect()

private def fillMemory(): Unit =
    Zone {
        val array = alloc[Int](2048)

        for i <- 0 until 2048 do array(i) = i
    }

def flushMemory(): Unit =
    fillMemory()
    scala.scalanative.runtime.gc_collect()

    fillMemory()
    scala.scalanative.runtime.gc_collect()
