import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*
import sfml.system.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/RectangleShape.test.cpp */
/* Commit: f4e0c4b */
class RectangleShapeTest extends NativeTest:

    @Test def constructorDefault(): Unit =
        val rectangle = RectangleShape()
        assertEquals(Vector2[Float](0, 0), rectangle.size)
        assertEquals(4, rectangle.pointCount)

        for i <- 0 until 4 do assertEquals(Vector2[Float](0, 0), rectangle.point(i))

    @Test def constructorSize(): Unit =
        val points = Array[Vector2[Float]](
            (0, 0),
            (9, 0),
            (9, 8),
            (0, 8)
        )

        val rectangle = RectangleShape((9, 8))
        assertEquals(Vector2[Float](9, 8), rectangle.size)
        assertEquals(4, rectangle.pointCount)

        for i <- 0 until 4 do assertEquals(points(i), rectangle.point(i))

    @Test def size(): Unit =
        val rectangle = RectangleShape((7, 6))
        rectangle.size = (5, 4)

        assertEquals(Vector2[Float](5, 4), rectangle.size)
