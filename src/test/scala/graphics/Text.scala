import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*
import sfml.window.*

class TextTest extends NativeTest:

    @Test def fontConstructor(): Unit =
        val font = Font()
        font.loadFromFile("src/test/resources/tuffy.ttf")

        val text = Text("", font)

        // TODO: Get string
        // TODO: Compare reference pointer
        assertEquals(30, text.characterSize)
        assertEquals(1f, text.letterSpacing, 0.0001)
        assertEquals(1f, text.lineSpacing, 0.0001)
        // TODO: Style
        assertEquals(Color.White(), text.fillColor)
        assertEquals(Color.Black(), text.outlineColor)
        assertEquals(0, text.outlineThickness, 0)
        // TODO: FindCharacterPos
        assertEquals(Rect[Float](), text.localBounds)
        assertEquals(Rect[Float](), text.globalBounds)

    @Test def getGlobalBounds(): Unit =
        val font = Font()
        font.loadFromFile("src/test/resources/tuffy.ttf")

        val text = Text("Hello, world!", font, 50)
        assertEquals(Rect[Float](3, 13, 242, 43), text.globalBounds)

class TextGraphicalTest extends GraphicalTest:

    @Test def graphicalTest(): Unit =
        snTestScreen.testName = "Text"

        // Setup
        val window = RenderWindow(VideoMode(1024, 768), "Test")

        val font = Font()
        font.loadFromFile("src/test/resources/tuffy.ttf")

        val text = Text("Hello World", font, 50)

        window.isOpen()

        // Control test
        window.clear()
        window.draw(text)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Text::setPosition
        text.position = (100, 100)

        window.clear()
        window.draw(text)
        window.display()
        snTestScreen.takeScreenshot(window)

        // Teardown
        window.close()
