import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*
import sfml.system.*

private class TriangleShape(size: Vector2[Float] = Vector2[Float]()) extends Shape:

    override def point(index: Long): Vector2[Float] =
        index match
            case 0 => Vector2(size.x / 2, 0)
            case 1 => Vector2(0, size.y)
            case _ => size

    override def pointCount: Long = 3

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/Shape.test.cpp */
/* Commit: f4e0c4b */
class ShapeTest extends NativeTest:

    @Test def constructorDefault(): Unit =
        val triangleShape = TriangleShape((0, 0))

        assertEquals(None, triangleShape.texture)
        assertEquals(Rect[Int](), triangleShape.textureRect)
        assertEquals(Color.White(), triangleShape.fillColor)
        assertEquals(Color.White(), triangleShape.outlineColor)
        assertEquals(0, triangleShape.outlineThickness, 0)
        assertEquals(Rect[Float](), triangleShape.localBounds)
        assertEquals(Rect[Float](), triangleShape.globalBounds)

    @Test def textureRect(): Unit =
        val triangleShape = TriangleShape()
        triangleShape.textureRect = (4, 5, 6, 7)

        assertEquals(Rect[Int](4, 5, 6, 7), triangleShape.textureRect)

    @Test def fillColor(): Unit =
        val triangleShape = TriangleShape()
        triangleShape.fillColor = Color.Cyan()

        assertEquals(Color.Cyan(), triangleShape.fillColor)

    @Test def outlineColor(): Unit =
        val triangleShape = TriangleShape()
        triangleShape.outlineColor = Color.Magenta()

        assertEquals(Color.Magenta(), triangleShape.outlineColor)

    @Test def outlineThickness(): Unit =
        val triangleShape = TriangleShape((10, 10))
        triangleShape.outlineThickness = 3.14f

        assertEquals(3.14f, triangleShape.outlineThickness, 0)

    @Test def virtualFunctions(): Unit =
        val triangleShape = TriangleShape((2, 2))

        assertEquals(3, triangleShape.pointCount)

        assertEquals(Vector2(1f, 0f), triangleShape.point(0))
        assertEquals(Vector2(0f, 2f), triangleShape.point(1))
        assertEquals(Vector2(2f, 2f), triangleShape.point(2))

    // Need update() to be called
    // @Test def bounds(): Unit =
    //     val triangleShape = TriangleShape((2, 3))

    //     assertEquals(Rect[Float](0, 0, 2, 3), triangleShape.localBounds)
    //     assertEquals(Rect[Float](0, 0, 2, 3), triangleShape.globalBounds)

    //     triangleShape.move((1, 1))
    //     triangleShape.rotate(90)

    //     assertEquals(Rect[Float](0, 0, 2, 3), triangleShape.localBounds)
    //     assertEquals(-2f, triangleShape.globalBounds.left, 0.0001)
    //     assertEquals(1f, triangleShape.globalBounds.top, 0.0001)
    //     assertEquals(3f, triangleShape.globalBounds.width, 0.0001)
    //     assertEquals(2f, triangleShape.globalBounds.height, 0.0001)
