import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*
import sfml.system.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/Rect.test.cpp */
/* Commit: 9cb4a68 */
class RectTest extends NativeTest:

    @Test def constructorDefault(): Unit =
        val rectangle = Rect[Int]()

        assertEquals(0, rectangle.left)
        assertEquals(0, rectangle.top)
        assertEquals(0, rectangle.width)
        assertEquals(0, rectangle.height)

    @Test def constructorElements(): Unit =
        val rectangle = Rect(1, 2, 3, 4)

        assertEquals(1, rectangle.left)
        assertEquals(2, rectangle.top)
        assertEquals(3, rectangle.width)
        assertEquals(4, rectangle.height)

    @Test def constructorVector2(): Unit =
        val rectangle = Rect[Int]((1, 2), (3, 4))

        assertEquals(1, rectangle.left)
        assertEquals(2, rectangle.top)
        assertEquals(3, rectangle.width)
        assertEquals(4, rectangle.height)

    @Test def containsVector2(): Unit =
        val rectangle = Rect(0, 0, 10, 10)

        assertTrue(rectangle.contains((0, 0)))
        assertTrue(rectangle.contains((9, 0)))
        assertTrue(rectangle.contains((0, 9)))
        assertTrue(rectangle.contains((9, 9)))
        assertFalse(rectangle.contains((9, 10)))
        assertFalse(rectangle.contains((10, 9)))
        assertFalse(rectangle.contains((10, 10)))
        assertFalse(rectangle.contains((15, 15)))

    @Test def findIntersection(): Unit =
        val rectangle = Rect(0, 0, 10, 10)
        val intersectingRectangle = Rect(5, 5, 10, 10)

        val intersectionResult = rectangle.intersects(intersectingRectangle)
        assertTrue(intersectionResult.isDefined)

        val Some(intersection) = intersectionResult: @unchecked
        assertEquals(5, intersection.top)
        assertEquals(5, intersection.left)
        assertEquals(5, intersection.width)
        assertEquals(5, intersection.height)

    @Test def findNoIntersection(): Unit =
        val rectangle = Rect(0, 0, 10, 10)
        val intersectingRectangle = Rect(-5, -5, 5, 5)

        assertFalse(rectangle.intersects(intersectingRectangle).isDefined)
