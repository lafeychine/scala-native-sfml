import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*

private class DrawableClass(private var counter: Int = 0) extends Drawable:

    def callCount: Int = counter

    override def draw(target: RenderTarget, states: RenderStates): Unit =
        counter += 1

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/Drawable.test.cpp */
/* Commit: 976bbda */
class DrawableTest extends NativeTest:

    @Test
    def constructor(): Unit =
        val drawable = DrawableClass()
        assertEquals(0, drawable.callCount)

    @Test
    def draw(): Unit =
        val drawable = DrawableClass()
        val renderTexture = RenderTexture()

        assertTrue(renderTexture.create(32, 32))
        assertEquals(0, drawable.callCount)

        renderTexture.draw(drawable)
        assertEquals(1, drawable.callCount)
