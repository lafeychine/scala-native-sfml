import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*
import sfml.system.*
import sfml.window.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/Texture.test.cpp */
/* Commit: 5048c4d */
class TextureTest extends NativeTest:

    @Test def constructor(): Unit =
        val texture = Texture()

        /* TODO: assertEquals(Vector2(0, 0), texture.size) */
        assertFalse(texture.smooth)

        /* TODO: assertFalse(texture.srgb) */
        assertFalse(texture.repeated)

        /* TODO: assertEquals(0, texture.nativeHandle) */

    /* TODO: create test suite */

    /* TODO: copy test suite */

    /* TODO: update test suite */

    @Test def smooth(): Unit =
        val texture = Texture()
        assertFalse(texture.smooth)

        texture.smooth = true
        assertTrue(texture.smooth)

        texture.smooth = false
        assertFalse(texture.smooth)

    /* TODO: srgb test */

    @Test def repeated(): Unit =
        val texture = Texture()
        assertFalse(texture.repeated)

        texture.repeated = true
        assertTrue(texture.repeated)

        texture.repeated = false
        assertFalse(texture.repeated)

/* TODO: swap test */

/* TODO: maximum size test */
