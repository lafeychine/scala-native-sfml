import org.junit.Assert.*
import org.junit.Test

import sfml.system.*
import sfml.window.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/VideoMode.test.cpp */
/* Commit: ff5eaca */
class VideoModeTest extends NativeTest:

    @Test def constructorDefault(): Unit =
        val mode = VideoMode()

        assertEquals(0, mode.width)
        assertEquals(0, mode.height)
        assertEquals(0, mode.bitsPerPixel)

    @Test def constructorWidthHeight(): Unit =
        val mode = VideoMode(800, 600)

        assertEquals(800, mode.width)
        assertEquals(600, mode.height)
        assertEquals(32, mode.bitsPerPixel)

    @Test def constructorWidthHeightBitsPerPixel(): Unit =
        val mode = VideoMode(800, 600, 24)

        assertEquals(800, mode.width)
        assertEquals(600, mode.height)
        assertEquals(24, mode.bitsPerPixel)

    @Test def isValid(): Unit =
        val mode = VideoMode()

        assertFalse(mode.isValid)

    @Test def operatorEqual(): Unit =
        assertTrue(VideoMode() != VideoMode(1, 0))
        assertTrue(VideoMode() != VideoMode(0, 1))
        assertTrue(VideoMode() != VideoMode(0, 0, 1))
        assertTrue(VideoMode(720, 720) != VideoMode(720, 720, 24))
        assertTrue(VideoMode(1080, 1920, 16) != VideoMode(400, 600))

    @Test def operatorLess(): Unit =
        assertTrue(VideoMode() < VideoMode(0, 0, 1))
        assertTrue(VideoMode(800, 800, 24) < VideoMode(1080, 1920, 48))
        assertTrue(VideoMode(400, 600, 48) < VideoMode(600, 400, 48))
        assertTrue(VideoMode(400, 400) < VideoMode(400, 600, 48))

    @Test def operatorGreater(): Unit =
        assertTrue(VideoMode(1, 0) > VideoMode(0, 0, 1))
        assertTrue(VideoMode(800, 800, 48) > VideoMode(1080, 1920, 24))
        assertTrue(VideoMode(600, 400, 48) > VideoMode(400, 600, 48))
        assertTrue(VideoMode(400, 600, 48) > VideoMode(400, 400, 48))

    @Test def operatorLessEqual(): Unit =
        assertTrue(VideoMode() <= VideoMode(0, 0, 1))
        assertTrue(VideoMode(800, 800, 24) <= VideoMode(1080, 1920, 48))
        assertTrue(VideoMode(400, 600, 48) <= VideoMode(600, 400, 48))
        assertTrue(VideoMode(400, 400, 48) <= VideoMode(400, 600, 48))
        assertTrue(VideoMode() <= VideoMode())
        assertTrue(VideoMode(0, 0, 0) <= VideoMode(0, 0, 0))
        assertTrue(VideoMode(1080, 1920, 64) <= VideoMode(1080, 1920, 64))

    @Test def operatorGreaterEqual(): Unit =
        assertTrue(VideoMode(1, 0) >= VideoMode(0, 0, 1))
        assertTrue(VideoMode(800, 800, 48) >= VideoMode(1080, 1920, 24))
        assertTrue(VideoMode(600, 400, 48) >= VideoMode(400, 600, 48))
        assertTrue(VideoMode(400, 600, 48) >= VideoMode(400, 400, 48))
        assertTrue(VideoMode() >= VideoMode())
        assertTrue(VideoMode(0, 0, 0) >= VideoMode(0, 0, 0))
        assertTrue(VideoMode(1080, 1920, 64) >= VideoMode(1080, 1920, 64))
