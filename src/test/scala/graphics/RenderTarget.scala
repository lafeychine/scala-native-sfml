import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.*
import sfml.system.*

private class RenderTargetFrame extends RenderTarget:

    override def size: Vector2[Int] = Vector2(640, 480)

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/Graphics/RenderTarget.test.cpp */
/* Commit: c8c8673 */
class RenderTargetTest extends NativeTest:

    private def makeView(viewport: Rect[Float]): View =
        val view = View()
        view.viewport = viewport
        view

    @Test def constructor(): Unit =
        val renderTarget = RenderTargetFrame()

        assertEquals(Vector2[Float](500, 500), renderTarget.view.center)
        assertEquals(Vector2[Float](1000, 1000), renderTarget.view.size)
        assertEquals(Rect[Float]((0, 0), (1, 1)), renderTarget.view.viewport)
        assertEquals(Transform(.002, 0, -1, 0, -.002, 1, 0, 0, 1), renderTarget.view.transform)

        assertEquals(Vector2[Float](500, 500), renderTarget.defaultView.center)
        assertEquals(Vector2[Float](1000, 1000), renderTarget.defaultView.size)
        assertEquals(Rect[Float]((0, 0), (1, 1)), renderTarget.defaultView.viewport)
        assertEquals(Transform(.002, 0, -1, 0, -.002, 1, 0, 0, 1), renderTarget.defaultView.transform)

    @Test def view(): Unit =
        val renderTarget = RenderTargetFrame()
        renderTarget.view = View(Vector2(1, 2), Vector2(3, 4))

        assertEquals(Vector2[Float](1, 2), renderTarget.view.center)
        assertEquals(Vector2[Float](3, 4), renderTarget.view.size)

    @Test def viewport(): Unit =
        val renderTarget = RenderTargetFrame()

        assertEquals(Rect[Int]((0, 0), (640, 480)), renderTarget.viewport(makeView(Rect[Float]((0, 0), (1, 1)))))
        assertEquals(Rect[Int]((640, 480), (320, 120)), renderTarget.viewport(makeView(Rect[Float]((1, 1), (.5, .25)))))
        assertEquals(Rect[Int]((320, 240), (160, 360)), renderTarget.viewport(makeView(Rect[Float]((.5, .5), (.25, .75)))))

    @Test def mapPixelToCoords_Vector2Int: Unit =
        val renderTarget = RenderTargetFrame()
        val view = View()

        view.move((5, 5))
        view.viewport = Rect[Float]((0, 0), (.5, 1.0))

        renderTarget.view = view

        val Vector2(x1, y1) = renderTarget.mapPixelToCoords((0, 0))
        assertEquals(5, x1, 0.001)
        assertEquals(5, y1, 0.001)

        val Vector2(x2, y2) = renderTarget.mapPixelToCoords((1, 1))
        assertEquals(8.125, x2, 0.001)
        assertEquals(7.083, y2, 0.001)

        val Vector2(x3, y3) = renderTarget.mapPixelToCoords((320, 240))
        assertEquals(1005, x3, 0.001)
        assertEquals(505, y3, 0.001)

    @Test def mapPixelToCoords_Vector2Int_view: Unit =
        val renderTarget = RenderTargetFrame()
        val view = View()

        view.move((5, 5))
        view.viewport = Rect[Float]((.5, .5), (.5, 1.0))

        val Vector2(x1, y1) = renderTarget.mapPixelToCoords((0, 0), view)
        assertEquals(-995, x1, 0.001)
        assertEquals(-495, y1, 0.001)

        val Vector2(x2, y2) = renderTarget.mapPixelToCoords((320, 480), view)
        assertEquals(5, x2, 0.001)
        assertEquals(505, y2, 0.001)

    // @Test def mapCoordsToPixel_Vector2Float: Unit =
    //     val renderTarget = RenderTargetFrame()
    //     val view = View()

    //     view.move((5, 5))
    //     view.viewport = Rect[Float]((.25, .0), (1, 1))

    //     renderTarget.view = view

    //     assertEquals(Vector2[Int](156, -2), renderTarget.mapCoordsToPixel((0, 0)))
    //     assertEquals(Vector2[Int](-163, -2), renderTarget.mapCoordsToPixel(-500, 0))
    //     assertEquals(Vector2[Int](156, -122), renderTarget.mapCoordsToPixel(0, -250))

    // @Test def mapCoordsToPixel_Vector2Float_View: Unit =
    //     val renderTarget = RenderTargetFrame()
    //     val view = View()

    //     view.move((5, 5))
    //     view.viewport = Rect[Float]((0, 0), (.5, .25))

    //     assertEquals(Vector2[Int](-1, 0), renderTarget.mapCoordsToPixel((0, 0), view))
    //     assertEquals(Vector2[Int](100, 0), renderTarget.mapCoordsToPixel(320, 0, view))
    //     assertEquals(Vector2[Int](-1, 57), renderTarget.mapCoordsToPixel(0, 480, view))
    //     assertEquals(Vector2[Int](203, 57), renderTarget.mapCoordsToPixel(640, 480, view))
