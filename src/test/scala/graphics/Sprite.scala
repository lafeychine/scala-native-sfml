import org.junit.Test

import sfml.graphics.*
import sfml.window.*

class SpriteGraphicalTest extends GraphicalTest:

    @Test def graphicalTest(): Unit =
        snTestScreen.testName = "Sprite"

        // Setup
        val window = RenderWindow(VideoMode(1024, 768), "Test")

        val texture = Texture()
        texture.loadFromFile("src/test/resources/sfml.png")

        val sprite = Sprite(texture)

        window.isOpen()

        // Control test
        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setPosition
        sprite.position = (100, 100)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // Teardown
        window.close()
