import org.junit.Assert.*
import org.junit.Test

import sfml.system.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/System/Time.test.cpp */
/* Commit: ebe4b3c */
class TimeTest extends NativeTest:

    @Test def constructorDefault(): Unit =
        val time = Time()

        assertEquals(0, time.asSeconds, 0.0001)
        assertEquals(0, time.asMilliseconds)
        assertEquals(0, time.asMicroseconds)

    @Test def constructorFromSeconds(): Unit =
        val time = Time.seconds(123)

        assertEquals(123, time.asSeconds, 0.0001)
        assertEquals(123000, time.asMilliseconds)
        assertEquals(123000000, time.asMicroseconds)

        assertEquals(1000000000, Time.seconds(1000).asMicroseconds)
        assertEquals(0, Time.seconds(0.0000009).asMicroseconds)
        assertEquals(0, Time.seconds(0.0000001).asMicroseconds)
        assertEquals(0, Time.seconds(0.00000001).asMicroseconds)
        assertEquals(0, Time.seconds(0.000000001).asMicroseconds)
        assertEquals(0, Time.seconds(-0.000000001).asMicroseconds)
        assertEquals(0, Time.seconds(-0.00000001).asMicroseconds)
        assertEquals(0, Time.seconds(-0.0000001).asMicroseconds)
        assertEquals(0, Time.seconds(-0.0000009).asMicroseconds)
        assertEquals(-1000000000, Time.seconds(-1000).asMicroseconds)

    @Test def constructorFromMilliseconds(): Unit =
        val time = Time.milliseconds(42)

        assertEquals(0.042, time.asSeconds, 0.0001)
        assertEquals(42, time.asMilliseconds)
        assertEquals(42000, time.asMicroseconds)

    @Test def constructorFromMicroseconds(): Unit =
        val time = Time.microseconds(987654)

        assertEquals(0.987654, time.asSeconds, 0.0001)
        assertEquals(987, time.asMilliseconds)
        assertEquals(987654, time.asMicroseconds)

    @Test def zeroTime(): Unit =
        assertEquals(0, Time.Zero.asSeconds, 0.0001)
        assertEquals(0, Time.Zero.asMilliseconds)
        assertEquals(0, Time.Zero.asMicroseconds)

    @Test def operator_==(): Unit =
        assertEquals(Time(), Time())
        assertEquals(Time.seconds(1), Time.seconds(1))
        assertEquals(Time.seconds(10), Time.seconds(10))
        assertEquals(Time.milliseconds(450450), Time.microseconds(450450000))
        assertEquals(Time.seconds(0.5), Time.microseconds(500000))

    @Test def operator_!=(): Unit =
        assertNotEquals(Time.seconds(10.12), Time.seconds(10.121))
        assertNotEquals(Time.microseconds(123456), Time.milliseconds(123))

    @Test def operator_<(): Unit =
        assertTrue(Time.seconds(54.999) < Time.seconds(55))
        assertTrue(Time.microseconds(10) < Time.milliseconds(10))
        assertTrue(Time.milliseconds(1000) < Time.microseconds(1000001))

    @Test def operator_<=(): Unit =
        assertTrue(Time.milliseconds(100) <= Time.milliseconds(100))
        assertTrue(Time.seconds(0.0012) <= Time.microseconds(1201))

    @Test def operator_>(): Unit =
        assertTrue(Time.seconds(55.001) > Time.seconds(55))
        assertTrue(Time.microseconds(1) > Time.seconds(0.0000001))
        assertTrue(Time.microseconds(1000001) > Time.milliseconds(1000))

    @Test def operator_>=(): Unit =
        assertTrue(Time.milliseconds(100) >= Time.milliseconds(-100))
        assertTrue(Time.microseconds(1201) >= Time.seconds(0.0012))

    @Test def operator_unary_-(): Unit =
        assertEquals(Time.seconds(-1), -Time.seconds(1))
        assertEquals(Time.microseconds(1234), -Time.microseconds(-1234))

    @Test def operator_+(): Unit =
        assertEquals(Time.seconds(2), Time.seconds(1) + Time.seconds(1))
        assertEquals(Time.microseconds(400400), Time.milliseconds(400) + Time.microseconds(400))

    @Test def operator_+=(): Unit =
        val time = Time.seconds(1.5)

        time += Time.seconds(1)

        assertEquals(Time.seconds(2.5), time)

    @Test def operator_-(): Unit =
        assertEquals(Time.seconds(0), Time.seconds(1) - Time.seconds(1))
        assertEquals(Time.microseconds(399600), Time.milliseconds(400) - Time.microseconds(400))

    @Test def operator_-=(): Unit =
        val time = Time.seconds(1.5)

        time -= Time.seconds(10)

        assertEquals(Time.seconds(-8.5), time)

    @Test def operator_*(): Unit =
        assertEquals(Time.seconds(2), Time.seconds(1) * 2f)
        assertEquals(Time.seconds(6), Time.seconds(12) * 0.5f)
        assertEquals(Time.seconds(2), Time.seconds(1) * 2L)
        assertEquals(Time.seconds(84), Time.seconds(42) * 2L)

    @Test def operator_*=(): Unit =
        val time = Time.milliseconds(1000)

        time *= 10L

        assertEquals(Time.milliseconds(10000), time)

        time *= 0.1f

        assertEquals(Time.milliseconds(1000), time)

    @Test def operator_/(): Unit =
        assertEquals(Time.seconds(0.5), Time.seconds(1) / 2f)
        assertEquals(Time.seconds(24), Time.seconds(12) / 0.5f)
        assertEquals(Time.seconds(0.5), Time.seconds(1) / 2L)
        assertEquals(Time.seconds(21), Time.seconds(42) / 2L)

    @Test def operator_/=(): Unit =
        val time = Time.milliseconds(1000)

        time /= 2L

        assertEquals(Time.milliseconds(500), time)

        time /= 0.5f

        assertEquals(Time.milliseconds(1000), time)
