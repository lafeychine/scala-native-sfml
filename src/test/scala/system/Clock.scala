import org.junit.Assert.*
import org.junit.Test

import sfml.system.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/System/Clock.test.cpp */
/* Commit: e4bffe5 */
class ClockTest extends NativeTest:

    @SuppressWarnings(Array("org.wartremover.warts.ThreadSleep"))
    @Test def elapsedTime(): Unit =
        val clock = Clock()

        assertTrue(clock.elapsedTime >= Time.microseconds(0))

        val elapsed = clock.elapsedTime

        Thread.sleep(1)

        assertTrue(clock.elapsedTime > elapsed)

    @SuppressWarnings(Array("org.wartremover.warts.ThreadSleep"))
    @Test def restart(): Unit =
        val clock = Clock()

        assertTrue(clock.restart() >= Time.microseconds(0))

        Thread.sleep(1)

        val elapsed = clock.restart()

        assertTrue(clock.restart() < elapsed)
