import org.junit.Assert.*
import org.junit.Test

import sfml.system.*

/* Tests based on: https://github.com/SFML/SFML/blob/master/test/System/Vector2.test.cpp */
/* Commit: e4bffe5 */
class Vector2Test extends NativeTest:

    @Test def lvalueUsedDotty(): Unit =
        val firstVector = Vector2(1, 2)
        val secondVector = Vector2(3, 4)

        assertEquals(4, (firstVector + secondVector).x)
        assertEquals(6, (firstVector + secondVector).y)

    @Test def constructorDefault(): Unit =
        val vector: Vector2[Int] = Vector2.apply[Int]()

        assertEquals(0, vector.x)
        assertEquals(0, vector.y)

    @Test def constructorXYCoordinate(): Unit =
        val vector = Vector2[Int](1, 2)

        assertEquals(1, vector.x)
        assertEquals(2, vector.y)

    @Test def operator_unary_-(): Unit =
        val vector = Vector2(1, 2)
        val negativeVector = -vector

        assertEquals(-1, negativeVector.x)
        assertEquals(-2, negativeVector.y)

    @Test def operator_+=(): Unit =
        val firstVector = Vector2(2, 5)
        val secondVector = Vector2(8, 3)

        firstVector += secondVector

        assertEquals(10, firstVector.x)
        assertEquals(8, firstVector.y)

    @Test def operator_-=(): Unit =
        val firstVector = Vector2(2, 5)
        val secondVector = Vector2(8, 3)

        firstVector -= secondVector

        assertEquals(-6, firstVector.x)
        assertEquals(2, firstVector.y)

    @Test def operator_+(): Unit =
        val firstVector = Vector2(2, 5)
        val secondVector = Vector2(8, 3)

        val result = firstVector + secondVector

        assertEquals(10, result.x)
        assertEquals(8, result.y)

    @Test def operator_-(): Unit =
        val firstVector = Vector2(2, 5)
        val secondVector = Vector2(8, 3)

        val result = firstVector - secondVector

        assertEquals(-6, result.x)
        assertEquals(2, result.y)

    @Test def operator_*(): Unit =
        val vector = Vector2(26, 12)

        val result = vector * 2

        assertEquals(52, result.x)
        assertEquals(24, result.y)

    @Test def operator_*=(): Unit =
        val vector = Vector2(26, 12)

        vector *= 2

        assertEquals(52, vector.x)
        assertEquals(24, vector.y)

    @Test def operator_/(): Unit =
        val vector = Vector2(26f, 12f)

        val result = vector / 2.0

        assertEquals(13.0, result.x, 0.0001)
        assertEquals(6.0, result.y, 0.0001)

    @Test def operator_/=(): Unit =
        val vector = Vector2(26f, 12f)

        vector /= 2.0

        assertEquals(13.0, vector.x, 0.0001)
        assertEquals(6.0, vector.y, 0.0001)

    @Test def operator_==(): Unit =
        assertTrue(Vector2(1, 5) == Vector2(1, 5))
        assertFalse(Vector2(1, 5) == Vector2(6, 9))

    @Test def operator_!=(): Unit =
        assertTrue(Vector2(1, 5) != Vector2(6, 9))
        assertFalse(Vector2(1, 5) != Vector2(1, 5))

    @Test def destructureByValue(): Unit =
        val vector = Vector2(1, 2)

        var Vector2(x, y) = vector

        assertEquals(1, x)
        assertEquals(2, y)

        x = 3
        assertEquals(3, x)
        assertEquals(1, vector.x)

        y = 4
        assertEquals(4, y)
        assertEquals(2, vector.y)

    @Test def copyFields(): Unit =
        val vector = Vector2(1, 2)

        val newVector = vector.copy(x = 3)

        assertEquals(1, vector.x)
        assertEquals(2, vector.y)

        assertEquals(3, newVector.x)
        assertEquals(2, newVector.y)
