import org.junit.Assert.*
import org.junit.Test

import sfml.graphics.{Color, RenderWindow, Sprite, Texture}
import sfml.window.VideoMode

class SpriteTutorial extends GraphicalTest:
    @Test def graphicalTest(): Unit =
        snTestScreen.testName = "SpriteTutorial"

        // Setup
        val window = RenderWindow(VideoMode(1024, 768), "My window")

        val texture = Texture()
        assertTrue(texture.loadFromFile("src/test/resources/sfml.png"))

        val sprite = Sprite(texture)

        assertTrue(window.isOpen())

        // Control test
        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Texture::setSmooth
        texture.smooth = true

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Texture::setRepeated
        texture.repeated = true

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setTextureRect
        sprite.textureRect = (100, 100, 600, 400)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setColor
        sprite.color = Color(0, 255, 0)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setColor
        sprite.color = Color(255, 255, 255, 128)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setPosition
        sprite.position = (100, 50)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::move
        sprite.move((50, 20))

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setRotation
        sprite.rotation = 90

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::rotate
        sprite.rotate(15)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setScale
        sprite.scale = (0.5, 2.0)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::scale
        sprite.scale((1.5, 3.0))

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)

        // sf::Sprite::setOrigin
        sprite.origin = (25, 25)

        window.clear()
        window.draw(sprite)
        window.display()
        snTestScreen.takeScreenshot(window)
