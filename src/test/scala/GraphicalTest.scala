import org.junit.Assert.*
import org.junit.{After, Assume, Before}

import scala.io.Source

import scala.scalanative.posix.unistd.getpid

import java.nio.file.{Files, Paths}

trait GraphicalTest extends NativeTest:
    val snTestScreen = TestScreen()

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    private def compareScreenshots(): Unit =
        for i <- 0 to snTestScreen.screenshots do
            val cxxPath = s"${snTestScreen.folderPath}/cxx/screenshot_${i}.bmp"
            val scalaPath = s"${snTestScreen.folderPath}/scala/screenshot_${i}.bmp"

            val cxxScreenshot = Files.readAllBytes(Paths.get(cxxPath))
            val scalaScreenshot = Files.readAllBytes(Paths.get(scalaPath))

            assertTrue(s"Screenshot ${i} differs", cxxScreenshot.sameElements(scalaScreenshot))

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    private def cleanup() =
        execute(Array("rm", "-rf", s"${snTestScreen.folderPath}/cxx", s"${snTestScreen.folderPath}/scala"))

    @SuppressWarnings(Array("org.wartremover.warts.PlatformDefault"))
    private def execute(command: Array[String]): Unit =
        val process = Runtime.getRuntime.exec(command)
        val exitCode = process.waitFor()

        assertEquals(Source.fromInputStream(process.getErrorStream).mkString(""), 0, exitCode)

    @Before
    @SuppressWarnings(Array("org.wartremover.warts.Any", "org.wartremover.warts.Return"))
    def __init(): Unit =
        // NOTE: Currently, cannot pass arguments to JUnit -> Use environment variable instead
        // Source: junit-runtime/src/main/scala/scala/scalanative/junit/JUnitFramework.scala
        sys.env.get("SNSFML_SCREENSHOT_FOLDER_PATH") match
            case Some(path) => snTestScreen.folderPath = path + "/" + getpid().toString
            case None       => return Assume.assumeTrue("No screenshot folder found", false)

        cleanup()

        execute(Array("mkdir", "-p", s"${snTestScreen.folderPath}/cxx", s"${snTestScreen.folderPath}/scala"))

    @After
    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def __teardown(): Unit =
        super.__teardown()

        if sys.env.get("SNSFML_SCREENSHOT_FOLDER_PATH").isEmpty then ()
        else

            // NOTE: Currently, cannot use JUnit's rules to retrieve the test name -> Use of `testName` variable
            assertTrue("test name not set", snTestScreen.testName != "")

            execute(Array("./target/cxx/scala-native-sfml-test-out", s"${snTestScreen.folderPath}/cxx", snTestScreen.testName))

            compareScreenshots()

            cleanup()
