import org.junit.After
import org.junit.Assert.assertTrue

import scala.scalanative.unsafe.*

@extern object LSAN:
    // TODO: Use __lsan_ignore_object
    @name("__lsan_do_recoverable_leak_check")
    def leak_check(): CInt = extern

trait NativeTest:
    @After
    def __teardown(): Unit =
        flushMemory()
        assertTrue("memory leaked", LSAN.leak_check() == 0)
