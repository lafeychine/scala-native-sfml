package scala
package scalanative
package unsafe
package cxx

import libc.string.memcpy
import runtime.Intrinsics.{castLongToRawPtr, castRawPtrToObject}
import runtime.{BlobArray, fromRawPtr, toRawPtr}
import unsigned.UnsignedRichInt

/** A pointer to a resource that is managed by the C++ wrapper.
  *
  * This pointer should never be accessed by the user of a library.
  */
class ResourcePtr[T: Tag](private val ptr: RawPtr[T], val buffer: Option[BlobArray]):

    def ref: Ref[T] = Ref.toUnsafeRef(ptr)

    @SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.OptionPartial", "scalafix:OptionGetWarn"))
    private[cxx] inline def register_finalizer(inline dtor: Deallocator[T]): ResourcePtr[T] =
        val callback = CFuncPtr2.fromScalaFunction((ptr: RawPtr[T], offset: Long) => {
            dtor(Ref.toUnsafeRef(fromRawPtr(castLongToRawPtr(ptr.toLong + offset))))
        })

        val obj: RawPtr[Object] = fromRawPtr(buffer.get.atRaw(0)) - 2
        val offset: Long = ptr.toLong - obj.toLong

        GC.register_finalizer(obj, callback, offset, null, null)
        this

object ResourcePtr:

    // TODO: Handle dtor
    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    def retrieveCopy[T: Tag](ptr: RawPtr[T]): ResourcePtr[T] =
        val base = GC.base(ptr)

        if base == null then ResourceBuffer.shallow_copy(ptr)
        else ResourcePtr(ptr, Option(castRawPtrToObject(toRawPtr(base)).asInstanceOf[BlobArray]))

    // TODO: Handle dtor
    @SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.Throw"))
    def retrieve[T: Tag](ptr: RawPtr[T]): ResourcePtr[T] =
        val base = GC.base(ptr)

        if base == null then throw new RuntimeException("Unexpected pointer caught. Please report the bug to lafeychine/scala-native-sfml.")
        else ResourcePtr(ptr, Option(castRawPtrToObject(toRawPtr(base)).asInstanceOf[BlobArray]))

object ResourceBuffer:

    inline def apply[T: Tag](inline ctor: Allocator[T]): ResourcePtr[T] =
        val buffer = BlobArray.alloc(sizeof[T].toInt)

        val ptr: RawPtr[T] = fromRawPtr(buffer.atRaw(0))
        ctor(ptr)

        ResourcePtr(ptr, Option(buffer))

    inline def apply[T: Tag](inline dtor: Deallocator[T])(inline ctor: Allocator[T]): ResourcePtr[T] =
        ResourceBuffer(ctor).register_finalizer(dtor)

    inline def shallow_copy[T: Tag](ptr: RawPtr[T]): ResourcePtr[T] =
        ResourceBuffer((r: RawPtr[T]) => {
            memcpy(r.asInstanceOf[RawPtr[Byte]], ptr.asInstanceOf[RawPtr[Byte]], sizeof[T])
            ()
        })
