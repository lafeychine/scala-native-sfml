package scala
package scalanative
package unsafe
package cxx

opaque type CxxBoolean = CChar

object CxxBoolean:

    inline given Conversion[Boolean, CxxBoolean]:
        override def apply(boolean: Boolean): CxxBoolean = if boolean then 1 else 0

    inline given Conversion[CxxBoolean, Boolean]:
        override def apply(boolean: CxxBoolean): Boolean = boolean != 0
