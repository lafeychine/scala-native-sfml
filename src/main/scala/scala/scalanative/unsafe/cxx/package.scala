package scala
package scalanative
package unsafe

package object cxx:

    export scala.scalanative.unsafe.{CArray, CChar, CFloat, CInt, CLongLong, CUnsignedLongLong, Nat}

    opaque type Const[T] = T

    /** A raw pointer is just a typed pointer, without any added meaning. */
    type RawPtr[T] = unsafe.Ptr[T]

    opaque type Ptr[T] = RawPtr[T]

    object Ptr:

        def toRawPtr[T](ptr: Ptr[Const[T]]): RawPtr[T] = ptr


    opaque type Ref[T] = RawPtr[T]

    object Ref:

        inline given [T] => Conversion[Ref[T], RawPtr[T]]:
            override def apply(ref: Ref[T]): RawPtr[T] = ref

        inline given [T] => Conversion[Ref[T], Ref[Const[T]]]:
            override def apply(ref: Ref[T]): Ref[Const[T]] = ref

        def toUnsafeRef[T](ptr: RawPtr[T]): Ref[T] = ptr
        def toUnsafeRefConst[T](ptr: RawPtr[T]): Ref[Const[T]] = ptr

    type Allocator[T] = RawPtr[T] => Unit
    type Deallocator[T] = Ref[T] => Unit
