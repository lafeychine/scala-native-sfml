package scala
package scalanative
package unsafe
package cxx

import runtime.BlobArray

@link("gc")
@extern private[cxx] object GC:
    @name("GC_base")
    def base[T](ptr: RawPtr[T]): RawPtr[BlobArray] = extern

    @name("GC_register_finalizer_no_order")
    def register_finalizer[T](
        ptr: RawPtr[Object],
        callback: CFuncPtr2[RawPtr[T], Long, Unit],
        offset: Long,
        ofn: RawPtr[Byte],
        ocd: RawPtr[Byte]
    ): Unit = extern
