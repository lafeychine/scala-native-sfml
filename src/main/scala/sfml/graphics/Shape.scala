package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsafe.CFuncPtr0.fromScalaFunction
import scala.scalanative.unsafe.CFuncPtr1.fromScalaFunction
import scala.scalanative.unsafe.CFuncPtr2.fromScalaFunction
import scala.scalanative.unsigned.UnsignedRichLong

import sfml.internal.Type.booleanToSfBool
import sfml.internal.graphics.Shape as Self
import sfml.internal.graphics.Texture.sfTexture

import sfml.system.Vector2

/** Base class for textured shapes with outline.
  *
  * [[sfml.graphics.Shape Shape]] is a drawable class that allows to define and display a custom convex shape on a render target.
  *
  * It's only an abstract base, it needs to be specialized for concrete types of shapes (circle, rectangle, convex polygon, star,
  * ...).
  *
  * In addition to the attributes provided by the specialized shape classes, a shape always has the following attributes:
  *   - a texture
  *   - a texture rectangle
  *   - a fill color
  *   - an outline color
  *   - an outline thickness
  *
  * Each feature is optional, and can be disabled easily:
  *   - the texture can be `None`
  *   - the fill/outline colors can be [[sfml.graphics.Color.Transparent Transparent]]
  *   - the outline thickness can be zero
  *
  * You can write your own derived shape class, there are only two functions to override:
  *   - `pointCount` must return the number of points of the shape
  *   - `point` must return the points of the shape
  *
  * @see
  *   [[sfml.graphics.RectangleShape RectangleShape]], [[sfml.graphics.CircleShape CircleShape]], <!-- TODO ConvexShape -->
  *   [[sfml.graphics.Transformable Transformable]]
  *
  * @doxygenId
  *   classsf_1_1Shape
  *
  * @doxygenHash
  *   1658164ca17dde79d2954e52724b2b56
  */
abstract trait Shape extends Transformable with Drawable:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _shape: ResourcePtr[Self.sfShape] =
        AbstractResourceBuffer(Shape.vtable, this)(Self.dtor)(Self.ctor(_))

    private[sfml] final override lazy val _transformable = _shape.get(_.at2)

    /* Methods */

    override def draw(target: RenderTarget, states: RenderStates): Unit =
        RenderTarget.patch_draw(_shape.ptr.asInstanceOf[Ptr[Byte]], target, states)

    /** Get the global (non-minimal) bounding rectangle of the entity.
      *
      * The returned rectangle is in global coordinates, which means that it takes into account the transformations (translation,
      * rotation, scale, ...) that are applied to the entity. In other words, this function returns the bounds of the shape in the
      * global 2D world's coordinate system.
      *
      * This function does not necessarily return the minimal bounding rectangle. It merely ensures that the returned rectangle
      * covers all the vertices (but possibly more). This allows for a fast approximation of the bounds as a first check; you may
      * want to use more precise checks on top of that.
      *
      * @return
      *   Global bounding rectangle of the entity
      *
      * @doxygenId
      *   classsf_1_1Shape_1ac0e29425d908d5442060cc44790fe4da
      */
    def globalBounds: Rect[Float] =
        Rect.toRectFloat((data: Ptr[Self.sfShape]) => Self.getGlobalBounds(data))(_shape.ptr)

    /** Get the local bounding rectangle of the entity.
      *
      * The returned rectangle is in local coordinates, which means that it ignores the transformations (translation, rotation,
      * scale, ...) that are applied to the entity. In other words, this function returns the bounds of the entity in the entity's
      * coordinate system.
      *
      * @return
      *   Local bounding rectangle of the entity
      *
      * @doxygenId
      *   classsf_1_1Shape_1ae3294bcdf8713d33a862242ecf706443
      */
    def localBounds: Rect[Float] =
        Rect.toRectFloat((data: Ptr[Self.sfShape]) => Self.getLocalBounds(data))(_shape.ptr)

    /** Get a point of the shape.
      *
      * The returned point is in local coordinates, that is, the shape's transforms (position, rotation, scale) are not taken into
      * account. The result is undefined if index is out of the valid range.
      *
      * @param index
      *   Index of the point to get, in range [0 .. [[sfml.graphics.Shape.pointCount pointCount()]] - 1]
      *
      * @return
      *   index-th point of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.pointCount pointCount]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a40e5d83713eb9f0c999944cf96458085
      */
    def point(index: Long): Vector2[Float]

    /** Get the total number of points of the shape.
      *
      * @return
      *   Number of points of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.point point]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1af988dd61a29803fc04d02198e44b5643
      */
    def pointCount: Long

    /** Recompute the internal geometry of the shape.
      *
      * This function must be called by the derived class everytime the shape's points change (i.e. the result of either
      * [[sfml.graphics.Shape.pointCount pointCount]] or [[sfml.graphics.Shape.point point]] is different).
      *
      * @doxygenId
      *   classsf_1_1Shape_1adfb2bd966c8edbc5d6c92ebc375e4ac1
      */
    protected def update(): Unit =
        Self.update(_shape.ptr)

    /* Getter / Setter */

    /** Get the fill color of the shape.
      *
      * @return
      *   Fill color of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.fillColor_= fillColor_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1aa5da23e522d2dd11e3e7661c26164c78
      */
    def fillColor: Color =
        Self.getFillColor(_shape.ptr)

    /** Set the fill color of the shape.
      *
      * This color is modulated (multiplied) with the shape's texture if any. It can be used to colorize the shape, or change its
      * global opacity. You can use [[sfml.graphics.Color.Transparent Transparent]] to make the inside of the shape transparent, and
      * have the outline alone. By default, the shape's fill color is opaque white.
      *
      * @param color
      *   New color of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.fillColor fillColor]], [[sfml.graphics.Shape.outlineColor_= outlineColor_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a3506f9b5d916fec14d583d16f23c2485
      */
    def fillColor_=(color: Color): Unit =
        Self.setFillColor(_shape.ptr, color)

    /** Get the outline color of the shape.
      *
      * @return
      *   Outline color of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.outlineColor_= outlineColor_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a4aa05b59851468e948ac9682b9c71abb
      */
    def outlineColor: Color =
        Self.getOutlineColor(_shape.ptr)

    /** Set the outline color of the shape.
      *
      * By default, the shape's outline color is opaque white.
      *
      * @param color
      *   New outline color of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.outlineColor]], [[sfml.graphics.Shape.fillColor_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a5978f41ee349ac3c52942996dcb184f7
      */
    def outlineColor_=(color: Color): Unit =
        Self.setOutlineColor(_shape.ptr, color)

    /** Get the outline thickness of the shape.
      *
      * @return
      *   Outline thickness of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.outlineThickness_= outlineThickness_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a1d4d5299c573a905e5833fc4dce783a7
      */
    def outlineThickness: Float =
        Self.getOutlineThickness(_shape.ptr)

    /** Set the thickness of the shape's outline.
      *
      * Note that negative values are allowed (so that the outline expands towards the center of the shape), and using zero disables
      * the outline. By default, the outline thickness is 0.
      *
      * @param thickness
      *   New outline thickness
      *
      * @see
      *   [[sfml.graphics.Shape.outlineThickness outlineThickness]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a5ad336ad74fc1f567fce3b7e44cf87dc
      */
    def outlineThickness_=(thickness: Float): Unit =
        Self.setOutlineThickness(_shape.ptr, thickness)

    /** Get the source texture of the shape.
      *
      * If the shape has no source texture, `None` is returned.
      *
      * @return
      *   Optional to the shape's texture
      *
      * @see
      *   [[sfml.graphics.Shape.texture_= texture_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1af4c345931cd651ffb8f7a177446e28f7
      *
      * @doxygenHash
      *   73d48752a8d450c7d55734a231abe85a
      */
    def texture: Option[Immutable[Texture]] =
        Option(Self.getTexture(_shape.ptr)).map(Immutable(_))

    /** Change the source texture of the shape.
      *
      * The `texture` argument refers to a texture that must exist as long as the shape uses it. Indeed, the shape doesn't store its
      * own copy of the texture, but rather keeps a pointer to the one that you passed to this function. If the source texture is
      * destroyed and the shape tries to use it, the behavior is undefined. `texture` can be `None` to disable texturing. If
      * `resetRect` is true, the TextureRect property of the shape is automatically adjusted to the size of the new texture. If it
      * is false, the texture rect is left unchanged.
      *
      * @param texture
      *   New texture
      *
      * @param resetRect
      *   Should the texture rect be reset to the size of the new texture?
      *
      * @see
      *   [[sfml.graphics.Shape.texture texture]], [[sfml.graphics.Shape.textureRect_= textureRect_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1af8fb22bab1956325be5d62282711e3b6
      */
    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    def texture_=(texture: Option[Immutable[Texture]], resetRect: Boolean = false) =
        val texturePtr: Ptr[sfTexture] = texture.map(_.get).orNull

        Self.setTexture(_shape.ptr, texturePtr, resetRect)

    /** Get the sub-rectangle of the texture displayed by the shape.
      *
      * @return
      *   Texture rectangle of the shape
      *
      * @see
      *   [[sfml.graphics.Shape.textureRect_= textureRect_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1ad8adbb54823c8eff1830a938e164daa4
      */
    def textureRect: Rect[Int] =
        Rect.toRectInt(Self.getTextureRect(_shape.ptr))()

    /** Set the sub-rectangle of the texture that the shape will display.
      *
      * The texture rect is useful when you don't want to display the whole texture, but rather a part of it. By default, the
      * texture rect covers the entire texture.
      *
      * @param rect
      *   Rectangle defining the region of the texture to display
      *
      * @see
      *   [[sfml.graphics.Shape.textureRect textureRect]], [[sfml.graphics.Shape.texture_= texture_=]]
      *
      * @doxygenId
      *   classsf_1_1Shape_1a2029cc820d1740d14ac794b82525e157
      */
    def textureRect_=(rect: Rect[Int]): Unit =
        Self.setTextureRect(_shape.ptr, rect)

object Shape:

    private[sfml] inline given Conversion[Shape, Ptr[Self.sfShape]]:
        override def apply(shape: Shape) = shape._shape.ptr

    /* Constructors */

    private[sfml] val vtable = Drawable.vtable.extendsWith(
        CFuncPtr.toPtr((ptr: Ptr[Wrapped[Shape]]) => ptr.get.pointCount.toULong),
        CFuncPtr.toPtr((ptr: Ptr[Wrapped[Shape]], index: CSize) => {
            Vector2.toInlineVector2f(ptr.get.point(index.toLong))
        })
    )

    /* Immutable */

    extension (shape: Immutable[Shape])

        def point(index: Long): Vector2[Float] = shape.get.point(index)

        def pointCount: Long = shape.get.pointCount
