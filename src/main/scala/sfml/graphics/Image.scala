package sfml
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.graphics.Image as Self

trait Image(using private[sfml] ctor: Image.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _image: ResourcePtr[Self.sfImage] =
        ResourceBuffer(Self.dtor)(ctor)

    /* Methods */

    def saveToFile(filename: stdlib.String): Boolean =
        Self.saveToFile(_image.ptr, filename)

object Image:

    private[sfml] inline given Conversion[Image, Ptr[Self.sfImage]]:
        override def apply(image: Image) = image._image.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfImage]

    private[sfml] def apply(f: Ptr[Self.sfImage] => Unit): Image =
        new Object with Image(using Constructor(f))
