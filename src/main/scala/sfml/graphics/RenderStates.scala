package sfml
package graphics

import scala.annotation.targetName

import scala.scalanative.unsafe.*

import sfml.internal.graphics.RenderStates as Self
import sfml.internal.graphics.Shader.sfShader
import sfml.internal.graphics.Texture.sfTexture

trait RenderStates(using private[sfml] ctor: RenderStates.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _renderStates: ResourcePtr[Self.sfRenderStates] =
        ResourceBuffer(ctor)

    /* Methods */

    def blendMode: BlendMode =
        BlendMode(_renderStates.get(_.at1))

    def transform: Transform =
        Transform(_renderStates.get(_.at2))

    def texture: Option[Immutable[Texture]] =
        Option.when(_renderStates.ptr.at3 != null)(Immutable(Texture(_renderStates.get(_._3))))

    def shader: Option[Immutable[Shader]] =
        Option.when(_renderStates.ptr.at4 != null)(Immutable(Shader(_renderStates.get(_._4))))

object RenderStates:

    private[sfml] inline given Conversion[RenderStates, Ptr[Self.sfRenderStates]]:
        override def apply(renderStates: RenderStates) = renderStates._renderStates.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfRenderStates]

    object ctor:

        def apply(): ctor =
            Constructor(Self.ctor(_))

        def apply(theBlendMode: BlendMode): ctor =
            Constructor(Self.ctor_BlendMode(_, theBlendMode))

        def apply(theTransform: Transform): ctor =
            Constructor(Self.ctor_Transform(_, theTransform))

        @SuppressWarnings(Array("org.wartremover.warts.Null"))
        @targetName("RenderStates_ctor_Texture")
        def apply(theTexture: Option[Texture]): ctor =
            val theTexturePtr: Ptr[sfTexture] = theTexture.orNull

            Constructor(Self.ctor_Texture(_, theTexturePtr))

        @SuppressWarnings(Array("org.wartremover.warts.Null"))
        @targetName("RenderStates_ctor_Shader")
        def apply(theShader: Option[Shader]): ctor =
            val theShaderPtr: Ptr[sfShader] = theShader.orNull

            Constructor(Self.ctor_Shader(_, theShaderPtr))

        @SuppressWarnings(Array("org.wartremover.warts.Null"))
        def apply(
            theBlendMode: BlendMode,
            theTransform: Transform,
            theTexture: Option[Texture],
            theShader: Option[Shader]
        ): ctor =
            val theTexturePtr: Ptr[sfTexture] = theTexture.orNull
            val theShaderPtr: Ptr[sfShader] = theShader.orNull

            Constructor(
                Self.ctor_BlendMode_Transform_Texture_Shader(
                    _,
                    theBlendMode,
                    theTransform,
                    theTexturePtr,
                    theShaderPtr
                )
            )

    def apply(): RenderStates =
        new Object with RenderStates(using ctor())

    def apply(theBlendMode: BlendMode): RenderStates =
        new Object with RenderStates(using ctor(theBlendMode))

    def apply(theTransform: Transform): RenderStates =
        new Object with RenderStates(using ctor(theTransform))

    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    @targetName("RenderStates_ctor_Texture")
    def apply(theTexture: Option[Texture]): RenderStates =
        new Object with RenderStates(using ctor(theTexture))

    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    @targetName("RenderStates_ctor_Shader")
    def apply(theShader: Option[Shader]): RenderStates =
        new Object with RenderStates(using ctor(theShader))

    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    def apply(
        theBlendMode: BlendMode,
        theTransform: Transform,
        theTexture: Option[Texture],
        theShader: Option[Shader]
    ): RenderStates =
        new Object with RenderStates(using ctor(theBlendMode, theTransform, theTexture, theShader))
