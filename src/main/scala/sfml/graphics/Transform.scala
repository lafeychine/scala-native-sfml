package sfml
package graphics

import scala.collection.immutable.ArraySeq

import scala.scalanative.unsafe.cxx.*

import sfml.internal.graphics.Transform as Self

import sfml.system.Vector2

trait Transform(using private[sfml] ctor: Transform.ctor):

    /* Fields */

    private[sfml] val _transform: Transform.ctor = ctor

    /* Methods */

    def combine(rhs: Immutable[Transform]): Transform =
        Transform(Self.combine(this, rhs))

    def inverse(): Transform =
        Immutable(this).inverse()

    def matrix: ArraySeq[Float] =
        Immutable(this).matrix

    def rotate(angle: Float): Transform =
        Transform(Self.rotate(this, angle))

    def rotate(angle: Float, centerX: Float, centerY: Float): Transform =
        Transform(Self.rotate(this, angle, centerX, centerY))

    def rotate(angle: Float, center: Immutable[Vector2[Float]]): Transform =
        Transform(Self.rotate(this, angle, Ref.toUnsafeRefConst(center.get)))

    def scale(scaleX: Float, scaleY: Float): Transform =
        Transform(Self.scale(this, scaleX, scaleY))

    def scale(scaleX: Float, scaleY: Float, centerX: Float, centerY: Float): Transform =
        Transform(Self.scale(this, scaleX, scaleY, centerX, centerY))

    def scale(factors: Immutable[Vector2[Float]]): Transform =
        Transform(Self.scale(this, Ref.toUnsafeRefConst(factors.get)))

    def scale(factors: Immutable[Vector2[Float]], center: Immutable[Vector2[Float]]): Transform =
        Transform(Self.scale(this, Ref.toUnsafeRefConst(factors.get), Ref.toUnsafeRefConst(center.get)))

    def transformPoint(x: Float, y: Float): Vector2[Float] =
        Immutable(this).transformPoint(x, y)

    def transformPoint(point: Immutable[Vector2[Float]]): Vector2[Float] =
        Immutable(this).transformPoint(point)

    def transformRect(rectangle: Immutable[Rect[Float]]): Rect[Float] =
        Immutable(this).transformRect(rectangle)

    def translate(x: Float, y: Float): Transform =
        Transform(Self.translate(this, x, y))

    def translate(offset: Immutable[Vector2[Float]]): Transform =
        Transform(Self.translate(this, Ref.toUnsafeRefConst(offset.get)))

    /* Operators */

    def *(rhs: Immutable[Transform]): Transform =
        Immutable(this) * rhs

    def *=(rhs: Immutable[Transform]): Unit =
        Self.mL_sfTransform(this, rhs)

    def *(rhs: Immutable[Vector2[Float]]): Vector2[Float] =
        Immutable(this) * rhs

    inline def ==(rhs: Immutable[Transform]): Boolean =
        Immutable(this) === rhs

    inline def !=(rhs: Immutable[Transform]): Boolean =
        Immutable(this) !== rhs

    /* Scala operators */

    /* TODO: .copy() */

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: Transform    => this == Immutable(rhs)
            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def toString(): java.lang.String =
        s"Transform(${matrix.mkString(", ")})"

object Transform:

    private[sfml] inline given conversion_Transform_RawPtr: Conversion[Transform, RawPtr[Self.sfTransform]]:
        override def apply(transform: Transform) = transform._transform.ref

    private[sfml] inline given Conversion[Transform, Ref[Self.sfTransform]]:
        override def apply(transform: Transform) = transform._transform.ref

    private[sfml] inline given Conversion[Immutable[Transform], Ref[Const[Self.sfTransform]]]:
        override def apply(transform: Immutable[Transform]) = transform.get._transform.ref

    /* Constructors */

    private[sfml] opaque type ctor = ResourcePtr[Self.sfTransform]

    private[sfml] def apply(transform: ResourcePtr[Self.sfTransform]): Transform =
        new Object with Transform(using transform)

    private[sfml] def apply(f: Allocator[Self.sfTransform]): Transform =
        new Object with Transform(using ctor(f))

    private[sfml] def apply(ref: Ref[Self.sfTransform]): Transform =
        new Object with Transform(using ResourcePtr.retrieveCopy(ref))

    object ctor:

        private[sfml] def apply(f: Allocator[Self.sfTransform]): ctor =
            ResourceBuffer(Self.dtor)(f)

        def apply(): ctor =
            ctor(Self.ctor(_))

        def apply(
            a00: Float,
            a01: Float,
            a02: Float,
            a10: Float,
            a11: Float,
            a12: Float,
            a20: Float,
            a21: Float,
            a22: Float
        ): ctor =
            ctor(Self.ctor(_, a00, a01, a02, a10, a11, a12, a20, a21, a22))

    def apply(): Transform =
        Transform(ctor())

    def apply(
        a00: Float,
        a01: Float,
        a02: Float,
        a10: Float,
        a11: Float,
        a12: Float,
        a20: Float,
        a21: Float,
        a22: Float
    ): Transform =
        Transform(ctor(a00, a01, a02, a10, a11, a12, a20, a21, a22))

    /* Immutable */

    extension (transform: Immutable[Transform])

        def inverse(): Transform =
            Transform(Self.getInverse(_, transform))

        def matrix: ArraySeq[Float] =
            val matrix = Self.getMatrix(transform)
            ArraySeq.tabulate(16)(!(Ptr.toRawPtr(matrix)).at(_))

        def transformPoint(x: Float, y: Float): Vector2[Float] =
            Vector2(Self.transformPoint(_, transform, x, y))

        def transformPoint(point: Immutable[Vector2[Float]]): Vector2[Float] =
            Vector2(Self.transformPoint(_, transform, Ref.toUnsafeRefConst(point.get)))

        def transformRect(rectangle: Immutable[Rect[Float]]): Rect[Float] =
            Rect(Self.transformRect(_, transform, Ref.toUnsafeRefConst(rectangle.get)))

        def *(rhs: Immutable[Transform]): Transform =
            Transform(Self.ml_sfTransform(_, transform, rhs))

        def *(rhs: Immutable[Vector2[Float]]): Vector2[Float] =
            Vector2(Self.ml_sfVector2f(_, transform, Ref.toUnsafeRefConst(rhs.get)))

        def ===(rhs: Immutable[Transform]): Boolean =
            Self.eq_sfTransform(transform, rhs)

        def !==(rhs: Immutable[Transform]): Boolean =
            Self.ne_sfTransform(transform, rhs)

    /* Static methods */

    def Identity(): Transform =
        Transform(Self.Identity(_))
