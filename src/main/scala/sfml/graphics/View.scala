package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsafe.cxx.Ref

import sfml.internal.graphics.View as Self

import sfml.system.Vector2

/** 2D camera that defines what region is shown on screen
  *
  * [[sfml.graphics.View View]] defines a camera in the 2D scene.
  *
  * This is a very powerful concept: you can scroll, rotate or zoom the entire scene without altering the way that your drawable
  * objects are drawn.
  *
  * A view is composed of a source rectangle, which defines what part of the 2D scene is shown, and a target viewport, which defines
  * where the contents of the source rectangle will be displayed on the render target (window or texture).
  *
  * The viewport allows to map the scene to a custom part of the render target, and can be used for split-screen or for displaying a
  * minimap, for example. If the source rectangle doesn't have the same size as the viewport, its contents will be stretched to fit
  * in.
  *
  * To apply a view, you have to assign it to the render target. Then, objects drawn in this render target will be affected by the
  * view until you use another view.
  *
  * ```scala
  * //{
  * val window: RenderWindow = ???
  * val view: View = ???
  *
  * val someSprite: Sprite = ???
  * val someText: Text = ???
  *
  * //}
  *
  * // Initialize the view to a rectangle located at (100, 100) and with a size of 400x200
  * view.reset((100, 100, 400, 200))
  *
  * // Rotate it by 45 degrees
  * view.rotate(45)
  *
  * // Set its target viewport to be half of the window
  * view.viewport = (0f, 0f, 0.5f, 1f)
  *
  * // Apply it
  * window.view = view
  *
  * // Render stuff
  * window.draw(someSprite)
  *
  * // Set the default view back
  * window.view = window.defaultView
  *
  * // Render stuff not affected by the view
  * window.draw(someText)
  * ```
  *
  * See also the note on coordinates and undistorted rendering in [[sfml.graphics.Transformable Transformable]].
  *
  * @see
  *   [[sfml.graphics.RenderWindow RenderWindow]], [[sfml.graphics.RenderTexture RenderTexture]]
  *
  * @doxygenId
  *   classsf_1_1View
  *
  * @doxygenHash
  *   40ad4b0e8480b92a7f369d7fb344522f
  */
trait View(using private[sfml] ctor: View.ctor):

    /* Fields */

    private[sfml] val _view: View.ctor = ctor

    /* Methods */

    /** Move the view relatively to its current position.
      *
      * @param offsetX
      *   X coordinate of the move offset
      *
      * @param offsetY
      *   Y coordinate of the move offset
      *
      * @see
      *   [[sfml.graphics.View.center_= center_=]], [[sfml.graphics.View.rotate rotate]], [[sfml.graphics.View.zoom zoom]]
      *
      * @doxygenId
      *   classsf_1_1View_1a0c82144b837caf812f7cb25a43d80c41
      */
    def move(offsetX: Float, offsetY: Float): Unit =
        Self.move(this, offsetX, offsetY)

    /** Move the view relatively to its current position.
      *
      * @param offset
      *   Move offset
      *
      * @see
      *   [[sfml.graphics.View.center_= center_=]], [[sfml.graphics.View.rotate rotate]], [[sfml.graphics.View.zoom zoom]]
      *
      * @doxygenId
      *   classsf_1_1View_1a4c98a6e04fed756dfaff8f629de50862
      */
    def move(offset: Vector2[Float]): Unit =
        Self.move(this, offset)

    /** Reset the view to the given rectangle.
      *
      * Note that this function resets the rotation angle to 0.
      *
      * @param rectangle
      *   Rectangle defining the zone to display
      *
      * @see
      *   [[sfml.graphics.View.center_= center_=]], [[sfml.graphics.View.size_= size_=]],
      *   [[sfml.graphics.View.rotation_= rotation_=]]
      *
      * @doxygenId
      *   classsf_1_1View_1ac95b636eafab3922b7e8304fb6c00d7d
      */
    def reset(rect: Rect[Float]): Unit =
        Self.reset(this, rect)

    /** Rotate the view relatively to its current orientation.
      *
      * @param angle
      *   Angle to rotate, in degrees
      *
      * @see
      *   [[sfml.graphics.View.rotation_= rotation_=]], [[sfml.graphics.View.move move]], [[sfml.graphics.View.zoom zoom]]
      *
      * @doxygenId
      *   classsf_1_1View_1a5fd3901aae1845586ca40add94faa378
      */
    def rotate(angle: Float): Unit =
        Self.rotate(this, angle)

    /** Resize the view rectangle relatively to its current size.
      *
      * Resizing the view simulates a zoom, as the zone displayed on screen grows or shrinks. `factor` is a multiplier:
      *   - 1 keeps the size unchanged
      *   - > 1 makes the view bigger (objects appear smaller)
      *   - < 1 makes the view smaller (objects appear bigger)
      *
      * @param factor
      *   Zoom factor to apply
      *
      * @see
      *   [[sfml.graphics.View.size_= size_=]], [[sfml.graphics.View.move move]], [[sfml.graphics.View.rotate rotate]]
      *
      * @doxygenId
      *   classsf_1_1View_1a4a72a360a5792fbe4e99cd6feaf7726e
      */
    def zoom(factor: Float): Unit =
        Self.zoom(this, factor)

    /* Getter / Setter */

    /** Get the center of the view.
      *
      * @return
      *   Center of the view
      *
      * @see
      *   [[sfml.graphics.View.size size]], [[sfml.graphics.View.center_= center_=]]
      *
      * @doxygenId
      *   classsf_1_1View_1a8bd01cd2bcad03e547232b190c215b09
      */
    def center: Vector2[Float] =
        Vector2.toVector2Float(Self.getCenter(this))()

    /** Set the center of the view.
      *
      * @param x
      *   X coordinate of the new center
      *
      * @param y
      *   Y coordinate of the new center
      *
      * @see
      *   [[sfml.graphics.View.size_= size_=]], [[sfml.graphics.View.center center]]
      *
      * @doxygenId
      *   classsf_1_1View_1aa8e3fedb008306ff9811163545fb75f2
      */
    def center_=(x: Float, y: Float) =
        Self.setCenter(this, x, y)

    /** Set the center of the view.
      *
      * @param center
      *   New center
      *
      * @see
      *   [[sfml.graphics.View.size_= size_=]], [[sfml.graphics.View.center center]]
      *
      * @doxygenId
      *   classsf_1_1View_1ab0296b03793e0873e6ae9e15311f3e78
      */
    def center_=(center: Vector2[Float]) =
        Self.setCenter(this, center)

    /** Get the current orientation of the view.
      *
      * @return
      *   Rotation angle of the view, in degrees
      *
      * @see
      *   [[sfml.graphics.View.rotation_= rotation_=]]
      *
      * @doxygenId
      *   classsf_1_1View_1a324d8885f4ab17f1f7b0313580c9b84e
      */
    def rotation: Float =
        Self.getRotation(this)

    /** Set the orientation of the view.
      *
      * The default rotation of a view is 0 degree.
      *
      * @param angle
      *   New angle, in degrees
      *
      * @see
      *   [[sfml.graphics.View.rotation rotation]]
      *
      * @doxygenId
      *   classsf_1_1View_1a24d0503c9c51f5ef5918612786d325c1
      */
    def rotation_=(angle: Float) =
        Self.setRotation(this, angle)

    /** Get the size of the view.
      *
      * @return
      *   Size of the view
      *
      * @see
      *   [[sfml.graphics.View.center center]], [[sfml.graphics.View.size_= size_=]]
      *
      * @doxygenId
      *   classsf_1_1View_1a57e4a87cf0d724678675d22a0093719a
      */
    def size: Vector2[Float] =
        Vector2.toVector2Float(Self.getSize(this))()

    /** Set the size of the view.
      *
      * @param width
      *   New width of the view
      *
      * @param height
      *   New height of the view
      *
      * @see
      *   [[sfml.graphics.View.center_= center_=]], [[sfml.graphics.View.center center]]
      *
      * @doxygenId
      *   classsf_1_1View_1a9525b73fe9fbaceb9568faf56b399dab
      */
    def size_=(width: Float, height: Float) =
        Self.setSize(this, width, height)

    /** Set the size of the view.
      *
      * @param size
      *   New size
      *
      * @see
      *   [[sfml.graphics.View.center_= center_=]], [[sfml.graphics.View.center center]]
      *
      * @doxygenId
      *   classsf_1_1View_1a9e08d471ce21aa0e69ce55ff9de66d29
      */
    def size_=(size: Vector2[Float]) =
        Self.setSize(this, size)

    /** Get the projection transform of the view.
      *
      * This function is meant for internal use only.
      *
      * @return
      *   Projection transform defining the view
      *
      * @see
      *   [[sfml.graphics.View.inverseTransform inverseTransform]]
      *
      * @doxygenId
      *   classsf_1_1View_1ac9c1dab0cb8c1ac143b031035d821ce5
      */
    def transform: Immutable[Transform] =
        Immutable(Transform(Ref.toUnsafeRef(Self.getTransform(this))))

    /** Get the inverse projection transform of the view.
      *
      * This function is meant for internal use only.
      *
      * @return
      *   Inverse of the projection transform defining the view
      *
      * @see
      *   [[sfml.graphics.View.transform transform]]
      *
      * @doxygenId
      *   classsf_1_1View_1aa685c17a56aae7c7df4c90ea6285fd46
      */
    def inverseTransform(): Immutable[Transform] =
        Immutable(Transform(Ref.toUnsafeRef(Self.getInverseTransform(this))))

    /** Get the target viewport rectangle of the view.
      *
      * @return
      *   Viewport rectangle, expressed as a factor of the target size
      *
      * @see
      *   [[sfml.graphics.View.viewport_= viewport_=]]
      *
      * @doxygenId
      *   classsf_1_1View_1aa2006fa4269078be4fd5ca999dcb6244
      */
    def viewport: Rect[Float] =
        Rect.toRectFloat(Self.getViewport(this))()

    /** Set the target viewport.
      *
      * The viewport is the rectangle into which the contents of the view are displayed, expressed as a factor (between 0 and 1) of
      * the size of the [[sfml.graphics.RenderTarget RenderTarget]] to which the view is applied. For example, a view which takes
      * the left side of the target would be defined with `view.viewport = (0f, 0f, 0.5f, 1f)`. By default, a view has a viewport
      * which covers the entire target.
      *
      * @param viewport
      *   New viewport rectangle
      *
      * @see
      *   [[sfml.graphics.View.viewport viewport]]
      *
      * @doxygenId
      *   classsf_1_1View_1a8eaec46b7d332fe834f016d0187d4b4a
      *
      * @doxygenHash
      *   355ded9e6070d26c121adf42c40fd160
      */
    def viewport_=(viewport: Rect[Float]): Unit =
        Self.setViewport(this, viewport)

    /* Scala operators */

    def copy(): View =
        View(ResourceBuffer.shallow_copy(this))

object View:

    private[sfml] inline given Conversion[View, Ptr[Self.sfView]]:
        override def apply(view: View) = view._view.ptr

    /* Constructors */

    private[sfml] opaque type ctor = ResourcePtr[Self.sfView]

    private[sfml] def apply(view: ResourcePtr[Self.sfView]): View =
        new Object with View(using view)

    private[sfml] def apply(f: Ptr[Self.sfView] => Unit): View =
        new Object with View(using ctor(f))

    private[sfml] def apply(ref: Ptr[Self.sfView]): View =
        new Object with View(using ResourcePtr.retrieveCopy(ref))

    object ctor:

        private[sfml] def apply(f: Ptr[Self.sfView] => Unit): ctor =
            ResourceBuffer(Self.dtor)(f)

        /** Default constructor.
          *
          * This constructor creates a default view of (0, 0, 1000, 1000)
          *
          * @doxygenId
          *   classsf_1_1View_1a28c38308ff089ae5bdacd001d12286d3
          */
        def apply(): ctor =
            ctor(Self.ctor())

        /** Construct the view from its center and size.
          *
          * @param center
          *   Center of the zone to display
          *
          * @param size
          *   Size of zone to display
          *
          * @doxygenId
          *   classsf_1_1View_1afdaf84cfc910ef160450d63603457ea4
          */
        def apply(center: Vector2[Float], size: Vector2[Float]): ctor =
            ctor(Self.ctor(center, size))

        /** Construct the view from a rectangle.
          *
          * @param rectangle
          *   Rectangle defining the zone to display
          *
          * @doxygenId
          *   classsf_1_1View_1a1d63bc49e041b3b1ff992bb6430e1326
          */
        def apply(rectangle: Rect[Float]): ctor =
            ctor(Self.ctor(rectangle))

    /** Default constructor.
      *
      * This constructor creates a default view of (0, 0, 1000, 1000)
      *
      * @doxygenId
      *   classsf_1_1View_1a28c38308ff089ae5bdacd001d12286d3
      */
    def apply(): View =
        View(ctor())

    /** Construct the view from its center and size.
      *
      * @param center
      *   Center of the zone to display
      *
      * @param size
      *   Size of zone to display
      *
      * @doxygenId
      *   classsf_1_1View_1afdaf84cfc910ef160450d63603457ea4
      */
    def apply(center: Vector2[Float], size: Vector2[Float]): View =
        View(ctor(center, size))

    /** Construct the view from a rectangle.
      *
      * @param rectangle
      *   Rectangle defining the zone to display
      *
      * @doxygenId
      *   classsf_1_1View_1a1d63bc49e041b3b1ff992bb6430e1326
      */
    def apply(rectangle: Rect[Float]): View =
        View(ctor(rectangle))

    /* Immutable */

    extension (view: Immutable[View])

        def center: Vector2[Float] = view.get.center

        def rotation: Float = view.get.rotation

        def size: Vector2[Float] = view.get.size

        def viewport: Rect[Float] = view.get.viewport

        def transform: Immutable[Transform] = view.get.transform

        def inverseTransform(): Immutable[Transform] =
            view.get.inverseTransform()

        def copy(): View =
            view.get.copy()
