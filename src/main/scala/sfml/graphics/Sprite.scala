package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.Type.booleanToSfBool
import sfml.internal.graphics.Sprite as Self

/** [[sfml.graphics.Drawable Drawable]] representation of a texture, with its own transformations, color, etc.
  *
  * [[sfml.graphics.Sprite Sprite]] is a drawable class that allows to easily display a texture (or a part of it) on a render
  * target.
  *
  * It inherits all the functions from [[sfml.graphics.Transformable Transformable]]: position, rotation, scale, origin. It also
  * adds sprite-specific properties such as the texture to use, the part of it to display, and some convenience functions to change
  * the overall color of the sprite, or to get its bounding rectangle.
  *
  * [[sfml.graphics.Sprite Sprite]] works in combination with the [[sfml.graphics.Texture Texture]] class, which loads and provides
  * the pixel data of a given texture.
  *
  * The separation of [[sfml.graphics.Sprite Sprite]] and [[sfml.graphics.Texture Texture]] allows more flexibility and better
  * performances: indeed a [[sfml.graphics.Texture Texture]] is a heavy resource, and any operation on it is slow (often too slow
  * for real-time applications). On the other side, a [[sfml.graphics.Sprite Sprite]] is a lightweight object which can use the
  * pixel data of a [[sfml.graphics.Texture Texture]] and draw it with its own transformation/color/blending attributes.
  *
  * It is important to note that the [[sfml.graphics.Sprite Sprite]] instance doesn't copy the texture that it uses, it only keeps a
  * reference to it. Thus, a [[sfml.graphics.Texture Texture]] must not be destroyed while it is used by a
  * [[sfml.graphics.Sprite Sprite]] (i.e. never write a function that uses a local [[sfml.graphics.Texture Texture]] instance for
  * creating a sprite).
  *
  * See also the note on coordinates and undistorted rendering in [[sfml.graphics.Transformable Transformable]].
  *
  * ```scala
  * //{
  * import sfml.window.VideoMode
  *
  * val window: RenderWindow = ???
  *
  * //}
  * val texture = Texture()
  * texture.loadFromFile("texture.png")
  *
  * val sprite = Sprite()
  * sprite.texture = texture
  * sprite.textureRect = Rect(10, 10, 50, 30)
  * sprite.color = Color(255, 255, 255, 200)
  * sprite.position = (100, 25)
  *
  * window.draw(sprite)
  * ```
  *
  * @see
  *   [[sfml.graphics.Texture Texture]], [[sfml.graphics.Transformable Transformable]]
  *
  * @doxygenId
  *   classsf_1_1Sprite
  *
  * @doxygenHash
  *   4bffd26e5b01d2671337a9dc4d00070f
  */
trait Sprite(using private[sfml] ctor: Sprite.ctor) extends Transformable with Drawable:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _sprite: ResourcePtr[Self.sfSprite] =
        ResourceBuffer(ctor)

    private[sfml] override lazy val _transformable = _sprite.get(_.at2)

    /* Methods */

    override def draw(target: RenderTarget, states: RenderStates): Unit =
        RenderTarget.patch_draw(_sprite.ptr.asInstanceOf[Ptr[Byte]], target, states)

    /** Get the global color of the sprite.
      *
      * @return
      *   Global color of the sprite
      *
      * @see
      *   [[sfml.graphics.Sprite.color_= color_=]]
      *
      * @doxygenId
      *   classsf_1_1Sprite_1af4a3ee8177fdd6e472a360a0a837d7cf
      */
    def color: Immutable[Color] =
        Immutable(Self.getColor(_sprite.ptr))

    /** Set the global color of the sprite.
      *
      * This color is modulated (multiplied) with the sprite's texture. It can be used to colorize the sprite, or change its global
      * opacity. By default, the sprite's color is opaque white.
      *
      * @param color
      *   New color of the sprite
      *
      * @see
      *   [[sfml.graphics.Sprite.color color]]
      *
      * @doxygenId
      *   classsf_1_1Sprite_1a14def44da6437bfea20c4df5e71aba4c
      */
    def color_=(color: Immutable[Color]): Unit =
        Self.setColor(_sprite.ptr, color.get)

    /** Get the global bounding rectangle of the entity.
      *
      * The returned rectangle is in global coordinates, which means that it takes into account the transformations (translation,
      * rotation, scale, ...) that are applied to the entity. In other words, this function returns the bounds of the sprite in the
      * global 2D world's coordinate system.
      *
      * @return
      *   Global bounding rectangle of the entity
      *
      * @doxygenId
      *   classsf_1_1Sprite_1aa795483096b90745b2e799532963e271
      */
    def globalBounds: Immutable[Rect[Float]] =
        transform.transformRect(localBounds.get)

    /** Get the local bounding rectangle of the entity.
      *
      * The returned rectangle is in local coordinates, which means that it ignores the transformations (translation, rotation,
      * scale, ...) that are applied to the entity. In other words, this function returns the bounds of the entity in the entity's
      * coordinate system.
      *
      * @return
      *   Local bounding rectangle of the entity
      *
      * @doxygenId
      *   classsf_1_1Sprite_1ab2f4c781464da6f8a52b1df6058a48b8
      */
    def localBounds: Immutable[Rect[Float]] =
        val width = textureRect.get.width.abs.toFloat
        val height = textureRect.get.height.abs.toFloat

        Rect[Float](0, 0, width, height)

    // NOTE: To be able to use [`texture_=`]
    def texture = ()

    /** Change the source texture of the sprite.
      *
      * The `texture` argument refers to a texture that must exist as long as the sprite uses it. Indeed, the sprite doesn't store
      * its own copy of the texture, but rather keeps a pointer to the one that you passed to this function. If the source texture
      * is destroyed and the sprite tries to use it, the behavior is undefined. If `resetRect` is true, the TextureRect property of
      * the sprite is automatically adjusted to the size of the new texture. If it is false, the texture rect is left unchanged.
      *
      * @param texture
      *   New texture
      *
      * @param resetRect
      *   Should the texture rect be reset to the size of the new texture?
      *
      * @see
      *   [[sfml.graphics.Sprite.texture texture]], [[sfml.graphics.Sprite.textureRect_= textureRect_=]]
      *
      * @doxygenId
      *   classsf_1_1Sprite_1a3729c88d88ac38c19317c18e87242560
      */
    def texture_=(texture: Immutable[Texture], resetRect: Boolean = false) =
        Self.setTexture(_sprite.ptr, texture.get, resetRect)

    /** Get the sub-rectangle of the texture displayed by the sprite.
      *
      * @return
      *   [[sfml.graphics.Texture Texture]] rectangle of the sprite
      *
      * @see
      *   [[sfml.graphics.Sprite.textureRect_= textureRect_=]]
      *
      * @doxygenId
      *   classsf_1_1Sprite_1afb19e5b4f39d17cf4d95752b3a79bcb6
      */
    def textureRect: Immutable[Rect[Int]] =
        Rect.toRectInt(Self.getTextureRect(_sprite.ptr))()

    /** Set the sub-rectangle of the texture that the sprite will display.
      *
      * The texture rect is useful when you don't want to display the whole texture, but rather a part of it. By default, the
      * texture rect covers the entire texture.
      *
      * @param rectangle
      *   Rectangle defining the region of the texture to display
      *
      * @see
      *   [[sfml.graphics.Sprite.textureRect textureRect]], [[sfml.graphics.Sprite.texture_= texture_=]]
      *
      * @doxygenId
      *   classsf_1_1Sprite_1a3fefec419a4e6a90c0fd54c793d82ec2
      */
    def textureRect_=(rectangle: Immutable[Rect[Int]]): Unit =
        Self.setTextureRect(_sprite.ptr, rectangle.get)

object Sprite:

    private[sfml] inline given Conversion[Sprite, Ptr[Self.sfSprite]]:
        override def apply(sprite: Sprite) = sprite._sprite.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfSprite]

    object ctor:

        /** Default constructor.
          *
          * Creates an empty sprite with no source texture.
          *
          * @doxygenId
          *   classsf_1_1Sprite_1a92559fbca895a96758abf5eabab96984
          */
        def apply(): ctor =
            Constructor(Self.ctor(_))

        /** Construct the sprite from a source texture.
          *
          * @param texture
          *   Source texture
          *
          * @see
          *   [[sfml.graphics.Sprite.texture_= texture_=]]
          *
          * @doxygenId
          *   classsf_1_1Sprite_1a2a9fca374d7abf084bb1c143a879ff4a
          */
        def apply(texture: Texture): ctor =
            Constructor(Self.ctor(_, texture))

    /** Default constructor.
      *
      * Creates an empty sprite with no source texture.
      *
      * @doxygenId
      *   classsf_1_1Sprite_1a92559fbca895a96758abf5eabab96984
      */
    def apply(): Sprite =
        new Object with Sprite(using ctor())

    /** Construct the sprite from a source texture.
      *
      * @param texture
      *   Source texture
      *
      * @see
      *   [[sfml.graphics.Sprite.texture_= texture_=]]
      *
      * @doxygenId
      *   classsf_1_1Sprite_1a2a9fca374d7abf084bb1c143a879ff4a
      */
    def apply(texture: Texture): Sprite =
        new Object with Sprite(using ctor(texture))

    /* Immutable */

    extension (sprite: Immutable[Sprite])

        def draw(target: RenderTarget, states: RenderStates): Unit =
            sprite.get.draw(target, states)

        def color: Immutable[Color] = sprite.get.color

        def globalBounds: Immutable[Rect[Float]] = sprite.get.globalBounds

        def localBounds: Immutable[Rect[Float]] = sprite.get.localBounds

        def textureRect: Immutable[Rect[Int]] = sprite.get.textureRect
