package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsafe.CFuncPtr3.fromScalaFunction

/* NOTE: Internal objects are likely to use [sfml.graphics.RenderTarget.patch_draw] method.
 * This is *not* provided by default to allow users to use Draw trait on their own objects.
 * Users will call [sfml.graphics.RenderTarget.draw] method accordingly. */

/** Abstract base class for objects that can be drawn to a render target.
  *
  * [[sfml.graphics.Drawable Drawable]] is a very simple base class that allows objects of derived classes to be drawn to a
  * [[sfml.graphics.RenderTarget RenderTarget]].
  *
  * All you have to do in your derived class is to override the [[sfml.graphics.Drawable.draw draw]] function.
  *
  * Note that inheriting from [[sfml.graphics.Drawable Drawable]] is not mandatory, but it allows this nice syntax
  * `window.draw(object)` rather than `object.draw(window)`, which is more consistent with other SFML classes.
  *
  * ```scala
  * class MyDrawable(sprite: Sprite) extends Drawable:
  *     override def draw(target: RenderTarget, states: RenderStates): Unit =
  *         // You can draw other high-level objects
  *         target.draw(sprite, states)
  *
  *         <!-- TODO: Low-level API -->
  *         <!-- TODO: OpenGL code -->
  * ```
  *
  * @see
  *   [[sfml.graphics.RenderTarget RenderTarget]]
  *
  * @doxygenId
  *   classsf_1_1Drawable
  *
  * @doxygenHash
  *   a8668fa1eaa85935a45c5f7e07025c9f
  */
trait Drawable:

    /** Draw the object to a render target.
      *
      * This is a function that has to be implemented by the derived class to define how the drawable should be drawn.
      *
      * @param target
      *   Render target to draw to
      *
      * @param states
      *   Current render states
      *
      * @doxygenId
      *   classsf_1_1Drawable_1a90d2c88bba9b035a0844eccb380ef631
      */
    // TODO: Check if `protected` should be used
    def draw(target: RenderTarget, states: RenderStates): Unit

object Drawable:

    private[sfml] val vtable = VTable(
        CFuncPtr.toPtr((self: Ptr[Wrapped[Drawable]], target: Ptr[Wrapped[RenderTarget]], states: Ptr[Wrapped[RenderStates]]) =>
            self.get.draw(target.get, states.get)
        )
    )

    extension (drawable: Immutable[Drawable])
        def draw(target: RenderTarget, states: RenderStates): Unit =
            drawable.get.draw(target, states)
