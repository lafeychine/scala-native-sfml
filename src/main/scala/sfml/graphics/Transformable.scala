package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsafe.cxx.Ref

import sfml.internal.graphics.Transformable as Self

import sfml.system.Vector2

/** Decomposed transform defined by a position, a rotation and a scale.
  *
  * This class is provided for convenience, on top of [[sfml.graphics.Transform Transform]].
  *
  * [[sfml.graphics.Transform Transform]], as a low-level class, offers a great level of flexibility but it is not always convenient
  * to manage. Indeed, one can easily combine any kind of operation, such as a translation followed by a rotation followed by a
  * scaling, but once the result transform is built, there's no way to go backward and, let's say, change only the rotation without
  * modifying the translation and scaling. The entire transform must be recomputed, which means that you need to retrieve the
  * initial translation and scale factors as well, and combine them the same way you did before updating the rotation. This is a
  * tedious operation, and it requires to store all the individual components of the final transform.
  *
  * That's exactly what [[sfml.graphics.Transformable Transformable]] was written for: it hides these variables and the composed
  * transform behind an easy to use interface. You can set or get any of the individual components without worrying about the
  * others. It also provides the composed transform (as a [[sfml.graphics.Transform Transform]]), and keeps it up-to-date.
  *
  * In addition to the position, rotation and scale, [[sfml.graphics.Transformable Transformable]] provides an "origin" component,
  * which represents the local origin of the three other components. Let's take an example with a 10x10 pixels sprite. By default,
  * the sprite is positioned/rotated/scaled relatively to its top-left corner, because it is the local point (0, 0). But if we
  * change the origin to be (5, 5), the sprite will be positioned/rotated/scaled around its center instead. And if we set the origin
  * to (10, 10), it will be transformed around its bottom-right corner.
  *
  * To keep the [[sfml.graphics.Transformable Transformable]] class simple, there's only one origin for all the components. You
  * cannot position the sprite relatively to its top-left corner while rotating it around its center, for example. To do such
  * things, use [[sfml.graphics.Transform Transform]] directly.
  *
  * [[sfml.graphics.Transformable Transformable]] can be used as a base class. It is often combined with
  * [[sfml.graphics.Drawable Drawable]] – that's what SFML's sprites, texts and shapes do.
  *
  * ```scala
  * //{
  * val window: RenderWindow = ???
  *
  * //}
  * class MyEntity extends Transformable with Drawable:
  *     override def draw(target: RenderTarget, states: RenderStates): Unit =
  *         states.transform *= transform
  *         target.draw(???, states)
  *
  * val entity = MyEntity()
  * entity.position = (10, 20)
  * entity.rotation = 45
  * window.draw(entity)
  * ```
  *
  * It can also be used as a member, if you don't want to use its API directly (because you don't need all its functions, or you
  * have different naming conventions for example).
  *
  * ```scala
  * //{
  * case class MyVector2(x: Float, y: Float)
  *
  * //}
  * class MyEntity:
  *     private val myTransform = Transformable()
  *
  *     def position_=(v: MyVector2): Unit =
  *         myTransform.position = (v.x, v.y)
  *
  *     def draw(target: RenderTarget): Unit =
  *         target.draw(???, RenderStates(myTransform.transform))
  * ```
  *
  * A note on coordinates and undistorted rendering: By default, SFML (or more exactly, OpenGL) may interpolate drawable objects
  * such as sprites or texts when rendering. While this allows transitions like slow movements or rotations to appear smoothly, it
  * can lead to unwanted results in some cases, for example blurred or distorted objects. In order to render a
  * [[sfml.graphics.Drawable Drawable]] object pixel-perfectly, make sure the involved coordinates allow a 1:1 mapping of pixels in
  * the window to texels (pixels in the texture). More specifically, this means:
  *   - The object's position, origin and scale have no fractional part
  *   - The object's and the view's rotation are a multiple of 90 degrees
  *   - The view's center and size have no fractional part
  *
  * @see
  *   [[sfml.graphics.Transform Transform]]
  *
  * @doxygenId
  *   classsf_1_1Transformable
  *
  * @doxygenHash
  *   10712ba31f80a90e3a54a999fb58510d
  */
trait Transformable:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _transformable: ResourcePtr[Self.sfTransformable] =
        ResourceBuffer(Self.ctor(_))

    /* Methods */

    /** Move the object by a given offset.
      *
      * This function adds to the current position of the object, unlike [[sfml.graphics.Transformable.position_= position_=]] which
      * overwrites it. Thus, it is equivalent to the following code:
      * ```scala
      * //{
      * import sfml.system.Vector2
      *
      * val obj: Transformable = ???
      * val offsetX, offsetY: Float = ???
      *
      * //}
      * val pos: Vector2[Float] = obj.position
      * obj.position = (pos.x + offsetX, pos.y + offsetY)
      * ```
      *
      * @param offsetX
      *   X offset
      *
      * @param offsetY
      *   Y offset
      *
      * @see
      *   [[sfml.graphics.Transformable.position_= position_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a86b461d6a941ad390c2ad8b6a4a20391
      *
      * @doxygenHash
      *   0dac4f239ee2d413a50a7647f25496bd
      */
    def move(offsetX: Float, offsetY: Float): Unit =
        Self.move(_transformable.ptr, offsetX, offsetY)

    /** Move the object by a given offset.
      *
      * This function adds to the current position of the object, unlike setPosition which overwrites it. Thus, it is equivalent to
      * the following code:
      * ```scala
      * //{
      * import sfml.system.Vector2
      *
      * val obj: Transformable = ???
      * val offset: Vector2[Float] = ???
      *
      * //}
      * obj.position = obj.position + offset
      * ```
      *
      * @param offset
      *   Offset
      *
      * @see
      *   [[sfml.graphics.Transformable.position_= position_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1ab9ca691522f6ddc1a40406849b87c469
      *
      * @doxygenHash
      *   554723f781edd0d6c7f6ad10f11d575d
      */
    def move(offset: Vector2[Float]): Unit =
        Self.move(_transformable.ptr, offset)

    /** Rotate the object.
      *
      * This function adds to the current rotation of the object, unlike setRotation which overwrites it. Thus, it is equivalent to
      * the following code:
      * ```scala
      * //{
      * val obj: Transformable = ???
      * val angle: Float = ???
      *
      * //}
      * obj.rotation = obj.rotation + angle
      * ```
      *
      * @param angle
      *   Angle of rotation, in degrees
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a32baf2bf1a74699b03bf8c95030a38ed
      *
      * @doxygenHash
      *   0fda69e815a94f5eb97798bd55f89ee3
      */
    def rotate(angle: Float): Unit =
        Self.rotate(_transformable.ptr, angle)

    /** Scale the object.
      *
      * This function multiplies the current scale of the object, unlike setScale which overwrites it. Thus, it is equivalent to the
      * following code:
      * ```scala
      * //{
      * import sfml.system.Vector2
      *
      * val obj: Transformable = ???
      * val factorX, factorY: Float = ???
      *
      * //}
      * val scale: Vector2[Float] = obj.scale
      * obj.scale = (scale.x + factorX, scale.y + factorY)
      * ```
      *
      * @param factorX
      *   Horizontal scale factor
      *
      * @param factorY
      *   Vertical scale factor
      *
      * @see
      *   [[sfml.graphics.Transformable.scale_= scale_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1aaec50b46b3f41b054763304d1e727471
      *
      * @doxygenHash
      *   7b98120ccffec08dde55a01370f68db3
      */
    def scale(factorX: Float, factorY: Float): Unit =
        Self.scale(_transformable.ptr, factorX, factorY)

    /** Scale the object.
      *
      * This function multiplies the current scale of the object, unlike setScale which overwrites it. Thus, it is equivalent to the
      * following code:
      * ```scala
      * //{
      * import sfml.system.Vector2
      *
      * val obj: Transformable = ???
      * val factor: Vector2[Float] = ???
      *
      * //}
      * val scale: Vector2[Float] = obj.scale
      * obj.scale = (scale.x * factor.x, scale.y * factor.y)
      * ```
      *
      * @param factor
      *   Scale factors
      *
      * @see
      *   [[sfml.graphics.Transformable.scale_= scale_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a4c48a87f1626047e448f9c1a68ff167e
      *
      * @doxygenHash
      *   30c03205e3a81e202aa2ab780dfafbcb
      */
    def scale(factor: Vector2[Float]): Unit =
        Self.scale(_transformable.ptr, factor)

    /* Getter / Setter */

    /** get the local origin of the object
      *
      * @return
      *   Current origin
      *
      * @see
      *   [[sfml.graphics.Transformable.origin_= origin_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a898b33eb6513161eb5c747a072364f15
      */
    def origin: Vector2[Float] =
        Vector2.toVector2Float(Self.getOrigin(_transformable.ptr))()

    /** set the local origin of the object
      *
      * The origin of an object defines the center point for all transformations (position, scale, rotation). The coordinates of
      * this point must be relative to the top-left corner of the object, and ignore all transformations (position, scale,
      * rotation). The default origin of a transformable object is (0, 0).
      *
      * @param x
      *   X coordinate of the new origin
      *
      * @param y
      *   Y coordinate of the new origin
      *
      * @see
      *   [[sfml.graphics.Transformable.origin origin]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a56c67bd80aae8418d13fb96c034d25ec
      */
    def origin_=(x: Float, y: Float) =
        Self.setOrigin(_transformable.ptr, x, y)

    /** set the local origin of the object
      *
      * The origin of an object defines the center point for all transformations (position, scale, rotation). The coordinates of
      * this point must be relative to the top-left corner of the object, and ignore all transformations (position, scale,
      * rotation). The default origin of a transformable object is (0, 0).
      *
      * @param origin
      *   New origin
      *
      * @see
      *   [[sfml.graphics.Transformable.origin origin]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1aa93a835ffbf3bee2098dfbbc695a7f05
      */
    def origin_=(origin: Vector2[Float]) =
        Self.setOrigin(_transformable.ptr, origin)

    /** get the position of the object
      *
      * @return
      *   Current position
      *
      * @see
      *   [[sfml.graphics.Transformable.position_= position_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1aea8b18e91a7bf7be589851bb9dd11241
      */
    def position: Vector2[Float] =
        Vector2.toVector2Float(Self.getPosition(_transformable.ptr))()

    /** set the position of the object
      *
      * This function completely overwrites the previous position. See the move function to apply an offset based on the previous
      * position instead. The default position of a transformable object is (0, 0).
      *
      * @param x
      *   X coordinate of the new position
      *
      * @param y
      *   Y coordinate of the new position
      *
      * @see
      *   [[sfml.graphics.Transformable.move move]], [[sfml.graphics.Transformable.position position]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a4dbfb1a7c80688b0b4c477d706550208
      */
    def position_=(x: Float, y: Float) =
        Self.setPosition(_transformable.ptr, x, y)

    /** set the position of the object
      *
      * This function completely overwrites the previous position. See the move function to apply an offset based on the previous
      * position instead. The default position of a transformable object is (0, 0).
      *
      * @param position
      *   New position
      *
      * @see
      *   [[sfml.graphics.Transformable.move move]], [[sfml.graphics.Transformable.position position]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1af1a42209ce2b5d3f07b00f917bcd8015
      */
    def position_=(position: Vector2[Float]) =
        Self.setPosition(_transformable.ptr, position)

    /** get the orientation of the object
      *
      * The rotation is always in the range [0, 360].
      *
      * @return
      *   Current rotation, in degrees
      *
      * @see
      *   [[sfml.graphics.Transformable.rotation_= rotation_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1aa00b5c5d4a06ac24a94dd72c56931d3a
      */
    def rotation: Float =
        Self.getRotation(_transformable.ptr)

    /** set the orientation of the object
      *
      * This function completely overwrites the previous rotation. See the rotate function to add an angle based on the previous
      * rotation instead. The default rotation of a transformable object is 0.
      *
      * @param angle
      *   New rotation, in degrees
      *
      * @see
      *   [[sfml.graphics.Transformable.rotate rotate]], [[sfml.graphics.Transformable.rotation rotation]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a32baf2bf1a74699b03bf8c95030a38ed
      */
    def rotation_=(angle: Float) =
        Self.setRotation(_transformable.ptr, angle)

    /** get the current scale of the object
      *
      * @return
      *   Current scale factors
      *
      * @see
      *   [[sfml.graphics.Transformable.scale_= scale_=]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a7bcae0e924213f2e89edd8926f2453af
      */
    def scale: Vector2[Float] =
        Vector2.toVector2Float(Self.getScale(_transformable.ptr))()

    /** set the scale factors of the object
      *
      * This function completely overwrites the previous scale. See the scale function to add a factor based on the previous scale
      * instead. The default scale of a transformable object is (1, 1).
      *
      * @param factorX
      *   New horizontal scale factor
      *
      * @param factorY
      *   New vertical scale factor
      *
      * @see
      *   [[sfml.graphics.Transformable.scale scale]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1aaec50b46b3f41b054763304d1e727471
      *
      * @doxygenHash
      *   7b98120ccffec08dde55a01370f68db3
      */
    def scale_=(x: Float, y: Float) =
        Self.setScale(_transformable.ptr, x, y)

    /** set the scale factors of the object
      *
      * This function completely overwrites the previous scale. See the scale function to add a factor based on the previous scale
      * instead. The default scale of a transformable object is (1, 1).
      *
      * @param factors
      *   New scale factors
      *
      * @see
      *   [[sfml.graphics.Transformable.scale scale]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a4c48a87f1626047e448f9c1a68ff167e
      *
      * @doxygenHash
      *   30c03205e3a81e202aa2ab780dfafbcb
      */
    def scale_=(factors: Vector2[Float]) =
        Self.setScale(_transformable.ptr, factors)

    /** get the combined transform of the object
      *
      * @return
      *   Transform combining the position/rotation/scale/origin of the object
      *
      * @see
      *   [[sfml.graphics.Transformable.inverseTransform inverseTransform]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1a3e1b4772a451ec66ac7e6af655726154
      */
    def transform: Transform =
        Transform(Ref.toUnsafeRef(Self.getTransform(_transformable.ptr)))

    /** get the inverse of the combined transform of the object
      *
      * @return
      *   Inverse of the combined transformations applied to the object
      *
      * @see
      *   [[sfml.graphics.Transformable.transform transform]]
      *
      * @doxygenId
      *   classsf_1_1Transformable_1ac5e75d724436069d2268791c6b486916
      */
    def inverseTransform(): Transform =
        Transform(Ref.toUnsafeRef(Self.getInverseTransform(_transformable.ptr)))

object Transformable:

    private[sfml] inline given Conversion[Transformable, Ptr[Self.sfTransformable]]:
        override def apply(transformable: Transformable) = transformable._transformable.ptr

    /* Constructors */

    /** Default constructor.
      *
      * @doxygenId
      *   classsf_1_1Transformable_1ae71710de0fef423121bab1c684954a2e
      */
    def apply(): Transformable =
        new Object with Transformable

    /* Immutable */

    extension (transformable: Immutable[Transformable])
        def origin: Vector2[Float] = transformable.get.origin

        def position: Vector2[Float] = transformable.get.position

        def rotation: Float = transformable.get.rotation

        def scale: Vector2[Float] = transformable.get.scale

        def transform: Transform = transformable.get.transform

        def inverseTransform(): Transform = transformable.get.inverseTransform()
