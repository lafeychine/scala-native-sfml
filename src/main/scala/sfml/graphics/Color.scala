package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.{UnsignedRichByte, UnsignedRichInt}

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.graphics.Color as Self

/** Utility class for manipulating RGBA colors.
  *
  * [[sfml.graphics.Color Color]] is a simple color class composed of 4 components:
  *   - Red
  *   - Green
  *   - Blue
  *   - Alpha (opacity)
  *
  * Each component is a public member, an unsigned integer in the range [0, 255]. Thus, colors can be constructed and manipulated
  * very easily:
  * ```scala
  * val color = Color(255, 0, 0) // red
  * val black = color.copy(r = 0) // make it black
  * val blue = color.copy(b = 128.toByte) // make it dark blue
  * ```
  *
  * The fourth component of colors, named "alpha", represents the opacity of the color. A color with an alpha value of 255 will be
  * fully opaque, while an alpha value of 0 will make a color fully transparent, whatever the value of the other components is.
  *
  * The most common colors are already defined as static variables:
  * ```scala
  * val black = Color.Black()
  * val white = Color.White()
  * val red = Color.Red()
  * val green = Color.Green()
  * val blue = Color.Blue()
  * val yellow = Color.Yellow()
  * val magenta = Color.Magenta()
  * val cyan = Color.Cyan()
  * val transparent = Color.Transparent()
  * ```
  *
  * @param r
  *   Red component.
  *
  * @param g
  *   Green component.
  *
  * @param b
  *   Blue component.
  *
  * @param a
  *   Alpha (opacity) component.
  *
  * @doxygenId
  *   classsf_1_1Color
  *
  * @doxygenHash
  *   9e7aa5880dc20518578ef13c4c9b22ff
  */
trait Color(using private[sfml] ctor: Color.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _color: ResourcePtr[Self.sfColor] =
        ResourceBuffer(ctor)

    /* Methods */

    def r: Int =
        _color.ptr._1.toByte

    def r_=(r: Int): Unit =
        _color.ptr._1 = r.toUByte

    def g: Int =
        _color.ptr._2.toByte

    def g_=(g: Int): Unit =
        _color.ptr._2 = g.toUByte

    def b: Int =
        _color.ptr._3.toByte

    def b_=(b: Int): Unit =
        _color.ptr._3 = b.toUByte

    def a: Int =
        _color.ptr._4.toByte

    def a_=(a: Int): Unit =
        _color.ptr._4 = a.toUByte

    def ==(rhs: Immutable[Color]): Boolean =
        Self.eq_sfColor(_color.ptr, rhs.get)

    def !=(rhs: Immutable[Color]): Boolean =
        Self.ne_sfColor(_color.ptr, rhs.get)

    /* Scala operators */

    def copy(r: Int = r, g: Int = g, b: Int = b, a: Int = a): Color =
        Color(r, g, b, a)

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: Color        => this == Immutable(rhs)
            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def toString(): java.lang.String =
        s"Color($r, $g, $b, $a)"

object Color:

    private[sfml] inline given Conversion[Color, Ptr[Self.sfColor]]:
        override def apply(color: Color) = color._color.ptr

    private[sfml] inline given Conversion[Ptr[Self.sfColor], Color]:
        override def apply(color: Ptr[Self.sfColor]) = Color(ResourcePtr.retrieveCopy(color))

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfColor]

    object ctor:

        /** Default constructor.
          *
          * Constructs an opaque black color. It is equivalent to `Color(0, 0, 0, 255)`
          *
          * @doxygenId
          *   classsf_1_1Color_1ac2eb4393fb11ad3fa3ccf34e92fe08e4
          */
        def apply(): ctor =
            Constructor(Self.ctor(_))

        /** Construct the color from its 4 RGBA components.
          *
          * @param red
          *   Red component (in the range [0, 255])
          *
          * @param green
          *   Green component (in the range [0, 255])
          *
          * @param blue
          *   Blue component (in the range [0, 255])
          *
          * @param alpha
          *   Alpha (opacity) component (in the range [0, 255])
          *
          * @doxygenId
          *   classsf_1_1Color_1ac791dc61be4c60baac50fe700f1c9850
          */
        def apply(red: Int, green: Int, blue: Int, alpha: Int = 255): ctor =
            Constructor(Self.ctor(_, red.toUByte, green.toUByte, blue.toUByte, alpha.toUByte))

    def apply(): Color =
        new Object with Color(using ctor())

    def apply(red: Int, green: Int, blue: Int, alpha: Int = 255): Color =
        new Object with Color(using ctor(red, green, blue, alpha))

    private[sfml] def apply(color: ResourcePtr[Self.sfColor]): Color =
        new Object with Color {
            private[sfml] override lazy val _color = color
        }

    def unapply(color: Color): (Int, Int, Int, Int) =
        (color.r, color.g, color.b, color.a)

    /* Immutable */

    extension (color: Immutable[Color])

        def r: Int = color.get.r

        def g: Int = color.get.g

        def b: Int = color.get.b

        def a: Int = color.get.a

        def copy(r: Int = color.r, g: Int = color.g, b: Int = color.b, a: Int = color.a): Color =
            color.get.copy(r, g, b, a)

    /* Static methods */

    def Black(): Color = Color(0, 0, 0)

    def White(): Color = Color(255, 255, 255)

    def Red(): Color = Color(255, 0, 0)

    def Green(): Color = Color(0, 255, 0)

    def Blue(): Color = Color(0, 0, 255)

    def Yellow(): Color = Color(255, 255, 0)

    def Magenta(): Color = Color(255, 0, 255)

    def Cyan(): Color = Color(0, 255, 255)

    def Transparent(): Color = Color(0, 0, 0, 0)
