package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.graphics.Texture as Self

import sfml.window.Window

/** [[sfml.graphics.Image Image]] living on the graphics card that can be used for drawing.
  *
  * [[sfml.graphics.Texture Texture]] stores pixels that can be drawn, with a sprite for example.
  *
  * A texture lives in the graphics card memory, therefore it is very fast to draw a texture to a render target, or copy a render
  * target to a texture (the graphics card can access both directly).
  *
  * Being stored in the graphics card memory has some drawbacks. A texture cannot be manipulated as freely as a
  * [[sfml.graphics.Image Image]], you need to prepare the pixels first and then upload them to the texture in a single operation
  * (see [[sfml.graphics.Texture.update update]]).
  *
  * <!-- TODO Add basic Image -->
  *
  * Since they live in the graphics card memory, the pixels of a texture cannot be accessed without a slow copy first. And they
  * cannot be accessed individually. Therefore, if you need to read the texture's pixels (like for pixel-perfect collisions), it is
  * recommended to store the collision information separately, for example in an array of booleans.
  *
  * Like [[sfml.graphics.Image Image]], [[sfml.graphics.Texture Texture]] can handle a unique internal representation of pixels,
  * which is RGBA 32 bits. This means that a pixel must be composed of 8 bits red, green, blue and alpha channels – just like a
  * [[sfml.graphics.Color Color]].
  *
  * ```scala
  * //{
  * val window: RenderWindow = ???
  * val sprite: Sprite = ???
  *
  * //}
  * // This example shows the most common use of Texture: drawing a sprite
  *
  * // Load a texture from a file
  * val texture = Texture()
  * if !(texture.loadFromFile("texture.png")) then
  *     // error...
  *     ???
  *
  * // Assign it to a sprite sf::Sprite sprite;
  * sprite.texture = texture
  *
  * // Draw the textured sprite
  * window.draw(sprite)
  * ```
  *
  * <!-- TODO Another example --> <!-- TODO OpenGL -->
  *
  * @see
  *   [[sfml.graphics.Sprite Sprite]], [[sfml.graphics.Image Image]], [[sfml.graphics.RenderTexture RenderTexture]]
  *
  * @doxygenId
  *   classsf_1_1Image
  *
  * @doxygenHash
  *   a93f9555c559eb8f7dc5ad1086dfa1b8
  */
trait Texture:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _texture: ResourcePtr[Self.sfTexture] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    /* Methods */

    /** Create the texture.
      *
      * If this function fails, the texture is left unchanged.
      *
      * @param width
      *   Width of the texture
      *
      * @param height
      *   Height of the texture
      *
      * @return
      *   True if creation was successful
      *
      * @doxygenId
      *   classsf_1_1Texture_1a89b4c7d204acf1033c3a1b6e0a3ad0a3
      */
    def create(width: Int, height: Int): Boolean =
        Self.create(_texture.ptr, width.toUInt, height.toUInt)

    /** Copy the texture pixels to an image.
      *
      * This function performs a slow operation that downloads the texture's pixels from the graphics card and copies them to a new
      * image, potentially applying transformations to pixels if necessary (texture may be padded or flipped).
      *
      * @return
      *   Image containing the texture's pixels
      *
      * <!-- TODO Image.loadFromImage -->
      *
      * @doxygenId
      *   classsf_1_1Texture_1a77e18a70de2e525ac5e4a7cd95f614b9
      *
      * @doxygenHash
      *   7d1b15933649f92e88a9f7d3fa5c6154
      */
    def copyToImage(): Image =
        Image(Self.copyToImage(_texture.ptr))

    /** Load the texture from a file on disk.
      *
      * <!-- TODO Image.loadFromFile -->
      *
      * The `area` argument can be used to load only a sub-rectangle of the whole image. If you want the entire image then leave the
      * default value (which is an empty [[sfml.graphics.Rect Rect]]). If the `area` rectangle crosses the bounds of the image, it
      * is adjusted to fit the image size.
      *
      * <!-- TODO getMaximumSize -->
      *
      * If this function fails, the texture is left unchanged.
      *
      * @param filename
      *   Path of the image file to load
      *
      * @param area
      *   Area of the image to load
      *
      * @return
      *   True if loading was successful
      *
      * <!-- TODO loadFromMemory, loadFromStream, loadFromImage -->
      *
      * @doxygenId
      *   classsf_1_1Texture_1a8e1b56eabfe33e2e0e1cb03712c7fcc7
      *
      * @doxygenHash
      *   6829979eaeb94efa13b0fc0189acfce4
      */
    def loadFromFile(filename: stdlib.String, area: Rect[Int] = Rect()): Boolean =
        Self.loadFromFile(_texture.ptr, filename, area)

    /** Tell whether the smooth filter is enabled or not.
      *
      * @return
      *   True if smoothing is enabled, false if it is disabled
      *
      * @see
      *   [[sfml.graphics.Texture.smooth_= smooth_=]]
      *
      * @doxygenId
      *   classsf_1_1Texture_1a3ebb050b5a71e1d40ba66eb1a060e103
      */
    def smooth: Boolean =
        Self.isSmooth(_texture.ptr)

    /** Enable or disable the smooth filter.
      *
      * When the filter is activated, the texture appears smoother so that pixels are less noticeable. However if you want the
      * texture to look exactly the same as its source file, you should leave it disabled. The smooth filter is disabled by default.
      *
      * @param smooth
      *   True to enable smoothing, false to disable it
      *
      * @see
      *   [[sfml.graphics.Texture.smooth smooth]]
      *
      * @doxygenId
      *   classsf_1_1Texture_1a0c3bd6825b9a99714f10d44179d74324
      */
    def smooth_=(smooth: Boolean) =
        Self.setSmooth(_texture.ptr, smooth)

    /** Tell whether the texture is repeated or not.
      *
      * @return
      *   True if repeat mode is enabled, false if it is disabled
      *
      * @see
      *   [[sfml.graphics.Texture.repeated_= repeated_=]]
      *
      * @doxygenId
      *   classsf_1_1Texture_1af1a1a32ca5c799204b2bea4040df7647
      */
    def repeated: Boolean =
        Self.isRepeated(_texture.ptr)

    /** Enable or disable repeating.
      *
      * Repeating is involved when using texture coordinates outside the texture rectangle [0, 0, width, height]. In this case, if
      * repeat mode is enabled, the whole texture will be repeated as many times as needed to reach the coordinate (for example, if
      * the X texture coordinate is 3 * width, the texture will be repeated 3 times). If repeat mode is disabled, the "extra space"
      * will instead be filled with border pixels. Warning: on very old graphics cards, white pixels may appear when the texture is
      * repeated. With such cards, repeat mode can be used reliably only if the texture has power-of-two dimensions (such as
      * 256x128). Repeating is disabled by default.
      *
      * @param repeated
      *   True to repeat the texture, false to disable repeating
      *
      * @see
      *   [[sfml.graphics.Texture.repeated repeated]]
      *
      * @doxygenId
      *   classsf_1_1Texture_1aaa87d1eff053b9d4d34a24c784a28658
      */
    def repeated_=(repeated: Boolean) =
        Self.setRepeated(_texture.ptr, repeated)

    /** Update the texture from the contents of a window.
      *
      * Although the source window can be smaller than the texture, this function is usually used for updating the whole texture.
      * The other overload, which has (x, y) additional arguments, is more convenient for updating a sub-area of the texture.
      *
      * No additional check is performed on the size of the window, passing a window bigger than the texture will lead to an
      * undefined behavior.
      *
      * This function does nothing if either the texture or the window was not previously created.
      *
      * @param window
      *   Window to copy to the texture
      *
      * @doxygenId
      *   classsf_1_1Texture_1ad3cceef238f7d5d2108a98dd38c17fc5
      */
    def update(window: Immutable[Window]): Unit =
        Self.update(_texture.ptr, window.get)

    /** Update a part of the texture from the contents of a window.
      *
      * No additional check is performed on the size of the window, passing an invalid combination of window size and offset will
      * lead to an undefined behavior.
      *
      * This function does nothing if either the texture or the window was not previously created.
      *
      * @param window
      *   Window to copy to the texture
      *
      * @param x
      *   X offset in the texture where to copy the source window
      *
      * @param y
      *   Y offset in the texture where to copy the source window
      *
      * @doxygenId
      *   classsf_1_1Texture_1a154f246eb8059b602076009ab1cfd175
      */
    def update(window: Immutable[Window], x: Int, y: Int): Unit =
        Self.update(_texture.ptr, window.get, x.toUInt, y.toUInt)

object Texture:

    private[sfml] inline given Conversion[Texture, Ptr[Self.sfTexture]]:
        override def apply(texture: Texture) = texture._texture.ptr

    private[sfml] inline given Conversion[Ptr[Self.sfTexture], Texture]:
        override def apply(texture: Ptr[Self.sfTexture]) = Texture(ResourcePtr.retrieve(texture))

    /* Constructors */

    /** Default constructor.
      *
      * Creates an empty texture.
      *
      * @doxygenId
      *   classsf_1_1Texture_1a3e04674853b8533bf981db3173e3a4a7
      */
    def apply(): Texture =
        new Object with Texture

    private[sfml] def apply(texture: ResourcePtr[Self.sfTexture]): Texture =
        new Object with Texture {
            private[sfml] override lazy val _texture = texture
        }

    extension (texture: Ptr[Self.sfTexture])
        private[sfml] def close(): Unit =
            Self.dtor(texture)

    /* Immutable */

    extension (texture: Immutable[Texture])

        def smooth: Boolean = texture.get.smooth

        def repeated: Boolean = texture.get.repeated
