package sfml
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.graphics.VertexArray as Self

import sfml.stdlib.Vector

object VertexArray:

    private[sfml] def dtor(vertexArray: Ptr[Self.sfVertexArray]): Unit =
        Vector.dtor(vertexArray.at2)
