package sfml
package graphics

import scala.annotation.{nowarn, targetName}
import scala.compiletime.error
import scala.math.Numeric.Implicits.infixNumericOps

import scala.scalanative.unsafe.*

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.graphics.Rect as Self
import sfml.internal.system.Vector2.*

import sfml.system.Vector2

trait Rect[T <: Float | Int](using private[sfml] val ctor: Rect.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _rect: ResourcePtr[Self.sfRect[T]] =
        ctor match
            case Rect.Type.Float(ctor) => ResourceBuffer(ctor).asInstanceOf[ResourcePtr[Self.sfRect[T]]]
            case Rect.Type.Int(ctor)   => ResourceBuffer(ctor).asInstanceOf[ResourcePtr[Self.sfRect[T]]]

    /* Methods */

    transparent inline def left: Float | Int =
        inline _rect.ptr match
            case ptr: Ptr[Self.sfRect[Float]] => (ptr: Ptr[Self.sfRect[Float]])._1
            case ptr: Ptr[Self.sfRect[Int]]   => (ptr: Ptr[Self.sfRect[Int]])._1
            case _                            => error("This expression must be inside an inline function")

    inline def left_=(left: T): Unit =
        inline (_rect.ptr, left) match
            case tpl: (Ptr[Self.sfRect[Float]], Float) => (tpl._1: Ptr[Self.sfRect[Float]])._1 = tpl._2
            case tpl: (Ptr[Self.sfRect[Int]], Int)     => (tpl._1: Ptr[Self.sfRect[Int]])._1 = tpl._2
            case _                                     => error("This expression must be inside an inline function")

    transparent inline def top: Float | Int =
        inline _rect.ptr match
            case ptr: Ptr[Self.sfRect[Float]] => (ptr: Ptr[Self.sfRect[Float]])._2
            case ptr: Ptr[Self.sfRect[Int]]   => (ptr: Ptr[Self.sfRect[Int]])._2
            case _                            => error("This expression must be inside an inline function")

    inline def top_=(top: T): Unit =
        inline (_rect.ptr, top) match
            case tpl: (Ptr[Self.sfRect[Float]], Float) => (tpl._1: Ptr[Self.sfRect[Float]])._2 = tpl._2
            case tpl: (Ptr[Self.sfRect[Int]], Int)     => (tpl._1: Ptr[Self.sfRect[Int]])._2 = tpl._2
            case _                                     => error("This expression must be inside an inline function")

    transparent inline def width: Float | Int =
        inline _rect.ptr match
            case ptr: Ptr[Self.sfRect[Float]] => (ptr: Ptr[Self.sfRect[Float]])._3
            case ptr: Ptr[Self.sfRect[Int]]   => (ptr: Ptr[Self.sfRect[Int]])._3
            case _                            => error("This expression must be inside an inline function")

    inline def width_=(width: T): Unit =
        inline (_rect.ptr, width) match
            case tpl: (Ptr[Self.sfRect[Float]], Float) => (tpl._1: Ptr[Self.sfRect[Float]])._3 = tpl._2
            case tpl: (Ptr[Self.sfRect[Int]], Int)     => (tpl._1: Ptr[Self.sfRect[Int]])._3 = tpl._2
            case _                                     => error("This expression must be inside an inline function")

    transparent inline def height: Float | Int =
        inline _rect.ptr match
            case ptr: Ptr[Self.sfRect[Float]] => (ptr: Ptr[Self.sfRect[Float]])._4
            case ptr: Ptr[Self.sfRect[Int]]   => (ptr: Ptr[Self.sfRect[Int]])._4
            case _                            => error("This expression must be inside an inline function")

    inline def height_=(height: T): Unit =
        inline (_rect.ptr, height) match
            case tpl: (Ptr[Self.sfRect[Float]], Float) => (tpl._1: Ptr[Self.sfRect[Float]])._4 = tpl._2
            case tpl: (Ptr[Self.sfRect[Int]], Int)     => (tpl._1: Ptr[Self.sfRect[Int]])._4 = tpl._2
            case _                                     => error("This expression must be inside an inline function")

    inline def contains(x: T, y: T): Boolean =
        inline (_rect.ptr, x, y) match
            case tpl: (Ptr[Self.sfRect[Float]], Float, Float) => Self.Float.contains(tpl._1, tpl._2, tpl._3)
            case tpl: (Ptr[Self.sfRect[Int]], Int, Int)       => Self.Int.contains(tpl._1, tpl._2, tpl._3)
            case _                                            => error("This expression must be inside an inline function")

    inline def contains(point: Vector2[T]): Boolean =
        inline (_rect.ptr, point._vector2.ptr) match
            case tpl: (Ptr[Self.sfRect[Float]], Ptr[sfVector2[Float]]) => Self.Float.contains(tpl._1, tpl._2)
            case tpl: (Ptr[Self.sfRect[Int]], Ptr[sfVector2[Int]])     => Self.Int.contains(tpl._1, tpl._2)
            case _                                                     => error("This expression must be inside an inline function")

    transparent inline def intersects(rectangle: Immutable[Rect[T]]): Option[Rect[Float]] | Option[Rect[Int]] =
        inline (_rect.ptr, rectangle.get._rect.ptr) match
            case tpl: (Ptr[Self.sfRect[Float]], Ptr[Self.sfRect[Float]]) =>
                val intersection = Rect[Float]()

                Option.when(Self.Float.intersects(tpl._1, tpl._2, intersection._rect.ptr))(intersection)

            case tpl: (Ptr[Self.sfRect[Int]], Ptr[Self.sfRect[Int]]) =>
                val intersection = Rect[Int]()

                Option.when(Self.Int.intersects(tpl._1, tpl._2, intersection._rect.ptr))(intersection)

            case _ => error("This expression must be inside an inline function")

    inline def ==(rhs: Immutable[Rect[T]]): Boolean =
        inline (_rect.ptr, rhs.get._rect.ptr) match
            case tpl: (Ptr[Self.sfRect[Float]], Ptr[Self.sfRect[Float]]) => Self.Float.eq_sfFloatRect(tpl._1, tpl._2)
            case tpl: (Ptr[Self.sfRect[Int]], Ptr[Self.sfRect[Int]])     => Self.Int.eq_sfIntRect(tpl._1, tpl._2)
            case _ => error("This expression must be inside an inline function")

    inline def !=(rhs: Immutable[Rect[T]]): Boolean =
        inline (_rect.ptr, rhs.get._rect.ptr) match
            case tpl: (Ptr[Self.sfRect[Float]], Ptr[Self.sfRect[Float]]) => Self.Float.ne_sfFloatRect(tpl._1, tpl._2)
            case tpl: (Ptr[Self.sfRect[Int]], Ptr[Self.sfRect[Int]])     => Self.Int.ne_sfIntRect(tpl._1, tpl._2)
            case _ => error("This expression must be inside an inline function")

    /* Scala operators */

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: Rect[?] =>
                ctor match
                    case Rect.Type.Float(_) =>
                        rhs.ctor match
                            case Rect.Type.Float(_) =>
                                this.asInstanceOf[Rect[Float]] == Immutable(rhs.asInstanceOf[Rect[Float]])
                            case _ => false

                    case Rect.Type.Int(_) =>
                        rhs.ctor match
                            case Rect.Type.Int(_) =>
                                this.asInstanceOf[Rect[Int]] == Immutable(rhs.asInstanceOf[Rect[Int]])
                            case _ => false

            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def toString(): java.lang.String =
        ctor match
            case Rect.Type.Float(_) =>
                val r = this.asInstanceOf[Rect[Float]]
                s"Rect(${r.left}, ${r.top}, ${r.width}, ${r.height})"
            case Rect.Type.Int(_) =>
                val r = this.asInstanceOf[Rect[Int]]
                s"Rect(${r.left}, ${r.top}, ${r.width}, ${r.height})"

object Rect:

    private[sfml] inline given rectFloatToSfRectFloat: Conversion[Rect[Float], Ptr[Self.sfRect[Float]]]:
        override def apply(rect: Rect[Float]) = rect._rect.ptr

    private[sfml] inline given rectIntToSfRectInt: Conversion[Rect[Int], Ptr[Self.sfRect[Int]]]:
        override def apply(rect: Rect[Int]) = rect._rect.ptr

    /* Constructors */

    private[sfml] enum Type:
        case Float(val ctor: Constructor[Self.sfRect[scala.Float]])
        case Int(val ctor: Constructor[Self.sfRect[scala.Int]])

    private[sfml] type ctor = Type

    object ctor:

        inline def apply[T <: Float | Int](rectLeft: T, rectTop: T, rectWidth: T, rectHeight: T): ctor =
            inline (rectLeft, rectTop, rectWidth, rectHeight) match
                case (rectLeft: Float, rectTop: Float, rectWidth: Float, rectHeight: Float) =>
                    Type.Float(Constructor(Self.Float.ctor(_, rectLeft, rectTop, rectWidth, rectHeight)))

                case (rectLeft: Int, rectTop: Int, rectWidth: Int, rectHeight: Int) =>
                    Type.Int(Constructor(Self.Int.ctor(_, rectLeft, rectTop, rectWidth, rectHeight)))

                case _ => error("This expression must be inside an inline function")

        inline def apply[T <: Float | Int](position: Vector2[T], size: Vector2[T]): ctor =
            inline (position, size) match
                case (position: Vector2[Float], size: Vector2[Float]) =>
                    Type.Float(Constructor(Self.Float.ctor(_, position._vector2.ptr, size._vector2.ptr)))

                case (position: Vector2[Int], size: Vector2[Int]) =>
                    Type.Int(Constructor(Self.Int.ctor(_, position._vector2.ptr, size._vector2.ptr)))

                case _ => error("This expression must be inside an inline function")

        inline def apply[T <: Float | Int]()(using ev: Numeric[T]): ctor =
            Rect.ctor(ev.zero, ev.zero, ev.zero, ev.zero)

    @nowarn
    inline def apply[T <: Float | Int](rectLeft: T, rectTop: T, rectWidth: T, rectHeight: T): Rect[T] =
        new Object with Rect[T](using ctor(rectLeft, rectTop, rectWidth, rectHeight))

    @nowarn
    inline def apply[T <: Float | Int](position: Vector2[T], size: Vector2[T]) =
        new Object with Rect[T](using ctor(position, size))

    @nowarn
    inline def apply[T <: Float | Int]()(using Numeric[T]) =
        new Object with Rect[T](using ctor())

    @targetName("applyFloat")
    private[sfml] def apply(f: Ptr[Self.sfRect[Float]] => Unit): Rect[Float] =
        new Object with Rect[Float](using Type.Float(Constructor(f)))

    @targetName("applyInt")
    private[sfml] def apply(f: Ptr[Self.sfRect[Int]] => Unit): Rect[Int] =
        new Object with Rect[Int](using Type.Int(Constructor(f)))

    transparent inline def unapply[T <: Float | Int](rect: Rect[T]): (Float, Float, Float, Float) | (Int, Int, Int, Int) =
        inline rect._rect.ptr match
            case ptr: Ptr[Self.sfRect[Float]] =>
                val rect = ptr: Ptr[Self.sfRect[Float]]
                (rect._1, rect._2, rect._3, rect._4)

            case ptr: Ptr[Self.sfRect[Int]] =>
                val rect = ptr: Ptr[Self.sfRect[Int]]
                (rect._1, rect._2, rect._3, rect._4)

            case _ => error("This expression must be inside an inline function")

    inline given rectFloatToRectInt: Conversion[Rect[Float], Rect[Int]]:
        override def apply(rect: Rect[Float]) = Rect(rect.left.toInt, rect.top.toInt, rect.width.toInt, rect.height.toInt)

    inline given rectIntToRectFloat: Conversion[Rect[Int], Rect[Float]]:
        override def apply(rect: Rect[Int]) = Rect(rect.left.toFloat, rect.top.toFloat, rect.width.toFloat, rect.height.toFloat)

    inline given tupleToRectFloat: [T: Numeric] => Conversion[(T, T, T, T), Rect[Float]]:
        override def apply(tuple: (T, T, T, T)) = Rect(tuple._1.toFloat, tuple._2.toFloat, tuple._3.toFloat, tuple._4.toFloat)

    inline given tupleToRectInt: [T: Numeric] => Conversion[(T, T, T, T), Rect[Int]]:
        override def apply(tuple: (T, T, T, T)) = Rect(tuple._1.toInt, tuple._2.toInt, tuple._3.toInt, tuple._4.toInt)

    inline given tupleToImmutableRectFloat: [T: Numeric] => Conversion[(T, T, T, T), Immutable[Rect[Float]]]:
        override def apply(tuple: (T, T, T, T)) = Rect(tuple._1.toFloat, tuple._2.toFloat, tuple._3.toFloat, tuple._4.toFloat)

    inline given tupleToImmutableRectInt: [T: Numeric] => Conversion[(T, T, T, T), Immutable[Rect[Int]]]:
        override def apply(tuple: (T, T, T, T)) = Rect(tuple._1.toInt, tuple._2.toInt, tuple._3.toInt, tuple._4.toInt)

    extension (rect: Ptr[Self.sfRect[Float]])
        private[sfml] def toRectFloat(): Rect[Float] =
            Rect(rect._1, rect._2, rect._3, rect._4)

    extension (rect: Ptr[Self.sfRect[Int]])
        private[sfml] def toRectInt(): Rect[Int] =
            Rect(rect._1, rect._2, rect._3, rect._4)

    /* Immutable */

    extension [T <: Float | Int](rect: Immutable[Rect[T]])

        transparent inline def left: Float | Int = rect.get.left

        transparent inline def top: Float | Int = rect.get.top

        transparent inline def width: Float | Int = rect.get.width

        transparent inline def height: Float | Int = rect.get.height

        inline def contains(x: T, y: T): Boolean =
            rect.get.contains(x, y)

        inline def contains(point: Vector2[T]): Boolean =
            rect.get.contains(point)

        inline def intersects(rectangle: Rect[T]): Option[Rect[Float]] | Option[Rect[Int]] =
            rect.get.intersects(rectangle)

    /* Return by value handlers */

    private[sfml] def toRectFloat(
        callback: CFuncPtr0[Self.sfRect[Float]]
    )(): Rect[Float] =
        ReturnTypeHandler(Self.Float.typeHandler, callback)().toRectFloat()

    private[sfml] def toRectFloat[T1](
        callback: CFuncPtr1[Ptr[T1], Self.sfRect[Float]]
    )(arg1: Ptr[T1]): Rect[Float] =
        ReturnTypeHandler(Self.Float.typeHandler, callback)(arg1).toRectFloat()

    private[sfml] def toRectFloat[T1: Tag, T2: Tag](
        callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], Self.sfRect[Float]]
    )(arg1: T1, arg2: T2): Rect[Float] =
        ReturnTypeHandler(Self.Float.typeHandler, callback)(arg1, arg2).toRectFloat()

    private[sfml] def toRectFloat[T1: Tag, T2: Tag, T3: Tag](
        callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], Self.sfRect[Float]]
    )(arg1: T1, arg2: T2, arg3: T3): Rect[Float] =
        ReturnTypeHandler(Self.Float.typeHandler, callback)(arg1, arg2, arg3).toRectFloat()

    private[sfml] def toRectInt(
        callback: CFuncPtr0[Self.sfRect[Int]]
    )(): Rect[Int] =
        ReturnTypeHandler(Self.Int.typeHandler, callback)().toRectInt()

    private[sfml] def toRectInt[T1](
        callback: CFuncPtr1[Ptr[T1], Self.sfRect[Int]]
    )(arg1: Ptr[T1]): Rect[Int] =
        ReturnTypeHandler(Self.Int.typeHandler, callback)(arg1).toRectInt()

    private[sfml] def toRectInt[T1: Tag, T2: Tag](
        callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], Self.sfRect[Int]]
    )(arg1: T1, arg2: T2): Rect[Int] =
        ReturnTypeHandler(Self.Int.typeHandler, callback)(arg1, arg2).toRectInt()

    private[sfml] def toRectInt[T1: Tag, T2: Tag, T3: Tag](
        callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], Self.sfRect[Int]]
    )(arg1: T1, arg2: T2, arg3: T3): Rect[Int] =
        ReturnTypeHandler(Self.Int.typeHandler, callback)(arg1, arg2, arg3).toRectInt()
