package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.Type.{booleanToSfBool, sfBoolToBoolean}
import sfml.internal.graphics.RenderTexture as Self

import sfml.system.Vector2
import sfml.window.ContextSettings

trait RenderTexture extends RenderTarget:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _renderTexture: ResourcePtr[Self.sfRenderTexture] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    private[sfml] final override lazy val _renderTarget = _renderTexture.get(_.at1)

    /* Methods */

    def create(width: Int, height: Int, depthBuffer: Boolean): Boolean =
        Self.create(_renderTexture.ptr, width.toUInt, height.toUInt, depthBuffer)

    def create(width: Int, height: Int, settings: ContextSettings = ContextSettings()): Boolean =
        Self.create(_renderTexture.ptr, width.toUInt, height.toUInt, settings)

    def display(): Unit =
        Self.display(_renderTexture.ptr)

    def texture: Immutable[Texture] =
        Immutable(Self.getTexture(_renderTexture.ptr))

    override def size: Vector2[Int] =
        Vector2.toVector2Int[Self.sfRenderTexture]((data: Ptr[Self.sfRenderTexture]) => {
            Self.getSize(data)
        })(_renderTexture.ptr)

object RenderTexture:

    private[sfml] inline given Conversion[RenderTexture, Ptr[Self.sfRenderTexture]]:
        override def apply(renderTexture: RenderTexture) = renderTexture._renderTexture.ptr

    /* Constructors */

    def apply(): RenderTexture =
        new Object with RenderTexture

    /* Immutable */

    extension (renderTexture: Immutable[RenderTexture])

        def texture: Immutable[Texture] = renderTexture.get.texture

        def size: Vector2[Int] = renderTexture.get.size
