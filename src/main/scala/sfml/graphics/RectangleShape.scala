package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichLong

import sfml.internal.graphics.RectangleShape as Self

import sfml.system.Vector2

/** Specialized shape representing a rectangle.
  *
  * This class inherits all the functions of [[sfml.graphics.Transformable Transformable]] (position, rotation, scale, bounds, ...)
  * as well as the functions of [[sfml.graphics.Shape Shape]] (outline, color, texture, ...).
  *
  * ```scala
  * //{
  * val window: RenderWindow = ???
  *
  * //}
  * val rectangle = RectangleShape()
  * rectangle.size = (100, 50)
  * rectangle.outlineColor = Color.Red()
  * rectangle.outlineThickness = 5
  * rectangle.position = (10, 20)
  *
  * window.draw(rectangle)
  * ```
  *
  * @see
  *   [[sfml.graphics.Shape Shape]], [[sfml.graphics.CircleShape CircleShape]] <!-- TODO: ConvexShape -->
  *
  * @doxygenId
  *   classsf_1_1RectangleShape
  *
  * @doxygenHash
  *   68b126dd4ebd663af129af6e7728ca1e
  */
trait RectangleShape(using private[sfml] ctor: RectangleShape.ctor) extends Shape:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _rectangleShape: ResourcePtr[Self.sfRectangleShape] =
        ResourceBuffer(Self.dtor)(ctor)

    private[sfml] final override lazy val _shape = _rectangleShape.get(_.at1)

    /* Methods */

    override def point(index: Long): Vector2[Float] =
        Vector2.toVector2Float((data: Ptr[CStruct2[Ptr[Self.sfRectangleShape], CSize]]) => Self.getPoint(data._1, data._2))(
            _rectangleShape.ptr,
            index.toCSize
        )

    override def pointCount: Long =
        Self.getPointCount(_rectangleShape.ptr).toLong

    /** Get the size of the rectangle.
      *
      * @return
      *   Size of the rectangle
      *
      * @see
      *   [[sfml.graphics.RectangleShape.size_= size_=]]
      *
      * @doxygenId
      *   classsf_1_1RectangleShape_1af6819a7b842b83863f21e7a9c63097e7
      */
    def size: Vector2[Float] =
        Vector2.toVector2Float(Self.getSize(_rectangleShape.ptr))()

    /** Set the size of the rectangle.
      *
      * @param size
      *   New size of the rectangle
      *
      * @see
      *   [[sfml.graphics.RectangleShape.size size]]
      *
      * @doxygenId
      *   classsf_1_1RectangleShape_1a5c65d374d4a259dfdc24efdd24a5dbec
      */
    def size_=(size: Vector2[Float]): Unit =
        Self.setSize(_rectangleShape.ptr, size)

object RectangleShape:

    private[sfml] inline given Conversion[RectangleShape, Ptr[Self.sfRectangleShape]]:
        override def apply(rectangleShape: RectangleShape) = rectangleShape._rectangleShape.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfRectangleShape]

    object ctor:

        /** Default constructor.
          *
          * @param size
          *   Size of the rectangle
          *
          * @doxygenId
          *   classsf_1_1RectangleShape_1a83a2be157ebee85c95ed491c3e78dd7c
          */
        def apply(size: Vector2[Float] = Vector2[Float]()): ctor =
            Constructor(Self.ctor(_, size))

    /** Default constructor.
      *
      * @param size
      *   Size of the rectangle
      *
      * @doxygenId
      *   classsf_1_1RectangleShape_1a83a2be157ebee85c95ed491c3e78dd7c
      */
    def apply(size: Vector2[Float] = Vector2[Float]()) =
        new Object with RectangleShape(using ctor(size))
