package sfml
package graphics

import scala.compiletime.error

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.graphics.Shader as Self

import sfml.system.{Vector2, Vector3}

trait Shader:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _shader: ResourcePtr[Self.sfShader] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    /* Methods */

    def loadFromFile(filename: stdlib.String, shaderType: ShaderType): Boolean =
        Self.loadFromFile(_shader.ptr, filename, shaderType.ordinal.toUInt)

    inline def uniform_=[T <: Float | Int](name: stdlib.String, vector: Vector2[T]): Unit =
        inline vector match
            case vector: Vector2[Float] =>
                Self.setUniform_sfVector2f(_shader.ptr, name, vector)

            case vector: Vector2[Int] =>
                Self.setUniform_sfVector2i(_shader.ptr, name, vector)

            case _ => error("This expression must be inside an inline function")

    inline def uniform_=[T <: Float | Int](name: stdlib.String, vector: Vector3[T]): Unit =
        inline vector match
            case vector: Vector3[Float] =>
                Self.setUniform_sfVector3f(_shader.ptr, name, vector)

            case vector: Vector3[Int] =>
                Self.setUniform_sfVector3i(_shader.ptr, name, vector)

            case _ => error("This expression must be inside an inline function")

object Shader:

    private[sfml] inline given Conversion[Shader, Ptr[Self.sfShader]]:
        override def apply(shader: Shader) = shader._shader.ptr

    inline given Conversion[Shader, RenderStates]:
        override def apply(shader: Shader) = RenderStates(Option(shader))

    /* Constructors */

    def apply(): Shader =
        new Object with Shader

    private[sfml] def apply(shader: ResourcePtr[Self.sfShader]): Shader =
        new Object with Shader {
            private[sfml] override lazy val _shader = shader
        }
