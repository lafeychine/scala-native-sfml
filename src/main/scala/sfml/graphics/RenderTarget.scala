package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsafe.CFuncPtr1.fromScalaFunction

import sfml.internal.graphics.RenderTarget as Self
import sfml.internal.graphics.View.sfView
import sfml.internal.system.Vector2.sfVector2

import sfml.system.Vector2

/** Base class for all render targets (window, texture, ...)
  *
  * [[sfml.graphics.RenderTarget RenderTarget]] defines the common behavior of all the 2D render targets usable in the graphics
  * module.
  *
  * It makes it possible to draw 2D entities like sprites, shapes, text without using any OpenGL command directly.
  *
  * A [[sfml.graphics.RenderTarget RenderTarget]] is also able to use views ([[sfml.graphics.View View]]), which are a kind of 2D
  * cameras. With views you can globally scroll, rotate or zoom everything that is drawn, without having to transform every single
  * entity. See the documentation of [[sfml.graphics.View View]] for more details and sample pieces of code about this class.
  *
  * On top of that, render targets are still able to render direct OpenGL stuff. It is even possible to mix together OpenGL calls
  * and regular SFML drawing commands. When doing so, make sure that OpenGL states are not messed up by calling the
  * pushGLStates/popGLStates functions.
  *
  * @see
  *   [[sfml.graphics.RenderWindow RenderWindow]], [[sfml.graphics.RenderTexture RenderTexture]], [[sfml.graphics.View View]]
  *
  * @doxygenId
  *   classsf_1_1RenderTarget
  */
abstract trait RenderTarget:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _renderTarget: ResourcePtr[Self.sfRenderTarget] =
        AbstractResourceBuffer(RenderTarget.vtable, this)(Self.dtor)(Self.ctor(_))

    /* Methods */

    /** Clear the entire target with a single color.
      *
      * This function is usually called once every frame, to clear the previous contents of the target.
      *
      * @param color
      *   Fill color to use to clear the render target
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a6bb6f0ba348f2b1e2f46114aeaf60f26
      */
    def clear(color: Color = Color.Black()): Unit =
        Self.clear(_renderTarget.ptr, color)

    /** Get the default view of the render target.
      *
      * The default view has the initial size of the render target, and never changes after the target has been created.
      *
      * @return
      *   The default view of the render target
      *
      * @see
      *   [[sfml.graphics.RenderTarget.view_= view_=]], [[sfml.graphics.RenderTarget.view view]]
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a7741129e3ef7ab4f0a40024fca13480c
      */
    def defaultView: Immutable[View] =
        Immutable(View(Self.getDefaultView(_renderTarget.ptr)))

    /** Draw a drawable object to the render target.
      *
      * @param drawable
      *   Object to draw states Render states to use for drawing
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a12417a3bcc245c41d957b29583556f39
      */
    def draw(drawable: Immutable[Drawable], states: Immutable[RenderStates] = Immutable(RenderStates())): Unit =
        drawable.get.draw(this, states.get)

    /** Convert a point from target coordinates to world coordinates, using the current view.
      *
      * This function is an overload of the mapPixelToCoords function that implicitly uses the current view. It is equivalent to:
      * ```scala
      * //{
      * import sfml.system.Vector2
      *
      * val target: RenderTarget = ???
      * val point: Vector2[Int] = ???
      *
      * //}
      * target.mapPixelToCoords(point, target.view)
      * ```
      *
      * @param point
      *   Pixel to convert
      *
      * @return
      *   The converted point, in "world" coordinates
      *
      * <!-- TODO @see: mapCoordsToPixel -->
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a0103ebebafa43a97e6e6414f8560d5e3
      *
      * @doxygenHash
      *   0be01659667687b8d1161f011c1b71e1
      */
    def mapPixelToCoords(point: Immutable[Vector2[Int]]): Vector2[Float] =
        Vector2.toVector2Float((data: Ptr[CStruct2[Ptr[Self.sfRenderTarget], Ptr[sfVector2[Int]]]]) =>
            Self.mapPixelToCoords(data._1, data._2)
        )(this, point.get)

    /** Convert a point from target coordinates to world coordinates.
      *
      * This function finds the 2D position that matches the given pixel of the render target. In other words, it does the inverse
      * of what the graphics card does, to find the initial position of a rendered pixel.
      *
      * Initially, both coordinate systems (world units and target pixels) match perfectly. But if you define a custom view or
      * resize your render target, this assertion is not true anymore, i.e. a point located at (10, 50) in your render target may
      * map to the point (150, 75) in your 2D world – if the view is translated by (140, 25).
      *
      * For render-windows, this function is typically used to find which point (or object) is located below the mouse cursor.
      *
      * This version uses a custom view for calculations, see the other overload of the function if you want to use the current view
      * of the render target.
      *
      * @param point
      *   Pixel to convert
      *
      * @param view
      *   The view to use for converting the point
      *
      * @return
      *   The converted point, in "world" units
      *
      * <!-- TODO @see: mapCoordsToPixel -->
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a2d3e9d7c4a1f5ea7e52b06f53e3011f9
      *
      * @doxygenHash
      *   62d7c644b023c5f78e3f149556c2ad9d
      */
    def mapPixelToCoords(point: Immutable[Vector2[Int]], view: Immutable[View]): Vector2[Float] =
        Vector2.toVector2Float((data: Ptr[CStruct3[Ptr[Self.sfRenderTarget], Ptr[sfVector2[Int]], Ptr[sfView]]]) =>
            Self.mapPixelToCoords(data._1, data._2, data._3)
        )(this, point.get, view.get)

    /** Get the view currently in use in the render target.
      *
      * @return
      *   The view object that is currently used
      *
      * @see
      *   [[sfml.graphics.RenderTarget.view_= view_=]], [[sfml.graphics.RenderTarget.defaultView defaultView]]
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1adbf8dc5a1f4abbe15a3fbb915844c7ea
      */
    def view: Immutable[View] =
        Immutable(View(Self.getView(_renderTarget.ptr)))

    /** Change the current active view.
      *
      * The view is like a 2D camera, it controls which part of the 2D scene is visible, and how it is viewed in the render target.
      * The new view will affect everything that is drawn, until another view is set. The render target keeps its own copy of the
      * view object, so it is not necessary to keep the original one alive after calling this function. To restore the original view
      * of the target, you can pass the result of [[sfml.graphics.RenderTarget.defaultView defaultView]] to this function.
      *
      * @param view
      *   New view to use
      *
      * @see
      *   [[sfml.graphics.RenderTarget.view view]], [[sfml.graphics.RenderTarget.defaultView defaultView]]
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a063db6dd0a14913504af30e50cb6d946
      */
    def view_=(view: Immutable[View]): Unit =
        Self.setView(_renderTarget.ptr, view.get)

    /** Get the viewport of a view, applied to this render target.
      *
      * The viewport is defined in the view as a ratio, this function simply applies this ratio to the current dimensions of the
      * render target to calculate the pixels rectangle that the viewport actually covers in the target.
      *
      * @param view
      *   The view for which we want to compute the viewport
      *
      * @return
      *   Viewport rectangle, expressed in pixels
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a865d462915dc2a1fae2ebfb3300382ac
      */
    def viewport(view: Immutable[View]): Rect[Int] =
        Rect.toRectInt((data: Ptr[CStruct2[Ptr[Self.sfRenderTarget], Ptr[sfView]]]) => Self.getViewport(data._1, data._2))(
            this,
            view.get
        )

    /** Return the size of the rendering region of the target.
      *
      * @return
      *   Size in pixels
      *
      * @doxygenId
      *   classsf_1_1RenderTarget_1a2e5ade2457d9fb4c4907ae5b3d9e94a5
      */
    def size: Vector2[Int]

object RenderTarget:

    private[sfml] inline given Conversion[RenderTarget, Ptr[Self.sfRenderTarget]]:
        override def apply(renderTarget: RenderTarget) = renderTarget._renderTarget.ptr

    private[sfml] def patch_draw(self: Ptr[Byte], target: RenderTarget, states: RenderStates): Unit =
        // NOTE: Use this endpoint to avoid us splitting `states` in the stack
        Self.draw(target, self, states)

    /* Constructors */

    private[sfml] val vtable = VTable(
        CFuncPtr.toPtr((ptr: Ptr[Wrapped[RenderTarget]]) => {
            Vector2.toInlineVector2i(ptr.get.size)
        })
    )

    /* Immutable */

    extension (renderTarget: Immutable[RenderTarget])

        def defaultView: Immutable[View] = renderTarget.get.defaultView

        def mapPixelToCoords(point: Immutable[Vector2[Int]]): Vector2[Float] =
            renderTarget.get.mapPixelToCoords(point)

        def mapPixelToCoords(point: Immutable[Vector2[Int]], view: View): Vector2[Float] =
            renderTarget.get.mapPixelToCoords(point, view)

        def view: Immutable[View] = renderTarget.get.view

        def viewport(view: Immutable[View]): Rect[Int] =
            renderTarget.get.viewport(view)
