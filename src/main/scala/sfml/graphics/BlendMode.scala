package sfml
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.graphics.BlendMode as Self

/** Blending modes for drawing.
  *
  * [[sfml.graphics.BlendMode BlendMode]] is a class that represents a blend mode.
  *
  * A blend mode determines how the colors of an object you draw are mixed with the colors that are already in the buffer.
  *
  * The class is composed of 6 components, each of which has its own public member variable:
  *   - Color Source Factor (`colorSrcFactor`)
  *   - Color Destination Factor (`colorDstFactor`)
  *   - Color Blend Equation (`colorEquation`)
  *   - Alpha Source Factor (`alphaSrcFactor`)
  *   - Alpha Destination Factor (`alphaDstFactor`)
  *   - Alpha Blend Equation (`alphaEquation`)
  *
  * The source factor specifies how the pixel you are drawing contributes to the final color. The destination factor specifies how
  * the pixel already drawn in the buffer contributes to the final color.
  *
  * The color channels RGB (red, green, blue; simply referred to as color) and A (alpha; the transparency) can be treated
  * separately. This separation can be useful for specific blend modes, but most often you won't need it and will simply treat the
  * color as a single unit.
  *
  * The blend factors and equations correspond to their OpenGL equivalents. In general, the color of the resulting pixel is
  * calculated according to the following formula (`src` is the color of the source pixel, `dst` the color of the destination pixel,
  * the other variables correspond to the public members, with the equations being + or - operators):
  * ```scala sc:nocompile
  * dst.rgb = colorSrcFactor * src.rgb (colorEquation) colorDstFactor * dst.rgb
  * dst.a   = alphaSrcFactor * src.a   (alphaEquation) alphaDstFactor * dst.a
  * ```
  *
  * All factors and colors are represented as floating point numbers between 0 and 1. Where necessary, the result is clamped to fit
  * in that range.
  *
  * The most common blending modes are defined as constants:
  * ```scala
  * val alphaBlending = BlendMode.Alpha()
  * val additiveBlending = BlendMode.Add()
  * val multiplicativeBlending = BlendMode.Multiply()
  * val noBlending = BlendMode.None()
  * ```
  *
  * In SFML, a blend mode can be specified every time you draw a [[sfml.graphics.Drawable Drawable]] object to a render target. It
  * is part of the [[sfml.graphics.RenderStates RenderStates]] compound that is passed to the member function
  * [[sfml.graphics.RenderTarget.draw RenderTarget.draw()]].
  *
  * @see
  *   [[sfml.graphics.RenderStates RenderStates]], [[sfml.graphics.RenderTarget RenderTarget]]
  *
  * @param colorSrcFactor
  *   Source blending factor for the color channels.
  *
  * @param colorDstFactor
  *   Destination blending factor for the color channels.
  *
  * @param colorEquation
  *   Blending equation for the color channels.
  *
  * @param alphaSrcFactor
  *   Source blending factor for the alpha channel.
  *
  * @param alphaDstFactor
  *   Destination blending factor for the alpha channel.
  *
  * @param alphaEquation
  *   Blending equation for the alpha channel.
  *
  * @doxygenId
  *   structsf_1_1BlendMode
  *
  * @doxygenHash
  *   46e3b088b323699799924b090d8fdbea
  */
trait BlendMode(using private[sfml] ctor: BlendMode.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _blendMode: ResourcePtr[Self.sfBlendMode] =
        ResourceBuffer(ctor)

    /* Methods */

    /** Source blending factor for the color channels.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1a32d1a55dbfada86a06d9b881dc8ccf7b
      */
    def colorSrcFactor: BlendMode.Factor =
        BlendMode.Factor.fromOrdinal(_blendMode.ptr._1)

    /** Destination blending factor for the color channels.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1adee68ee59e7f1bf71d12db03d251104d
      */
    def colorDstFactor: BlendMode.Factor =
        BlendMode.Factor.fromOrdinal(_blendMode.ptr._2)

    /** Blending equation for the color channels.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1aed12f06eb7f50a1b95b892b0964857b1
      */
    def colorEquation: BlendMode.Equation =
        BlendMode.Equation.fromOrdinal(_blendMode.ptr._3)

    /** Source blending factor for the alpha channel.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1aa94e44f8e1042a7357e8eff78c61a1be
      */
    def alphaSrcFactor: BlendMode.Factor =
        BlendMode.Factor.fromOrdinal(_blendMode.ptr._4)

    /** Destination blending factor for the alpha channel.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1aaf85b6b7943181cc81745569c4851e4e
      */
    def alphaDstFactor: BlendMode.Factor =
        BlendMode.Factor.fromOrdinal(_blendMode.ptr._5)

    /** Blending equation for the alpha channel.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1a68f5a305e0912946f39ba6c9265710c4
      */
    def alphaEquation: BlendMode.Equation =
        BlendMode.Equation.fromOrdinal(_blendMode.ptr._6)

object BlendMode:

    private[sfml] inline given Conversion[BlendMode, Ptr[Self.sfBlendMode]]:
        override def apply(blendMode: BlendMode) = blendMode._blendMode.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfBlendMode]

    object ctor:

        /** Default constructor.
          *
          * Constructs a blending mode that does alpha blending.
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1a7faef75eae1fb47bbe93f45f38e3d345
          */
        def apply(): ctor =
            Constructor(Self.ctor(_))

        /** Construct the blend mode given the factors and equation.
          *
          * This constructor uses the same factors and equation for both color and alpha components. It also defaults to the Add
          * equation.
          *
          * @param sourceFactor
          *   Specifies how to compute the source factor for the color and alpha channels.
          *
          * @param destinationFactor
          *   Specifies how to compute the destination factor for the color and alpha channels.
          *
          * @param blendEquation
          *   Specifies how to combine the source and destination colors and alpha.
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1a23c7452cc8e9eb943c3aea6234ce4297
          */
        def apply(
            sourceFactor: BlendMode.Factor,
            destinationFactor: BlendMode.Factor,
            blendEquation: BlendMode.Equation = BlendMode.Equation.Add
        ): ctor =
            Constructor(Self.ctor(_, sourceFactor.ordinal, destinationFactor.ordinal, blendEquation.ordinal))

        /** Construct the blend mode given the factors and equation.
          *
          * @param colorSourceFactor
          *   Specifies how to compute the source factor for the color channels.
          *
          * @param colorDestinationFactor
          *   Specifies how to compute the destination factor for the color channels.
          *
          * @param colorBlendEquation
          *   Specifies how to combine the source and destination colors.
          *
          * @param alphaSourceFactor
          *   Specifies how to compute the source factor.
          *
          * @param alphaDestinationFactor
          *   Specifies how to compute the destination factor.
          *
          * @param alphaBlendEquation
          *   Specifies how to combine the source and destination alphas.
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1a69a12c596114e77126616e7e0f7d798b
          */
        def apply(
            colorSrcFactor: BlendMode.Factor,
            colorDstFactor: BlendMode.Factor,
            colorEquation: BlendMode.Equation,
            alphaSrcFactor: BlendMode.Factor,
            alphaDstFactor: BlendMode.Factor,
            alphaEquation: BlendMode.Equation
        ): ctor =
            Constructor(
                Self.ctor(
                    _,
                    colorSrcFactor.ordinal,
                    colorDstFactor.ordinal,
                    colorEquation.ordinal,
                    alphaSrcFactor.ordinal,
                    alphaDstFactor.ordinal,
                    alphaEquation.ordinal
                )
            )

    /** Default constructor.
      *
      * Constructs a blending mode that does alpha blending.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1a7faef75eae1fb47bbe93f45f38e3d345
      */
    def apply(): BlendMode =
        new Object with BlendMode(using ctor())

    /** Construct the blend mode given the factors and equation.
      *
      * This constructor uses the same factors and equation for both color and alpha components. It also defaults to the Add
      * equation.
      *
      * @param sourceFactor
      *   Specifies how to compute the source factor for the color and alpha channels.
      *
      * @param destinationFactor
      *   Specifies how to compute the destination factor for the color and alpha channels.
      *
      * @param blendEquation
      *   Specifies how to combine the source and destination colors and alpha.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1a23c7452cc8e9eb943c3aea6234ce4297
      */
    def apply(
        sourceFactor: BlendMode.Factor,
        destinationFactor: BlendMode.Factor,
        blendEquation: BlendMode.Equation = BlendMode.Equation.Add
    ): BlendMode =
        new Object with BlendMode(using ctor(sourceFactor, destinationFactor, blendEquation))

    /** Construct the blend mode given the factors and equation.
      *
      * @param colorSourceFactor
      *   Specifies how to compute the source factor for the color channels.
      *
      * @param colorDestinationFactor
      *   Specifies how to compute the destination factor for the color channels.
      *
      * @param colorBlendEquation
      *   Specifies how to combine the source and destination colors.
      *
      * @param alphaSourceFactor
      *   Specifies how to compute the source factor.
      *
      * @param alphaDestinationFactor
      *   Specifies how to compute the destination factor.
      *
      * @param alphaBlendEquation
      *   Specifies how to combine the source and destination alphas.
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1a69a12c596114e77126616e7e0f7d798b
      */
    def apply(
        colorSrcFactor: BlendMode.Factor,
        colorDstFactor: BlendMode.Factor,
        colorEquation: BlendMode.Equation,
        alphaSrcFactor: BlendMode.Factor,
        alphaDstFactor: BlendMode.Factor,
        alphaEquation: BlendMode.Equation
    ): BlendMode =
        new Object
            with BlendMode(using ctor(colorSrcFactor, colorDstFactor, colorEquation, alphaSrcFactor, alphaDstFactor, alphaEquation))

    private[sfml] def apply(blendMode: ResourcePtr[Self.sfBlendMode]): BlendMode =
        new Object with BlendMode {
            private[sfml] override lazy val _blendMode = blendMode
        }

    /* Related enums */

    /** Enumeration of the blending factors.
      *
      * <!-- TODO: OpenGL equivalents -->
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbb
      *
      * @doxygenHash
      *   509cde17f6a3be1b528af0d2277dc4fc
      */
    enum Factor:
        /** (0, 0, 0, 0)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbafda2d66c3c3da15cd3b42338fbf6d2ba
          */
        case Zero

        /** (1, 1, 1, 1)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbaa2d3ba8b8bb2233c9d357cbb94bf4181
          */
        case One

        /** (src.r, src.g, src.b, src.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbad679bb0ecaf15c188d7f2e1fab572188
          */
        case SrcColor

        /** (1, 1, 1, 1) - (src.r, src.g, src.b, src.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbba5971ffdbca63382058ccba76bfce219e
          */
        case OneMinusSrcColor

        /** (dst.r, dst.g, dst.b, dst.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbba3d85281c3eab7153f2bd9faae3e7523a
          */
        case DstColor

        /** (1, 1, 1, 1) - (dst.r, dst.g, dst.b, dst.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbac8198db20d14506a841d1091ced1cae2
          */
        case OneMinusDstColor

        /** (src.a, src.a, src.a, src.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbaac0ae68df2930b4d616c3e7abeec7d41
          */
        case SrcAlpha

        /** (1, 1, 1, 1) - (src.a, src.a, src.a, src.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbaab57e8616bf4c21d8ee923178acdf2c8
          */
        case OneMinusSrcAlpha

        /** (dst.a, dst.a, dst.a, dst.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbba5e3dc9a6f117aaa5f7433e1f4662a5f7
          */
        case DstAlpha

        /** (1, 1, 1, 1) - (dst.a, dst.a, dst.a, dst.a)
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1afb9852caf356b53bb0de460c58a9ebbbab4e5c63f189f26075e5939ad1a2ce4e4
          */
        case OneMinusDstAlpha

    /** Enumeration of the blending equations.
      *
      * <!-- TODO: OpenGL equivalents -->
      *
      * @doxygenId
      *   structsf_1_1BlendMode_1a7bce470e2e384c4f9c8d9595faef7c32
      *
      * @doxygenHash
      *   3fa6413363a93a3e42c8fb3c7b293ed3
      */
    enum Equation:
        /** Pixel = Src * SrcFactor + Dst * DstFactor.
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1a7bce470e2e384c4f9c8d9595faef7c32a50c081d8f36cf7b77632966e15d38966
          */
        case Add

        /** Pixel = Src * SrcFactor - Dst * DstFactor.
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1a7bce470e2e384c4f9c8d9595faef7c32a14c825be24f8412fc5ed5b49f19bc0d0
          */
        case Subtract

        /** Pixel = Dst * DstFactor - Src * SrcFactor.
          *
          * @doxygenId
          *   structsf_1_1BlendMode_1a7bce470e2e384c4f9c8d9595faef7c32a2d04acf59e91811128e7d0ef076f65f0
          */
        case ReverseSubtract

    /* Static methods */

    def Alpha(): BlendMode =
        BlendMode(Factor.SrcAlpha, Factor.OneMinusSrcAlpha, Equation.Add, Factor.One, Factor.OneMinusSrcAlpha, Equation.Add)

    def Add(): BlendMode =
        BlendMode(Factor.SrcAlpha, Factor.One, Equation.Add, Factor.One, Factor.One, Equation.Add)

    def Multiply(): BlendMode =
        BlendMode(Factor.DstColor, Factor.Zero)

    def None(): BlendMode =
        BlendMode(Factor.One, Factor.Zero)
