package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.graphics.Text as Self

import sfml.system.String

/** Graphical text that can be drawn to a render target.
  *
  * [[sfml.graphics.Text Text]] is a drawable class that allows to easily display some text with custom style and color on a render
  * target.
  *
  * It inherits all the functions from [[sfml.graphics.Transformable Transformable]]: position, rotation, scale, origin. It also
  * adds text-specific properties such as the font to use, the character size, the font style (bold, italic, underlined and strike
  * through), the text color, the outline thickness, the outline color, the character spacing, the line spacing and the text to
  * display of course. It also provides convenience functions to calculate the graphical size of the text, or to get the global
  * position of a given character.
  *
  * [[sfml.graphics.Text Text]] works in combination with the [[sfml.graphics.Font Font]] class, which loads and provides the glyphs
  * (visual characters) of a given font.
  *
  * The separation of [[sfml.graphics.Font Font]] and [[sfml.graphics.Text Text]] allows more flexibility and better performances:
  * indeed a [[sfml.graphics.Font Font]] is a heavy resource, and any operation on it is slow (often too slow for real-time
  * applications). On the other side, a [[sfml.graphics.Text Text]] is a lightweight object which can combine the glyphs data and
  * metrics of a [[sfml.graphics.Font Font]] to display any text on a render target.
  *
  * It is important to note that the [[sfml.graphics.Text Text]] instance doesn't copy the font that it uses, it only keeps a
  * reference to it. Thus, a [[sfml.graphics.Font Font]] must not be destructed while it is used by a [[sfml.graphics.Text Text]]
  * (i.e. never write a function that uses a local [[sfml.graphics.Font Font]] instance for creating a text).
  *
  * See also the note on coordinates and undistorted rendering in [[sfml.graphics.Transformable Transformable]].
  *
  * ```scala
  * //{
  * val window: RenderWindow = ???
  *
  * //}
  * // Declare and load a font
  * val font = Font()
  * font.loadFromFile("arial.ttf")
  *
  * // Create a text
  * val text = Text("hello", font)
  * text.characterSize = 30;
  * <!-- TODO Text.Style -->
  * text.fillColor = Color.Red()
  *
  * // Draw it
  * window.draw(text)
  * ```
  *
  * @see
  *   [[sfml.graphics.Font Font]], [[sfml.graphics.Transformable Transformable]]
  *
  * @doxygenId
  *   classsf_1_1Text
  *
  * @doxygenHash
  *   29b0460783b3769bccdcae263d5c892c
  */
trait Text(using private[sfml] ctor: Text.ctor) extends Transformable with Drawable:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _text: ResourcePtr[Self.sfText] =
        ResourceBuffer(Text.dtor)(ctor)

    private[sfml] override lazy val _transformable = _text.get(_.at2)

    /* Methods */

    override def draw(target: RenderTarget, states: RenderStates): Unit =
        RenderTarget.patch_draw(_text.ptr.asInstanceOf[Ptr[Byte]], target, states)

    /** Get the global bounding rectangle of the entity.
      *
      * The returned rectangle is in global coordinates, which means that it takes into account the transformations (translation,
      * rotation, scale, ...) that are applied to the entity. In other words, this function returns the bounds of the text in the
      * global 2D world's coordinate system.
      *
      * @return
      *   Global bounding rectangle of the entity
      *
      * @doxygenId
      *   classsf_1_1Text_1ad33ed96ce9fbe99610f7f8b6874a16b4
      */
    def globalBounds: Rect[Float] =
        Rect.toRectFloat((data: Ptr[Self.sfText]) => Self.getGlobalBounds(data))(_text.ptr)

    /** Get the local bounding rectangle of the entity.
      *
      * The returned rectangle is in local coordinates, which means that it ignores the transformations (translation, rotation,
      * scale, ...) that are applied to the entity. In other words, this function returns the bounds of the entity in the entity's
      * coordinate system.
      *
      * @return
      *   Local bounding rectangle of the entity
      *
      * @doxygenId
      *   classsf_1_1Text_1a3e6b3b298827f853b41165eee2cbbc66
      */
    def localBounds: Rect[Float] =
        Self.ensureGeometryUpdate(_text.ptr)

        Rect.toRectFloat(_text.ptr.at7)()

    /* Getter / Setter */

    /** Get the character size.
      *
      * @return
      *   Size of the characters, in pixels
      *
      * @see
      *   [[sfml.graphics.Text.characterSize_= characterSize_=]]
      *
      * @doxygenId
      *   classsf_1_1Text_1a46d1d7f1d513bb8d434e985a93ea5224
      */
    def characterSize: Int =
        Self.getCharacterSize(_text.ptr).toInt

    /** Set the character size.
      *
      * The default size is 30.
      *
      * Note that if the used font is a bitmap font, it is not scalable, thus not all requested sizes will be available to use. This
      * needs to be taken into consideration when setting the character size. If you need to display text of a certain size, make
      * sure the corresponding bitmap font that supports that size is used.
      *
      * @param size
      *   New character size, in pixels
      *
      * @see
      *   [[sfml.graphics.Text.characterSize characterSize]]
      *
      * @doxygenId
      *   classsf_1_1Text_1ae96f835fc1bff858f8a23c5b01eaaf7e
      */
    def characterSize_=(size: Int) =
        Self.setCharacterSize(_text.ptr, size.toUInt)

    /** Get the fill color of the text.
      *
      * @return
      *   Fill color of the text
      *
      * @see
      *   [[sfml.graphics.Text.fillColor_= fillColor_=]]
      *
      * @deprecated
      *   There is now fill and outline colors instead of a single global color. Use [[sfml.graphics.Text.fillColor fillColor]] or
      *   [[sfml.graphics.Text.outlineColor outlineColor]] instead.
      *
      * @doxygenId
      *   classsf_1_1Text_1a810cc4aa8eb5998f9eb9d02b1c099660
      */
    def color: Color =
        Self.getColor(_text.ptr)

    /** Set the fill color of the text.
      *
      * By default, the text's fill color is opaque white. Setting the fill color to a transparent color with an outline will cause
      * the outline to be displayed in the fill area of the text.
      *
      * @param color
      *   New fill color of the text
      *
      * @see
      *   [[sfml.graphics.Text.fillColor fillColor]]
      *
      * @deprecated
      *   There is now fill and outline colors instead of a single global color. Use [[sfml.graphics.Text.fillColor_= fillColor_=]]
      *   or [[sfml.graphics.Text.outlineColor_= outlineColor_=]] instead.
      *
      * @doxygenId
      *   classsf_1_1Text_1a6ce65272d6d63ed01118366e92c68132
      */
    def color_=(color: Color) =
        Self.setColor(_text.ptr, color)

    /** Get the fill color of the text.
      *
      * @return
      *   Fill color of the text
      *
      * @see
      *   [[sfml.graphics.Text.fillColor_= fillColor_=]]
      *
      * @doxygenId
      *   classsf_1_1Text_1a10400757492ec7fa97454488314ca39b
      */
    def fillColor: Color =
        Self.getFillColor(_text.ptr)

    /** Set the fill color of the text.
      *
      * By default, the text's fill color is opaque white. Setting the fill color to a transparent color with an outline will cause
      * the outline to be displayed in the fill area of the text.
      *
      * @param color
      *   New fill color of the text
      *
      * @see
      *   [[sfml.graphics.Text.fillColor fillColor]]
      *
      * @doxygenId
      *   classsf_1_1Text_1ab7bb3babac5a6da1802b2c3e1a3e6dcc
      */
    def fillColor_=(color: Color) =
        Self.setFillColor(_text.ptr, color)

    def font: Option[Immutable[Font]] =
        Option(Self.getFont(_text.ptr)).map(ResourcePtr.retrieve).map(ptr => Immutable(Font(ptr)))

    /** Set the text's font.
      *
      * The `font` argument refers to a font that must exist as long as the text uses it. Indeed, the text doesn't store its own
      * copy of the font, but rather keeps a pointer to the one that you passed to this function. If the font is destroyed and the
      * text tries to use it, the behavior is undefined.
      *
      * @param font
      *   New font
      *
      * @see
      *   [[sfml.graphics.Text.font font]]
      *
      * @doxygenId
      *   classsf_1_1Text_1a2927805d1ae92d57f15034ea34756b81
      */
    def font_=(font: Font) =
        Self.setFont(_text.ptr, font)

    /** Get the size of the letter spacing factor.
      *
      * @return
      *   Size of the letter spacing factor
      *
      * @see
      *   [[sfml.graphics.Text.letterSpacing_= letterSpacing_=]]
      *
      * @doxygenId
      *   classsf_1_1Text_1a028fc6e561bd9a0671254419b498b889
      */
    def letterSpacing: Float =
        Self.getLetterSpacing(_text.ptr)

    /** Set the letter spacing factor.
      *
      * The default spacing between letters is defined by the font. This factor doesn't directly apply to the existing spacing
      * between each character, it rather adds a fixed space between them which is calculated from the font metrics and the
      * character size. Note that factors below 1 (including negative numbers) bring characters closer to each other. By default the
      * letter spacing factor is 1.
      *
      * @param spacingFactor
      *   New letter spacing factor
      *
      * @see
      *   [[sfml.graphics.Text.letterSpacing letterSpacing]]
      *
      * @doxygenId
      *   classsf_1_1Text_1ab516110605edb0191a7873138ac42af2
      */
    def letterSpacing_=(spacingFactor: Float) =
        Self.setLetterSpacing(_text.ptr, spacingFactor)

    /** Get the size of the line spacing factor.
      *
      * @return
      *   Size of the line spacing factor
      *
      * @see
      *   [[sfml.graphics.Text.lineSpacing_= lineSpacing_=]]
      *
      * @doxygenId
      *   classsf_1_1Text_1a670622e1c299dfd6518afe289c7cd248
      */
    def lineSpacing: Float =
        Self.getLineSpacing(_text.ptr)

    /** Set the line spacing factor.
      *
      * The default spacing between lines is defined by the font. This method enables you to set a factor for the spacing between
      * lines. By default the line spacing factor is 1.
      *
      * @param spacingFactor
      *   New line spacing factor
      *
      * @see
      *   [[sfml.graphics.Text.lineSpacing lineSpacing]]
      *
      * @doxygenId
      *   classsf_1_1Text_1af6505688f79e2e2d90bd68f4d767e965
      */
    def lineSpacing_=(spacingFactor: Float) =
        Self.setLineSpacing(_text.ptr, spacingFactor)

    /** Get the outline color of the text.
      *
      * @return
      *   Outline color of the text
      *
      * @see
      *   [[sfml.graphics.Text.outlineColor_= outlineColor_=]]
      *
      * @doxygenId
      *   classsf_1_1Text_1ade9256ff9d43c9481fcf5f4003fe0141
      */
    def outlineColor: Color =
        Self.getOutlineColor(_text.ptr)

    /** Set the outline color of the text.
      *
      * By default, the text's outline color is opaque black.
      *
      * @param color
      *   New outline color of the text
      *
      * @see
      *   [[sfml.graphics.Text.outlineColor outlineColor]]
      *
      * @doxygenId
      *   classsf_1_1Text_1aa19ec69c3b894e963602a6804ca68fe4
      */
    def outlineColor_=(color: Color) =
        Self.setOutlineColor(_text.ptr, color)

    /** Get the outline thickness of the text.
      *
      * @return
      *   Outline thickness of the text, in pixels
      *
      * @see
      *   [[sfml.graphics.Text.outlineThickness_= outlineThickness_=]]
      *
      * @doxygenId
      *   classsf_1_1Text_1af6bf01c23189edf52c8b38708db6f3f6
      */
    def outlineThickness: Float =
        Self.getOutlineThickness(_text.ptr)

    /** Set the thickness of the text's outline.
      *
      * By default, the outline thickness is 0.
      *
      * Be aware that using a negative value for the outline thickness will cause distorted rendering.
      *
      * @param thickness
      *   New outline thickness, in pixels
      *
      * @see
      *   [[sfml.graphics.Text.outlineThickness outlineThickness]]
      *
      * @doxygenId
      *   classsf_1_1Text_1ab0e6be3b40124557bf53737fe6a6ce77
      */
    def outlineThickness_=(thickness: Float) =
        Self.setOutlineThickness(_text.ptr, thickness)

    // TODO: Self.getString
    def string = ()

    /** Set the text's string.
      *
      * A text's string is empty by default.
      *
      * @param string
      *   New string
      *
      * <!-- TODO @see string -->
      *
      * @doxygenId
      *   classsf_1_1Text_1a7d3b3359f286fd9503d1ced25b7b6c33
      *
      * @doxygenHash
      *   0849c676724ef0896dcee3425658b073
      */
    def string_=(string: String) =
        Self.setString(_text.ptr, string)

object Text:

    private[sfml] inline given Conversion[Text, Ptr[Self.sfText]]:
        override def apply(text: Text) = text._text.ptr

    private[sfml] def dtor(ptr: Ptr[Self.sfText]): Unit =
        String.dtor(ptr.at3)
        VertexArray.dtor(ptr.at5)
        VertexArray.dtor(ptr.at6)

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfText]

    object ctor:

        /** Default constructor.
          *
          * Creates an empty text.
          *
          * @doxygenId
          *   classsf_1_1Text_1aff7cab6a92e5948c9d1481cb2d87eb84
          */
        def apply(): ctor =
            Constructor(Self.ctor(_))

        /** Construct the text from a string, font and size.
          *
          * Note that if the used font is a bitmap font, it is not scalable, thus not all requested sizes will be available to use.
          * This needs to be taken into consideration when setting the character size. If you need to display text of a certain
          * size, make sure the corresponding bitmap font that supports that size is used.
          *
          * @param string
          *   Text assigned to the string
          *
          * @param font
          *   Font used to draw the string
          *
          * @param characterSize
          *   Base size of characters, in pixels
          *
          * @doxygenId
          *   classsf_1_1Text_1a614019e0b5c0ed39a99d32483a51f2c5
          */
        def apply(string: String, font: Font, characterSize: Int = 30): ctor =
            Constructor(Self.ctor(_, string, font, characterSize.toUInt))

    /** Default constructor.
      *
      * Creates an empty text.
      *
      * @doxygenId
      *   classsf_1_1Text_1aff7cab6a92e5948c9d1481cb2d87eb84
      */
    def apply(): Text =
        new Object with Text(using ctor())

    /** Construct the text from a string, font and size.
      *
      * Note that if the used font is a bitmap font, it is not scalable, thus not all requested sizes will be available to use. This
      * needs to be taken into consideration when setting the character size. If you need to display text of a certain size, make
      * sure the corresponding bitmap font that supports that size is used.
      *
      * @param string
      *   Text assigned to the string
      *
      * @param font
      *   Font used to draw the string
      *
      * @param characterSize
      *   Base size of characters, in pixels
      *
      * @doxygenId
      *   classsf_1_1Text_1a614019e0b5c0ed39a99d32483a51f2c5
      */
    def apply(string: String, font: Font, characterSize: Int = 30): Text =
        new Object with Text(using ctor(string, font, characterSize))

    /* Immutable */

    extension (text: Immutable[Text])

        def globalBounds: Rect[Float] = text.get.globalBounds

        def localBounds: Rect[Float] = text.get.localBounds

        def characterSize: Int = text.get.characterSize

        def color: Color = text.get.color

        def fillColor: Color = text.get.fillColor

        def letterSpacing: Float = text.get.letterSpacing

        def lineSpacing: Float = text.get.lineSpacing

        def outlineColor: Color = text.get.outlineColor

        def outlineThickness: Float = text.get.outlineThickness
