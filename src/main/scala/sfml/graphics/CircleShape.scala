package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichLong

import sfml.internal.graphics.CircleShape as Self

import sfml.system.Vector2

/** Specialized shape representing a circle.
  *
  * This class inherits all the functions of [[sfml.graphics.Transformable Transformable]] (position, rotation, scale, bounds, ...)
  * as well as the functions of [[sfml.graphics.Shape Shape]] (outline, color, texture, ...).
  *
  * Since the graphics card can't draw perfect circles, we have to fake them with multiple triangles connected to each other. The
  * "points count" property of [[sfml.graphics.CircleShape CircleShape]] defines how many of these triangles to use, and therefore
  * defines the quality of the circle.
  *
  * The number of points can also be used for another purpose; with small numbers you can create any regular polygon shape:
  * equilateral triangle, square, pentagon, hexagon, ...
  *
  * ```scala
  * //{
  * val window: RenderWindow = ???
  *
  * //}
  * val circle = CircleShape()
  * circle.radius = 150
  * circle.outlineColor = Color.Red()
  * circle.outlineThickness = 5
  * circle.position = (10, 20)
  *
  * window.draw(circle)
  * ```
  *
  * @see
  *   [[sfml.graphics.Shape Shape]], [[sfml.graphics.RectangleShape RectangleShape]] <!-- TODO: ConvexShape -->
  *
  * @doxygenId
  *   classsf_1_1CircleShape
  *
  * @doxygenHash
  *   4e24d194062cac62aeb0f863278914ca
  */
trait CircleShape(using private[sfml] ctor: CircleShape.ctor) extends Shape:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _circleShape: ResourcePtr[Self.sfCircleShape] =
        ResourceBuffer(Self.dtor)(ctor)

    private[sfml] final override lazy val _shape = _circleShape.get(_.at1)

    /* Methods */

    override def point(index: Long): Vector2[Float] =
        Vector2.toVector2Float((data: Ptr[CStruct2[Ptr[Self.sfCircleShape], CSize]]) => {
            Self.getPoint(data._1, data._2)
        })(_circleShape.ptr, index.toCSize)

    override def pointCount: Long =
        Self.getPointCount(_circleShape.ptr).toLong

    def pointCount_=(count: Long) =
        Self.setPointCount(_circleShape.ptr, count.toCSize)

    /** Get the radius of the circle.
      *
      * @return
      *   Radius of the circle
      *
      * @see
      *   [[sfml.graphics.CircleShape.radius_= radius_=]]
      *
      * @doxygenId
      *   classsf_1_1CircleShape_1aa3dd5a1b5031486ce5b6f09d43674aa3
      */
    def radius: Float =
        Self.getRadius(_circleShape.ptr)

    /** Set the radius of the circle.
      *
      * @param radius
      *   New radius of the circle
      *
      * @see
      *   [[sfml.graphics.CircleShape.radius radius]]
      *
      * @doxygenId
      *   classsf_1_1CircleShape_1a21cdf85fc2f201e10222a241af864be0
      */
    def radius_=(radius: Float): Unit =
        Self.setRadius(_circleShape.ptr, radius)

object CircleShape:

    private[sfml] inline given Conversion[CircleShape, Ptr[Self.sfCircleShape]]:
        override def apply(circleShape: CircleShape) = circleShape._circleShape.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfCircleShape]

    object ctor:

        /** Default constructor.
          *
          * @param radius
          *   Radius of the circle
          *
          * @param pointCount
          *   Number of points composing the circle
          *
          * @doxygenId
          *   classsf_1_1CircleShape_1aaebe705e7180cd55588eb19488af3af1
          */
        def apply(radius: Float = 0, pointCount: Long = 30): ctor =
            Constructor(Self.ctor(_, radius, pointCount.toCSize))

    /** Default constructor.
      *
      * @param radius
      *   Radius of the circle
      *
      * @param pointCount
      *   Number of points composing the circle
      *
      * @doxygenId
      *   classsf_1_1CircleShape_1aaebe705e7180cd55588eb19488af3af1
      */
    def apply(radius: Float = 0, pointCount: Long = 30): CircleShape =
        new Object with CircleShape(using ctor(radius, pointCount))
