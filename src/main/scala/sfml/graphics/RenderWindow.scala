package sfml
package graphics

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.graphics.RenderWindow as Self

import sfml.system.{String, Vector2}
import sfml.window.{ContextSettings, Style, VideoMode, Window}

/** [[sfml.window.Window Window]] that can serve as a target for 2D drawing.
  *
  * [[sfml.graphics.RenderWindow RenderWindow]] is the main class of the Graphics module.
  *
  * It defines an OS window that can be painted using the other classes of the graphics module.
  *
  * [[sfml.graphics.RenderWindow RenderWindow]] is derived from [[sfml.window.Window Window]], thus it inherits all its features:
  * events, window management, OpenGL rendering, etc. See the documentation of [[sfml.window.Window Window]] for a more complete
  * description of all these features, as well as code examples.
  *
  * On top of that, [[sfml.graphics.RenderWindow RenderWindow]] adds more features related to 2D drawing with the graphics module
  * (see its base class [[sfml.graphics.RenderTarget RenderTarget]] for more details). Here is a typical rendering and event loop
  * with a [[sfml.graphics.RenderWindow RenderWindow]]:
  * ```scala
  * //{
  * import sfml.window.{Event, VideoMode}
  *
  * val sprite: Sprite = ???
  * val circle: CircleShape = ???
  * val text: Text = ???
  *
  * //}
  * // Declare and create a new render-window
  * val window = RenderWindow(VideoMode(800, 600), "SFML window")
  *
  * // Limit the framerate to 60 frames per second (this step is optional)
  * window.framerateLimit = 60;
  *
  * // The main loop - ends as soon as the window is closed
  * while window.isOpen() do
  *     // Event processing
  *     for event <- window.pollEvent() do
  *         event match
  *             // Request for closing the window
  *             case Event.Closed() => window.close()
  *             case _              => ()
  *
  *     // Clear the whole window before rendering a new frame
  *     window.clear()
  *
  *     // Draw some graphical entities
  *     window.draw(sprite)
  *     window.draw(circle)
  *     window.draw(text)
  *
  *     // End the current frame and display its contents on screen
  *     window.display()
  * ```
  *
  * <!-- TODO OpenGL -->
  *
  * @doxygenId
  *   classsf_1_1RenderWindow
  *
  * @doxygenHash
  *   215d1944bf7ebf8e6c17cfcae6373f43
  */
trait RenderWindow(using private[sfml] ctor: RenderWindow.ctor) extends Window with RenderTarget:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _renderWindow: ResourcePtr[Self.sfRenderWindow] =
        ResourceBuffer(Self.dtor)(ctor)

    private[sfml] override lazy val _window = _renderWindow.get(_.at1)
    private[sfml] override lazy val _renderTarget = _renderWindow.get(_.at2)

    /* Methods */

    /** Get the size of the rendering region of the window.
      *
      * The size doesn't include the titlebar and borders of the window.
      *
      * @return
      *   Size in pixels
      *
      * @doxygenId
      *   classsf_1_1RenderWindow_1ae3eacf93661c8068fca7a78d57dc7e14
      */
    override def size: Vector2[Int] =
        Vector2(Self.getSize(this))

object RenderWindow:

    private[sfml] inline given Conversion[RenderWindow, Ptr[Self.sfRenderWindow]]:
        override def apply(renderWindow: RenderWindow) = renderWindow._renderWindow.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfRenderWindow]

    object ctor:

        /** Construct a new window.
          *
          * This constructor creates the window with the size and pixel depth defined in `mode`. An optional style can be passed to
          * customize the look and behavior of the window (borders, title bar, resizable, closable, ...).
          *
          * The fourth parameter is an optional structure specifying advanced OpenGL context settings such as antialiasing,
          * depth-buffer bits, etc. You shouldn't care about these parameters for a regular usage of the graphics module.
          *
          * @param mode
          *   Video mode to use (defines the width, height and depth of the rendering area of the window)
          *
          * @param title
          *   Title of the window
          *
          * @param style
          *   Window style, a bitwise OR combination of [[sfml.window.Style Style]] enumerators
          *
          * @param settings
          *   Additional settings for the underlying OpenGL context
          *
          * @doxygenId
          *   classsf_1_1RenderWindow_1aebef983e01f677bf5a66cefc4d547647
          */
        def apply(
            mode: VideoMode,
            title: String,
            style: Style = Style.Default,
            settings: ContextSettings = ContextSettings()
        ): ctor =
            Constructor(Self.ctor(mode, title, style.value.toUInt, settings))

    /** Construct a new window.
      *
      * This constructor creates the window with the size and pixel depth defined in `mode`. An optional style can be passed to
      * customize the look and behavior of the window (borders, title bar, resizable, closable, ...).
      *
      * The fourth parameter is an optional structure specifying advanced OpenGL context settings such as antialiasing, depth-buffer
      * bits, etc. You shouldn't care about these parameters for a regular usage of the graphics module.
      *
      * @param mode
      *   Video mode to use (defines the width, height and depth of the rendering area of the window)
      *
      * @param title
      *   Title of the window
      *
      * @param style
      *   Window style, a bitwise OR combination of [[sfml.window.Style Style]] enumerators
      *
      * @param settings
      *   Additional settings for the underlying OpenGL context
      *
      * @doxygenId
      *   classsf_1_1RenderWindow_1aebef983e01f677bf5a66cefc4d547647
      */
    def apply(
        mode: VideoMode,
        title: String,
        style: Style = Style.Default,
        settings: ContextSettings = ContextSettings()
    ): RenderWindow =
        new Object with RenderWindow(using ctor(mode, title, style, settings))
