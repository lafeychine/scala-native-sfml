package sfml

/** 2D graphics module: sprites, text, shapes, ...
  *
  * @doxygenId
  *   group__graphics
  */
package object graphics:
    enum ShaderType:
        case Vertex
        case Geometry
        case Fragment
