package sfml

/** Provides OpenGL-based windows, and abstractions for events and input handling.
  *
  * @doxygenId
  *   group__window
  */
package object window:
    /** Enumeration of the window styles.
      *
      * @doxygenId
      *   group__window_1ga58847fff37f6c46b9c9851d6448705a0
      */
    enum Style(val value: Int):
        /** No border / title bar (this flag and all others are mutually exclusive)
          *
          * @doxygenId
          *   group__window_1gga58847fff37f6c46b9c9851d6448705a0a8c35a9c8507559e455387fc4a83ce422
          */
        case None extends Style(0)

        /** Title bar + fixed border.
          *
          * @doxygenId
          *   group__window_1gga58847fff37f6c46b9c9851d6448705a0ab4c8b32b05ed715928513787cb1e85b6
          */
        case Titlebar extends Style(1 << 0)

        /** Title bar + resizable border + maximize button.
          *
          * @doxygenId
          *   group__window_1gga58847fff37f6c46b9c9851d6448705a0accff967648ebcd5db2007eff7352b50f
          */
        case Resize extends Style(1 << 1)

        /** Title bar + close button.
          *
          * @doxygenId
          *   group__window_1gga58847fff37f6c46b9c9851d6448705a0ae07a7d411d5acf28f4a9a4b76a3a9493
          */
        case Close extends Style(1 << 2)

        /** Fullscreen mode (this flag and all others are mutually exclusive)
          *
          * @doxygenId
          *   group__window_1gga58847fff37f6c46b9c9851d6448705a0a6288ec86830245cf957e2d234f79f50d
          */
        case Fullscreen extends Style(1 << 3)

        /** Default window style.
          *
          * @doxygenId
          *   group__window_1gga58847fff37f6c46b9c9851d6448705a0a5597cd420fc461807e4a201c92adea37
          */
        case Default extends Style(Titlebar.value | Resize.value | Close.value)
