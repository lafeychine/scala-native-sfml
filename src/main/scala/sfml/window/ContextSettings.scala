package sfml
package window

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.Type.{booleanToSfBool, sfBoolToBoolean}
import sfml.internal.window.ContextSettings as Self

trait ContextSettings(using private[sfml] ctor: ContextSettings.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _contextSettings: ResourcePtr[Self.sfContextSettings] =
        ResourceBuffer(ctor)

    /* Methods */

    def depthBits: Int =
        _contextSettings.ptr._1.toInt

    def stencilBits: Int =
        _contextSettings.ptr._2.toInt

    def antialiasingLevel: Int =
        _contextSettings.ptr._3.toInt

    def majorVersion: Int =
        _contextSettings.ptr._4.toInt

    def minorVersion: Int =
        _contextSettings.ptr._5.toInt

    def attributeFlags: Option[ContextSettings.Attribute] =
        _contextSettings.ptr._6.toInt match
            case ContextSettings.Attribute.Default.value => Option(ContextSettings.Attribute.Default)
            case ContextSettings.Attribute.Core.value    => Option(ContextSettings.Attribute.Core)
            case ContextSettings.Attribute.Debug.value   => Option(ContextSettings.Attribute.Debug)
            case _                                       => None

    def sRgbCapable: Boolean =
        _contextSettings.ptr._7

object ContextSettings:

    private[sfml] inline given Conversion[ContextSettings, Ptr[Self.sfContextSettings]]:
        override def apply(contextSettings: ContextSettings) = contextSettings._contextSettings.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfContextSettings]

    object ctor:

        def apply(
            depthBits: Int = 0,
            stencilBits: Int = 0,
            antialiasingLevel: Int = 0,
            majorVersion: Int = 1,
            minorVersion: Int = 1,
            attributeFlags: ContextSettings.Attribute = ContextSettings.Attribute.Default,
            sRgbCapable: Boolean = false
        ): ctor =
            Constructor((r: Ptr[Self.sfContextSettings]) => {
                r._1 = depthBits.toUInt
                r._2 = stencilBits.toUInt
                r._3 = antialiasingLevel.toUInt
                r._4 = majorVersion.toUInt
                r._5 = minorVersion.toUInt
                r._6 = attributeFlags.value.toUInt
                r._7 = sRgbCapable
            })

    def apply(
        depthBits: Int = 0,
        stencilBits: Int = 0,
        antialiasingLevel: Int = 0,
        majorVersion: Int = 1,
        minorVersion: Int = 1,
        attributeFlags: ContextSettings.Attribute = ContextSettings.Attribute.Default,
        sRgbCapable: Boolean = false
    ): ContextSettings =
        new Object
            with ContextSettings(using
                ctor(
                    depthBits,
                    stencilBits,
                    antialiasingLevel,
                    majorVersion,
                    minorVersion,
                    attributeFlags,
                    sRgbCapable
                )
            )

    /* Related enums  */

    enum Attribute(val value: Int):
        case Default extends Attribute(0)
        case Core extends Attribute(1 << 0)
        case Debug extends Attribute(1 << 2)
