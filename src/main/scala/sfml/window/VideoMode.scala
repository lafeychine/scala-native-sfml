package sfml
package window

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.window.VideoMode as Self

trait VideoMode(using private[sfml] ctor: VideoMode.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _videoMode: ResourcePtr[Self.sfVideoMode] =
        ResourceBuffer(ctor)

    /* Methods */

    def width: Int =
        _videoMode.ptr._1.toInt

    def width_=(width: Int): Unit =
        _videoMode.ptr._1 = width.toUInt

    def height: Int =
        _videoMode.ptr._2.toInt

    def height_=(height: Int): Unit =
        _videoMode.ptr._2 = height.toUInt

    def bitsPerPixel: Int =
        _videoMode.ptr._3.toInt

    def bitsPerPixel_=(bitsPerPixel: Int): Unit =
        _videoMode.ptr._3 = bitsPerPixel.toUInt

    def isValid: Boolean =
        Self.isValid(_videoMode.ptr)

    def <(rhs: Immutable[VideoMode]): Boolean =
        Self.lt_sfVideoMode(_videoMode.ptr, rhs.get)

    def <=(rhs: Immutable[VideoMode]): Boolean =
        Self.le_sfVideoMode(_videoMode.ptr, rhs.get)

    def >(rhs: Immutable[VideoMode]): Boolean =
        Self.gt_sfVideoMode(_videoMode.ptr, rhs.get)

    def >=(rhs: Immutable[VideoMode]): Boolean =
        Self.ge_sfVideoMode(_videoMode.ptr, rhs.get)

    def ==(rhs: Immutable[VideoMode]): Boolean =
        Self.eq_sfVideoMode(_videoMode.ptr, rhs.get)

    def !=(rhs: Immutable[VideoMode]): Boolean =
        Self.ne_sfVideoMode(_videoMode.ptr, rhs.get)

    /* Scala operators */

    def copy(width: Int = width, height: Int = height, bitsPerPixel: Int = bitsPerPixel): VideoMode =
        VideoMode(width, height, bitsPerPixel)

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: VideoMode    => this == Immutable(rhs)
            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def toString: String =
        s"VideoMode($width, $height, $bitsPerPixel)"

object VideoMode:

    private[sfml] inline given Conversion[VideoMode, Ptr[Self.sfVideoMode]]:
        override def apply(videoMode: VideoMode) = videoMode._videoMode.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfVideoMode]

    object ctor:

        def apply(): ctor =
            Constructor(Self.ctor(_))

        def apply(width: Int, height: Int, bitsPerPixel: Int = 32): ctor =
            Constructor(Self.ctor(_, width.toUInt, height.toUInt, bitsPerPixel.toUInt))

    def apply(): VideoMode =
        new Object with VideoMode(using ctor())

    def apply(width: Int, height: Int, bitsPerPixel: Int = 32): VideoMode =
        new Object with VideoMode(using ctor(width, height, bitsPerPixel))

    def unapply(videoMode: VideoMode): (Int, Int, Int) =
        (videoMode.width, videoMode.height, videoMode.bitsPerPixel)

    /* Immutable */

    extension (videoMode: Immutable[VideoMode])

        def width: Int = videoMode.get.width

        def height: Int = videoMode.get.height

        def bitsPerPixel: Int = videoMode.get.bitsPerPixel

        def isValid: Boolean = videoMode.get.isValid

        def <(rhs: Immutable[VideoMode]): Boolean =
            videoMode.get < rhs

        def <=(rhs: Immutable[VideoMode]): Boolean =
            videoMode.get <= rhs

        def >(rhs: Immutable[VideoMode]): Boolean =
            videoMode.get > rhs

        def >=(rhs: Immutable[VideoMode]): Boolean =
            videoMode.get >= rhs

        def copy(
            width: Int = videoMode.width,
            height: Int = videoMode.height,
            bitsPerPixel: Int = videoMode.bitsPerPixel
        ): VideoMode =
            videoMode.get.copy(width, height, bitsPerPixel)
