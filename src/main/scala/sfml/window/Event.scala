package sfml
package window

import scala.scalanative.unsafe.*

import sfml.internal.window.Event as Self

/** Defines a system event and its parameters.
  *
  * [[sfml.window.Event Event]] holds all the informations about a system event that just happened.
  *
  * Events are retrieved using the [[sfml.window.Window.pollEvent pollEvent]] function. <!-- TODO Window.waitEvent -->
  *
  * A [[sfml.window.Event Event]] instance contains the type of the event (mouse moved, key pressed, window closed, ...) as well as
  * the details about this particular event.
  *
  * ```scala
  * //{
  * val window: Window = ???
  * val doSomethingWithTheNewSize: (Int, Int) => Unit = ???
  *
  * //}
  * for event <- window.pollEvent() do
  *     event match
  *         // Request for closing the window
  *         case Event.Closed() => window.close()
  *
  *         // The escape key was pressed
  *         case Event.KeyPressed(Keyboard.Key.Escape, _, _, _, _, _) => window.close()
  *
  *         // The window was resized
  *         case Event.Resized(width, height) => doSomethingWithTheNewSize(width, height)
  *
  *         // etc...
  *
  *         case _ => ()
  * ```
  *
  * @doxygenId
  *   classsf_1_1Event
  *
  * @doxygenHash
  *   0e547fefdea400d429c15c522d341a56
  */
enum Event:
    /** The window requested to be closed
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa316e4212e083f1dce79efd8d9e9c0a95
      *
      * @doxygenHash
      *   0daaa237ee8da09111868c5e0429796a
      */
    case Closed()

    /** The window was resized
      *
      * @param width
      *   New width, in pixels.
      *
      * @param height
      *   New height, in pixels.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa67fd26d7e520bc6722db3ff47ef24941
      *
      * @doxygenHash
      *   2e27f4e22daa55406dc7ee420f248cc7
      */
    case Resized(val width: Int, val height: Int)

    /** The window lost the focus
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aabd7877b5011a337268357c973e8347bd
      *
      * @doxygenHash
      *   a3553f16295cd2bec91c8cb0903c360a
      */
    case LostFocus()

    /** The window gained the focus
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa8c5003ced508499933d540df8a6023ec
      *
      * @doxygenHash
      *   b211d8806003946fbe16fbf92ccfcf19
      */
    case GainedFocus()

    /** A character was entered
      *
      * @param unicode
      *   UTF-32 Unicode value of the character.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa7e09871dc984080ff528e4f7e073e874
      *
      * @doxygenHash
      *   6fdd1f7a2be38da37641d356f07dbbbd
      */
    case TextEntered(val unicode: Int)

    /** A key was pressed
      *
      * @param code
      *   Code of the key that has been pressed.
      *
      * @param scancode
      *   Physical code of the key that has been pressed.
      *
      * @param alt
      *   Is the Alt key pressed?
      *
      * @param control
      *   Is the Control key pressed?
      *
      * @param shift
      *   Is the Shift key pressed?
      *
      * @param system
      *   Is the System key pressed?
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aac3c7abfaa98c73bfe6be0b57df09c71b
      *
      * @doxygenHash
      *   762bd8747b9b70dc67d4cbd5c423f0c9
      */
    case KeyPressed(
        val code: Keyboard.Key,
        val scancode: Keyboard.Scancode,
        val alt: Boolean,
        val control: Boolean,
        val shift: Boolean,
        val system: Boolean
    )

    /** A key was released
      *
      * @param code
      *   Code of the key that has been released.
      *
      * @param scancode
      *   Physical code of the key that has been released.
      *
      * @param alt
      *   Is the Alt key released?
      *
      * @param control
      *   Is the Control key released?
      *
      * @param shift
      *   Is the Shift key released?
      *
      * @param system
      *   Is the System key released?
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aaa5bcc1e603d5a6f4c137af39558bd5d1
      *
      * @doxygenHash
      *   4c564faf389db01178f57fd86543c60c
      */
    case KeyReleased(
        val code: Keyboard.Key,
        val scancode: Keyboard.Scancode,
        val alt: Boolean,
        val control: Boolean,
        val shift: Boolean,
        val system: Boolean
    )

    /** The mouse wheel was scrolled
      *
      * @param wheel
      *   Which wheel (for mice with multiple ones).
      *
      * @param delta
      *   Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
      *
      * @param x
      *   X position of the mouse pointer, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the mouse pointer, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa2fb6925eb3e7f3d468faf639dbd129ad
      *
      * @doxygenHash
      *   085bc905cc5fb5794c3599e0eb677046
      */
    case MouseWheelScrolled(val wheel: Mouse.Wheel, val delta: Float, val x: Int, val y: Int)

    /** A mouse button was pressed
      *
      * @param button
      *   Code of the button that has been pressed.
      *
      * @param x
      *   X position of the mouse pointer, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the mouse pointer, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa55a3dcc8bf6c40e37f9ff2cdf606481f
      *
      * @doxygenHash
      *   da7b25b0951ea2202d2204f3201f5668
      */
    case MouseButtonPressed(val button: Mouse.Button, val x: Int, val y: Int)

    /** A mouse button was released
      *
      * @param button
      *   Code of the button that has been released.
      *
      * @param x
      *   X position of the mouse pointer, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the mouse pointer, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa9be69ecc07e484467ebbb133182fe5c1
      *
      * @doxygenHash
      *   828eddb8b035138667d7b735b61b89f4
      */
    case MouseButtonReleased(val button: Mouse.Button, val x: Int, val y: Int)

    /** The mouse cursor moved
      *
      * @param x
      *   X position of the mouse pointer, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the mouse pointer, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa4ff4fc3b3dc857e3617a63feb54be209
      *
      * @doxygenHash
      *   aa00d0603163018b14ba4aec45158308
      */
    case MouseMoved(val x: Int, val y: Int)

    /** The mouse cursor entered the area of the window
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa50d98590a953e74c7ccf3dabadb22067
      *
      * @doxygenHash
      *   b5c533fe96d8246bebddf05d5016ae95
      */
    case MouseEntered()

    /** The mouse cursor left the area of the window
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aaa90b8526b328e0246d04b026de17c6e7
      *
      * @doxygenHash
      *   74ce76c2b5faac344d037d13f3e00b21
      */
    case MouseLeft()

    // TODO: Implement Joystick
    // case JoystickButtonPressed(val joystickId: Int, val button: Int)
    // case JoystickButtonReleased(val joystickId: Int, val button: Int)
    // case JoystickMoved(val joystickId: Int, val axis: Joystick.Axis, val position: Float)
    // case JoystickConnected(val joystickId: Int)
    // case JoystickDisconnected(val joystickId: Int)

    /** A touch event began
      *
      * @param finger
      *   Index of the finger in case of multi-touch events.
      *
      * @param x
      *   X position of the touch, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the touch, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aae6f8231ad6013d063929a09b6c28f515
      *
      * @doxygenHash
      *   8955740fe0a09a6a9cd5d250909524c8
      */
    case TouchBegan(val finger: Int, val x: Int, val y: Int)

    /** A touch moved
      *
      * @param finger
      *   Index of the finger in case of multi-touch events.
      *
      * @param x
      *   X position of the touch, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the touch, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aa9524b7d7665212c6d56f623b5b8311a9
      *
      * @doxygenHash
      *   f41f849126d296f8103edd267526cb9b
      */
    case TouchMoved(val finger: Int, val x: Int, val y: Int)

    /** A touch event ended
      *
      * @param finger
      *   Index of the finger in case of multi-touch events.
      *
      * @param x
      *   X position of the touch, relative to the left of the owner window.
      *
      * @param y
      *   Y position of the touch, relative to the top of the owner window.
      *
      * @doxygenId
      *   classsf_1_1Event_1af41fa9ed45c02449030699f671331d4aabc7123492dbca320da5c03fea1a141e5
      *
      * @doxygenHash
      *   3b1d06fb1bde30be45dbf891bc6757ed
      */
    case TouchEnded(val finger: Int, val x: Int, val y: Int)
    case SensorChanged(val sensor: Sensor.Type, val x: Float, val y: Float, val z: Float)

object Event:
    private[sfml] def apply(event: Ptr[Self.sfEvent]): Option[Event] =
        !event match
            case 0  => Option(Closed())
            case 1  => Option(Resized(event.asInstanceOf[Ptr[Self.sfSizeEvent]]))
            case 2  => Option(LostFocus())
            case 3  => Option(GainedFocus())
            case 4  => Option(TextEntered(event.asInstanceOf[Ptr[Self.sfTextEvent]]))
            case 5  => Option(KeyPressed(event.asInstanceOf[Ptr[Self.sfKeyEvent]]))
            case 6  => Option(KeyReleased(event.asInstanceOf[Ptr[Self.sfKeyEvent]]))
            case 7  => None // Deprecated: MouseWheelMoved
            case 8  => Option(MouseWheelScrolled(event.asInstanceOf[Ptr[Self.sfMouseWheelScrollEvent]]))
            case 9  => Option(MouseButtonPressed(event.asInstanceOf[Ptr[Self.sfMouseButtonEvent]]))
            case 10 => Option(MouseButtonReleased(event.asInstanceOf[Ptr[Self.sfMouseButtonEvent]]))
            case 11 => Option(MouseMoved(event.asInstanceOf[Ptr[Self.sfMouseMoveEvent]]))
            case 12 => Option(MouseEntered())
            case 13 => Option(MouseLeft())
            // TODO: Implement Joystick
            // case 14 => Option(JoystickButtonPressed(event.asInstanceOf[Ptr[Self.sfJoystickButtonEvent]]))
            // case 15 => Option(JoystickButtonReleased(event.asInstanceOf[Ptr[Self.sfJoystickButtonEvent]]))
            // case 16 => Option(JoystickMoved(event.asInstanceOf[Ptr[Self.sfJoystickMoveEvent]]))
            // case 17 => Option(JoystickConnected(event.asInstanceOf[Ptr[Self.sfJoystickConnectEvent]]))
            // case 18 => Option(JoystickDisconnected(event.asInstanceOf[Ptr[Self.sfJoystickConnectEvent]]))
            case 19 => Option(TouchBegan(event.asInstanceOf[Ptr[Self.sfTouchEvent]]))
            case 20 => Option(TouchMoved(event.asInstanceOf[Ptr[Self.sfTouchEvent]]))
            case 21 => Option(TouchEnded(event.asInstanceOf[Ptr[Self.sfTouchEvent]]))
            case 22 => Option(SensorChanged(event.asInstanceOf[Ptr[Self.sfSensorEvent]]))
            case _  => None

    object Resized:
        private[sfml] def apply(event: Ptr[Self.sfSizeEvent]): Resized =
            Resized(event._2.toInt, event._3.toInt)

    object TextEntered:
        private[sfml] def apply(event: Ptr[Self.sfTextEvent]): TextEntered =
            TextEntered(event._2.toInt)

    object KeyPressed:
        private[sfml] def apply(event: Ptr[Self.sfKeyEvent]): KeyPressed =
            val code = if event._2 < 0 then Keyboard.Key.Unknown else Keyboard.Key.fromOrdinal(event._2)
            val scancode = if event._3 < 0 then Keyboard.Scancode.Unknown else Keyboard.Scancode.fromOrdinal(event._3)

            KeyPressed(code, scancode, event._4, event._5, event._6, event._7)

    object KeyReleased:
        private[sfml] def apply(event: Ptr[Self.sfKeyEvent]): KeyReleased =
            val code = if event._2 < 0 then Keyboard.Key.Unknown else Keyboard.Key.fromOrdinal(event._2)
            val scancode = if event._3 < 0 then Keyboard.Scancode.Unknown else Keyboard.Scancode.fromOrdinal(event._3)

            KeyReleased(code, scancode, event._4, event._5, event._6, event._7)

    object MouseWheelScrolled:
        private[sfml] def apply(event: Ptr[Self.sfMouseWheelScrollEvent]): MouseWheelScrolled =
            MouseWheelScrolled(Mouse.Wheel.fromOrdinal(event._2), event._3, event._4, event._5)

    object MouseButtonPressed:
        private[sfml] def apply(event: Ptr[Self.sfMouseButtonEvent]): MouseButtonPressed =
            MouseButtonPressed(Mouse.Button.fromOrdinal(event._2), event._3, event._4)

    object MouseButtonReleased:
        private[sfml] def apply(event: Ptr[Self.sfMouseButtonEvent]): MouseButtonReleased =
            MouseButtonReleased(Mouse.Button.fromOrdinal(event._2), event._3, event._4)

    object MouseMoved:
        private[sfml] def apply(event: Ptr[Self.sfMouseMoveEvent]): MouseMoved =
            MouseMoved(event._2, event._3)

    // TODO: Implement Joystick
    // object JoystickButtonPressed:
    //     private[sfml] def apply(event: Ptr[Self.sfJoystickButtonEvent]): JoystickButtonPressed =
    //         JoystickButtonPressed(event._2.toInt, event._3.toInt)

    // object JoystickButtonReleased:
    //     private[sfml] def apply(event: Ptr[Self.sfJoystickButtonEvent]): JoystickButtonReleased =
    //         JoystickButtonReleased(event._2.toInt, event._3.toInt)

    // object JoystickMoved:
    //     private[sfml] def apply(event: Ptr[Self.sfJoystickMoveEvent]): JoystickMoved =
    //         JoystickMoved(event._2.toInt, Joystick.Axis.fromOrdinal(event._3), event._4)

    // object JoystickConnected:
    //     private[sfml] def apply(event: Ptr[Self.sfJoystickConnectEvent]): JoystickConnected =
    //         JoystickConnected(event._2.toInt)

    // object JoystickDisconnected:
    //     private[sfml] def apply(event: Ptr[Self.sfJoystickConnectEvent]): JoystickDisconnected =
    //         JoystickDisconnected(event._2.toInt)

    object TouchBegan:
        private[sfml] def apply(event: Ptr[Self.sfTouchEvent]): TouchBegan =
            TouchBegan(event._2.toInt, event._3, event._4)

    object TouchMoved:
        private[sfml] def apply(event: Ptr[Self.sfTouchEvent]): TouchMoved =
            TouchMoved(event._2.toInt, event._3, event._4)

    object TouchEnded:
        private[sfml] def apply(event: Ptr[Self.sfTouchEvent]): TouchEnded =
            TouchEnded(event._2.toInt, event._3, event._4)

    object SensorChanged:
        private[sfml] def apply(event: Ptr[Self.sfSensorEvent]): SensorChanged =
            SensorChanged(Sensor.Type.fromOrdinal(event._2), event._3, event._4, event._5)
