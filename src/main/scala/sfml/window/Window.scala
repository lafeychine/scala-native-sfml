/* TODO: WindowBase */

package sfml
package window

import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.Type.{booleanToSfBool, sfBoolToBoolean, split}
import sfml.internal.window.Event.sfEvent
import sfml.internal.window.Window as Self

import sfml.system.{String, Vector2}

/** Window that serves as a target for OpenGL rendering.
  *
  * [[sfml.window.Window Window]] is the main class of the Window module.
  *
  * It defines an OS window that is able to receive an OpenGL rendering.
  *
  * A [[sfml.window.Window Window]] can create its own new window, or be embedded into an already existing control using the
  * create(handle) function. This can be useful for embedding an OpenGL rendering area into a view which is part of a bigger GUI
  * with existing windows, controls, etc. It can also serve as embedding an OpenGL rendering area into a window created by another
  * (probably richer) GUI library like Qt or wxWidgets.
  *
  * The [[sfml.window.Window Window]] class provides a simple interface for manipulating the window: move, resize, show/hide,
  * control mouse cursor, etc. It also provides event handling through its [[sfml.window.Window.pollEvent pollEvent]] function <!--
  * TODO waitEvent -->.
  *
  * Note that OpenGL experts can pass their own parameters (antialiasing level, bits for the depth and stencil buffers, etc.) to the
  * OpenGL context attached to the window, with the [[sfml.window.ContextSettings ContextSettings]] structure which is passed as an
  * optional argument when creating the window.
  *
  * <!-- TODO SFML_DEFINE_DISCRETE_GPU_PREFERENCE -->
  *
  * ```scala
  * // Declare and create a new render-window
  * val window = Window(VideoMode(800, 600), "SFML window")
  *
  * // Limit the framerate to 60 frames per second (this step is optional)
  * window.framerateLimit = 60;
  *
  * // The main loop - ends as soon as the window is closed
  * while window.isOpen() do
  *     // Event processing
  *     for event <- window.pollEvent() do
  *         event match
  *             // Request for closing the window
  *             case Event.Closed() => window.close()
  *             case _              => ()
  *
  *     <!-- TODO OpenGL rendering -->
  *
  *     // End the current frame and display its contents on screen
  *     window.display()
  * ```
  *
  * @doxygenId
  *   classsf_1_1Window
  *
  * @doxygenHash
  *   d59483b37bec6593b41bf17fb2df2426
  */
trait Window(using private[sfml] ctor: Window.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _window: ResourcePtr[Self.sfWindow] =
        ResourceBuffer(Self.dtor)(ctor)

    /* Methods */

    def close(): Unit =
        Self.close(_window.ptr)

    /** Display on screen what has been rendered to the window so far.
      *
      * This function is typically called after all OpenGL rendering has been done for the current frame, in order to show it on
      * screen.
      *
      * @doxygenId
      *   classsf_1_1Window_1adabf839cb103ac96cfc82f781638772a
      */
    def display(): Unit =
        Self.display(_window.ptr)

    // NOTE: To be able to use [`framerateLimit_=`]
    def framerateLimit = ()

    /** Limit the framerate to a maximum fixed frequency.
      *
      * If a limit is set, the window will use a small delay after each call to [[sfml.window.Window.display display]] to ensure
      * that the current frame lasted long enough to match the framerate limit. SFML will try to match the given limit as much as it
      * can, but since it internally uses sleep, whose precision depends on the underlying OS, the results may be a little unprecise
      * as well (for example, you can get 65 FPS when requesting 60).
      *
      * @param limit
      *   Framerate limit, in frames per seconds (use 0 to disable limit)
      *
      * @doxygenId
      *   classsf_1_1Window_1af4322d315baf93405bf0d5087ad5e784
      */
    def framerateLimit_=(limit: Int) =
        Self.setFramerateLimit(_window.ptr, limit.toUInt)

    /** Tell whether or not the window is open.
      *
      * This function returns whether or not the window exists. <!-- TODO: visible_= -->
      *
      * <!-- TODO: Race condition with close() -->
      *
      * @return
      *   True if the window is open, false if it has been closed
      *
      * @doxygenId
      *   classsf_1_1WindowBase_1aa43559822564ef958dc664a90c57cba0
      *
      * @doxygenHash
      *   2a224fec71af575e38f79bc88d0515f3
      */
    def isOpen(): Boolean =
        Self.isOpen(_window.ptr)

    // NOTE: To be able to use [`mouseCursorVisible_=`]
    def mouseCursorVisible = ()

    /** Show or hide the mouse cursor.
      *
      * The mouse cursor is visible by default.
      *
      * @param visible
      *   True to show the mouse cursor, false to hide it
      *
      * @doxygenId
      *   classsf_1_1WindowBase_1afa4a3372b2870294d1579d8621fe3c1a
      */
    def mouseCursorVisible_=(visible: Boolean) =
        Self.setMouseCursorVisible(_window.ptr, visible)

    /** Pop the event on top of the event queue, if any, and return it.
      *
      * This function is not blocking: if there's no pending event then the list of events is empty.
      * ```scala
      * //{
      * val window: Window = ???
      *
      * //}
      * for event <- window.pollEvent() do
      *     event match
      *         // process event...
      *         case _ => ()
      * ```
      *
      * @param event
      *   Event to be returned
      *
      * @return
      *   A lazy list of events
      *
      * <!-- TODO @see waitEvent -->
      *
      * @doxygenId
      *   classsf_1_1WindowBase_1a6a143de089c8716bd42c38c781268f7f
      *
      * @doxygenHash
      *   abf5bd114ee2d4e45c45e4cc2a1e735b
      */
    def pollEvent(): LazyList[Event] =
        def polling(event: Ptr[sfEvent]): Option[Event] =
            if Self.pollEvent(_window.ptr, event) then { Event(event) }
            else { None }

        Zone {
            val event = alloc[sfEvent]()

            LazyList.unfold(polling(event))(_.map(_ -> polling(event)))
        }

    /** Get the size of the rendering region of the window.
      *
      * The size doesn't include the titlebar and borders of the window.
      *
      * @return
      *   Size in pixels
      *
      * <!-- TODO @see size_= -->
      *
      * @doxygenId
      *   classsf_1_1WindowBase_1a188a482d916a972d59d6b0700132e379
      *
      * @doxygenHash
      *   3cf831a230bdccef91f8cbe4a7488242
      */
    def size: Vector2[Int] =
        Vector2.toVector2Int[Self.sfWindow]((data: Ptr[Self.sfWindow]) => Self.getSize(data))(_window.ptr)

    // NOTE: To be able to use [`verticalSync_=`]
    def verticalSync = ()

    /** Enable or disable vertical synchronization.
      *
      * Activating vertical synchronization will limit the number of frames displayed to the refresh rate of the monitor. This can
      * avoid some visual artifacts, and limit the framerate to a good value (but not constant across different computers).
      *
      * Vertical synchronization is disabled by default.
      *
      * @param enabled
      *   True to enable v-sync, false to deactivate it
      *
      * @doxygenId
      *   classsf_1_1Window_1a59041c4556e0351048f8aff366034f61
      */
    def verticalSync_=(enabled: Boolean) =
        Self.setVerticalSyncEnabled(_window.ptr, enabled)

object Window:

    private[sfml] inline given Conversion[Window, Ptr[Self.sfWindow]]:
        override def apply(window: Window) = window._window.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfWindow]

    object ctor:

        /** Construct a new window.
          *
          * This constructor creates the window with the size and pixel depth defined in `mode`. An optional style can be passed to
          * customize the look and behavior of the window (borders, title bar, resizable, closable, ...). If `style` contains
          * [[sfml.window.Style.Fullscreen Fullscreen]], then `mode` must be a valid video mode.
          *
          * The fourth parameter is an optional structure specifying advanced OpenGL context settings such as antialiasing,
          * depth-buffer bits, etc.
          *
          * @param mode
          *   Video mode to use (defines the width, height and depth of the rendering area of the window)
          *
          * @param title
          *   Title of the window
          *
          * @param style
          *   Window style, a bitwise OR combination of [[sfml.window.Style Style]] enumerators
          *
          * @param settings
          *   Additional settings for the underlying OpenGL context
          *
          * @doxygenId
          *   classsf_1_1Window_1a1bee771baecbae6d357871929dc042a2
          */
        def apply(
            mode: VideoMode,
            title: String,
            style: Style = Style.Default,
            settings: ContextSettings = ContextSettings()
        ): ctor =
            Constructor((r: Ptr[Self.sfWindow]) =>
                val modeSplit = split(mode)

                Self.ctor(r, modeSplit(0), modeSplit(1), title, style.value.toUInt, settings)
            )

    /** Construct a new window.
      *
      * This constructor creates the window with the size and pixel depth defined in `mode`. An optional style can be passed to
      * customize the look and behavior of the window (borders, title bar, resizable, closable, ...). If `style` contains
      * [[sfml.window.Style.Fullscreen Fullscreen]], then `mode` must be a valid video mode.
      *
      * The fourth parameter is an optional structure specifying advanced OpenGL context settings such as antialiasing, depth-buffer
      * bits, etc.
      *
      * @param mode
      *   Video mode to use (defines the width, height and depth of the rendering area of the window)
      *
      * @param title
      *   Title of the window
      *
      * @param style
      *   Window style, a bitwise OR combination of [[sfml.window.Style Style]] enumerators
      *
      * @param settings
      *   Additional settings for the underlying OpenGL context
      *
      * @doxygenId
      *   classsf_1_1Window_1a1bee771baecbae6d357871929dc042a2
      */
    def apply(mode: VideoMode, title: String, style: Style = Style.Default, settings: ContextSettings = ContextSettings()): Window =
        new Object with Window(using ctor(mode, title, style, settings))

    /* Immutable */

    extension (window: Immutable[Window])

        def isOpen(): Boolean = window.get.isOpen()

        def size: Vector2[Int] = window.get.size
