package sfml

import scala.scalanative.runtime.*
import scala.scalanative.unsafe.*

private[sfml] class VTable(val vtable: LongArray):

    def this(fptrs: CVoidPtr*) =
        this(LongArray.alloc(fptrs.length + 2))

        vtable(0) = 0 // Offset
        vtable(1) = 0 // RTTI is not supported

        for (elem, i) <- fptrs.zipWithIndex do vtable(i + 2) = elem.toLong

    inline def extendsWith(fptrs: CVoidPtr*): VTable =
        val newVTable = LongArray.alloc(vtable.length + fptrs.length)

        for i <- 0 until vtable.length do newVTable(i) = vtable(i)
        for i <- 0 until fptrs.length do newVTable(i + vtable.length) = fptrs(i).toLong

        VTable(newVTable)

    def toVPtr: VTable.VPtr = vtable.at(0).asInstanceOf[VTable.VPtr]

private[sfml] object VTable:

    private type VFunc = Ptr[Byte]

    type VTable = Ptr[VFunc]
    type VPtr = Ptr[VTable]
