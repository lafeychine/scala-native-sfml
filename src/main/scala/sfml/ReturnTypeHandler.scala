package sfml

import scala.scalanative.unsafe.*

private[sfml] object ReturnTypeHandler:
    /* Generic return type handler (working for sizeof[R] <= 8) */
    opaque type callback = Ptr[Byte]
    opaque type args = Ptr[Byte]

    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    inline def apply[R: Tag](
        inline genericHandler: (Ptr[R], callback, args) => Unit,
        inline callback: CFuncPtr0[R]
    )(): Ptr[R] =
        ResourceBuffer((r: Ptr[R]) => {
            genericHandler(r, CFuncPtr.toPtr(callback).asInstanceOf[callback], null)
        }).ptr

    inline def apply[T1, R: Tag](
        inline genericHandler: (Ptr[R], callback, args) => Unit,
        inline callback: CFuncPtr1[Ptr[T1], R]
    )(inline arg1: Ptr[T1]): Ptr[R] =
        ResourceBuffer((r: Ptr[R]) => {
            genericHandler(r, CFuncPtr.toPtr(callback).asInstanceOf[callback], arg1.asInstanceOf[args])
        }).ptr

    inline def apply[T1: Tag, T2: Tag, R: Tag](
        inline genericHandler: (Ptr[R], callback, args) => Unit,
        inline callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], R]
    )(inline arg1: T1, inline arg2: T2): Ptr[R] =
        ResourceBuffer((r: Ptr[R]) =>
            Zone {
                val args = alloc[CStruct2[T1, T2]]()
                args._1 = arg1
                args._2 = arg2

                genericHandler(r, CFuncPtr.toPtr(callback).asInstanceOf[callback], args.asInstanceOf[args])
            }
        ).ptr

    inline def apply[T1: Tag, T2: Tag, T3: Tag, R: Tag](
        inline genericHandler: (Ptr[R], callback, args) => Unit,
        inline callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], R]
    )(inline arg1: T1, inline arg2: T2, inline arg3: T3): Ptr[R] =
        ResourceBuffer((r: Ptr[R]) =>
            Zone {
                val args = alloc[CStruct3[T1, T2, T3]]()
                args._1 = arg1
                args._2 = arg2
                args._3 = arg3

                genericHandler(r, CFuncPtr.toPtr(callback).asInstanceOf[callback], args.asInstanceOf[args])
            }
        ).ptr
