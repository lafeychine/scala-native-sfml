package sfml
package internal
package system

import scala.scalanative.unsafe.*

private[sfml] object Clock:

    type sfClock = CUnsignedLongLong

    @linkCppRuntime
    @link("sfml-system")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf5ClockC2Ev")
        def ctor(result: Ptr[sfClock]): Unit = extern

        @name("___ZN2sf5ClockD2Ev")
        def dtor(self: Ptr[sfClock]): Unit = extern

        @name("___ZNK2sf5Clock14getElapsedTimeEv")
        def getElapsedTime(result: Ptr[Time.sfTime], self: Ptr[sfClock]): Unit = extern

        @name("___ZN2sf5Clock7restartEv")
        def restart(result: Ptr[Time.sfTime], self: Ptr[sfClock]): Unit = extern

    /* Public member functions */

    inline def ctor(): Ptr[Time.sfTime] => Unit =
        Bindings.ctor(_)

    inline def dtor(self: Ref[sfClock]): Unit =
        Bindings.dtor(self)

    inline def getElapsedTime(self: Ref[sfClock]): Ptr[Time.sfTime] => Unit =
        Bindings.getElapsedTime(_, self)

    inline def restart(self: Ref[sfClock]): Ptr[Time.sfTime] => Unit =
        Bindings.restart(_, self)
