package sfml
package internal
package system

import scala.scalanative.meta.LinktimeInfo
import scala.scalanative.unsafe.*

import sfml.internal.stdlib.Locale

private[sfml] object String:
    type sfString = stdlib.String.stdString

    @linkCppRuntime
    @link("sfml-system")
    @extern private object Linux:
        @name("_ZN2sf6StringC2EPKcRKSt6locale")
        def ctor(self: Ptr[sfString], string: CString, locale: Ptr[Locale.stdLocale]): Unit = extern

    @linkCppRuntime
    @link("sfml-system")
    @extern private object Mac:
        @name("_ZN2sf6StringC2EPKcRKNSt3__16localeE")
        def ctor(self: Ptr[sfString], string: CString, locale: Ptr[Locale.stdLocale]): Unit = extern

    inline def ctor(self: Ptr[sfString], string: CString, locale: Ptr[Locale.stdLocale]): Unit =
        if LinktimeInfo.isLinux then
            Linux.ctor(self, string, locale)
        else if LinktimeInfo.isMac then
            Mac.ctor(self, string, locale)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")
