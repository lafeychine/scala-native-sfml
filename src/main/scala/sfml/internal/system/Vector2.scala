package sfml
package internal
package system

import scala.scalanative.unsafe.*

private[sfml] object Vector2:
    type sfVector2[T] = T match
        case Float => CStruct2[CFloat, CFloat]
        case Int   => CStruct2[CInt, CInt]

    private object Float:
        @linkCppRuntime
        @link("sfml-system")
        @extern object Generic:
            @name("_ZN2sf7Vector2IfEC2Eff")
            def ctor(self: Ptr[sfVector2[Float]], x: CFloat, y: CFloat): Unit = extern

            @name("_ZN2sfngIfEENS_7Vector2IT_EERKS3_")
            def ng(self: Ptr[sfVector2[Float]]): sfVector2[Float] = extern

            @name("_ZN2sfplIfEENS_7Vector2IT_EERKS3_S5_")
            def pl_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): sfVector2[Float] = extern

            @name("_ZN2sfpLIfEERNS_7Vector2IT_EES4_RKS3_")
            def pL_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Ptr[sfVector2[Float]] = extern

            @name("_ZN2sfmiIfEENS_7Vector2IT_EERKS3_S5_")
            def mi_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): sfVector2[Float] = extern

            @name("_ZN2sfmIIfEERNS_7Vector2IT_EES4_RKS3_")
            def mI_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Ptr[sfVector2[Float]] = extern

            @name("_ZN2sfmlIfEENS_7Vector2IT_EERKS3_S2_")
            def ml_float(self: Ptr[sfVector2[Float]], rhs: CFloat): sfVector2[Float] = extern

            @name("_ZN2sfmLIfEERNS_7Vector2IT_EES4_S2_")
            def mL_float(self: Ptr[sfVector2[Float]], rhs: CFloat): Ptr[sfVector2[Float]] = extern

            @name("_ZN2sfdvIfEENS_7Vector2IT_EERKS3_S2_")
            def dv_float(self: Ptr[sfVector2[Float]], rhs: CFloat): sfVector2[Float] = extern

            @name("_ZN2sfdVIfEERNS_7Vector2IT_EES4_S2_")
            def dV_float(self: Ptr[sfVector2[Float]], rhs: CFloat): Ptr[sfVector2[Float]] = extern

            @name("_ZN2sfeqIfEEbRKNS_7Vector2IT_EES5_")
            def eq_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Type.sfBool = extern

            @name("_ZN2sfneIfEEbRKNS_7Vector2IT_EES5_")
            def ne_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Type.sfBool = extern

            @name("_Z22glue_returnTypeHandlerPN2sf7Vector2IfEEPFS1_PvES3_")
            def typeHandler(self: Ptr[sfVector2[Float]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit = extern

            @name("_Z22glue_inline_sfVector2fRN2sf7Vector2IfEE")
            def inlineHandler(self: Ptr[sfVector2[Float]]): sfVector2[Float] = extern

    private object Int:
        @linkCppRuntime
        @link("sfml-system")
        @extern object Generic:
            @name("_ZN2sf7Vector2IiEC2Eii")
            def ctor(self: Ptr[sfVector2[Int]], x: CInt, y: CInt): Unit = extern

            @name("_ZN2sfngIiEENS_7Vector2IT_EERKS3_")
            def ng(self: Ptr[sfVector2[Int]]): sfVector2[Int] = extern

            @name("_ZN2sfplIiEENS_7Vector2IT_EERKS3_S5_")
            def pl_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): sfVector2[Int] = extern

            @name("_ZN2sfpLIiEERNS_7Vector2IT_EES4_RKS3_")
            def pL_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Ptr[sfVector2[Int]] = extern

            @name("_ZN2sfmiIiEENS_7Vector2IT_EERKS3_S5_")
            def mi_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): sfVector2[Int] = extern

            @name("_ZN2sfmIIiEERNS_7Vector2IT_EES4_RKS3_")
            def mI_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Ptr[sfVector2[Int]] = extern

            @name("_ZN2sfmlIiEENS_7Vector2IT_EERKS3_S2_")
            def ml_int(self: Ptr[sfVector2[Int]], rhs: CInt): sfVector2[Int] = extern

            @name("_ZN2sfmLIiEERNS_7Vector2IT_EES4_S2_")
            def mL_int(self: Ptr[sfVector2[Int]], rhs: CInt): Ptr[sfVector2[Int]] = extern

            @name("_ZN2sfdvIiEENS_7Vector2IT_EERKS3_S2_")
            def dv_int(self: Ptr[sfVector2[Int]], rhs: CInt): sfVector2[Int] = extern

            @name("_ZN2sfdVIiEERNS_7Vector2IT_EES4_S2_")
            def dV_int(self: Ptr[sfVector2[Int]], rhs: CInt): Ptr[sfVector2[Int]] = extern

            @name("_ZN2sfeqIiEEbRKNS_7Vector2IT_EES5_")
            def eq_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Type.sfBool = extern

            @name("_ZN2sfneIiEEbRKNS_7Vector2IT_EES5_")
            def ne_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Type.sfBool = extern

            @name("_Z22glue_returnTypeHandlerPN2sf7Vector2IiEEPFS1_PvES3_")
            def typeHandler(self: Ptr[sfVector2[Int]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit = extern

            @name("_Z22glue_inline_sfVector2iRN2sf7Vector2IiEE")
            def inlineHandler(self: Ptr[sfVector2[Int]]): sfVector2[Int] = extern

    inline def sfVector2f_ctor(self: Ptr[sfVector2[Float]], x: CFloat, y: CFloat): Unit =
        Float.Generic.ctor(self, x, y)

    inline def sfVector2i_ctor(self: Ptr[sfVector2[Int]], x: CInt, y: CInt): Unit =
        Int.Generic.ctor(self, x, y)

    inline def sfVector2f_ng(self: Ptr[sfVector2[Float]]): sfVector2[Float] =
        Float.Generic.ng(self)

    inline def sfVector2i_ng(self: Ptr[sfVector2[Int]]): sfVector2[Int] =
        Int.Generic.ng(self)

    inline def sfVector2f_pl_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): sfVector2[Float] =
        Float.Generic.pl_sfVector2f(self, rhs)

    inline def sfVector2i_pl_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): sfVector2[Int] =
        Int.Generic.pl_sfVector2i(self, rhs)

    inline def sfVector2f_pL_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Ptr[sfVector2[Float]] =
        Float.Generic.pL_sfVector2f(self, rhs)

    inline def sfVector2i_pL_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Ptr[sfVector2[Int]] =
        Int.Generic.pL_sfVector2i(self, rhs)

    inline def sfVector2f_mi_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): sfVector2[Float] =
        Float.Generic.mi_sfVector2f(self, rhs)

    inline def sfVector2i_mi_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): sfVector2[Int] =
        Int.Generic.mi_sfVector2i(self, rhs)

    inline def sfVector2f_mI_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Ptr[sfVector2[Float]] =
        Float.Generic.mI_sfVector2f(self, rhs)

    inline def sfVector2i_mI_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Ptr[sfVector2[Int]] =
        Int.Generic.mI_sfVector2i(self, rhs)

    inline def sfVector2f_ml_float(self: Ptr[sfVector2[Float]], rhs: CFloat): sfVector2[Float] =
        Float.Generic.ml_float(self, rhs)

    inline def sfVector2i_ml_int(self: Ptr[sfVector2[Int]], rhs: CInt): sfVector2[Int] =
        Int.Generic.ml_int(self, rhs)

    inline def sfVector2f_mL_float(self: Ptr[sfVector2[Float]], rhs: CFloat): Ptr[sfVector2[Float]] =
        Float.Generic.mL_float(self, rhs)

    inline def sfVector2i_mL_int(self: Ptr[sfVector2[Int]], rhs: CInt): Ptr[sfVector2[Int]] =
        Int.Generic.mL_int(self, rhs)

    inline def sfVector2f_dv_float(self: Ptr[sfVector2[Float]], rhs: CFloat): sfVector2[Float] =
        Float.Generic.dv_float(self, rhs)

    inline def sfVector2i_dv_int(self: Ptr[sfVector2[Int]], rhs: CInt): sfVector2[Int] =
        Int.Generic.dv_int(self, rhs)

    inline def sfVector2f_dV_float(self: Ptr[sfVector2[Float]], rhs: CFloat): Ptr[sfVector2[Float]] =
        Float.Generic.dV_float(self, rhs)

    inline def sfVector2i_dV_int(self: Ptr[sfVector2[Int]], rhs: CInt): Ptr[sfVector2[Int]] =
        Int.Generic.dV_int(self, rhs)

    inline def sfVector2f_eq_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Type.sfBool =
        Float.Generic.eq_sfVector2f(self, rhs)

    inline def sfVector2i_eq_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Type.sfBool =
        Int.Generic.eq_sfVector2i(self, rhs)

    inline def sfVector2f_ne_sfVector2f(self: Ptr[sfVector2[Float]], rhs: Ptr[sfVector2[Float]]): Type.sfBool =
        Float.Generic.ne_sfVector2f(self, rhs)

    inline def sfVector2i_ne_sfVector2i(self: Ptr[sfVector2[Int]], rhs: Ptr[sfVector2[Int]]): Type.sfBool =
        Int.Generic.ne_sfVector2i(self, rhs)

    inline def sfVector2f_typeHandler(self: Ptr[sfVector2[Float]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit =
        Float.Generic.typeHandler(self, callback, args)

    inline def sfVector2i_typeHandler(self: Ptr[sfVector2[Int]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit =
        Int.Generic.typeHandler(self, callback, args)

    inline def sfVector2f_inlineHandler(self: Ptr[sfVector2[Float]]): sfVector2[Float] =
        Float.Generic.inlineHandler(self)

    inline def sfVector2i_inlineHandler(self: Ptr[sfVector2[Int]]): sfVector2[Int] =
        Int.Generic.inlineHandler(self)
