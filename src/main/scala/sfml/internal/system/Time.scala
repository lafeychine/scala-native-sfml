package sfml
package internal
package system

import scala.scalanative.unsafe.*

private[sfml] object Time:

    type sfTime = CUnsignedLongLong

    @linkCppRuntime
    @link("sfml-system")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf4TimeC2Ev")
        def ctor(result: Ptr[sfTime]): Unit = extern

        @name("___ZN2sf4TimeD2Ev")
        def dtor(result: Ptr[sfTime]): Unit = extern

        @name("___ZNK2sf4Time9asSecondsEv")
        def asSeconds(self: sfTime): CFloat = extern

        @name("___ZNK2sf4Time14asMillisecondsEv")
        def asMilliseconds(self: sfTime): CInt = extern

        @name("___ZNK2sf4Time14asMicrosecondsEv")
        def asMicroseconds(self: sfTime): CLongLong = extern

        /* Static public attributes */

        @name("___ZN2sf4Time4ZeroE")
        def Zero(result: Ptr[sfTime]): Unit = extern

        /* Related symbols */

        @name("___ZN2sf7secondsEf")
        def seconds(result: Ptr[sfTime], seconds: CFloat): Unit = extern

        @name("___ZN2sf12millisecondsEi")
        def milliseconds(result: Ptr[sfTime], milliseconds: CInt): Unit = extern

        @name("___ZN2sf12microsecondsEx")
        def microseconds(result: Ptr[sfTime], microseconds: CLongLong): Unit = extern

        @name("___ZN2sfeqENS_4TimeES0_")
        def eq_sfTime(self: sfTime, rhs: sfTime): CxxBool = extern

        @name("___ZN2sfneENS_4TimeES0_")
        def ne_sfTime(self: sfTime, rhs: sfTime): CxxBool = extern

        @name("___ZN2sfltENS_4TimeES0_")
        def lt_sfTime(self: sfTime, rhs: sfTime): CxxBool = extern

        @name("___ZN2sfgtENS_4TimeES0_")
        def gt_sfTime(self: sfTime, rhs: sfTime): CxxBool = extern

        @name("___ZN2sfleENS_4TimeES0_")
        def le_sfTime(self: sfTime, rhs: sfTime): CxxBool = extern

        @name("___ZN2sfgeENS_4TimeES0_")
        def ge_sfTime(self: sfTime, rhs: sfTime): CxxBool = extern

        @name("___ZN2sfngENS_4TimeE")
        def ng(result: Ptr[sfTime], self: sfTime): Unit = extern

        @name("___ZN2sfplENS_4TimeES0_")
        def pl_sfTime(result: Ptr[sfTime], self: sfTime, rhs: sfTime): Unit = extern

        @name("___ZN2sfpLERNS_4TimeES0_")
        def pL_sfTime(self: Ptr[sfTime], rhs: sfTime): Unit = extern

        @name("___ZN2sfmiENS_4TimeES0_")
        def mi_sfTime(result: Ptr[sfTime], self: sfTime, rhs: sfTime): Unit = extern

        @name("___ZN2sfmIERNS_4TimeES0_")
        def mI_sfTime(self: Ptr[sfTime], rhs: sfTime): Unit = extern

        @name("___ZN2sfmlENS_4TimeEf")
        def ml_float(result: Ptr[sfTime], self: sfTime, rhs: CFloat): Unit = extern

        @name("___ZN2sfmlENS_4TimeEx")
        def ml_longlong(result: Ptr[sfTime], self: sfTime, rhs: CLongLong): Unit = extern

        @name("___ZN2sfmLERNS_4TimeEf")
        def mL_float(self: Ptr[sfTime], rhs: CFloat): Unit = extern

        @name("___ZN2sfmLERNS_4TimeEx")
        def mL_longlong(self: Ptr[sfTime], rhs: CLongLong): Unit = extern

        @name("___ZN2sfdvENS_4TimeEf")
        def dv_float(result: Ptr[sfTime], self: sfTime, rhs: CFloat): Unit = extern

        @name("___ZN2sfdvENS_4TimeEx")
        def dv_longlong(result: Ptr[sfTime], self: sfTime, rhs: CLongLong): Unit = extern

        @name("___ZN2sfdVERNS_4TimeEf")
        def dV_float(self: Ptr[sfTime], rhs: CFloat): Unit = extern

        @name("___ZN2sfdVERNS_4TimeEx")
        def dV_longlong(self: Ptr[sfTime], rhs: CLongLong): Unit = extern

        @name("___ZN2sfdvENS_4TimeES0_")
        def dv_sfTime(self: sfTime, rhs: sfTime): CFloat = extern

        @name("___ZN2sfrmENS_4TimeES0_")
        def rm_sfTime(result: Ptr[sfTime], self: sfTime, rhs: sfTime): Unit = extern

        @name("___ZN2sfrMERNS_4TimeES0_")
        def rM_sfTime(self: Ptr[sfTime], rhs: sfTime): Unit = extern

    /* Public member functions */

    inline def ctor(): Ptr[sfTime] => Unit =
        Bindings.ctor(_)

    inline def dtor(self: Ref[sfTime]): Unit =
        Bindings.dtor(self)

    inline def asSeconds(self: Ref[Const[sfTime]]): Float =
        Bindings.asSeconds(!self)

    inline def asMilliseconds(self: Ref[Const[sfTime]]): Int =
        Bindings.asMilliseconds(!self)

    inline def asMicroseconds(self: Ref[Const[sfTime]]): Long =
        Bindings.asMicroseconds(!self)

    /* Static public attributes */

    inline def Zero(): Ptr[sfTime] => Unit =
        Bindings.Zero(_)

    /* Related symbols */

    inline def seconds(seconds: Float): Ptr[sfTime] => Unit =
        Bindings.seconds(_, seconds)

    inline def milliseconds(milliseconds: Int): Ptr[sfTime] => Unit =
        Bindings.milliseconds(_, milliseconds)

    inline def microseconds(microseconds: Long): Ptr[sfTime] => Unit =
        Bindings.microseconds(_, microseconds)

    inline def eq_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Boolean =
        Bindings.eq_sfTime(!self, !rhs)

    inline def ne_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Boolean =
        Bindings.ne_sfTime(!self, !rhs)

    inline def lt_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Boolean =
        Bindings.lt_sfTime(!self, !rhs)

    inline def gt_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Boolean =
        Bindings.gt_sfTime(!self, !rhs)

    inline def le_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Boolean =
        Bindings.le_sfTime(!self, !rhs)

    inline def ge_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Boolean =
        Bindings.ge_sfTime(!self, !rhs)

    inline def ng(self: Ref[Const[sfTime]]): Ptr[sfTime] => Unit =
        Bindings.ng(_, !self)

    inline def pl_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Ptr[sfTime] => Unit =
        Bindings.pl_sfTime(_, !self, !rhs)

    inline def pL_sfTime(self: Ref[sfTime], rhs: Ref[Const[sfTime]]): Unit =
        Bindings.pL_sfTime(self, !rhs)

    inline def mi_sfTime(self: Ref[Const[sfTime]], rhs: Ref[Const[sfTime]]): Ptr[sfTime] => Unit =
        Bindings.mi_sfTime(_, !self, !rhs)

    inline def mI_sfTime(self: Ref[sfTime], rhs: Ref[Const[sfTime]]): Unit =
        Bindings.mI_sfTime(self, !rhs)

    inline def ml_float(self: Ref[Const[sfTime]], rhs: CFloat): Ptr[sfTime] => Unit =
        Bindings.ml_float(_, !self, rhs)

    inline def ml_longlong(self: Ref[Const[sfTime]], rhs: CLongLong): Ptr[sfTime] => Unit =
        Bindings.ml_longlong(_, !self, rhs)

    inline def mL_float(self: Ref[sfTime], rhs: CFloat): Unit =
        Bindings.mL_float(self, rhs)

    inline def mL_longlong(self: Ref[sfTime], rhs: CLongLong): Unit =
        Bindings.mL_longlong(self, rhs)

    inline def dv_float(self: Ref[Const[sfTime]], rhs: CFloat): Ptr[sfTime] => Unit =
        Bindings.dv_float(_, !self, rhs)

    inline def dv_longlong(self: Ref[Const[sfTime]], rhs: CLongLong): Ptr[sfTime] => Unit =
        Bindings.dv_longlong(_, !self, rhs)

    inline def dV_float(self: Ref[sfTime], rhs: CFloat): Unit =
        Bindings.dV_float(self, rhs)

    inline def dV_longlong(self: Ref[sfTime], rhs: CLongLong): Unit =
        Bindings.dV_longlong(self, rhs)
