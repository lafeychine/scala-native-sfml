package sfml
package internal
package system

import scala.scalanative.unsafe.*

private[sfml] object Vector3:
    type sfVector3[T] = T match
        case Float => CStruct3[CFloat, CFloat, CFloat]
        case Int   => CStruct3[CInt, CInt, CInt]

    private object Float:
        @linkCppRuntime
        @link("sfml-system")
        @extern object Generic:
            @name("_ZN2sf7Vector3IfEC2Efff")
            def ctor(self: Ptr[sfVector3[Float]], x: CFloat, y: CFloat, z: CFloat): Unit = extern

            @name("_ZN2sfngIfEENS_7Vector3IT_EERKS3_")
            def ng(self: Ptr[sfVector3[Float]]): sfVector3[Float] = extern

            @name("_ZN2sfplIfEENS_7Vector3IT_EERKS3_S5_")
            def pl_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): sfVector3[Float] = extern

            @name("_ZN2sfpLIfEERNS_7Vector3IT_EES4_RKS3_")
            def pL_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Ptr[sfVector3[Float]] = extern

            @name("_ZN2sfmiIfEENS_7Vector3IT_EERKS3_S5_")
            def mi_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): sfVector3[Float] = extern

            @name("_ZN2sfmIIfEERNS_7Vector3IT_EES4_RKS3_")
            def mI_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Ptr[sfVector3[Float]] = extern

            @name("_ZN2sfmlIfEENS_7Vector3IT_EERKS3_S2_")
            def ml_float(self: Ptr[sfVector3[Float]], rhs: CFloat): sfVector3[Float] = extern

            @name("_ZN2sfmLIfEERNS_7Vector3IT_EES4_S2_")
            def mL_float(self: Ptr[sfVector3[Float]], rhs: CFloat): Ptr[sfVector3[Float]] = extern

            @name("_ZN2sfdvIfEENS_7Vector3IT_EERKS3_S2_")
            def dv_float(self: Ptr[sfVector3[Float]], rhs: CFloat): sfVector3[Float] = extern

            @name("_ZN2sfdVIfEERNS_7Vector3IT_EES4_S2_")
            def dV_float(self: Ptr[sfVector3[Float]], rhs: CFloat): Ptr[sfVector3[Float]] = extern

            @name("_ZN2sfeqIfEEbRKNS_7Vector3IT_EES5_")
            def eq_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Type.sfBool = extern

            @name("_ZN2sfneIfEEbRKNS_7Vector3IT_EES5_")
            def ne_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Type.sfBool = extern

            @name("_Z22glue_returnTypeHandlerPN2sf7Vector3IfEEPFS1_PvES3_")
            def typeHandler(self: Ptr[sfVector3[Float]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit = extern

    private object Int:
        @linkCppRuntime
        @link("sfml-system")
        @extern object Generic:
            @name("_ZN2sf7Vector3IiEC2Eiii")
            def ctor(self: Ptr[sfVector3[Int]], x: CInt, y: CInt, z: CInt): Unit = extern

            @name("_ZN2sfngIiEENS_7Vector3IT_EERKS3_")
            def ng(self: Ptr[sfVector3[Int]]): sfVector3[Int] = extern

            @name("_ZN2sfplIiEENS_7Vector3IT_EERKS3_S5_")
            def pl_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): sfVector3[Int] = extern

            @name("_ZN2sfpLIiEERNS_7Vector3IT_EES4_RKS3_")
            def pL_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Ptr[sfVector3[Int]] = extern

            @name("_ZN2sfmiIiEENS_7Vector3IT_EERKS3_S5_")
            def mi_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): sfVector3[Int] = extern

            @name("_ZN2sfmIIiEERNS_7Vector3IT_EES4_RKS3_")
            def mI_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Ptr[sfVector3[Int]] = extern

            @name("_ZN2sfmlIiEENS_7Vector3IT_EERKS3_S2_")
            def ml_int(self: Ptr[sfVector3[Int]], rhs: CInt): sfVector3[Int] = extern

            @name("_ZN2sfmLIiEERNS_7Vector3IT_EES4_S2_")
            def mL_int(self: Ptr[sfVector3[Int]], rhs: CInt): Ptr[sfVector3[Int]] = extern

            @name("_ZN2sfdvIiEENS_7Vector3IT_EERKS3_S2_")
            def dv_int(self: Ptr[sfVector3[Int]], rhs: CInt): sfVector3[Int] = extern

            @name("_ZN2sfdVIiEERNS_7Vector3IT_EES4_S2_")
            def dV_int(self: Ptr[sfVector3[Int]], rhs: CInt): Ptr[sfVector3[Int]] = extern

            @name("_ZN2sfeqIiEEbRKNS_7Vector3IT_EES5_")
            def eq_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Type.sfBool = extern

            @name("_ZN2sfneIiEEbRKNS_7Vector3IT_EES5_")
            def ne_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Type.sfBool = extern

            @name("_Z22glue_returnTypeHandlerPN2sf7Vector3IiEEPFS1_PvES3_")
            def typeHandler(self: Ptr[sfVector3[Int]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit = extern

    inline def sfVector3f_ctor(self: Ptr[sfVector3[Float]], x: CFloat, y: CFloat, z: CFloat): Unit =
        Float.Generic.ctor(self, x, y, z)

    inline def sfVector3i_ctor(self: Ptr[sfVector3[Int]], x: CInt, y: CInt, z: CInt): Unit =
        Int.Generic.ctor(self, x, y, z)

    inline def sfVector3f_ng(self: Ptr[sfVector3[Float]]): sfVector3[Float] =
        Float.Generic.ng(self)

    inline def sfVector3i_ng(self: Ptr[sfVector3[Int]]): sfVector3[Int] =
        Int.Generic.ng(self)

    inline def sfVector3f_pl_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): sfVector3[Float] =
        Float.Generic.pl_sfVector3f(self, rhs)

    inline def sfVector3i_pl_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): sfVector3[Int] =
        Int.Generic.pl_sfVector3i(self, rhs)

    inline def sfVector3f_pL_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Ptr[sfVector3[Float]] =
        Float.Generic.pL_sfVector3f(self, rhs)

    inline def sfVector3i_pL_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Ptr[sfVector3[Int]] =
        Int.Generic.pL_sfVector3i(self, rhs)

    inline def sfVector3f_mi_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): sfVector3[Float] =
        Float.Generic.mi_sfVector3f(self, rhs)

    inline def sfVector3i_mi_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): sfVector3[Int] =
        Int.Generic.mi_sfVector3i(self, rhs)

    inline def sfVector3f_mI_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Ptr[sfVector3[Float]] =
        Float.Generic.mI_sfVector3f(self, rhs)

    inline def sfVector3i_mI_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Ptr[sfVector3[Int]] =
        Int.Generic.mI_sfVector3i(self, rhs)

    inline def sfVector3f_ml_float(self: Ptr[sfVector3[Float]], rhs: CFloat): sfVector3[Float] =
        Float.Generic.ml_float(self, rhs)

    inline def sfVector3i_ml_int(self: Ptr[sfVector3[Int]], rhs: CInt): sfVector3[Int] =
        Int.Generic.ml_int(self, rhs)

    inline def sfVector3f_mL_float(self: Ptr[sfVector3[Float]], rhs: CFloat): Ptr[sfVector3[Float]] =
        Float.Generic.mL_float(self, rhs)

    inline def sfVector3i_mL_int(self: Ptr[sfVector3[Int]], rhs: CInt): Ptr[sfVector3[Int]] =
        Int.Generic.mL_int(self, rhs)

    inline def sfVector3f_dv_float(self: Ptr[sfVector3[Float]], rhs: CFloat): sfVector3[Float] =
        Float.Generic.dv_float(self, rhs)

    inline def sfVector3f_dV_float(self: Ptr[sfVector3[Float]], rhs: CFloat): Ptr[sfVector3[Float]] =
        Float.Generic.dV_float(self, rhs)

    inline def sfVector3i_dv_int(self: Ptr[sfVector3[Int]], rhs: CInt): sfVector3[Int] =
        Int.Generic.dv_int(self, rhs)

    inline def sfVector3i_dV_int(self: Ptr[sfVector3[Int]], rhs: CInt): Ptr[sfVector3[Int]] =
        Int.Generic.dV_int(self, rhs)

    inline def sfVector3f_eq_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Type.sfBool =
        Float.Generic.eq_sfVector3f(self, rhs)

    inline def sfVector3i_eq_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Type.sfBool =
        Int.Generic.eq_sfVector3i(self, rhs)

    inline def sfVector3f_ne_sfVector3f(self: Ptr[sfVector3[Float]], rhs: Ptr[sfVector3[Float]]): Type.sfBool =
        Float.Generic.ne_sfVector3f(self, rhs)

    inline def sfVector3i_ne_sfVector3i(self: Ptr[sfVector3[Int]], rhs: Ptr[sfVector3[Int]]): Type.sfBool =
        Int.Generic.ne_sfVector3i(self, rhs)

    inline def sfVector3f_typeHandler(self: Ptr[sfVector3[Float]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit =
        Float.Generic.typeHandler(self, callback, args)

    inline def sfVector3i_typeHandler(self: Ptr[sfVector3[Int]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit =
        Int.Generic.typeHandler(self, callback, args)
