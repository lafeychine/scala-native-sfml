package sfml
package internal
package window

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2

private[sfml] object Mouse:
    @linkCppRuntime
    @link("sfml-window")
    @extern private object Generic:
        @name("_ZN2sf5Mouse15isButtonPressedENS0_6ButtonE")
        def isButtonPressed(button: CInt): Type.sfBool = extern

        @name("_ZN2sf5Mouse11getPositionEv")
        def getPosition(): Vector2.sfVector2[Int] = extern

        @name("_ZN2sf5Mouse11getPositionERKNS_10WindowBaseE")
        def getPosition(relativeTo: Ptr[Window.sfWindow]): Vector2.sfVector2[Int] = extern

        @name("_ZN2sf5Mouse11setPositionERKNS_7Vector2IiEE")
        def setPosition(position: Ptr[Vector2.sfVector2[Int]]): Unit = extern

        @name("_ZN2sf5Mouse11setPositionERKNS_7Vector2IiEERKNS_10WindowBaseE")
        def setPosition(position: Ptr[Vector2.sfVector2[Int]], relativeTo: Ptr[Window.sfWindow]): Unit = extern

    inline def isButtonPressed(button: CInt): Type.sfBool =
        Generic.isButtonPressed(button)

    inline def getPosition(): Vector2.sfVector2[Int] =
        Generic.getPosition()

    inline def getPosition(relativeTo: Ptr[Window.sfWindow]): Vector2.sfVector2[Int] =
        Generic.getPosition(relativeTo)

    inline def setPosition(position: Ptr[Vector2.sfVector2[Int]]): Unit =
        Generic.setPosition(position)

    inline def setPosition(position: Ptr[Vector2.sfVector2[Int]], relativeTo: Ptr[Window.sfWindow]): Unit =
        Generic.setPosition(position, relativeTo)
