package sfml
package internal
package window

import scala.scalanative.unsafe.*

private[sfml] object ContextSettings:
    type sfContextSettings = CStruct7[CUnsignedInt, CUnsignedInt, CUnsignedInt, CUnsignedInt, CUnsignedInt, CUnsignedInt, CChar]
