package sfml
package internal
package window

import scala.scalanative.unsafe.*

import sfml.internal.system.{String, Vector2}

private[sfml] object Window:
    type sfWindow = CArray[Byte, Nat.Digit2[Nat._4, Nat._8]]

    @linkCppRuntime
    @link("sfml-window")
    @extern private object Generic:
        @name("_ZN2sf6WindowC2ENS_9VideoModeERKNS_6StringEjRKNS_15ContextSettingsE")
        def ctor(self: Ptr[sfWindow], modeHigh: Type.sfSplit[VideoMode.sfVideoMode], modeLow: Type.sfSplit[VideoMode.sfVideoMode], title: Ptr[String.sfString], style: Type.sfUint32, settings: Ptr[ContextSettings.sfContextSettings]): Unit = extern

        @name("_ZN2sf6WindowD2Ev")
        def dtor(self: Ptr[sfWindow]): Unit = extern

        @name("_ZN2sf6Window5closeEv")
        def close(self: Ptr[sfWindow]): Unit = extern

        @name("_ZN2sf6Window7displayEv")
        def display(self: Ptr[sfWindow]): Unit = extern

        @name("_ZN2sf6Window17setFramerateLimitEj")
        def setFramerateLimit(self: Ptr[sfWindow], limit: CUnsignedInt): Unit = extern

        @name("_ZNK2sf10WindowBase6isOpenEv")
        def isOpen(self: Ptr[sfWindow]): Type.sfBool = extern

        @name("_ZN2sf10WindowBase21setMouseCursorVisibleEb")
        def setMouseCursorVisible(self: Ptr[sfWindow], visible: Type.sfBool): Unit = extern

        @name("_ZN2sf10WindowBase9pollEventERNS_5EventE")
        def pollEvent(self: Ptr[sfWindow], event: Ptr[Event.sfEvent]): Type.sfBool = extern

        @name("_ZNK2sf10WindowBase7getSizeEv")
        def getSize(self: Ptr[sfWindow]): Vector2.sfVector2[Int] = extern

        @name("_ZN2sf6Window22setVerticalSyncEnabledEb")
        def setVerticalSyncEnabled(self: Ptr[sfWindow], enabled: Type.sfBool): Unit = extern

    inline def ctor(self: Ptr[sfWindow], modeHigh: Type.sfSplit[VideoMode.sfVideoMode], modeLow: Type.sfSplit[VideoMode.sfVideoMode], title: Ptr[String.sfString], style: Type.sfUint32, settings: Ptr[ContextSettings.sfContextSettings]): Unit =
        Generic.ctor(self, modeHigh, modeLow, title, style, settings)

    inline def dtor(self: Ptr[sfWindow]): Unit =
        Generic.dtor(self)

    inline def close(self: Ptr[sfWindow]): Unit =
        Generic.close(self)

    inline def display(self: Ptr[sfWindow]): Unit =
        Generic.display(self)

    inline def setFramerateLimit(self: Ptr[sfWindow], limit: CUnsignedInt): Unit =
        Generic.setFramerateLimit(self, limit)

    inline def isOpen(self: Ptr[sfWindow]): Type.sfBool =
        Generic.isOpen(self)

    inline def setMouseCursorVisible(self: Ptr[sfWindow], visible: Type.sfBool): Unit =
        Generic.setMouseCursorVisible(self, visible)

    inline def pollEvent(self: Ptr[sfWindow], event: Ptr[Event.sfEvent]): Type.sfBool =
        Generic.pollEvent(self, event)

    inline def getSize(self: Ptr[sfWindow]): Vector2.sfVector2[Int] =
        Generic.getSize(self)

    inline def setVerticalSyncEnabled(self: Ptr[sfWindow], enabled: Type.sfBool): Unit =
        Generic.setVerticalSyncEnabled(self, enabled)
