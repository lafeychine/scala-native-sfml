package sfml
package internal
package window

import scala.scalanative.unsafe.*

private[sfml] object VideoMode:
    type sfVideoMode = CStruct3[CUnsignedInt, CUnsignedInt, CUnsignedInt]

    @linkCppRuntime
    @link("sfml-window")
    @extern private object Generic:
        @name("_ZN2sf9VideoModeC2Ev")
        def ctor(self: Ptr[sfVideoMode]): Unit = extern

        @name("_ZN2sf9VideoModeC2Ejjj")
        def ctor(self: Ptr[sfVideoMode], width: CUnsignedInt, height: CUnsignedInt, bitsPerPixel: CUnsignedInt): Unit = extern

        @name("_ZNK2sf9VideoMode7isValidEv")
        def isValid(self: Ptr[sfVideoMode]): Type.sfBool = extern

        @name("_ZN2sfltERKNS_9VideoModeES2_")
        def lt_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool = extern

        @name("_ZN2sfleERKNS_9VideoModeES2_")
        def le_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool = extern

        @name("_ZN2sfgtERKNS_9VideoModeES2_")
        def gt_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool = extern

        @name("_ZN2sfgeERKNS_9VideoModeES2_")
        def ge_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool = extern

        @name("_ZN2sfeqERKNS_9VideoModeES2_")
        def eq_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool = extern

        @name("_ZN2sfneERKNS_9VideoModeES2_")
        def ne_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool = extern

    inline def ctor(self: Ptr[sfVideoMode]): Unit =
        Generic.ctor(self)

    inline def ctor(self: Ptr[sfVideoMode], width: CUnsignedInt, height: CUnsignedInt, bitsPerPixel: CUnsignedInt): Unit =
        Generic.ctor(self, width, height, bitsPerPixel)

    inline def isValid(self: Ptr[sfVideoMode]): Type.sfBool =
        Generic.isValid(self)

    inline def lt_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool =
        Generic.lt_sfVideoMode(self, rhs)

    inline def le_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool =
        Generic.le_sfVideoMode(self, rhs)

    inline def gt_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool =
        Generic.gt_sfVideoMode(self, rhs)

    inline def ge_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool =
        Generic.ge_sfVideoMode(self, rhs)

    inline def eq_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool =
        Generic.eq_sfVideoMode(self, rhs)

    inline def ne_sfVideoMode(self: Ptr[sfVideoMode], rhs: Ptr[sfVideoMode]): Type.sfBool =
        Generic.ne_sfVideoMode(self, rhs)
