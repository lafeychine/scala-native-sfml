package sfml
package internal
package stdlib

import scala.scalanative.meta.LinktimeInfo
import scala.scalanative.unsafe.*

private[sfml] object Locale:
    type stdLocale = CArray[Byte, Nat._8]

    @extern private object Linux:
        @name("_ZNSt6localeC2Ev")
        def ctor(self: Ptr[stdLocale]): Unit = extern

    @extern private object Mac:
        @name("_ZNSt3__16localeC2Ev")
        def ctor(self: Ptr[stdLocale]): Unit = extern

    inline def ctor(self: Ptr[stdLocale]): Unit =
        if LinktimeInfo.isLinux then
            Linux.ctor(self)
        else if LinktimeInfo.isMac then
            Mac.ctor(self)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")
