package sfml
package internal
package stdlib

import scala.scalanative.unsafe.*

private[sfml] object String:
    type stdString = CArray[Byte, Nat.Digit2[Nat._3, Nat._2]]

    @extern private object Generic:
        @name("_ZNSt6StringC2EPKc")
        def ctor(self: Ptr[stdString], cstr: CString): Unit = extern

        @name("_ZNSt6StringD2Ev")
        def dtor(self: Ptr[stdString]): Unit = extern

    inline def ctor(self: Ptr[stdString], cstr: CString): Unit =
        Generic.ctor(self, cstr)

    inline def dtor(self: Ptr[stdString]): Unit =
        Generic.dtor(self)
