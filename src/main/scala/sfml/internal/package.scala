package sfml

import scala.scalanative.unsafe.*

package object internal:

    type Const[T] = T
    type Ref[T] = Ptr[T]

    type CxxBool = CChar

    val cxxFalse: CxxBool = 0
    val cxxTrue: CxxBool = 1

    inline given Conversion[Boolean, CxxBool]:
        override def apply(bool: Boolean): CxxBool = if bool then cxxTrue else cxxFalse

    inline given Conversion[CxxBool, Boolean]:
        override def apply(bool: CxxBool): Boolean = bool != cxxFalse
