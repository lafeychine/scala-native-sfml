package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String

private[sfml] object Image:

    type sfImage = CArray[Byte, Nat.Digit2[Nat._3, Nat._2]]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf5ImageD2Ev")
        def dtor(self: Ptr[sfImage]): Unit = extern

        @name("___ZNK2sf5Image10saveToFileERKSs")
        def saveToFile(self: Ptr[sfImage], filename: Ptr[String.stdString]): CxxBool = extern

    /* Public member functions */

    inline def dtor(self: Ref[sfImage]): Unit =
        Bindings.dtor(self)

    inline def saveToFile(self: Ref[Const[sfImage]], filename: Ref[Const[String.stdString]]): Boolean =
        Bindings.saveToFile(self, filename)
