package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.stdlib.Vector

private[sfml] object VertexArray:
    type sfVertexArray = CStruct3[
        CArray[Byte, Nat._8],
        Vector.stdVector,
        CArray[Byte, Nat._8]
    ]
