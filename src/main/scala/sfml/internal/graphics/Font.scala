package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String

private[sfml] object Font:

    type sfFont = CArray[Byte, Nat.Digit3[Nat._1, Nat._4, Nat._4]]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf4FontC2Ev")
        def ctor(result: Ptr[sfFont]): Unit = extern

        @name("___ZN2sf4FontD2Ev")
        def dtor(self: Ptr[sfFont]): Unit = extern

        @name("___ZN2sf4Font12loadFromFileERKSs")
        def loadFromFile(self: Ptr[sfFont], filename: Ptr[String.stdString]): CxxBool = extern

    /* Public member functions */

    inline def ctor(): Ptr[sfFont] => Unit =
        Bindings.ctor(_)

    inline def dtor(self: Ref[sfFont]): Unit =
        Bindings.dtor(self)

    inline def loadFromFile(self: Ref[sfFont], filename: Ref[Const[String.stdString]]): Boolean =
        Bindings.loadFromFile(self, filename)
