package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2

private[sfml] object View:

    type sfView = CArray[Byte, Nat.Digit3[Nat._1, Nat._6, Nat._8]]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf4ViewC2Ev")
        def ctor(result: Ptr[sfView]): Unit = extern

        @name("___ZN2sf4ViewC2ERKNS_4RectIfEE")
        def ctor(result: Ptr[sfView], rectangle: Ptr[Rect.sfRect[Float]]): Unit = extern

        @name("___ZN2sf4ViewC2ERKNS_7Vector2IfEES4_")
        def ctor(result: Ptr[sfView], center: Ptr[Vector2.sfVector2[Float]], size: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("___ZN2sf4ViewD2Ev")
        def dtor(self: Ptr[sfView]): Unit = extern

        @name("___ZN2sf4View9setCenterEff")
        def setCenter(self: Ptr[sfView], x: Float, y: Float): Unit = extern

        @name("___ZN2sf4View9setCenterERKNS_7Vector2IfEE")
        def setCenter(self: Ptr[sfView], center: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("___ZN2sf4View7setSizeEff")
        def setSize(self: Ptr[sfView], width: Float, height: Float): Unit = extern

        @name("___ZN2sf4View7setSizeERKNS_7Vector2IfEE")
        def setSize(self: Ptr[sfView], size: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("___ZN2sf4View11setRotationEf")
        def setRotation(self: Ptr[sfView], angle: Float): Unit = extern

        @name("___ZN2sf4View11setViewportERKNS_4RectIfEE")
        def setViewport(self: Ptr[sfView], viewport: Ptr[Rect.sfRect[Float]]): Unit = extern

        @name("___ZN2sf4View5resetERKNS_4RectIfEE")
        def reset(self: Ptr[sfView], rectangle: Ptr[Rect.sfRect[Float]]): Unit = extern

        @name("___ZNK2sf4View9getCenterEv")
        def getCenter(self: Ptr[sfView]): Ptr[Vector2.sfVector2[Float]] = extern

        @name("___ZNK2sf4View7getSizeEv")
        def getSize(self: Ptr[sfView]): Ptr[Vector2.sfVector2[Float]] = extern

        @name("___ZNK2sf4View11getRotationEv")
        def getRotation(self: Ptr[sfView]): Float = extern

        @name("___ZNK2sf4View11getViewportEv")
        def getViewport(self: Ptr[sfView]): Ptr[Rect.sfRect[Float]] = extern

        @name("___ZN2sf4View4moveEff")
        def move(self: Ptr[sfView], offsetX: Float, offsetY: Float): Unit = extern

        @name("___ZN2sf4View4moveERKNS_7Vector2IfEE")
        def move(self: Ptr[sfView], offset: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("___ZN2sf4View6rotateEf")
        def rotate(self: Ptr[sfView], angle: Float): Unit = extern

        @name("___ZN2sf4View4zoomEf")
        def zoom(self: Ptr[sfView], factor: Float): Unit = extern

        @name("___ZNK2sf4View12getTransformEv")
        def getTransform(self: Ptr[sfView]): Ptr[Transform.sfTransform] = extern

        @name("___ZNK2sf4View19getInverseTransformEv")
        def getInverseTransform(self: Ptr[sfView]): Ptr[Transform.sfTransform] = extern

    /* Public member functions */

    inline def ctor(): Ptr[sfView] => Unit =
        Bindings.ctor(_)

    inline def ctor(rectangle: Ref[Const[Rect.sfRect[Float]]]): Ptr[sfView] => Unit =
        Bindings.ctor(_, rectangle)

    inline def ctor(center: Ref[Const[Vector2.sfVector2[Float]]], size: Ref[Const[Vector2.sfVector2[Float]]]): Ptr[sfView] => Unit =
        Bindings.ctor(_, center, size)

    inline def dtor(self: Ref[sfView]): Unit =
        Bindings.dtor(self)

    inline def setCenter(self: Ref[sfView], x: Float, y: Float): Unit =
        Bindings.setCenter(self, x, y)

    inline def setCenter(self: Ref[sfView], center: Ptr[Vector2.sfVector2[Float]]): Unit =
        Bindings.setCenter(self, center)

    inline def setSize(self: Ref[sfView], width: Float, height: Float): Unit =
        Bindings.setSize(self, width, height)

    inline def setSize(self: Ref[sfView], size: Ref[Const[Vector2.sfVector2[Float]]]): Unit =
        Bindings.setSize(self, size)

    inline def setRotation(self: Ref[sfView], angle: Float): Unit =
        Bindings.setRotation(self, angle)

    inline def setViewport(self: Ref[sfView], viewport: Ref[Const[Rect.sfRect[Float]]]): Unit =
        Bindings.setViewport(self, viewport)

    inline def reset(self: Ref[sfView], rectangle: Ref[Const[Rect.sfRect[Float]]]): Unit =
        Bindings.reset(self, rectangle)

    inline def getCenter(self: Ref[Const[sfView]]): Ref[Const[Vector2.sfVector2[Float]]] =
        Bindings.getCenter(self)

    inline def getSize(self: Ref[Const[sfView]]): Ref[Const[Vector2.sfVector2[Float]]] =
        Bindings.getSize(self)

    inline def getRotation(self: Ref[Const[sfView]]): Float =
        Bindings.getRotation(self)

    inline def getViewport(self: Ref[Const[sfView]]): Ref[Const[Rect.sfRect[Float]]] =
        Bindings.getViewport(self)

    inline def move(self: Ref[sfView], offsetX: Float, offsetY: Float): Unit =
        Bindings.move(self, offsetX, offsetY)

    inline def move(self: Ref[sfView], offset: Ref[Const[Vector2.sfVector2[Float]]]): Unit =
        Bindings.move(self, offset)

    inline def rotate(self: Ref[sfView], angle: Float): Unit =
        Bindings.rotate(self, angle)

    inline def zoom(self: Ref[sfView], factor: Float): Unit =
        Bindings.zoom(self, factor)

    inline def getTransform(self: Ref[Const[sfView]]): Ref[Const[Transform.sfTransform]] =
        Bindings.getTransform(self)

    inline def getInverseTransform(self: Ref[Const[sfView]]): Ref[Const[Transform.sfTransform]] =
        Bindings.getInverseTransform(self)
