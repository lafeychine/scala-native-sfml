package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

private[sfml] object BlendMode:
    type sfFactor = CInt
    type sfEquation = CInt
    type sfBlendMode = CStruct6[sfFactor, sfFactor, sfEquation, sfFactor, sfFactor, sfEquation]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf9BlendModeC2Ev")
        def ctor(self: Ptr[sfBlendMode]): Unit = extern

        @name("_ZN2sf9BlendModeC2ENS0_6FactorES1_NS0_8EquationE")
        def ctor(self: Ptr[sfBlendMode], sourceFactor: sfFactor, destinationFactor: sfFactor, blendEquation: sfEquation): Unit = extern

        @name("_ZN2sf9BlendModeC2ENS0_6FactorES1_NS0_8EquationES1_S1_S2_")
        def ctor(self: Ptr[sfBlendMode], colorSrcFactor: sfFactor, colorDstFactor: sfFactor, colorEquation: sfEquation, alphaSrcFactor: sfFactor, alphaDstFactor: sfFactor, alphaEquation: sfEquation): Unit = extern

    inline def ctor(self: Ptr[sfBlendMode]): Unit =
        Generic.ctor(self)

    inline def ctor(self: Ptr[sfBlendMode], sourceFactor: sfFactor, destinationFactor: sfFactor, blendEquation: sfEquation): Unit =
        Generic.ctor(self, sourceFactor, destinationFactor, blendEquation)

    inline def ctor(self: Ptr[sfBlendMode], colorSrcFactor: sfFactor, colorDstFactor: sfFactor, colorEquation: sfEquation, alphaSrcFactor: sfFactor, alphaDstFactor: sfFactor, alphaEquation: sfEquation): Unit =
        Generic.ctor(self, colorSrcFactor, colorDstFactor, colorEquation, alphaSrcFactor, alphaDstFactor, alphaEquation)
