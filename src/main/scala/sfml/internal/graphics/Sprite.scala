package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

private[sfml] object Sprite:
    type sfSprite = CStruct3[
        Drawable.sfDrawable,
        Transformable.sfTransformable,
        CArray[Byte, Nat.Digit3[Nat._1, Nat._0, Nat._4]]
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf6SpriteC2Ev")
        def ctor(self: Ptr[sfSprite]): Unit = extern

        @name("_ZN2sf6SpriteC2ERKNS_7TextureE")
        def ctor(self: Ptr[sfSprite], texture: Ptr[Texture.sfTexture]): Unit = extern

        @name("_ZNK2sf6Sprite8getColorEv")
        def getColor(self: Ptr[sfSprite]): Ptr[Color.sfColor] = extern

        @name("_ZN2sf6Sprite8setColorERKNS_5ColorE")
        def setColor(self: Ptr[sfSprite], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZN2sf6Sprite10setTextureERKNS_7TextureEb")
        def setTexture(self: Ptr[sfSprite], texture: Ptr[Texture.sfTexture], resetRect: Type.sfBool): Unit = extern

        @name("_ZNK2sf6Sprite14getTextureRectEv")
        def getTextureRect(self: Ptr[sfSprite]): Ptr[Rect.sfRect[Int]] = extern

        @name("_ZN2sf6Sprite14setTextureRectERKNS_4RectIiEE")
        def setTextureRect(self: Ptr[sfSprite], rect: Ptr[Rect.sfRect[Int]]): Unit = extern

    def ctor(self: Ptr[sfSprite]): Unit =
        Generic.ctor(self)

    def ctor(self: Ptr[sfSprite], texture: Ptr[Texture.sfTexture]): Unit =
        Generic.ctor(self, texture)

    def getColor(self: Ptr[sfSprite]): Ptr[Color.sfColor] =
        Generic.getColor(self)

    def setColor(self: Ptr[sfSprite], color: Ptr[Color.sfColor]): Unit =
        Generic.setColor(self, color)

    def setTexture(self: Ptr[sfSprite], texture: Ptr[Texture.sfTexture], resetRect: Type.sfBool): Unit =
        Generic.setTexture(self, texture, resetRect)

    def getTextureRect(self: Ptr[sfSprite]): Ptr[Rect.sfRect[Int]] =
        Generic.getTextureRect(self)

    def setTextureRect(self: Ptr[sfSprite], rect: Ptr[Rect.sfRect[Int]]): Unit =
        Generic.setTextureRect(self, rect)
