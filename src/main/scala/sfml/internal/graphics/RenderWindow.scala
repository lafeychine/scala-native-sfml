package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.{String, Vector2}
import sfml.internal.window.{ContextSettings, VideoMode, Window}

private[sfml] object RenderWindow:

    type sfRenderWindow = CStruct2[Window.sfWindow, RenderTarget.sfRenderTarget]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf12RenderWindowC2ENS_9VideoModeERKNS_6StringEjRKNS_15ContextSettingsE")
        def ctor(result: Ptr[sfRenderWindow], mode: Ref[Const[VideoMode.sfVideoMode]], title: Ptr[String.sfString], style: CUnsignedInt, settings: Ptr[ContextSettings.sfContextSettings]): Unit = extern

        @name("___ZN2sf12RenderWindowD2Ev")
        def dtor(self: Ptr[sfRenderWindow]): Unit = extern

        @name("___ZNK2sf12RenderWindow7getSizeEv")
        def getSize(result: Ptr[Vector2.sfVector2[Int]], self: Ref[Const[sfRenderWindow]]): Unit = extern

    /* Public member functions */

    inline def ctor(mode: Ref[Const[VideoMode.sfVideoMode]], title: Ref[Const[String.sfString]], style: CUnsignedInt, settings: Ref[Const[ContextSettings.sfContextSettings]]): Ptr[sfRenderWindow] => Unit =
        Bindings.ctor(_, mode, title, style, settings)

    inline def dtor(self: Ref[sfRenderWindow]): Unit =
        Bindings.dtor(self)

    inline def getSize(self: Ref[Const[sfRenderWindow]]): Ptr[Vector2.sfVector2[Int]] => Unit =
        Bindings.getSize(_, self)
