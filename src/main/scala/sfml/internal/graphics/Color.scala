package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

private[sfml] object Color:
    type sfColor = CStruct4[Type.sfUint8, Type.sfUint8, Type.sfUint8, Type.sfUint8]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf5ColorC2Ev")
        def ctor(color: Ptr[sfColor]): Unit = extern

        @name("_ZN2sf5ColorC2Ehhhh")
        def ctor(color: Ptr[sfColor], r: Type.sfUint8, g: Type.sfUint8, b: Type.sfUint8, a: Type.sfUint8): Unit = extern

        @name("_ZN2sfeqERKNS_5ColorES2_")
        def eq_sfColor(self: Ptr[sfColor], rhs: Ptr[sfColor]): Type.sfBool = extern

        @name("_ZN2sfneERKNS_5ColorES2_")
        def ne_sfColor(self: Ptr[sfColor], rhs: Ptr[sfColor]): Type.sfBool = extern

    inline def ctor(color: Ptr[sfColor]): Unit =
        Generic.ctor(color)

    inline def ctor(color: Ptr[sfColor], r: Type.sfUint8, g: Type.sfUint8, b: Type.sfUint8, a: Type.sfUint8): Unit =
        Generic.ctor(color, r, g, b, a)

    inline def eq_sfColor(self: Ptr[sfColor], rhs: Ptr[sfColor]): Type.sfBool =
        Generic.eq_sfColor(self, rhs)

    inline def ne_sfColor(self: Ptr[sfColor], rhs: Ptr[sfColor]): Type.sfBool =
        Generic.ne_sfColor(self, rhs)
