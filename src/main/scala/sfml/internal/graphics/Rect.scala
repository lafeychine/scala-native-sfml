package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2.sfVector2

private[sfml] object Rect:
    type sfRect[T] = T match
        case Float => CStruct4[CFloat, CFloat, CFloat, CFloat]
        case Int => CStruct4[CInt, CInt, CInt, CInt]

    object Float:
        @linkCppRuntime
        @link("sfml-system")
        @extern object Generic:
            @name("_ZN2sf4RectIfEC2Effff")
            def ctor(self: Ptr[sfRect[Float]], rectLeft: CFloat, rectTop: CFloat, rectWidth: CFloat, rectHeight: CFloat): Unit = extern

            @name("_ZN2sf4RectIfEC2ERKNS_7Vector2IfEES5_")
            def ctor(self: Ptr[sfRect[Float]], position: Ptr[sfVector2[Float]], size: Ptr[sfVector2[Float]]): Unit = extern

            @name("_ZNK2sf4RectIfE8containsEff")
            def contains(self: Ptr[sfRect[Float]], x: CFloat, y: CFloat): Type.sfBool = extern

            @name("_ZNK2sf4RectIfE8containsERKNS_7Vector2IfEE")
            def contains(self: Ptr[sfRect[Float]], point: Ptr[sfVector2[Float]]): Type.sfBool = extern

            @name("_ZNK2sf4RectIfE10intersectsERKS1_RS1_")
            def intersects(self: Ptr[sfRect[Float]], rectangle: Ptr[sfRect[Float]], intersection: Ptr[sfRect[Float]]): Type.sfBool = extern

            @name("_ZN2sfeqIfEEbRKNS_4RectIT_EES5_")
            def eq_sfFloatRect(self: Ptr[sfRect[Float]], right: Ptr[sfRect[Float]]): Type.sfBool = extern

            @name("_ZN2sfneIfEEbRKNS_4RectIT_EES5_")
            def ne_sfFloatRect(self: Ptr[sfRect[Float]], right: Ptr[sfRect[Float]]): Type.sfBool = extern

            @name("_Z22glue_returnTypeHandlerPN2sf4RectIfEEPFS1_PvES3_")
            def typeHandler(self: Ptr[sfRect[Float]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit = extern

        inline def ctor(self: Ptr[sfRect[Float]], rectLeft: CFloat, rectTop: CFloat, rectWidth: CFloat, rectHeight: CFloat): Unit =
            Float.Generic.ctor(self, rectLeft, rectTop, rectWidth, rectHeight)

        inline def ctor(self: Ptr[sfRect[Float]], position: Ptr[sfVector2[Float]], size: Ptr[sfVector2[Float]]): Unit =
            Float.Generic.ctor(self, position, size)

        inline def contains(self: Ptr[sfRect[Float]], x: CFloat, y: CFloat): Type.sfBool =
            Float.Generic.contains(self, x, y)

        inline def contains(self: Ptr[sfRect[Float]], point: Ptr[sfVector2[Float]]): Type.sfBool =
            Float.Generic.contains(self, point)

        inline def intersects(self: Ptr[sfRect[Float]], rectangle: Ptr[sfRect[Float]], intersection: Ptr[sfRect[Float]]): Type.sfBool =
            Float.Generic.intersects(self, rectangle, intersection)

        inline def eq_sfFloatRect(self: Ptr[sfRect[Float]], right: Ptr[sfRect[Float]]): Type.sfBool =
            Float.Generic.eq_sfFloatRect(self, right)

        inline def ne_sfFloatRect(self: Ptr[sfRect[Float]], right: Ptr[sfRect[Float]]): Type.sfBool =
            Float.Generic.ne_sfFloatRect(self, right)

        inline def typeHandler(self: Ptr[sfRect[Float]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit =
            Float.Generic.typeHandler(self, callback, args)


    object Int:
        @linkCppRuntime
        @link("sfml-system")
        @extern object Generic:
            @name("_ZN2sf4RectIiEC2Eiiii")
            def ctor(self: Ptr[sfRect[Int]], rectLeft: CInt, rectTop: CInt, rectWidth: CInt, rectHeight: CInt): Unit = extern

            @name("_ZN2sf4RectIiEC2ERKNS_7Vector2IiEES5_")
            def ctor(self: Ptr[sfRect[Int]], position: Ptr[sfVector2[Int]], size: Ptr[sfVector2[Int]]): Unit = extern

            @name("_ZNK2sf4RectIiE8containsEii")
            def contains(self: Ptr[sfRect[Int]], x: CInt, y: CInt): Type.sfBool = extern

            @name("_ZNK2sf4RectIiE8containsERKNS_7Vector2IiEE")
            def contains(self: Ptr[sfRect[Int]], point: Ptr[sfVector2[Int]]): Type.sfBool = extern

            @name("_ZNK2sf4RectIiE10intersectsERKS1_RS1_")
            def intersects(self: Ptr[sfRect[Int]], rectangle: Ptr[sfRect[Int]], intersection: Ptr[sfRect[Int]]): Type.sfBool = extern

            @name("_ZN2sfeqIiEEbRKNS_4RectIT_EES5_")
            def eq_sfIntRect(self: Ptr[sfRect[Int]], right: Ptr[sfRect[Int]]): Type.sfBool = extern

            @name("_ZN2sfneIiEEbRKNS_4RectIT_EES5_")
            def ne_sfIntRect(self: Ptr[sfRect[Int]], right: Ptr[sfRect[Int]]): Type.sfBool = extern

            @name("_Z22glue_returnTypeHandlerPN2sf4RectIiEEPFS1_PvES3_")
            def typeHandler(self: Ptr[sfRect[Int]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit = extern

        inline def ctor(self: Ptr[sfRect[Int]], rectLeft: CInt, rectTop: CInt, rectWidth: CInt, rectHeight: CInt): Unit =
            Int.Generic.ctor(self, rectLeft, rectTop, rectWidth, rectHeight)

        inline def ctor(self: Ptr[sfRect[Int]], position: Ptr[sfVector2[Int]], size: Ptr[sfVector2[Int]]): Unit =
            Int.Generic.ctor(self, position, size)

        inline def contains(self: Ptr[sfRect[Int]], x: CInt, y: CInt): Type.sfBool =
            Int.Generic.contains(self, x, y)

        inline def contains(self: Ptr[sfRect[Int]], point: Ptr[sfVector2[Int]]): Type.sfBool =
            Int.Generic.contains(self, point)

        inline def intersects(self: Ptr[sfRect[Int]], rectangle: Ptr[sfRect[Int]], intersection: Ptr[sfRect[Int]]): Type.sfBool =
            Int.Generic.intersects(self, rectangle, intersection)

        inline def eq_sfIntRect(self: Ptr[sfRect[Int]], right: Ptr[sfRect[Int]]): Type.sfBool =
            Int.Generic.eq_sfIntRect(self, right)

        inline def ne_sfIntRect(self: Ptr[sfRect[Int]], right: Ptr[sfRect[Int]]): Type.sfBool =
            Int.Generic.ne_sfIntRect(self, right)

        inline def typeHandler(self: Ptr[sfRect[Int]], callback: ReturnTypeHandler.callback, args: ReturnTypeHandler.args): Unit =
            Int.Generic.typeHandler(self, callback, args)
