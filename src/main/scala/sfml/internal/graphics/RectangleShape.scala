package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2

private[sfml] object RectangleShape:
    type sfRectangleShape = CStruct2[
        Shape.sfShape,
        CArray[Byte, Nat._8]
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf14RectangleShapeC2ERKNS_7Vector2IfEE")
        def ctor(self: Ptr[sfRectangleShape], size: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZN2sf5ShapeD2Ev")
        def dtor(self: Ptr[sfRectangleShape]): Unit = extern

        @name("_ZNK2sf14RectangleShape8getPointEm")
        def getPoint(self: Ptr[sfRectangleShape], index: CSize): Vector2.sfVector2[Float] = extern

        @name("_ZNK2sf14RectangleShape13getPointCountEv")
        def getPointCount(self: Ptr[sfRectangleShape]): CSize = extern

        @name("_ZNK2sf14RectangleShape7getSizeEv")
        def getSize(self: Ptr[sfRectangleShape]): Ptr[Vector2.sfVector2[Float]] = extern

        @name("_ZN2sf14RectangleShape7setSizeERKNS_7Vector2IfEE")
        def setSize(self: Ptr[sfRectangleShape], size: Ptr[Vector2.sfVector2[Float]]): Unit = extern

    inline def ctor(self: Ptr[sfRectangleShape], size: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.ctor(self, size)

    inline def dtor(self: Ptr[sfRectangleShape]): Unit =
        Generic.dtor(self)

    inline def getPoint(self: Ptr[sfRectangleShape], index: CSize): Vector2.sfVector2[Float] =
        Generic.getPoint(self, index)

    inline def getPointCount(self: Ptr[sfRectangleShape]): CSize =
        Generic.getPointCount(self)

    inline def getSize(self: Ptr[sfRectangleShape]): Ptr[Vector2.sfVector2[Float]] =
        Generic.getSize(self)

    inline def setSize(self: Ptr[sfRectangleShape], size: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.setSize(self, size)
