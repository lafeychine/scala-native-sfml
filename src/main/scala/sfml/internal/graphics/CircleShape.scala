package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2

private[sfml] object CircleShape:
    type sfCircleShape = CStruct2[
        Shape.sfShape,
        CArray[Byte, Nat.Digit2[Nat._1, Nat._6]]
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf11CircleShapeC2Efm")
        def ctor(self: Ptr[sfCircleShape], radius: CFloat, pointCount: CSize): Unit = extern

        @name("_ZN2sf5ShapeD2Ev")
        def dtor(self: Ptr[sfCircleShape]): Unit = extern

        @name("_ZNK2sf11CircleShape8getPointEm")
        def getPoint(self: Ptr[sfCircleShape], index: CSize): Vector2.sfVector2[Float] = extern

        @name("_ZNK2sf11CircleShape13getPointCountEv")
        def getPointCount(self: Ptr[sfCircleShape]): CSize = extern

        @name("_ZN2sf11CircleShape13setPointCountEm")
        def setPointCount(self: Ptr[sfCircleShape], count: CSize): CSize = extern

        @name("_ZNK2sf11CircleShape9getRadiusEv")
        def getRadius(self: Ptr[sfCircleShape]): CFloat = extern

        @name("_ZN2sf11CircleShape9setRadiusEf")
        def setRadius(self: Ptr[sfCircleShape], radius: CFloat): Unit = extern

    inline def ctor(self: Ptr[sfCircleShape], radius: CFloat, pointCount: CSize): Unit =
        Generic.ctor(self, radius, pointCount)

    inline def dtor(self: Ptr[sfCircleShape]): Unit =
        Generic.dtor(self)

    inline def getPoint(self: Ptr[sfCircleShape], index: CSize): Vector2.sfVector2[Float] =
        Generic.getPoint(self, index)

    inline def getPointCount(self: Ptr[sfCircleShape]): CSize =
        Generic.getPointCount(self)

    inline def setPointCount(self: Ptr[sfCircleShape], count: CSize): CSize =
        Generic.setPointCount(self, count)

    inline def getRadius(self: Ptr[sfCircleShape]): CFloat =
        Generic.getRadius(self)

    inline def setRadius(self: Ptr[sfCircleShape], radius: CFloat): Unit =
        Generic.setRadius(self, radius)
