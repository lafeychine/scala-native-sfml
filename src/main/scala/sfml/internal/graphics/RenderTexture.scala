package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2
import sfml.internal.window.ContextSettings

private[sfml] object RenderTexture:
    type sfRenderTexture = CStruct3[
        RenderTarget.sfRenderTarget,
        Ptr[Byte],
        Texture.sfTexture
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf13RenderTextureC2Ev")
        def ctor(self: Ptr[sfRenderTexture]): Unit = extern

        @name("_ZN2sf13RenderTextureD2Ev")
        def dtor(self: Ptr[sfRenderTexture]): Unit = extern

        @name("_ZN2sf13RenderTexture6createEjjb")
        def create(self: Ptr[sfRenderTexture], width: CUnsignedInt, height: CUnsignedInt, depthBuffer: Type.sfBool): Type.sfBool = extern

        @name("_ZN2sf13RenderTexture6createEjjRKNS_15ContextSettingsE")
        def create(self: Ptr[sfRenderTexture], width: CUnsignedInt, height: CUnsignedInt, settings: Ptr[ContextSettings.sfContextSettings]): Type.sfBool = extern

        @name("_ZN2sf13RenderTexture7displayEv")
        def display(self: Ptr[sfRenderTexture]): Unit = extern

        @name("_ZNK2sf13RenderTexture7getSizeEv")
        def getSize(self: Ptr[sfRenderTexture]): Vector2.sfVector2[Int] = extern

        @name("_ZNK2sf13RenderTexture10getTextureEv")
        def getTexture(self: Ptr[sfRenderTexture]): Ptr[Texture.sfTexture] = extern

    inline def ctor(self: Ptr[sfRenderTexture]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfRenderTexture]): Unit =
        Generic.dtor(self)

    inline def create(self: Ptr[sfRenderTexture], width: CUnsignedInt, height: CUnsignedInt, depthBuffer: Type.sfBool): Type.sfBool =
        Generic.create(self, width, height, depthBuffer)

    inline def create(self: Ptr[sfRenderTexture], width: CUnsignedInt, height: CUnsignedInt, settings: Ptr[ContextSettings.sfContextSettings]): Type.sfBool =
        Generic.create(self, width, height, settings)

    inline def display(self: Ptr[sfRenderTexture]): Unit =
        Generic.display(self)

    inline def getSize(self: Ptr[sfRenderTexture]): Vector2.sfVector2[Int] =
        Generic.getSize(self)

    inline def getTexture(self: Ptr[sfRenderTexture]): Ptr[Texture.sfTexture] =
        Generic.getTexture(self)
