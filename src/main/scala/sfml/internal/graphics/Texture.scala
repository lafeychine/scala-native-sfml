package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String
import sfml.internal.system.Vector2
import sfml.internal.window.Window

private[sfml] object Texture:

    type sfTexture = CStruct10[
        Vector2.sfVector2[Int],
        Vector2.sfVector2[Int],
        CUnsignedInt,
        Type.sfBool,
        Type.sfBool,
        Type.sfBool,
        Type.sfBool,
        Type.sfBool,
        Type.sfBool,
        Type.sfUint64
    ];

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Bindings:

        /* Public member functions */

        @name("___ZN2sf7TextureC2Ev")
        def ctor(self: Ptr[sfTexture]): Unit = extern

        @name("___ZN2sf7TextureD2Ev")
        def dtor(self: Ptr[sfTexture]): Unit = extern

        @name("___ZN2sf7Texture6createEjj")
        def create(self: Ptr[sfTexture], width: CUnsignedInt, height: CUnsignedInt): CxxBool = extern

        @name("___ZN2sf7Texture12loadFromFileERKSsRKNS_4RectIiEE")
        def loadFromFile(self: Ptr[sfTexture], filename: Ptr[String.stdString], area: Ptr[Rect.sfRect[Int]]): CxxBool = extern

        @name("___ZNK2sf7Texture11copyToImageEv")
        def copyToImage(result: Ptr[Image.sfImage], self: Ptr[sfTexture]): Unit = extern

        @name("___ZN2sf7Texture6updateERKNS_6WindowE")
        def update(self: Ptr[sfTexture], window: Ptr[Window.sfWindow]): Unit = extern

        @name("___ZN2sf7Texture6updateERKNS_6WindowEjj")
        def update(self: Ptr[sfTexture], window: Ptr[Window.sfWindow], x: CUnsignedInt, y: CUnsignedInt): Unit = extern

        @name("___ZN2sf7Texture9setSmoothEb")
        def setSmooth(self: Ptr[sfTexture], smooth: CxxBool): Unit = extern

        @name("___ZNK2sf7Texture8isSmoothEv")
        def isSmooth(self: Ptr[sfTexture]): CxxBool = extern

        @name("___ZN2sf7Texture11setRepeatedEb")
        def setRepeated(self: Ptr[sfTexture], repeated: CxxBool): Unit = extern

        @name("___ZNK2sf7Texture10isRepeatedEv")
        def isRepeated(self: Ptr[sfTexture]): CxxBool = extern

    /* Public member functions */

    inline def ctor(self: Ptr[sfTexture]): Unit =
        Bindings.ctor(self)

    inline def dtor(self: Ref[sfTexture]): Unit =
        Bindings.dtor(self)

    inline def create(self: Ref[sfTexture], width: CUnsignedInt, height: CUnsignedInt): Boolean =
        Bindings.create(self, width, height)

    inline def loadFromFile(self: Ref[sfTexture], filename: Ref[Const[String.stdString]], area: Ref[Const[Rect.sfRect[Int]]]): Boolean =
        Bindings.loadFromFile(self, filename, area)

    inline def copyToImage(self: Ref[Const[sfTexture]]): Ptr[Image.sfImage] => Unit =
        Bindings.copyToImage(_, self)

    inline def update(self: Ref[sfTexture], window: Ref[Const[Window.sfWindow]]): Unit =
        Bindings.update(self, window)

    inline def update(self: Ref[sfTexture], window: Ref[Const[Window.sfWindow]], x: CUnsignedInt, y: CUnsignedInt): Unit =
        Bindings.update(self, window, x, y)

    inline def setSmooth(self: Ref[sfTexture], smooth: Boolean): Unit =
        Bindings.setSmooth(self, smooth)

    inline def isSmooth(self: Ref[Const[sfTexture]]): Boolean =
        Bindings.isSmooth(self)

    inline def setRepeated(self: Ref[sfTexture], repeated: Boolean): Unit =
        Bindings.setRepeated(self, repeated)

    inline def isRepeated(self: Ref[Const[sfTexture]]): Boolean =
        Bindings.isRepeated(self)
