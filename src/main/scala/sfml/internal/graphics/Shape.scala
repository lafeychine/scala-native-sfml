package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

private[sfml] object Shape:
    type sfShape = CStruct6[
        VTable.VPtr,
        Transformable.sfTransformable,
        CArray[Byte, Nat.Digit2[Nat._4, Nat._0]],
        VertexArray.sfVertexArray,
        VertexArray.sfVertexArray,
        CArray[Byte, Nat.Digit2[Nat._3, Nat._2]]
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf5ShapeC2Ev")
        def ctor(self: Ptr[sfShape]): Unit = extern

        @name("_ZN2sf5ShapeD2Ev")
        def dtor(self: Ptr[sfShape]): Unit = extern

        @name("_ZNK2sf5Shape15getGlobalBoundsEv")
        def getGlobalBounds(self: Ptr[sfShape]): Rect.sfRect[Float] = extern

        @name("_ZNK2sf5Shape14getLocalBoundsEv")
        def getLocalBounds(self: Ptr[sfShape]): Rect.sfRect[Float] = extern

        @name("_ZN2sf5Shape6updateEv")
        def update(self: Ptr[sfShape]): Unit = extern

        @name("_ZNK2sf5Shape12getFillColorEv")
        def getFillColor(self: Ptr[sfShape]): Ptr[Color.sfColor] = extern

        @name("_ZN2sf5Shape12setFillColorERKNS_5ColorE")
        def setFillColor(self: Ptr[sfShape], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZNK2sf5Shape15getOutlineColorEv")
        def getOutlineColor(self: Ptr[sfShape]): Ptr[Color.sfColor] = extern

        @name("_ZN2sf5Shape15setOutlineColorERKNS_5ColorE")
        def setOutlineColor(self: Ptr[sfShape], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZNK2sf5Shape19getOutlineThicknessEv")
        def getOutlineThickness(self: Ptr[sfShape]): Float = extern

        @name("_ZN2sf5Shape19setOutlineThicknessEf")
        def setOutlineThickness(self: Ptr[sfShape], thickness: Float): Unit = extern

        @name("_ZNK2sf5Shape10getTextureEv")
        def getTexture(self: Ptr[sfShape]): Ptr[Texture.sfTexture] = extern

        @name("_ZN2sf5Shape10setTextureEPKNS_7TextureEb")
        def setTexture(self: Ptr[sfShape], texture: Ptr[Texture.sfTexture], resetRect: Type.sfBool): Unit = extern

        @name("_ZNK2sf5Shape14getTextureRectEv")
        def getTextureRect(self: Ptr[sfShape]): Ptr[Rect.sfRect[Int]] = extern

        @name("_ZN2sf5Shape14setTextureRectERKNS_4RectIiEE")
        def setTextureRect(self: Ptr[sfShape], rect: Ptr[Rect.sfRect[Int]]): Unit = extern

    inline def ctor(self: Ptr[sfShape]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfShape]): Unit =
        Generic.dtor(self)

    inline def getGlobalBounds(self: Ptr[sfShape]): Rect.sfRect[Float] =
        Generic.getGlobalBounds(self)

    inline def getLocalBounds(self: Ptr[sfShape]): Rect.sfRect[Float] =
        Generic.getLocalBounds(self)

    inline def update(self: Ptr[sfShape]): Unit =
        Generic.update(self)

    inline def getFillColor(self: Ptr[sfShape]): Ptr[Color.sfColor] =
        Generic.getFillColor(self)

    inline def setFillColor(self: Ptr[sfShape], color: Ptr[Color.sfColor]): Unit =
        Generic.setFillColor(self, color)

    inline def getOutlineColor(self: Ptr[sfShape]): Ptr[Color.sfColor] =
        Generic.getOutlineColor(self)

    inline def setOutlineColor(self: Ptr[sfShape], color: Ptr[Color.sfColor]): Unit =
        Generic.setOutlineColor(self, color)

    inline def getOutlineThickness(self: Ptr[sfShape]): Float =
        Generic.getOutlineThickness(self)

    inline def setOutlineThickness(self: Ptr[sfShape], thickness: Float): Unit =
        Generic.setOutlineThickness(self, thickness)

    inline def getTexture(self: Ptr[sfShape]): Ptr[Texture.sfTexture] =
        Generic.getTexture(self)

    inline def setTexture(self: Ptr[sfShape], texture: Ptr[Texture.sfTexture], resetRect: Type.sfBool): Unit =
        Generic.setTexture(self, texture, resetRect)

    inline def getTextureRect(self: Ptr[sfShape]): Ptr[Rect.sfRect[Int]] =
        Generic.getTextureRect(self)

    inline def setTextureRect(self: Ptr[sfShape], rect: Ptr[Rect.sfRect[Int]]): Unit =
        Generic.setTextureRect(self, rect)
