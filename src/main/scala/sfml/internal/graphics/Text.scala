package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.String

private[sfml] object Text:
    type sfText = CStruct8[
        VTable.VPtr,
        Transformable.sfTransformable,
        String.sfString,
        CArray[Byte, Nat.Digit2[Nat._4, Nat._0]],
        VertexArray.sfVertexArray,
        VertexArray.sfVertexArray,
        Rect.sfRect[Float],
        CArray[Byte, Nat.Digit2[Nat._1, Nat._6]]
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf4TextC2Ev")
        def ctor(self: Ptr[sfText]): Unit = extern

        @name("_ZN2sf4TextC2ERKNS_6StringERKNS_4FontEj")
        def ctor(self: Ptr[sfText], string: Ptr[String.sfString], font: Ptr[Font.sfFont], characterSize: CUnsignedInt): Unit = extern

        @name("_ZNK2sf4Text20ensureGeometryUpdateEv")
        def ensureGeometryUpdate(self: Ptr[sfText]): Unit = extern

        // @name("_ZNK2sf4Text16findCharacterPosEm")
        // def findCharacterPos(self: Ptr[sfText], index: CSize): Vector2.sfVector2f = extern

        @name("_ZNK2sf4Text15getGlobalBoundsEv")
        def getGlobalBounds(self: Ptr[sfText]): Rect.sfRect[Float] = extern

        @name("_ZNK2sf4Text16getCharacterSizeEv")
        def getCharacterSize(self: Ptr[sfText]): CUnsignedInt = extern

        @name("_ZN2sf4Text16setCharacterSizeEj")
        def setCharacterSize(self: Ptr[sfText], size: CUnsignedInt): Unit = extern

        @name("_ZNK2sf4Text8getColorEv")
        def getColor(self: Ptr[sfText]): Ptr[Color.sfColor] = extern

        @name("_ZN2sf4Text8setColorERKNS_5ColorE")
        def setColor(self: Ptr[sfText], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZNK2sf4Text12getFillColorEv")
        def getFillColor(self: Ptr[sfText]): Ptr[Color.sfColor] = extern

        @name("_ZN2sf4Text12setFillColorERKNS_5ColorE")
        def setFillColor(self: Ptr[sfText], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZNK2sf4Text7getFontEv")
        def getFont(self: Ptr[sfText]): Ptr[Font.sfFont] = extern

        @name("_ZN2sf4Text7setFontERKNS_4FontE")
        def setFont(self: Ptr[sfText], font: Ptr[Font.sfFont]): Unit = extern

        @name("_ZNK2sf4Text16getLetterSpacingEv")
        def getLetterSpacing(self: Ptr[sfText]): CFloat = extern

        @name("_ZN2sf4Text16setLetterSpacingEf")
        def setLetterSpacing(self: Ptr[sfText], spacingFactor: CFloat): Unit = extern

        @name("_ZNK2sf4Text14getLineSpacingEv")
        def getLineSpacing(self: Ptr[sfText]): CFloat = extern

        @name("_ZN2sf4Text14setLineSpacingEf")
        def setLineSpacing(self: Ptr[sfText], spacingFactor: CFloat): Unit = extern

        @name("_ZNK2sf4Text15getOutlineColorEv")
        def getOutlineColor(self: Ptr[sfText]): Ptr[Color.sfColor] = extern

        @name("_ZN2sf4Text15setOutlineColorERKNS_5ColorE")
        def setOutlineColor(self: Ptr[sfText], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZNK2sf4Text19getOutlineThicknessEv")
        def getOutlineThickness(self: Ptr[sfText]): CFloat = extern

        @name("_ZN2sf4Text19setOutlineThicknessEf")
        def setOutlineThickness(self: Ptr[sfText], thickness: CFloat): Unit = extern

        // TODO: getString

        @name("_ZN2sf4Text9setStringERKNS_6StringE")
        def setString(self: Ptr[sfText], string: Ptr[String.sfString]): Unit = extern

    inline def ctor(self: Ptr[sfText]): Unit =
        Generic.ctor(self)

    inline def ctor(self: Ptr[sfText], string: Ptr[String.sfString], font: Ptr[Font.sfFont], characterSize: CUnsignedInt): Unit =
        Generic.ctor(self, string, font, characterSize)

    inline def ensureGeometryUpdate(self: Ptr[sfText]): Unit =
        Generic.ensureGeometryUpdate(self)

    inline def getGlobalBounds(self: Ptr[sfText]): Rect.sfRect[Float] =
        Generic.getGlobalBounds(self)

    inline def getCharacterSize(self: Ptr[sfText]): CUnsignedInt =
        Generic.getCharacterSize(self)

    inline def setCharacterSize(self: Ptr[sfText], size: CUnsignedInt): Unit =
        Generic.setCharacterSize(self, size)

    inline def getColor(self: Ptr[sfText]): Ptr[Color.sfColor] =
        Generic.getColor(self)

    inline def setColor(self: Ptr[sfText], color: Ptr[Color.sfColor]): Unit =
        Generic.setColor(self, color)

    inline def getFillColor(self: Ptr[sfText]): Ptr[Color.sfColor] =
        Generic.getFillColor(self)

    inline def setFillColor(self: Ptr[sfText], color: Ptr[Color.sfColor]): Unit =
        Generic.setFillColor(self, color)

    inline def getFont(self: Ptr[sfText]): Ptr[Font.sfFont] =
        Generic.getFont(self)

    inline def setFont(self: Ptr[sfText], font: Ptr[Font.sfFont]): Unit =
        Generic.setFont(self, font)

    inline def getLetterSpacing(self: Ptr[sfText]): CFloat =
        Generic.getLetterSpacing(self)

    inline def setLetterSpacing(self: Ptr[sfText], spacingFactor: CFloat): Unit =
        Generic.setLetterSpacing(self, spacingFactor)

    inline def getLineSpacing(self: Ptr[sfText]): CFloat =
        Generic.getLineSpacing(self)

    inline def setLineSpacing(self: Ptr[sfText], spacingFactor: CFloat): Unit =
        Generic.setLineSpacing(self, spacingFactor)

    inline def getOutlineColor(self: Ptr[sfText]): Ptr[Color.sfColor] =
        Generic.getOutlineColor(self)

    inline def setOutlineColor(self: Ptr[sfText], color: Ptr[Color.sfColor]): Unit =
        Generic.setOutlineColor(self, color)

    inline def getOutlineThickness(self: Ptr[sfText]): CFloat =
        Generic.getOutlineThickness(self)

    inline def setOutlineThickness(self: Ptr[sfText], thickness: CFloat): Unit =
        Generic.setOutlineThickness(self, thickness)

    inline def setString(self: Ptr[sfText], string: Ptr[String.sfString]): Unit =
        Generic.setString(self, string)
