package sfml
package internal
package graphics

import scala.scalanative.unsafe.{extern, link, linkCppRuntime, name}
import scala.scalanative.unsafe.cxx.*

import sfml.internal.system.Vector2

@linkCppRuntime
@link("sfml-graphics")
@extern private[sfml] object Transform:

    type sfTransform = CArray[CFloat, Nat.Digit2[Nat._1, Nat._6]]

    /* Public member functions */

    @name("___ZN2sf9TransformC2Ev")
    def ctor(result: RawPtr[sfTransform]): Unit = extern

    @name("___ZN2sf9TransformC2Efffffffff")
    def ctor(result: RawPtr[sfTransform], a00: CFloat, a01: CFloat, a02: CFloat, a10: CFloat, a11: CFloat, a12: CFloat, a20: CFloat, a21: CFloat, a22: CFloat): Unit = extern

    @name("___ZN2sf9TransformD2Ev")
    def dtor(self: Ref[sfTransform]): Unit = extern

    @name("___ZNK2sf9Transform9getMatrixEv")
    def getMatrix(self: Ref[Const[sfTransform]]): Ptr[Const[CArray[CFloat, Nat.Digit2[Nat._1, Nat._6]]]] = extern

    @name("___ZNK2sf9Transform10getInverseEv")
    def getInverse(result: RawPtr[sfTransform], self: Ref[Const[sfTransform]]): Unit = extern

    @name("___ZNK2sf9Transform14transformPointEff")
    def transformPoint(result: RawPtr[Vector2.sfVector2[Float]], self: Ref[Const[sfTransform]], x: CFloat, y: CFloat): Unit = extern

    @name("___ZNK2sf9Transform14transformPointERKNS_7Vector2IfEE")
    def transformPoint(result: RawPtr[Vector2.sfVector2[Float]], self: Ref[Const[sfTransform]], point: Ref[Const[Vector2.sfVector2[Float]]]): Unit = extern

    @name("___ZNK2sf9Transform13transformRectERKNS_4RectIfEE")
    def transformRect(result: RawPtr[Rect.sfRect[Float]], self: Ref[Const[sfTransform]], rectangle: Ref[Const[Rect.sfRect[Float]]]): Unit = extern

    @name("___ZN2sf9Transform7combineERKS0_")
    def combine(self: Ref[sfTransform], rhs: Ref[Const[sfTransform]]): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform9translateEff")
    def translate(self: Ref[sfTransform], x: CFloat, y: CFloat): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform9translateERKNS_7Vector2IfEE")
    def translate(self: Ref[sfTransform], offset: Ref[Const[Vector2.sfVector2[Float]]]): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform6rotateEf")
    def rotate(self: Ref[sfTransform], angle: CFloat): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform6rotateEfff")
    def rotate(self: Ref[sfTransform], angle: CFloat, centerX: CFloat, centerY: CFloat): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform6rotateEfRKNS_7Vector2IfEE")
    def rotate(self: Ref[sfTransform], angle: CFloat, center: Ref[Const[Vector2.sfVector2[Float]]]): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform5scaleEff")
    def scale(self: Ref[sfTransform], scaleX: CFloat, scaleY: CFloat): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform5scaleEffff")
    def scale(self: Ref[sfTransform], scaleX: CFloat, scaleY: CFloat, centerX: CFloat, centerY: CFloat): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform5scaleERKNS_7Vector2IfEE")
    def scale(self: Ref[sfTransform], factors: Ref[Const[Vector2.sfVector2[Float]]]): Ref[sfTransform] = extern

    @name("___ZN2sf9Transform5scaleERKNS_7Vector2IfEES4_")
    def scale(self: Ref[sfTransform], factors: Ref[Const[Vector2.sfVector2[Float]]], center: Ref[Const[Vector2.sfVector2[Float]]]): Ref[sfTransform] = extern

    /* Static public attributes */

    @name("___ZN2sf9Transform8IdentityE")
    def Identity(result: RawPtr[sfTransform]): Unit = extern

    /* Related symbols */

    @name("___ZN2sfmlERKNS_9TransformES2_")
    def ml_sfTransform(result: RawPtr[sfTransform], self: Ref[Const[sfTransform]], rhs: Ref[Const[sfTransform]]): Unit = extern

    @name("___ZN2sfmLERNS_9TransformERKS0_")
    def mL_sfTransform(self: Ref[sfTransform], rhs: Ref[Const[sfTransform]]): Unit = extern

    @name("___ZN2sfmlERKNS_9TransformERKNS_7Vector2IfEE")
    def ml_sfVector2f(result: RawPtr[Vector2.sfVector2[Float]], self: Ref[Const[sfTransform]], rhs: Ref[Const[Vector2.sfVector2[Float]]]): Unit = extern

    @name("___ZN2sfeqERKNS_9TransformES2_")
    def eq_sfTransform(self: Ref[Const[sfTransform]], rhs: Ref[Const[sfTransform]]): Boolean = extern

    @name("___ZN2sfneERKNS_9TransformES2_")
    def ne_sfTransform(self: Ref[Const[sfTransform]], rhs: Ref[Const[sfTransform]]): Boolean = extern
