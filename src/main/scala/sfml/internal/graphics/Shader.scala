package sfml
package internal
package graphics

import scala.scalanative.meta.LinktimeInfo
import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String
import sfml.internal.system.{Vector2, Vector3}

private[sfml] object Shader:
    type sfShader = CArray[Byte, Nat.Digit3[Nat._1, Nat._0, Nat._4]]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf6ShaderC2Ev")
        def ctor(self: Ptr[sfShader]): Unit = extern

        @name("_ZN2sf6ShaderD2Ev")
        def dtor(self: Ptr[sfShader]): Unit = extern

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Linux:
        @name("_ZN2sf6Shader12loadFromFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_4TypeE")
        def loadFromFile(self: Ptr[sfShader], filename: Ptr[String.stdString], shaderType: Type.sfUint32): Type.sfBool = extern

        @name("_ZN2sf6Shader10setUniformERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_7Vector2IfEE")
        def setUniform_sfVector2f(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZN2sf6Shader10setUniformERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_7Vector2IiEE")
        def setUniform_sfVector2i(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector2.sfVector2[Int]]): Unit = extern

        @name("_ZN2sf6Shader10setUniformERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_7Vector3IfEE")
        def setUniform_sfVector3f(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector3.sfVector3[Float]]): Unit = extern

        @name("_ZN2sf6Shader10setUniformERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKNS_7Vector3IiEE")
        def setUniform_sfVector3i(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector3.sfVector3[Int]]): Unit = extern

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Mac:
        @name("_ZN2sf6Shader12loadFromFileERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEENS0_4TypeE")
        def loadFromFile(self: Ptr[sfShader], filename: Ptr[String.stdString], shaderType: Type.sfUint32): Type.sfBool = extern

        @name("_ZN2sf6Shader10setUniformERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEERKNS_7Vector2IfEE")
        def setUniform_sfVector2f(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZN2sf6Shader10setUniformERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEERKNS_7Vector2IiEE")
        def setUniform_sfVector2i(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector2.sfVector2[Int]]): Unit = extern

        @name("_ZN2sf6Shader10setUniformERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEERKNS_7Vector3IfEE")
        def setUniform_sfVector3f(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector3.sfVector3[Float]]): Unit = extern

        @name("_ZN2sf6Shader10setUniformERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEERKNS_7Vector3IiEE")
        def setUniform_sfVector3i(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector3.sfVector3[Int]]): Unit = extern

    inline def ctor(self: Ptr[sfShader]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfShader]): Unit =
        Generic.dtor(self)

    inline def loadFromFile(self: Ptr[sfShader], filename: Ptr[String.stdString], shaderType: Type.sfUint32): Type.sfBool =
        if LinktimeInfo.isLinux then
            Linux.loadFromFile(self, filename, shaderType)
        else if LinktimeInfo.isMac then
            Mac.loadFromFile(self, filename, shaderType)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")

    inline def setUniform_sfVector2f(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector2.sfVector2[Float]]): Unit =
        if LinktimeInfo.isLinux then
            Linux.setUniform_sfVector2f(self, name, vector)
        else if LinktimeInfo.isMac then
            Mac.setUniform_sfVector2f(self, name, vector)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")

    inline def setUniform_sfVector2i(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector2.sfVector2[Int]]): Unit =
        if LinktimeInfo.isLinux then
            Linux.setUniform_sfVector2i(self, name, vector)
        else if LinktimeInfo.isMac then
            Mac.setUniform_sfVector2i(self, name, vector)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")

    inline def setUniform_sfVector3f(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector3.sfVector3[Float]]): Unit =
        if LinktimeInfo.isLinux then
            Linux.setUniform_sfVector3f(self, name, vector)
        else if LinktimeInfo.isMac then
            Mac.setUniform_sfVector3f(self, name, vector)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")

    inline def setUniform_sfVector3i(self: Ptr[Shader.sfShader], name: Ptr[String.stdString], vector: Ptr[Vector3.sfVector3[Int]]): Unit =
        if LinktimeInfo.isLinux then
            Linux.setUniform_sfVector3i(self, name, vector)
        else if LinktimeInfo.isMac then
            Mac.setUniform_sfVector3i(self, name, vector)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")
