package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2

private[sfml] object Transformable:
    type sfTransformable = CArray[Byte, Nat.Digit3[Nat._1, Nat._7, Nat._6]]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf13TransformableC2Ev")
        def ctor(self: Ptr[sfTransformable]): Unit = extern

        @name("_ZN2sf13TransformableD2Ev")
        def dtor(self: Ptr[sfTransformable]): Unit = extern

        @name("_ZN2sf13Transformable4moveEff")
        def move(self: Ptr[sfTransformable], offsetX: CFloat, offsetY: CFloat): Unit = extern

        @name("_ZN2sf13Transformable4moveERKNS_7Vector2IfEE")
        def move(self: Ptr[sfTransformable], offset: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZN2sf13Transformable6rotateEf")
        def rotate(self: Ptr[sfTransformable], angle: CFloat): Unit = extern

        @name("_ZN2sf13Transformable5scaleEff")
        def scale(self: Ptr[sfTransformable], factorX: CFloat, factorY: CFloat): Unit = extern

        @name("_ZN2sf13Transformable5scaleERKNS_7Vector2IfEE")
        def scale(self: Ptr[sfTransformable], factor: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZNK2sf13Transformable9getOriginEv")
        def getOrigin(self: Ptr[sfTransformable]): Ptr[Vector2.sfVector2[Float]] = extern

        @name("_ZN2sf13Transformable9setOriginEff")
        def setOrigin(self: Ptr[sfTransformable], x: CFloat, y: CFloat): Unit = extern

        @name("_ZN2sf13Transformable9setOriginERKNS_7Vector2IfEE")
        def setOrigin(self: Ptr[sfTransformable], factors: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZNK2sf13Transformable11getPositionEv")
        def getPosition(self: Ptr[sfTransformable]): Ptr[Vector2.sfVector2[Float]] = extern

        @name("_ZN2sf13Transformable11setPositionEff")
        def setPosition(self: Ptr[sfTransformable], x: CFloat, y: CFloat): Unit = extern

        @name("_ZN2sf13Transformable11setPositionERKNS_7Vector2IfEE")
        def setPosition(self: Ptr[sfTransformable], pos: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZNK2sf13Transformable11getRotationEv")
        def getRotation(self: Ptr[sfTransformable]): CFloat = extern

        @name("_ZN2sf13Transformable11setRotationEf")
        def setRotation(self: Ptr[sfTransformable], angle: CFloat): Unit = extern

        @name("_ZNK2sf13Transformable8getScaleEv")
        def getScale(self: Ptr[sfTransformable]): Ptr[Vector2.sfVector2[Float]] = extern

        @name("_ZN2sf13Transformable8setScaleEff")
        def setScale(self: Ptr[sfTransformable], x: CFloat, y: CFloat): Unit = extern

        @name("_ZN2sf13Transformable8setScaleERKNS_7Vector2IfEE")
        def setScale(self: Ptr[sfTransformable], factors: Ptr[Vector2.sfVector2[Float]]): Unit = extern

        @name("_ZNK2sf13Transformable12getTransformEv")
        def getTransform(self: Ptr[sfTransformable]): Ptr[Transform.sfTransform] = extern

        @name("_ZNK2sf13Transformable19getInverseTransformEv")
        def getInverseTransform(self: Ptr[sfTransformable]): Ptr[Transform.sfTransform] = extern

    def ctor(self: Ptr[sfTransformable]): Unit =
        Generic.ctor(self)

    def dtor(self: Ptr[sfTransformable]): Unit =
        Generic.dtor(self)

    def move(self: Ptr[sfTransformable], offsetX: CFloat, offsetY: CFloat): Unit =
        Generic.move(self, offsetX, offsetY)

    def move(self: Ptr[sfTransformable], offset: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.move(self, offset)

    def rotate(self: Ptr[sfTransformable], angle: CFloat): Unit =
        Generic.rotate(self, angle)

    def scale(self: Ptr[sfTransformable], factorX: CFloat, factorY: CFloat): Unit =
        Generic.scale(self, factorX, factorY)

    def scale(self: Ptr[sfTransformable], factor: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.scale(self, factor)

    def getOrigin(self: Ptr[sfTransformable]): Ptr[Vector2.sfVector2[Float]] =
        Generic.getOrigin(self)

    def setOrigin(self: Ptr[sfTransformable], x: CFloat, y: CFloat): Unit =
        Generic.setOrigin(self, x, y)

    def setOrigin(self: Ptr[sfTransformable], factors: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.setOrigin(self, factors)

    def getPosition(self: Ptr[sfTransformable]): Ptr[Vector2.sfVector2[Float]] =
        Generic.getPosition(self)

    def setPosition(self: Ptr[sfTransformable], x: CFloat, y: CFloat): Unit =
        Generic.setPosition(self, x, y)

    def setPosition(self: Ptr[sfTransformable], pos: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.setPosition(self, pos)

    def getRotation(self: Ptr[sfTransformable]): CFloat =
        Generic.getRotation(self)

    def setRotation(self: Ptr[sfTransformable], angle: CFloat): Unit =
        Generic.setRotation(self, angle)

    def getScale(self: Ptr[sfTransformable]): Ptr[Vector2.sfVector2[Float]] =
        Generic.getScale(self)

    def setScale(self: Ptr[sfTransformable], x: CFloat, y: CFloat): Unit =
        Generic.setScale(self, x, y)

    def setScale(self: Ptr[sfTransformable], factors: Ptr[Vector2.sfVector2[Float]]): Unit =
        Generic.setScale(self, factors)

    def getTransform(self: Ptr[sfTransformable]): Ptr[Transform.sfTransform] =
        Generic.getTransform(self)

    def getInverseTransform(self: Ptr[sfTransformable]): Ptr[Transform.sfTransform] =
        Generic.getInverseTransform(self)
