package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

import sfml.internal.system.Vector2

private[sfml] object RenderTarget:
    type sfRenderTarget = CStruct3[
        View.sfView,
        View.sfView,
        CArray[Byte, Nat.Digit3[Nat._1, Nat._4, Nat._4]]
    ]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf12RenderTargetC2Ev")
        def ctor(self: Ptr[sfRenderTarget]): Unit = extern

        @name("_ZN2sf12RenderTargetD2Ev")
        def dtor(self: Ptr[sfRenderTarget]): Unit = extern

        @name("_ZN2sf12RenderTarget5clearERKNS_5ColorE")
        def clear(self: Ptr[sfRenderTarget], color: Ptr[Color.sfColor]): Unit = extern

        @name("_ZN2sf12RenderTarget4drawERKNS_8DrawableERKNS_12RenderStatesE")
        def draw(self: Ptr[sfRenderTarget], drawable: Ptr[Byte], states: Ptr[RenderStates.sfRenderStates]): Unit = extern

        @name("_ZNK2sf12RenderTarget14getDefaultViewEv")
        def getDefaultView(self: Ptr[sfRenderTarget]): Ptr[View.sfView] = extern

        @name("_ZNK2sf12RenderTarget7getViewEv")
        def getView(self: Ptr[sfRenderTarget]): Ptr[View.sfView] = extern

        @name("_ZN2sf12RenderTarget7setViewERKNS_4ViewE")
        def setView(self: Ptr[sfRenderTarget], view: Ptr[View.sfView]): Unit = extern

        @name("_ZNK2sf12RenderTarget11getViewportERKNS_4ViewE")
        def getViewport(self: Ptr[sfRenderTarget], view: Ptr[View.sfView]): Rect.sfRect[Int] = extern

        @name("_ZNK2sf12RenderTarget16mapPixelToCoordsERKNS_7Vector2IiEE")
        def mapPixelToCoords(self: Ptr[sfRenderTarget], point: Ptr[Vector2.sfVector2[Int]]): Vector2.sfVector2[Float] = extern

        @name("_ZNK2sf12RenderTarget16mapPixelToCoordsERKNS_7Vector2IiEERKNS_4ViewE")
        def mapPixelToCoords(self: Ptr[sfRenderTarget], point: Ptr[Vector2.sfVector2[Int]], view: Ptr[View.sfView]): Vector2.sfVector2[Float] = extern

    inline def ctor(self: Ptr[sfRenderTarget]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfRenderTarget]): Unit =
        Generic.dtor(self)

    inline def clear(self: Ptr[sfRenderTarget], color: Ptr[Color.sfColor]): Unit =
        Generic.clear(self, color)

    inline def draw(self: Ptr[sfRenderTarget], drawable: Ptr[Byte], states: Ptr[RenderStates.sfRenderStates]): Unit =
        Generic.draw(self, drawable, states)

    inline def getDefaultView(self: Ptr[sfRenderTarget]): Ptr[View.sfView] =
        Generic.getDefaultView(self)

    inline def getView(self: Ptr[sfRenderTarget]): Ptr[View.sfView] =
        Generic.getView(self)

    inline def setView(self: Ptr[sfRenderTarget], view: Ptr[View.sfView]): Unit =
        Generic.setView(self, view)

    inline def getViewport(self: Ptr[sfRenderTarget], view: Ptr[View.sfView]): Rect.sfRect[Int] =
        Generic.getViewport(self, view)

    inline def mapPixelToCoords(self: Ptr[sfRenderTarget], point: Ptr[Vector2.sfVector2[Int]]): Vector2.sfVector2[Float] =
        Generic.mapPixelToCoords(self, point)

    inline def mapPixelToCoords(self: Ptr[sfRenderTarget], point: Ptr[Vector2.sfVector2[Int]], view: Ptr[View.sfView]): Vector2.sfVector2[Float] =
        Generic.mapPixelToCoords(self, point, view)
