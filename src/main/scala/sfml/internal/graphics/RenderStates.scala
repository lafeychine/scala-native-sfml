package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

private[sfml] object RenderStates:
    type sfRenderStates = CStruct4[BlendMode.sfBlendMode, Transform.sfTransform, Ptr[Texture.sfTexture], Ptr[Shader.sfShader]]

    @linkCppRuntime
    @link("sfml-graphics")
    @extern private object Generic:
        @name("_ZN2sf12RenderStatesC2Ev")
        def ctor(self: Ptr[sfRenderStates]): Unit = extern

        @name("_ZN2sf12RenderStatesC2ERKNS_9BlendModeE")
        def ctor_BlendMode(self: Ptr[sfRenderStates], theBlendMode: Ptr[BlendMode.sfBlendMode]): Unit = extern

        @name("_ZN2sf12RenderStatesC2ERKNS_9TransformE")
        def ctor_Transform(self: Ptr[sfRenderStates], theTransform: Ptr[Transform.sfTransform]): Unit = extern

        @name("_ZN2sf12RenderStatesC2EPKNS_7TextureE")
        def ctor_Texture(self: Ptr[sfRenderStates], theTexture: Ptr[Texture.sfTexture]): Unit = extern

        @name("_ZN2sf12RenderStatesC2EPKNS_6ShaderE")
        def ctor_Shader(self: Ptr[sfRenderStates], theShader: Ptr[Shader.sfShader]): Unit = extern

        @name("_ZN2sf12RenderStatesC2ERKNS_9BlendModeERKNS_9TransformEPKNS_7TextureEPKNS_6ShaderE")
        def ctor_BlendMode_Transform_Texture_Shader(self: Ptr[sfRenderStates], theBlendMode: Ptr[BlendMode.sfBlendMode], theTransform: Ptr[Transform.sfTransform], theTexture: Ptr[Texture.sfTexture], theShader: Ptr[Shader.sfShader]): Unit = extern

    inline def ctor(self: Ptr[sfRenderStates]): Unit =
        Generic.ctor(self)

    inline def ctor_BlendMode(self: Ptr[sfRenderStates], theBlendMode: Ptr[BlendMode.sfBlendMode]): Unit =
        Generic.ctor_BlendMode(self, theBlendMode)

    inline def ctor_Transform(self: Ptr[sfRenderStates], theTransform: Ptr[Transform.sfTransform]): Unit =
        Generic.ctor_Transform(self, theTransform)

    inline def ctor_Texture(self: Ptr[sfRenderStates], theTexture: Ptr[Texture.sfTexture]): Unit =
        Generic.ctor_Texture(self, theTexture)

    inline def ctor_Shader(self: Ptr[sfRenderStates], theShader: Ptr[Shader.sfShader]): Unit =
        Generic.ctor_Shader(self, theShader)

    inline def ctor_BlendMode_Transform_Texture_Shader(self: Ptr[sfRenderStates], theBlendMode: Ptr[BlendMode.sfBlendMode], theTransform: Ptr[Transform.sfTransform], theTexture: Ptr[Texture.sfTexture], theShader: Ptr[Shader.sfShader]): Unit =
        Generic.ctor_BlendMode_Transform_Texture_Shader(self, theBlendMode, theTransform, theTexture, theShader)
