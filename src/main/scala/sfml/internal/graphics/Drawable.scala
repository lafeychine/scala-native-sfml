package sfml
package internal
package graphics

import scala.scalanative.unsafe.*

private[sfml] object Drawable:
    type sfDrawable = VTable.VPtr
