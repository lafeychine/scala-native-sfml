package sfml
package internal
package audio

import scala.scalanative.meta.LinktimeInfo
import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String

private[sfml] object SoundBuffer:
    type sfSoundBuffer = CArray[Byte, Nat.Digit2[Nat._8, Nat._8]]

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Generic:
        @name("_ZN2sf11SoundBufferC2Ev")
        def ctor(self: Ptr[sfSoundBuffer]): Unit = extern

        @name("_ZN2sf11SoundBufferD2Ev")
        def dtor(self: Ptr[sfSoundBuffer]): Unit = extern

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Linux:
        @name("_ZN2sf11SoundBuffer12loadFromFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE")
        def loadFromFile(self: Ptr[sfSoundBuffer], filename: Ptr[String.stdString]): Type.sfBool = extern

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Mac:
        @name("_ZN2sf11SoundBuffer12loadFromFileERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE")
        def loadFromFile(self: Ptr[sfSoundBuffer], filename: Ptr[String.stdString]): Type.sfBool = extern

    inline def ctor(self: Ptr[sfSoundBuffer]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfSoundBuffer]): Unit =
        Generic.dtor(self)

    inline def loadFromFile(self: Ptr[sfSoundBuffer], filename: Ptr[String.stdString]): Type.sfBool =
        if LinktimeInfo.isLinux then
            Linux.loadFromFile(self, filename)
        else if LinktimeInfo.isMac then
            Mac.loadFromFile(self, filename)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")
