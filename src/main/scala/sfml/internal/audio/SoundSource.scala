package sfml
package internal
package audio

import scala.scalanative.unsafe.*

private[sfml] object SoundSource:
    type sfSoundSource = CArray[Byte, Nat.Digit2[Nat._1, Nat._6]]

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Generic:
        @name("_ZN2sf11SoundSourceC2Ev")
        def ctor(self: Ptr[sfSoundSource]): Unit = extern

        @name("_ZN2sf11SoundSourceD2Ev")
        def dtor(self: Ptr[sfSoundSource]): Unit = extern

        @name("_ZNK2sf11SoundSource9getStatusEv")
        def getStatus(self: Ptr[sfSoundSource]): CUnsignedInt = extern

        @name("_ZNK2sf11SoundSource14getAttenuationEv")
        def getAttenuation(self: Ptr[sfSoundSource]): CFloat = extern

        @name("_ZN2sf11SoundSource14setAttenuationEf")
        def setAttenuation(self: Ptr[sfSoundSource], attenuation: CFloat): Unit = extern

        @name("_ZNK2sf11SoundSource14getMinDistanceEv")
        def getMinDistance(self: Ptr[sfSoundSource]): CFloat = extern

        @name("_ZN2sf11SoundSource14setMinDistanceEf")
        def setMinDistance(self: Ptr[sfSoundSource], distance: CFloat): Unit = extern

        @name("_ZNK2sf11SoundSource8getPitchEv")
        def getPitch(self: Ptr[sfSoundSource]): CFloat = extern

        @name("_ZN2sf11SoundSource8setPitchEf")
        def setPitch(self: Ptr[sfSoundSource], pitch: CFloat): Unit = extern

        @name("_ZN2sf11SoundSource11setPositionEfff")
        def setPosition(self: Ptr[sfSoundSource], x: CFloat, y: CFloat, z: CFloat): Unit = extern

        @name("_ZNK2sf11SoundSource20isRelativeToListenerEv")
        def isRelativeToListener(self: Ptr[sfSoundSource]): Type.sfBool = extern

        @name("_ZN2sf11SoundSource21setRelativeToListenerEb")
        def setRelativeToListener(self: Ptr[sfSoundSource], relative: Type.sfBool): Unit = extern

        @name("_ZNK2sf11SoundSource9getVolumeEv")
        def getVolume(self: Ptr[sfSoundSource]): CFloat = extern

        @name("_ZN2sf11SoundSource9setVolumeEf")
        def setVolume(self: Ptr[sfSoundSource], volume: CFloat): Unit = extern

    inline def ctor(self: Ptr[sfSoundSource]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfSoundSource]): Unit =
        Generic.dtor(self)

    inline def getStatus(self: Ptr[sfSoundSource]): CUnsignedInt =
        Generic.getStatus(self)

    inline def getAttenuation(self: Ptr[sfSoundSource]): CFloat =
        Generic.getAttenuation(self)

    inline def setAttenuation(self: Ptr[sfSoundSource], attenuation: CFloat): Unit =
        Generic.setAttenuation(self, attenuation)

    inline def getMinDistance(self: Ptr[sfSoundSource]): CFloat =
        Generic.getMinDistance(self)

    inline def setMinDistance(self: Ptr[sfSoundSource], distance: CFloat): Unit =
        Generic.setMinDistance(self, distance)

    inline def getPitch(self: Ptr[sfSoundSource]): CFloat =
        Generic.getPitch(self)

    inline def setPitch(self: Ptr[sfSoundSource], pitch: CFloat): Unit =
        Generic.setPitch(self, pitch)

    inline def setPosition(self: Ptr[sfSoundSource], x: CFloat, y: CFloat, z: CFloat): Unit =
        Generic.setPosition(self, x, y, z)

    inline def isRelativeToListener(self: Ptr[sfSoundSource]): Type.sfBool =
        Generic.isRelativeToListener(self)

    inline def setRelativeToListener(self: Ptr[sfSoundSource], relative: Type.sfBool): Unit =
        Generic.setRelativeToListener(self, relative)

    inline def getVolume(self: Ptr[sfSoundSource]): CFloat =
        Generic.getVolume(self)

    inline def setVolume(self: Ptr[sfSoundSource], volume: CFloat): Unit =
        Generic.setVolume(self, volume)
