package sfml
package internal
package audio

import scala.scalanative.meta.LinktimeInfo
import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String

private[sfml] object Music:
    type sfMusic = CStruct2[SoundStream.sfSoundStream, CArray[Byte, Nat.Digit2[Nat._9, Nat._6]]]

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Generic:
        @name("_ZN2sf5MusicC2Ev")
        def ctor(self: Ptr[sfMusic]): Unit = extern

        @name("_ZN2sf5MusicD2Ev")
        def dtor(self: Ptr[sfMusic]): Unit = extern

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Linux:
        @name("_ZN2sf5Music12openFromFileERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE")
        def openFromFile(self: Ptr[sfMusic], filename: Ptr[String.stdString]): Type.sfBool = extern

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Mac:
        @name("_ZN2sf5Music12openFromFileERKNSt3__112basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE")
        def openFromFile(self: Ptr[sfMusic], filename: Ptr[String.stdString]): Type.sfBool = extern

    inline def ctor(self: Ptr[sfMusic]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfMusic]): Unit =
        Generic.dtor(self)

    inline def openFromFile(self: Ptr[sfMusic], filename: Ptr[String.stdString]): Type.sfBool =
        if LinktimeInfo.isLinux then
            Linux.openFromFile(self, filename)
        else if LinktimeInfo.isMac then
            Mac.openFromFile(self, filename)
        else throw new UnsupportedOperationException("This operation is not supported on this platform.")
