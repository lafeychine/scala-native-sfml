package sfml
package internal
package audio

import scala.scalanative.unsafe.*

private[sfml] object SoundStream:
    type sfSoundStream = CStruct2[SoundSource.sfSoundSource, CArray[Byte, Nat.Digit2[Nat._9, Nat._6]]]

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Generic:
        @name("_ZN2sf11SoundStreamC2Ev")
        def ctor(self: Ptr[sfSoundStream]): Unit = extern

        @name("_ZN2sf11SoundStreamD2Ev")
        def dtor(self: Ptr[sfSoundStream]): Unit = extern

        @name("_ZN2sf11SoundStream4playEv")
        def play(self: Ptr[sfSoundStream]): Unit = extern

        @name("_ZN2sf11SoundStream5pauseEv")
        def pause(self: Ptr[sfSoundStream]): Unit = extern

        @name("_ZN2sf11SoundStream4stopEv")
        def stop(self: Ptr[sfSoundStream]): Unit = extern

        @name("_ZNK2sf11SoundStream9getStatusEv")
        def getStatus(self: Ptr[sfSoundStream]): CUnsignedInt = extern

    inline def ctor(self: Ptr[sfSoundStream]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfSoundStream]): Unit =
        Generic.dtor(self)

    inline def play(self: Ptr[sfSoundStream]): Unit =
        Generic.play(self)

    inline def pause(self: Ptr[sfSoundStream]): Unit =
        Generic.pause(self)

    inline def stop(self: Ptr[sfSoundStream]): Unit =
        Generic.stop(self)

    inline def getStatus(self: Ptr[sfSoundStream]): CUnsignedInt =
        Generic.getStatus(self)
