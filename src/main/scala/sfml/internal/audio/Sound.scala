package sfml
package internal
package audio

import scala.scalanative.unsafe.*

private[sfml] object Sound:
    type sfSound = CStruct2[SoundSource.sfSoundSource, CArray[Byte, Nat._8]]

    @linkCppRuntime
    @link("sfml-audio")
    @extern private object Generic:
        @name("_ZN2sf5SoundC2Ev")
        def ctor(self: Ptr[sfSound]): Unit = extern

        @name("_ZN2sf5SoundD2Ev")
        def dtor(self: Ptr[sfSound]): Unit = extern

        @name("_ZN2sf5Sound4playEv")
        def play(self: Ptr[sfSound]): Unit = extern

        @name("_ZN2sf5Sound5pauseEv")
        def pause(self: Ptr[sfSound]): Unit = extern

        @name("_ZN2sf5Sound4stopEv")
        def stop(self: Ptr[sfSound]): Unit = extern

        @name("_ZNK2sf5Sound9getStatusEv")
        def getStatus(self: Ptr[sfSound]): CUnsignedInt = extern

        @name("_ZN2sf5Sound9setBufferERKNS_11SoundBufferE")
        def setBuffer(self: Ptr[sfSound], buffer: Ptr[SoundBuffer.sfSoundBuffer]): Unit = extern

        @name("_ZN2sf5Sound11resetBufferEv")
        def resetBuffer(self: Ptr[sfSound]): Unit = extern

    inline def ctor(self: Ptr[sfSound]): Unit =
        Generic.ctor(self)

    inline def dtor(self: Ptr[sfSound]): Unit =
        Generic.dtor(self)

    inline def play(self: Ptr[sfSound]): Unit =
        Generic.play(self)

    inline def pause(self: Ptr[sfSound]): Unit =
        Generic.pause(self)

    inline def stop(self: Ptr[sfSound]): Unit =
        Generic.stop(self)

    inline def getStatus(self: Ptr[sfSound]): CUnsignedInt =
        Generic.getStatus(self)

    inline def setBuffer(self: Ptr[sfSound], buffer: Ptr[SoundBuffer.sfSoundBuffer]): Unit =
        Generic.setBuffer(self, buffer)

    inline def resetBuffer(self: Ptr[sfSound]): Unit =
        Generic.resetBuffer(self)
