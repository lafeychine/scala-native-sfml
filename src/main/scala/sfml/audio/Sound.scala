package sfml
package audio

import scala.scalanative.unsafe.*

import sfml.internal.audio.Sound as Self

trait Sound extends SoundSource:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _sound: ResourcePtr[Self.sfSound] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    private[sfml] final override lazy val _soundSource = _sound.get(_.at1)

    /* Methods */

    override def play(): Unit =
        Self.play(_sound.ptr)

    override def pause(): Unit =
        Self.pause(_sound.ptr)

    override def stop(): Unit =
        Self.stop(_sound.ptr)

    override def status: Status =
        Status.fromOrdinal(Self.getStatus(_sound.ptr).toInt)

    // TODO: Add buffer
    // NOTE: To be able to use [`buffer_=`]
    def buffer = ()

    def buffer_=(buffer: SoundBuffer): Unit =
        Self.setBuffer(_sound.ptr, buffer)

    def resetBuffer(): Unit =
        Self.resetBuffer(_sound.ptr)

object Sound:

    private[sfml] inline given Conversion[Sound, Ptr[Self.sfSound]]:
        override def apply(sound: Sound) = sound._sound.ptr

    /* Constructors */

    def apply(): Sound =
        new Object with Sound
