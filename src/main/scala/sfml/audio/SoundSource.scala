package sfml
package audio

import scala.scalanative.unsafe.*

import sfml.internal.Type.{booleanToSfBool, sfBoolToBoolean}
import sfml.internal.audio.SoundSource as Self

/** Base class defining a sound's properties.
  *
  * [[sfml.audio.SoundSource SoundSource]] is not meant to be used directly, it only serves as a common base for all audio objects
  * that can live in the audio environment.
  *
  * <!-- TODO: Properties -->
  *
  * @see
  *   [[sfml.audio.SoundStream SoundStream]] <!-- TODO: sf::Sound -->
  *
  * @doxygenId
  *   classsf_1_1SoundSource
  *
  * @doxygenHash
  *   7ff52b826ec76cc5171c9a6813b1677e
  */
abstract trait SoundSource:

    /* Fields */

    /* TODO: Do the VTable */
    @scala.annotation.threadUnsafe
    private[sfml] lazy val _soundSource: ResourcePtr[Self.sfSoundSource] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    /* Methods */

    /** Start or resume playing the sound source.
      *
      * This function starts the source if it was stopped, resumes it if it was paused, and restarts it from the beginning if it was
      * already playing.
      *
      * @see
      *   [[sfml.audio.SoundSource.pause pause]], [[sfml.audio.SoundSource.stop stop]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a6e1bbb1f247ed8743faf3b1ed6f2bc21
      */
    def play(): Unit

    /** Pause the sound source.
      *
      * This function pauses the source if it was playing, otherwise (source already paused or stopped) it has no effect.
      *
      * @see
      *   [[sfml.audio.SoundSource.play play]], [[sfml.audio.SoundSource.stop stop]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a21553d4e8fcf136231dd8c7ad4630aba
      */
    def pause(): Unit

    /** Stop playing the sound source.
      *
      * This function stops the source if it was playing or paused, and does nothing if it was already stopped. It also resets the
      * playing position (unlike [[sfml.audio.SoundSource.pause pause]]).
      *
      * @see
      *   [[sfml.audio.SoundSource.play play]], [[sfml.audio.SoundSource.pause pause]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a06501a25b12376befcc7ee1ed4865fda
      */
    def stop(): Unit

    /** Get the current status of the sound (stopped, paused, playing)
      *
      * @return
      *   Current status of the sound
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1aa8d313c31b968159582a999aa66e5ed7
      */
    def status: Status =
        Status.fromOrdinal(Self.getStatus(_soundSource.ptr).toInt)

    /** Get the attenuation factor of the sound.
      *
      * @return
      *   Attenuation factor of the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.attenuation_= attenuation_=]], [[sfml.audio.SoundSource.minDistance minDistance]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a8ad7dafb4f1b4afbc638cebe24f48cc9
      */
    def attenuation: Float =
        Self.getAttenuation(_soundSource.ptr)

    /** Set the attenuation factor of the sound.
      *
      * The attenuation is a multiplicative factor which makes the sound more or less loud according to its distance from the
      * listener. An attenuation of 0 will produce a non-attenuated sound, i.e. its volume will always be the same whether it is
      * heard from near or from far. On the other hand, an attenuation value such as 100 will make the sound fade out very quickly
      * as it gets further from the listener. The default value of the attenuation is 1.
      *
      * @param attenuation
      *   New attenuation factor of the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.attenuation attenuation]], [[sfml.audio.SoundSource.minDistance_= minDistance_=]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1aa2adff44cd2f8b4e3c7315d7c2a45626
      */
    def attenuation_=(attenuation: Float): Unit =
        Self.setAttenuation(_soundSource.ptr, attenuation)

    /** Get the minimum distance of the sound.
      *
      * @return
      *   Minimum distance of the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.minDistance_= minDistance_=]], [[sfml.audio.SoundSource.attenuation attenuation]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a605ca7f359ec1c36fcccdcd4696562ac
      */
    def minDistance: Float =
        Self.getMinDistance(_soundSource.ptr)

    /** Set the minimum distance of the sound.
      *
      * The "minimum distance" of a sound is the maximum distance at which it is heard at its maximum volume. Further than the
      * minimum distance, it will start to fade out according to its attenuation factor. A value of 0 ("inside the head of the
      * listener") is an invalid value and is forbidden. The default value of the minimum distance is 1.
      *
      * @param distance
      *   New minimum distance of the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.minDistance minDistance]], [[sfml.audio.SoundSource.attenuation_= attenuation_=]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a75bbc2c34addc8b25a14edb908508afe
      */
    def minDistance_=(distance: Float): Unit =
        Self.setMinDistance(_soundSource.ptr, distance)

    /** Get the pitch of the sound.
      *
      * @return
      *   Pitch of the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.pitch_= pitch_=]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a4736acc2c802f927544c9ce52a44a9e4
      */
    def pitch: Float =
        Self.getPitch(_soundSource.ptr)

    /** Set the pitch of the sound.
      *
      * The pitch represents the perceived fundamental frequency of a sound; thus you can make a sound more acute or grave by
      * changing its pitch. A side effect of changing the pitch is to modify the playing speed of the sound as well. The default
      * value for the pitch is 1.
      *
      * @params
      *   pitch New pitch to apply to the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.pitch pitch]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a72a13695ed48b7f7b55e7cd4431f4bb6
      */
    def pitch_=(pitch: Float): Unit =
        Self.setPitch(_soundSource.ptr, pitch)

    // TODO: Add `position` and `position_=`
    // NOTE: To be able to use [`position_=`]
    def position = ()

    /** Set the 3D position of the sound in the audio scene.
      *
      * Only sounds with one channel (mono sounds) can be spatialized. The default position of a sound is (0, 0, 0).
      *
      * @param x
      *   X coordinate of the position of the sound in the scene
      *
      * @param y
      *   Y coordinate of the position of the sound in the scene
      *
      * @param z
      *   Z coordinate of the position of the sound in the scene
      *
      * <!-- TODO: position -->
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a0480257ea25d986eba6cc3c1a6f8d7c2
      *
      * @doxygenHash
      *   5a48f6d8a73e2188823ce7f79b472342
      */
    def position_=(x: Float, y: Float, z: Float): Unit =
        Self.setPosition(_soundSource.ptr, x, y, z)

    /** Tell whether the sound's position is relative to the listener or is absolute.
      *
      * @return
      *   True if the position is relative, false if it's absolute
      *
      * @see
      *   [[sfml.audio.SoundSource.relativeToListener_= relativeToListener_=]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1adcdb4ef32c2f4481d34aff0b5c31534b
      */
    def relativeToListener: Boolean =
        Self.isRelativeToListener(_soundSource.ptr)

    /** Make the sound's position relative to the listener or absolute.
      *
      * Making a sound relative to the listener will ensure that it will always be played the same way regardless of the position of
      * the listener. This can be useful for non-spatialized sounds, sounds that are produced by the listener, or sounds attached to
      * it. The default value is false (position is absolute).
      *
      * @param relative
      *   True to set the position relative, false to set it absolute
      *
      * @see
      *   [[sfml.audio.SoundSource.relativeToListener relativeToListener]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1ac478a8b813faf7dd575635b102081d0d
      */
    def relativeToListener_=(relative: Boolean): Unit =
        Self.setRelativeToListener(_soundSource.ptr, relative)

    /** Get the volume of the sound.
      *
      * @return
      *   Volume of the sound, in the range [0, 100]
      *
      * @see
      *   [[sfml.audio.SoundSource.volume_= volume_=]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a04243fb5edf64561689b1d58953fc4ce
      */
    def volume: Float =
        Self.getVolume(_soundSource.ptr)

    /** Set the volume of the sound.
      *
      * The volume is a value between 0 (mute) and 100 (full volume). The default value for the volume is 100.
      *
      * @param volume
      *   Volume of the sound
      *
      * @see
      *   [[sfml.audio.SoundSource.volume volume]]
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1a2f192f2b49fb8e2b82f3498d3663fcc2
      */
    def volume_=(volume: Float): Unit =
        Self.setVolume(_soundSource.ptr, volume)

object SoundSource:

    private[sfml] inline given Conversion[SoundSource, Ptr[Self.sfSoundSource]]:
        override def apply(soundSource: SoundSource) = soundSource._soundSource.ptr
