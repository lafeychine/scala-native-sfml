package sfml
package audio

import scala.scalanative.unsafe.*

import sfml.internal.audio.SoundStream as Self

/** Abstract base class for streamed audio sources.
  *
  * <!-- TODO: Comparison with sf::SoundBuffer --> Audio streams are never completely loaded in memory.
  *
  * Instead, the audio data is acquired continuously while the stream is playing. This behavior allows to play a sound with no
  * loading delay, and keeps the memory consumption very low.
  *
  * <!-- TODO: sf::Sound -->
  *
  * [[sfml.audio.SoundStream SoundStream]] is a base class that doesn't care about the stream source, which is left to the derived
  * class. SFML provides a built-in specialization for big files (see [[sfml.audio.Music Music]]). No network stream source is
  * provided, but you can write your own by combining this class with the network module.
  *
  * <!-- TODO: Derived class -->
  *
  * It is important to note that each [[sfml.audio.SoundStream SoundStream]] is played in its own separate thread, so that the
  * streaming loop doesn't block the rest of the program.
  *
  * <!-- TODO: OnGetData, OnSeek -->
  *
  * It is important to keep this in mind, because you may have to take care of synchronization issues if you share data between
  * threads.
  *
  * <!-- TODO: Usage example -->
  *
  * @see
  *   [[sfml.audio.Music Music]]
  *
  * @doxygenId
  *   classsf_1_1SoundStream
  *
  * @doxygenHash
  *   210afb8a1cab4ede43ca71682095d317
  */
abstract trait SoundStream extends SoundSource:

    /* Fields */

    /* TODO: Do the VTable */
    @scala.annotation.threadUnsafe
    private[sfml] lazy val _soundStream: ResourcePtr[Self.sfSoundStream] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    private[sfml] final override lazy val _soundSource = _soundStream.get(_.at1)

    /* Methods */

    override def play(): Unit =
        Self.play(_soundStream.ptr)

    override def pause(): Unit =
        Self.pause(_soundStream.ptr)

    override def stop(): Unit =
        Self.stop(_soundStream.ptr)

    override def status: Status =
        Status.fromOrdinal(Self.getStatus(_soundStream.ptr).toInt)

object SoundStream:

    private[sfml] inline given Conversion[SoundStream, Ptr[Self.sfSoundStream]]:
        override def apply(soundStream: SoundStream) = soundStream._soundStream.ptr
