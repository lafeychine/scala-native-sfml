package sfml
package audio

import scala.scalanative.unsafe.*

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.audio.Music as Self

/** Streamed music played from an audio file.
  *
  * Musics are sounds that are streamed rather than completely loaded in memory.
  *
  * This is especially useful for compressed musics that usually take hundreds of MB when they are uncompressed: by streaming it
  * instead of loading it entirely, you avoid saturating the memory and have almost no loading delay. This implies that the
  * underlying resource (file, stream or memory buffer) must remain valid for the lifetime of the [[sfml.audio.Music Music]] object.
  *
  * <!-- TODO: Comparison sfml.audio.SoundBuffer/sfml.audio.Sound -->
  *
  * As a sound stream, a music is played in its own thread in order not to block the rest of the program. This means that you can
  * leave the music alone after calling [[sfml.audio.Music.play play]], it will manage itself very well.
  *
  * ```scala
  * // Declare a new music
  * val music = Music()
  *
  * // Open it from an audio file
  * if !(music.openFromFile("music.ogg")) then
  *     // error...
  *     ???
  *
  * <!-- TODO: Change some parameters -->
  *
  * // Play it
  * music.play()
  * ```
  *
  * <!-- TODO @see sf::Sound, sf::SoundStream -->
  *
  * @doxygenId
  *   classsf_1_1Music
  *
  * @doxygenHash
  *   80a3b6ebd34af0f03c41b035cafc3afc
  */
trait Music extends SoundStream:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _music: ResourcePtr[Self.sfMusic] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    private[sfml] override lazy val _soundStream = _music.get(_.at1)

    /* Methods */

    /** Open a music from an audio file.
      *
      * This function doesn't start playing the music (call [[sfml.audio.SoundSource.play play]] to do so).
      *
      * <!-- TODO: Link to the documentation of sf::InputSoundFile for supported formats. -->
      *
      * Warning: Since the music is not loaded at once but rather streamed continuously, the file must remain accessible until the
      * [[sfml.audio.Music Music]] object loads a new music or is destroyed.
      *
      * @param filename
      *   Path of the music file to open
      *
      * @returns
      *   True if loading succeeded, false if it failed
      *
      * <!-- TODO @see openFromMemory, openFromStream -->
      *
      * @doxygenId
      *   classsf_1_1Music_1a3edc66e5f5b3f11e84b90eaec9c7d7c0
      *
      * @doxygenHash
      *   f3efb412d9da3735f934aa57c08d421a
      */
    def openFromFile(filename: stdlib.String): Boolean =
        Self.openFromFile(_music.ptr, filename)

object Music:

    private[sfml] inline given Conversion[Music, Ptr[Self.sfMusic]]:
        override def apply(music: Music) = music._music.ptr

    /* Constructors */

    /** Default constructor.
      *
      * @doxygenId
      *   classsf_1_1Music_1a0bc787d8e022b3a9b89cf2c28befd42e
      */
    def apply(): Music =
        new Object with Music
