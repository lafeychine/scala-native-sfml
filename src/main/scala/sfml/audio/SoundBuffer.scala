package sfml
package audio

import scala.scalanative.unsafe.*

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.audio.SoundBuffer as Self

trait SoundBuffer:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _soundBuffer: ResourcePtr[Self.sfSoundBuffer] =
        ResourceBuffer(Self.dtor)(Self.ctor(_))

    /* Methods */

    def loadFromFile(filename: stdlib.String): Boolean =
        Self.loadFromFile(_soundBuffer.ptr, filename)

object SoundBuffer:

    private[sfml] inline given Conversion[SoundBuffer, Ptr[Self.sfSoundBuffer]]:
        override def apply(soundBuffer: SoundBuffer) = soundBuffer._soundBuffer.ptr

    /* Constructors */

    def apply(): SoundBuffer =
        new Object with SoundBuffer
