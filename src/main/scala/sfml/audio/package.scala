package sfml

/** Sounds, streaming (musics or custom sources), recording, spatialization.
  *
  * @doxygenId
  *   group__audio
  */
package object audio:
    /** Enumeration of the sound source states.
      *
      * @doxygenId
      *   classsf_1_1SoundSource_1ac43af72c98c077500b239bc75b812f03
      */
    enum Status:
        /** Sound is not playing.
          *
          * @doxygenId
          *   classsf_1_1SoundSource_1ac43af72c98c077500b239bc75b812f03adabb01e8aa85b2f54b344890addf764a
          */
        case Stopped

        /** Sound is paused.
          *
          * @doxygenId
          *   classsf_1_1SoundSource_1ac43af72c98c077500b239bc75b812f03ac3ca1fcc0394267c9bdbe3dc0a8a7e41
          */
        case Paused

        /** Sound is playing.
          *
          * @doxygenId
          *   classsf_1_1SoundSource_1ac43af72c98c077500b239bc75b812f03af07bdea9f70ef7606dfc9f955beeee18
          */
        case Playing
