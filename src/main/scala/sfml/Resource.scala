package sfml

import scala.scalanative.libc.string.memcpy
import scala.scalanative.runtime.Intrinsics.{castLongToRawPtr, castRawPtrToObject}
import scala.scalanative.runtime.{BlobArray, fromRawPtr, toRawPtr}
import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

@link("gc")
@extern private object GC:
    @name("GC_base")
    def base[T](ptr: Ptr[T]): Ptr[BlobArray] = extern

    @name("GC_register_finalizer_no_order")
    def register_finalizer[T](
        ptr: Ptr[Object],
        callback: CFuncPtr2[Ptr[T], Long, Unit],
        offset: Long,
        ofn: Ptr[Byte],
        ocd: Ptr[Byte]
    ): Unit = extern

/** A pointer to a resource that is managed by the C++ wrapper.
  *
  * This pointer can never be accessed by the user of the library.
  */
private[sfml] class ResourcePtr[T: Tag](val ptr: Ptr[T], private[sfml] val buffer: Option[BlobArray]):

    def get[R: Tag](f: Ptr[T] => Ptr[R]): ResourcePtr[R] =
        ResourcePtr(f(ptr), buffer)

    @SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.OptionPartial", "scalafix:OptionGetWarn"))
    private[sfml] inline def register_finalizer(inline dtor: Ptr[T] => Unit): ResourcePtr[T] =
        val callback = CFuncPtr2.fromScalaFunction((ptr: Ptr[T], offset: Long) => {
            dtor(fromRawPtr(castLongToRawPtr(ptr.toLong + offset)))
        })

        val obj: Ptr[Object] = fromRawPtr(buffer.get.atRaw(0)) - 2
        val offset: Long = ptr.toLong - obj.toLong

        GC.register_finalizer(obj, callback, offset, null, null)
        this

private object ResourcePtr:

    // TODO: Delete after refactor
    inline given [T: Tag] => Conversion[ResourcePtr[T], scala.scalanative.unsafe.cxx.ResourcePtr[T]]:
        override def apply(ptr: ResourcePtr[T]): scala.scalanative.unsafe.cxx.ResourcePtr[T] =
            scala.scalanative.unsafe.cxx.ResourcePtr[T](ptr.ptr, ptr.buffer)

    // TODO: Handle dtor
    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    private[sfml] def retrieveCopy[T: Tag](ptr: Ptr[T]): ResourcePtr[T] =
        GC.base(ptr) match
            case null => ResourceBuffer.shallow_copy(ptr)
            case base => ResourcePtr(ptr, Option(castRawPtrToObject(toRawPtr(base)).asInstanceOf[BlobArray]))

    // TODO: Handle dtor
    @SuppressWarnings(Array("org.wartremover.warts.Null", "org.wartremover.warts.Throw"))
    private[sfml] def retrieve[T: Tag](ptr: Ptr[T]): ResourcePtr[T] =
        GC.base(ptr) match
            case null =>
                throw new RuntimeException("Unexpected pointer caught. Please report the bug to lafeychine/scala-native-sfml.")
            case base => ResourcePtr(ptr, Option(castRawPtrToObject(toRawPtr(base)).asInstanceOf[BlobArray]))

private[sfml] object ResourceBuffer:

    inline def apply[T: Tag](inline ctor: Ptr[T] => Unit): ResourcePtr[T] =
        val buffer = BlobArray.alloc(sizeof[T].toInt)

        val ptr: Ptr[T] = fromRawPtr(buffer.atRaw(0))
        ctor(ptr)

        ResourcePtr(ptr, Option(buffer))

    inline def apply[T: Tag](inline dtor: Ptr[T] => Unit)(inline ctor: Ptr[T] => Unit): ResourcePtr[T] =
        ResourceBuffer(ctor).register_finalizer(dtor)

    inline def shallow_copy[T: Tag](ptr: Ptr[T]): ResourcePtr[T] =
        ResourceBuffer((r: Ptr[T]) => {
            memcpy(r.asInstanceOf[Ptr[Byte]], ptr.asInstanceOf[Ptr[Byte]], sizeof[T])
            ()
        })

    inline def shallow_copy[T: Tag](inline dtor: Ptr[T] => Unit)(ptr: Ptr[T]): ResourcePtr[T] =
        shallow_copy(ptr).register_finalizer(dtor)

private[sfml] object AbstractResourceBuffer:

    inline def apply[T: Tag](inline vtable: VTable, inline self: Object)(inline ctor: Ptr[T] => Unit): ResourcePtr[T] =
        val buffer = BlobArray.alloc(sizeof[CStruct2[Object, T]].toInt)

        val obj_ptr: Ptr[Object] = fromRawPtr(buffer.atRaw(0))
        !(obj_ptr) = self

        val ptr: Ptr[T] = fromRawPtr(buffer.atRaw(sizeof[Object].toInt))
        ctor(ptr)
        !(ptr.asInstanceOf[Ptr[VTable.VPtr]]) = vtable.toVPtr

        ResourcePtr(ptr, Option(buffer))

    inline def apply[T: Tag](inline vtable: VTable, inline self: Object)(inline dtor: Ptr[T] => Unit)(
        inline ctor: Ptr[T] => Unit
    ): ResourcePtr[T] =
        AbstractResourceBuffer(vtable, self)(ctor).register_finalizer(dtor)

private[sfml] class Wrapped[T] private ()

private[sfml] object Wrapped:

    extension [T](ptr: Ptr[Wrapped[T]])
        @SuppressWarnings(Array("org.wartremover.warts.Nothing"))
        def get: T =
            (!(ptr.asInstanceOf[Ptr[Object]] - 1)).asInstanceOf[T]
