package sfml

/** Make objects immutable.
  *
  * Making an object immutable means that it can no longer be modified, even by functions. Functions that ensures that the object is
  * not modified can take an immutable object as a parameter.
  *
  * @tparam T
  *   Type of the object to be made immutable
  *
  * @note
  *   This wrapper is the C++ equivalent of the `const` keyword
  */
class Immutable[+T](private[sfml] val get: T):

    inline def ==(rhs: Immutable[?]): Boolean =
        this.get == rhs.get

    inline def !=(rhs: Immutable[?]): Boolean =
        this.get != rhs.get

    override def equals(obj: Any): Boolean =
        this.get.equals(obj)

    @SuppressWarnings(Array("org.wartremover.warts.ToString"))
    override def toString(): java.lang.String = this.get.toString()

object Immutable:

    inline given [T] => Conversion[T, Immutable[T]]:
        override def apply(x: T) = Immutable(x)
