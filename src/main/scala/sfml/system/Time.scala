package sfml
package system

import scala.scalanative.unsafe.*

import sfml.internal.system.Time as Self

trait Time(using private[sfml] ctor: Time.ctor):

    /* Fields */

    private[sfml] val _time: ResourcePtr[Self.sfTime] =
        ResourceBuffer(Self.dtor)(ctor)

    /* Methods */

    def asMicroseconds: Long =
        Self.asMicroseconds(this)

    def asMilliseconds: Int =
        Self.asMilliseconds(this)

    def asSeconds: Float =
        Self.asSeconds(this)

    def <(rhs: Immutable[Time]): Boolean =
        Self.lt_sfTime(this, rhs.get)

    def <=(rhs: Immutable[Time]): Boolean =
        Self.le_sfTime(this, rhs.get)

    def >(rhs: Immutable[Time]): Boolean =
        Self.gt_sfTime(this, rhs.get)

    def >=(rhs: Immutable[Time]): Boolean =
        Self.ge_sfTime(this, rhs.get)

    def unary_- : Time =
        Time(Self.ng(this))

    def +(rhs: Immutable[Time]): Time =
        Time(Self.pl_sfTime(this, rhs.get))

    def +=(rhs: Immutable[Time]): Unit =
        Self.pL_sfTime(this, rhs.get)

    def -(rhs: Immutable[Time]): Time =
        Time(Self.mi_sfTime(this, rhs.get))

    def -=(rhs: Immutable[Time]): Unit =
        Self.mI_sfTime(this, rhs.get)

    def *(rhs: Float): Time =
        Time(Self.ml_float(this, rhs))

    def *(rhs: Long): Time =
        Time(Self.ml_longlong(this, rhs))

    def *=(rhs: Float): Unit =
        Self.mL_float(this, rhs)

    def *=(rhs: Long): Unit =
        Self.mL_longlong(this, rhs)

    def /(rhs: Float): Time =
        Time(Self.dv_float(this, rhs))

    def /(rhs: Long): Time =
        Time(Self.dv_longlong(this, rhs))

    def /=(rhs: Float): Unit =
        Self.dV_float(this, rhs)

    def /=(rhs: Long): Unit =
        Self.dV_longlong(this, rhs)

    def ==(rhs: Immutable[Time]): Boolean =
        Self.eq_sfTime(this, rhs.get)

    def !=(rhs: Immutable[Time]): Boolean =
        Self.ne_sfTime(this, rhs.get)

    /* Scala operators */

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: Time         => this == Immutable(rhs)
            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

object Time:

    private[sfml] inline given Conversion[Time, Ptr[Self.sfTime]]:
        override def apply(time: Time) = time._time.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfTime]

    object ctor:

        def apply(): ctor =
            Constructor(Self.ctor())

    def apply(): Time =
        new Object with Time(using ctor())

    private[sfml] def apply(f: Ptr[Self.sfTime] => Unit): Time =
        new Object with Time(using Constructor(f))

    /* Immutable */

    extension (time: Immutable[Time])

        def asMicroseconds: Long = time.get.asMicroseconds

        def asMilliseconds: Int = time.get.asMilliseconds

        def asSeconds: Float = time.get.asSeconds

        def <(rhs: Time): Boolean =
            time.get < rhs

        def <=(rhs: Time): Boolean =
            time.get <= rhs

        def >(rhs: Time): Boolean =
            time.get > rhs

        def >=(rhs: Time): Boolean =
            time.get >= rhs

        def unary_- : Time =
            -time.get

        def +(rhs: Time): Time =
            time.get + rhs

        def -(rhs: Time): Time =
            time.get - rhs

        def *(rhs: Float): Time =
            time.get * rhs

        def *(rhs: Long): Time =
            time.get * rhs

        def /(rhs: Float): Time =
            time.get / rhs

        def /(rhs: Long): Time =
            time.get / rhs

    /* Static methods */

    def Zero: Time =
        Time(Self.Zero())

    def microseconds(amount: Long): Time =
        Time(Self.microseconds(amount))

    def milliseconds(amount: Int): Time =
        Time(Self.milliseconds(amount))

    def seconds(amount: Float): Time =
        Time(Self.seconds(amount))
