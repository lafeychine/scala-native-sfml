package sfml
package system

import scala.scalanative.unsafe.*

import sfml.internal.system.String as Self

import sfml.stdlib.Locale

trait String(using private[sfml] ctor: String.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _string: ResourcePtr[Self.sfString] =
        ResourceBuffer(String.dtor)(ctor)

object String:

    private[sfml] inline given Conversion[String, Ptr[Self.sfString]]:
        override def apply(string: String) = string._string.ptr

    inline given Conversion[java.lang.String, String]:
        override def apply(javaString: java.lang.String) = String(javaString)

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfString]

    object ctor:

        def apply(string: java.lang.String, locale: Locale = Locale()): ctor =
            Constructor(r =>
                Zone {
                    Self.ctor(r, toCString(string), locale)
                }
            )

    def apply(string: java.lang.String, locale: Locale = Locale()): String =
        new Object with String(using ctor(string, locale))

    private[sfml] def dtor(ptr: Ptr[Self.sfString]): Unit =
        internal.stdlib.String.dtor(ptr)
