package sfml
package system

import scala.annotation.targetName
import scala.compiletime.error
import scala.math.Numeric.Implicits.infixNumericOps

import scala.scalanative.unsafe.*

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.system.Vector2.*

trait Vector2[T <: Float | Int](using private[sfml] val ctor: Vector2.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _vector2: ResourcePtr[sfVector2[T]] =
        ctor match
            case Vector2.Type.Float(ctor) => ResourceBuffer(ctor).asInstanceOf[ResourcePtr[sfVector2[T]]]
            case Vector2.Type.Int(ctor)   => ResourceBuffer(ctor).asInstanceOf[ResourcePtr[sfVector2[T]]]

    /* Methods */

    transparent inline def x: Float | Int =
        inline _vector2.ptr match
            case ptr: Ptr[sfVector2[Float]] => (ptr: Ptr[sfVector2[Float]])._1
            case ptr: Ptr[sfVector2[Int]]   => (ptr: Ptr[sfVector2[Int]])._1
            case _                          => error("This expression must be inside an inline function")

    inline def x_=(x: T): Unit =
        inline (_vector2.ptr, x) match
            case tpl: (Ptr[sfVector2[Float]], Float) => (tpl._1: Ptr[sfVector2[Float]])._1 = tpl._2
            case tpl: (Ptr[sfVector2[Int]], Int)     => (tpl._1: Ptr[sfVector2[Int]])._1 = tpl._2
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def y: Float | Int =
        inline _vector2.ptr match
            case ptr: Ptr[sfVector2[Float]] => (ptr: Ptr[sfVector2[Float]])._2
            case ptr: Ptr[sfVector2[Int]]   => (ptr: Ptr[sfVector2[Int]])._2
            case _                          => error("This expression must be inside an inline function")

    inline def y_=(y: T): Unit =
        inline (_vector2.ptr, y) match
            case tpl: (Ptr[sfVector2[Float]], Float) => (tpl._1: Ptr[sfVector2[Float]])._2 = tpl._2
            case tpl: (Ptr[sfVector2[Int]], Int)     => (tpl._1: Ptr[sfVector2[Int]])._2 = tpl._2
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def unary_- : Vector2[Float] | Vector2[Int] =
        inline _vector2.ptr match
            case ptr: Ptr[sfVector2[Float]] =>
                Vector2.toVector2Float[sfVector2[Float]](sfVector2f_ng)(ptr)

            case ptr: Ptr[sfVector2[Int]] =>
                Vector2.toVector2Int[sfVector2[Int]](sfVector2i_ng)(ptr)

            case _ => error("This expression must be inside an inline function")

    transparent inline def +(rhs: Immutable[Vector2[T]]): Vector2[Float] | Vector2[Int] =
        inline (_vector2.ptr, rhs.get._vector2.ptr) match
            case tpl: (Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]) =>
                Vector2.toVector2Float((x: Ptr[CStruct2[Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]]]) =>
                    sfVector2f_pl_sfVector2f(x._1, x._2)
                )(tpl._1, tpl._2)

            case tpl: (Ptr[sfVector2[Int]], Ptr[sfVector2[Int]]) =>
                Vector2.toVector2Int((x: Ptr[CStruct2[Ptr[sfVector2[Int]], Ptr[sfVector2[Int]]]]) =>
                    sfVector2i_pl_sfVector2i(x._1, x._2)
                )(tpl._1, tpl._2)

            case _ => error("This expression must be inside an inline function")

    inline def +=(rhs: Immutable[Vector2[T]]): Unit =
        inline (_vector2.ptr, rhs.get._vector2.ptr) match
            case tpl: (Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]) => sfVector2f_pL_sfVector2f(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector2[Int]], Ptr[sfVector2[Int]])     => sfVector2i_pL_sfVector2i(tpl._1, tpl._2); ()
            case _                                                   => error("This expression must be inside an inline function")

    transparent inline def -(rhs: Immutable[Vector2[T]]): Vector2[Float] | Vector2[Int] =
        inline (_vector2.ptr, rhs.get._vector2.ptr) match
            case tpl: (Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]) =>
                Vector2.toVector2Float((x: Ptr[CStruct2[Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]]]) =>
                    sfVector2f_mi_sfVector2f(x._1, x._2)
                )(tpl._1, tpl._2)

            case tpl: (Ptr[sfVector2[Int]], Ptr[sfVector2[Int]]) =>
                Vector2.toVector2Int((x: Ptr[CStruct2[Ptr[sfVector2[Int]], Ptr[sfVector2[Int]]]]) =>
                    sfVector2i_mi_sfVector2i(x._1, x._2)
                )(tpl._1, tpl._2)

            case _ => error("This expression must be inside an inline function")

    inline def -=(rhs: Immutable[Vector2[T]]): Unit =
        inline (_vector2.ptr, rhs.get._vector2.ptr) match
            case tpl: (Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]) => sfVector2f_mI_sfVector2f(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector2[Int]], Ptr[sfVector2[Int]])     => sfVector2i_mI_sfVector2i(tpl._1, tpl._2); ()
            case _                                                   => error("This expression must be inside an inline function")

    transparent inline def *(rhs: T): Vector2[Float] | Vector2[Int] =
        inline (_vector2.ptr, rhs) match
            case tpl: (Ptr[sfVector2[Float]], Float) =>
                Vector2.toVector2Float((x: Ptr[CStruct2[Ptr[sfVector2[Float]], CFloat]]) => sfVector2f_ml_float(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case tpl: (Ptr[sfVector2[Int]], Int) =>
                Vector2.toVector2Int((x: Ptr[CStruct2[Ptr[sfVector2[Int]], CInt]]) => sfVector2i_ml_int(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case _ => error("This expression must be inside an inline function")

    inline def *=(rhs: T): Unit =
        inline (_vector2.ptr, rhs) match
            case tpl: (Ptr[sfVector2[Float]], Float) => sfVector2f_mL_float(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector2[Int]], Int)     => sfVector2i_mL_int(tpl._1, tpl._2); ()
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def /(rhs: T): Vector2[Float] | Vector2[Int] =
        inline (_vector2.ptr, rhs) match
            case tpl: (Ptr[sfVector2[Float]], Float) =>
                Vector2.toVector2Float((x: Ptr[CStruct2[Ptr[sfVector2[Float]], CFloat]]) => sfVector2f_dv_float(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case tpl: (Ptr[sfVector2[Int]], Int) =>
                Vector2.toVector2Int((x: Ptr[CStruct2[Ptr[sfVector2[Int]], CInt]]) => sfVector2i_dv_int(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case _ => error("This expression must be inside an inline function")

    inline def /=(rhs: T): Unit =
        inline (_vector2.ptr, rhs) match
            case tpl: (Ptr[sfVector2[Float]], Float) => sfVector2f_dV_float(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector2[Int]], Int)     => sfVector2i_dV_int(tpl._1, tpl._2); ()
            case _                                   => error("This expression must be inside an inline function")

    inline def ==(rhs: Immutable[Vector2[T]]): Boolean =
        inline (_vector2.ptr, rhs.get._vector2.ptr) match
            case tpl: (Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]) => sfVector2f_eq_sfVector2f(tpl._1, tpl._2)
            case tpl: (Ptr[sfVector2[Int]], Ptr[sfVector2[Int]])     => sfVector2i_eq_sfVector2i(tpl._1, tpl._2)
            case _                                                   => error("This expression must be inside an inline function")

    inline def !=(rhs: Immutable[Vector2[T]]): Boolean =
        inline (_vector2.ptr, rhs.get._vector2.ptr) match
            case tpl: (Ptr[sfVector2[Float]], Ptr[sfVector2[Float]]) => sfVector2f_ne_sfVector2f(tpl._1, tpl._2)
            case tpl: (Ptr[sfVector2[Int]], Ptr[sfVector2[Int]])     => sfVector2i_ne_sfVector2i(tpl._1, tpl._2)
            case _                                                   => error("This expression must be inside an inline function")

    /* Scala operators */

    transparent inline def copy(inline x: Float | Int = x, inline y: Float | Int = y): Vector2[Int] | Vector2[Float] =
        inline x match
            case x: Float =>
                inline y match
                    case y: Float => Vector2[Float](x, y)
                    case y: Int   => Vector2[Float](x, y.toFloat)
                    case _: (Float | Int) =>
                        inline _vector2.ptr match
                            case ptr: Ptr[sfVector2[Float]] => Vector2[Float](x, (ptr: Ptr[sfVector2[Float]])._2)
                            case ptr: Ptr[sfVector2[Int]]   => Vector2[Float](x, (ptr: Ptr[sfVector2[Int]])._2.toFloat)
                            case _                          => error("This expression must be inside an inline function")

            case x: Int =>
                inline y match
                    case y: Float => Vector2[Float](x.toFloat, y)
                    case y: Int   => Vector2[Int](x, y)
                    case _: (Float | Int) =>
                        inline _vector2.ptr match
                            case ptr: Ptr[sfVector2[Float]] => Vector2[Float](x.toFloat, (ptr: Ptr[sfVector2[Float]])._2)
                            case ptr: Ptr[sfVector2[Int]]   => Vector2[Int](x, (ptr: Ptr[sfVector2[Int]])._2)
                            case _                          => error("This expression must be inside an inline function")

            case _: (Float | Int) =>
                inline y match
                    case y: Float =>
                        inline _vector2.ptr match
                            case ptr: Ptr[sfVector2[Float]] => Vector2[Float]((ptr: Ptr[sfVector2[Float]])._1, y)
                            case ptr: Ptr[sfVector2[Int]]   => Vector2[Float]((ptr: Ptr[sfVector2[Int]])._1.toFloat, y)
                            case _                          => error("This expression must be inside an inline function")

                    case y: Int =>
                        inline _vector2.ptr match
                            case ptr: Ptr[sfVector2[Float]] => Vector2[Float]((ptr: Ptr[sfVector2[Float]])._1, y.toFloat)
                            case ptr: Ptr[sfVector2[Int]]   => Vector2[Int]((ptr: Ptr[sfVector2[Int]])._1, y)
                            case _                          => error("This expression must be inside an inline function")

                    case _: (Float | Int) =>
                        inline _vector2.ptr match
                            case ptr: Ptr[sfVector2[Float]] =>
                                Vector2[Float]((ptr: Ptr[sfVector2[Float]])._1, (ptr: Ptr[sfVector2[Float]])._2)
                            case ptr: Ptr[sfVector2[Int]] =>
                                Vector2[Int]((ptr: Ptr[sfVector2[Int]])._1, (ptr: Ptr[sfVector2[Int]])._2)
                            case _ => error("This expression must be inside an inline function")

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: Vector2[?] =>
                ctor match
                    case Vector2.Type.Float(_) =>
                        rhs.ctor match
                            case Vector2.Type.Float(_) =>
                                this.asInstanceOf[Vector2[Float]] == Immutable(rhs.asInstanceOf[Vector2[Float]])
                            case _ => false

                    case Vector2.Type.Int(_) =>
                        rhs.ctor match
                            case Vector2.Type.Int(_) =>
                                this.asInstanceOf[Vector2[Int]] == Immutable(rhs.asInstanceOf[Vector2[Int]])
                            case _ => false

            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def toString(): java.lang.String =
        ctor match
            case Vector2.Type.Float(_) =>
                val v = this.asInstanceOf[Vector2[Float]]
                s"Vector2(${v.x}, ${v.y})"
            case Vector2.Type.Int(_) =>
                val v = this.asInstanceOf[Vector2[Int]]
                s"Vector2(${v.x}, ${v.y})"

object Vector2:

    private[sfml] inline given vector2FloatToSfVector2Float: Conversion[Vector2[Float], Ptr[sfVector2[Float]]]:
        override def apply(vector2: Vector2[Float]) = vector2._vector2.ptr

    private[sfml] inline given vector2IntToSfVector2Int: Conversion[Vector2[Int], Ptr[sfVector2[Int]]]:
        override def apply(vector2: Vector2[Int]) = vector2._vector2.ptr

    /* Constructors */

    private[sfml] enum Type:
        case Float(ctor: Constructor[sfVector2[scala.Float]])
        case Int(ctor: Constructor[sfVector2[scala.Int]])

    private[sfml] type ctor = Type

    object ctor:

        def apply[T <: Float | Int](x: T, y: T): ctor =
            (x, y) match
                case (x: Float, y: Float) => Type.Float(Constructor(sfVector2f_ctor(_, x, y)))
                case (x: Int, y: Int)     => Type.Int(Constructor(sfVector2i_ctor(_, x, y)))

        def apply[T <: Float | Int]()(using ev: Numeric[T]): ctor =
            Vector2.ctor(ev.zero, ev.zero)

    def apply[T <: Float | Int](x: T, y: T): Vector2[T] =
        new Object with Vector2[T](using ctor(x, y))

    def apply[T <: Float | Int]()(using Numeric[T]): Vector2[T] =
        new Object with Vector2[T](using ctor())

    @targetName("applyFloat")
    private[sfml] def apply(f: Ptr[sfVector2[Float]] => Unit): Vector2[Float] =
        new Object with Vector2[Float](using Type.Float(Constructor(f)))

    @targetName("applyInt")
    private[sfml] def apply(f: Ptr[sfVector2[Int]] => Unit): Vector2[Int] =
        new Object with Vector2[Int](using Type.Int(Constructor(f)))

    transparent inline def unapply[T <: Float | Int](vector2: Vector2[T]): (Float, Float) | (Int, Int) =
        inline vector2._vector2.ptr match
            case ptr: Ptr[sfVector2[Float]] =>
                val vector2 = ptr: Ptr[sfVector2[Float]]
                (vector2._1, vector2._2)

            case ptr: Ptr[sfVector2[Int]] =>
                val vector2 = ptr: Ptr[sfVector2[Int]]
                (vector2._1, vector2._2)

            case _ => error("This expression must be inside an inline function")

    inline given vector2FloatToVector2Int: Conversion[Vector2[Float], Vector2[Int]]:
        override def apply(vector2: Vector2[Float]) = Vector2(vector2.x.toInt, vector2.y.toInt)

    inline given vector2IntToVector2Float: Conversion[Vector2[Int], Vector2[Float]]:
        override def apply(vector2: Vector2[Int]) = Vector2(vector2.x.toFloat, vector2.y.toFloat)

    inline given tupleToVector2Float: [T: Numeric] => Conversion[(T, T), Vector2[Float]]:
        override def apply(tuple: (T, T)) = Vector2(tuple._1.toFloat, tuple._2.toFloat)

    inline given tupleToVector2Int: [T: Numeric] => Conversion[(T, T), Vector2[Int]]:
        override def apply(tuple: (T, T)) = Vector2(tuple._1.toInt, tuple._2.toInt)

    inline given tupleToImmutableVector2Float: [T: Numeric] => Conversion[(T, T), Immutable[Vector2[Float]]]:
        override def apply(tuple: (T, T)) = Vector2(tuple._1.toFloat, tuple._2.toFloat)

    inline given tupleToImmutableVector2Int: [T: Numeric] => Conversion[(T, T), Immutable[Vector2[Int]]]:
        override def apply(tuple: (T, T)) = Vector2(tuple._1.toInt, tuple._2.toInt)

    extension (vector2: Ptr[sfVector2[Float]])
        private[sfml] def toVector2Float(): Vector2[Float] =
            Vector2(vector2._1, vector2._2)

        private[sfml] def toInlineVector2f: sfVector2[Float] =
            sfVector2f_inlineHandler(vector2)

    extension (vector2: Ptr[sfVector2[Int]])
        private[sfml] def toVector2Int(): Vector2[Int] =
            Vector2(vector2._1, vector2._2)

        private[sfml] def toInlineVector2i: sfVector2[Int] =
            sfVector2i_inlineHandler(vector2)

    /* Immutable */

    extension [T <: Float | Int](vector2: Immutable[Vector2[T]])

        transparent inline def x: Float | Int = vector2.get.x

        transparent inline def y: Float | Int = vector2.get.y

        transparent inline def unary_- : Vector2[Float] | Vector2[Int] = -vector2.get

        transparent inline def +(rhs: Immutable[Vector2[T]]): Vector2[Float] | Vector2[Int] =
            vector2.get + rhs

        transparent inline def -(rhs: Immutable[Vector2[T]]): Vector2[Float] | Vector2[Int] =
            vector2.get - rhs

        transparent inline def *(rhs: T): Vector2[Float] | Vector2[Int] =
            vector2.get * rhs

        transparent inline def /(rhs: T): Vector2[Float] | Vector2[Int] =
            vector2.get / rhs

        transparent inline def copy(
            inline x: Float | Int = vector2.x,
            inline y: Float | Int = vector2.y
        ): Vector2[Float] | Vector2[Int] =
            vector2.get.copy(x, y)

    /* Return by value handlers */

    private[sfml] def toVector2Float(
        callback: CFuncPtr0[sfVector2[Float]]
    )(): Vector2[Float] =
        ReturnTypeHandler(sfVector2f_typeHandler, callback)().toVector2Float()

    private[sfml] def toVector2Float[T1](
        callback: CFuncPtr1[Ptr[T1], sfVector2[Float]]
    )(arg1: Ptr[T1]): Vector2[Float] =
        ReturnTypeHandler(sfVector2f_typeHandler, callback)(arg1).toVector2Float()

    private[sfml] def toVector2Float[T1: Tag, T2: Tag](
        callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], sfVector2[Float]]
    )(arg1: T1, arg2: T2): Vector2[Float] =
        ReturnTypeHandler(sfVector2f_typeHandler, callback)(arg1, arg2).toVector2Float()

    private[sfml] def toVector2Float[T1: Tag, T2: Tag, T3: Tag](
        callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], sfVector2[Float]]
    )(arg1: T1, arg2: T2, arg3: T3): Vector2[Float] =
        ReturnTypeHandler(sfVector2f_typeHandler, callback)(arg1, arg2, arg3).toVector2Float()

    private[sfml] def toVector2Int(
        callback: CFuncPtr0[sfVector2[Int]]
    )(): Vector2[Int] =
        ReturnTypeHandler(sfVector2i_typeHandler, callback)().toVector2Int()

    private[sfml] def toVector2Int[T1](
        callback: CFuncPtr1[Ptr[T1], sfVector2[Int]]
    )(arg1: Ptr[T1]): Vector2[Int] =
        ReturnTypeHandler(sfVector2i_typeHandler, callback)(arg1).toVector2Int()

    private[sfml] def toVector2Int[T1: Tag, T2: Tag](
        callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], sfVector2[Int]]
    )(arg1: T1, arg2: T2): Vector2[Int] =
        ReturnTypeHandler(sfVector2i_typeHandler, callback)(arg1, arg2).toVector2Int()

    private[sfml] def toVector2Int[T1: Tag, T2: Tag, T3: Tag](
        callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], sfVector2[Int]]
    )(arg1: T1, arg2: T2, arg3: T3): Vector2[Int] =
        ReturnTypeHandler(sfVector2i_typeHandler, callback)(arg1, arg2, arg3).toVector2Int()
