package sfml
package system

import scala.compiletime.error
import scala.math.Numeric.Implicits.infixNumericOps

import scala.scalanative.unsafe.*

import sfml.internal.Type.sfBoolToBoolean
import sfml.internal.system.Vector3.*

trait Vector3[T <: Float | Int](using private[sfml] val ctor: Vector3.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _vector3: ResourcePtr[sfVector3[T]] =
        ctor match
            case Vector3.Type.Float(ctor) => ResourceBuffer(ctor).asInstanceOf[ResourcePtr[sfVector3[T]]]
            case Vector3.Type.Int(ctor)   => ResourceBuffer(ctor).asInstanceOf[ResourcePtr[sfVector3[T]]]

    /* Methods */

    transparent inline def x: Float | Int =
        inline _vector3.ptr match
            case ptr: Ptr[sfVector3[Float]] => (ptr: Ptr[sfVector3[Float]])._1
            case ptr: Ptr[sfVector3[Int]]   => (ptr: Ptr[sfVector3[Int]])._1
            case _                          => error("This expression must be inside an inline function")

    inline def x_=(x: T): Unit =
        inline (_vector3.ptr, x) match
            case tpl: (Ptr[sfVector3[Float]], Float) => (tpl._1: Ptr[sfVector3[Float]])._1 = tpl._2
            case tpl: (Ptr[sfVector3[Int]], Int)     => (tpl._1: Ptr[sfVector3[Int]])._1 = tpl._2
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def y: Float | Int =
        inline _vector3.ptr match
            case ptr: Ptr[sfVector3[Float]] => (ptr: Ptr[sfVector3[Float]])._2
            case ptr: Ptr[sfVector3[Int]]   => (ptr: Ptr[sfVector3[Int]])._2
            case _                          => error("This expression must be inside an inline function")

    inline def y_=(y: T): Unit =
        inline (_vector3.ptr, y) match
            case tpl: (Ptr[sfVector3[Float]], Float) => (tpl._1: Ptr[sfVector3[Float]])._2 = tpl._2
            case tpl: (Ptr[sfVector3[Int]], Int)     => (tpl._1: Ptr[sfVector3[Int]])._2 = tpl._2
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def z: Float | Int =
        inline _vector3.ptr match
            case ptr: Ptr[sfVector3[Float]] => (ptr: Ptr[sfVector3[Float]])._3
            case ptr: Ptr[sfVector3[Int]]   => (ptr: Ptr[sfVector3[Int]])._3
            case _                          => error("This expression must be inside an inline function")

    inline def z_=(z: T): Unit =
        inline (_vector3.ptr, z) match
            case tpl: (Ptr[sfVector3[Float]], Float) => (tpl._1: Ptr[sfVector3[Float]])._3 = tpl._2
            case tpl: (Ptr[sfVector3[Int]], Int)     => (tpl._1: Ptr[sfVector3[Int]])._3 = tpl._2
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def unary_- : Vector3[Float] | Vector3[Int] =
        inline _vector3.ptr match
            case ptr: Ptr[sfVector3[Float]] =>
                Vector3.toVector3Float[sfVector3[Float]](sfVector3f_ng)(ptr)

            case ptr: Ptr[sfVector3[Int]] =>
                Vector3.toVector3Int[sfVector3[Int]](sfVector3i_ng)(ptr)

            case _ => error("This expression must be inside an inline function")

    transparent inline def +(rhs: Immutable[Vector3[T]]): Vector3[Float] | Vector3[Int] =
        inline (_vector3.ptr, rhs.get._vector3.ptr) match
            case tpl: (Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]) =>
                Vector3.toVector3Float((x: Ptr[CStruct2[Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]]]) =>
                    sfVector3f_pl_sfVector3f(x._1, x._2)
                )(tpl._1, tpl._2)

            case tpl: (Ptr[sfVector3[Int]], Ptr[sfVector3[Int]]) =>
                Vector3.toVector3Int((x: Ptr[CStruct2[Ptr[sfVector3[Int]], Ptr[sfVector3[Int]]]]) =>
                    sfVector3i_pl_sfVector3i(x._1, x._2)
                )(tpl._1, tpl._2)

            case _ => error("This expression must be inside an inline function")

    inline def +=(rhs: Immutable[Vector3[T]]): Unit =
        inline (_vector3.ptr, rhs.get._vector3.ptr) match
            case tpl: (Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]) => sfVector3f_pL_sfVector3f(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector3[Int]], Ptr[sfVector3[Int]])     => sfVector3i_pL_sfVector3i(tpl._1, tpl._2); ()
            case _                                                   => error("This expression must be inside an inline function")

    transparent inline def -(rhs: Immutable[Vector3[T]]): Vector3[Float] | Vector3[Int] =
        inline (_vector3.ptr, rhs.get._vector3.ptr) match
            case tpl: (Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]) =>
                Vector3.toVector3Float((x: Ptr[CStruct2[Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]]]) =>
                    sfVector3f_mi_sfVector3f(x._1, x._2)
                )(tpl._1, tpl._2)

            case tpl: (Ptr[sfVector3[Int]], Ptr[sfVector3[Int]]) =>
                Vector3.toVector3Int((x: Ptr[CStruct2[Ptr[sfVector3[Int]], Ptr[sfVector3[Int]]]]) =>
                    sfVector3i_mi_sfVector3i(x._1, x._2)
                )(tpl._1, tpl._2)

            case _ => error("This expression must be inside an inline function")

    inline def -=(rhs: Immutable[Vector3[T]]): Unit =
        inline (_vector3.ptr, rhs.get._vector3.ptr) match
            case tpl: (Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]) => sfVector3f_mI_sfVector3f(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector3[Int]], Ptr[sfVector3[Int]])     => sfVector3i_mI_sfVector3i(tpl._1, tpl._2); ()
            case _                                                   => error("This expression must be inside an inline function")

    transparent inline def *(rhs: T): Vector3[Float] | Vector3[Int] =
        inline (_vector3.ptr, rhs) match
            case tpl: (Ptr[sfVector3[Float]], Float) =>
                Vector3.toVector3Float((x: Ptr[CStruct2[Ptr[sfVector3[Float]], CFloat]]) => sfVector3f_ml_float(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case tpl: (Ptr[sfVector3[Int]], Int) =>
                Vector3.toVector3Int((x: Ptr[CStruct2[Ptr[sfVector3[Int]], CInt]]) => sfVector3i_ml_int(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case _ => error("This expression must be inside an inline function")

    inline def *=(rhs: T): Unit =
        inline (_vector3.ptr, rhs) match
            case tpl: (Ptr[sfVector3[Float]], Float) => sfVector3f_mL_float(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector3[Int]], Int)     => sfVector3i_mL_int(tpl._1, tpl._2); ()
            case _                                   => error("This expression must be inside an inline function")

    transparent inline def /(rhs: T): Vector3[Float] | Vector3[Int] =
        inline (_vector3.ptr, rhs) match
            case tpl: (Ptr[sfVector3[Float]], Float) =>
                Vector3.toVector3Float((x: Ptr[CStruct2[Ptr[sfVector3[Float]], CFloat]]) => sfVector3f_dv_float(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case tpl: (Ptr[sfVector3[Int]], Int) =>
                Vector3.toVector3Int((x: Ptr[CStruct2[Ptr[sfVector3[Int]], CInt]]) => sfVector3i_dv_int(x._1, x._2))(
                    tpl._1,
                    tpl._2
                )

            case _ => error("This expression must be inside an inline function")

    inline def /=(rhs: T): Unit =
        inline (_vector3.ptr, rhs) match
            case tpl: (Ptr[sfVector3[Float]], Float) => sfVector3f_dV_float(tpl._1, tpl._2); ()
            case tpl: (Ptr[sfVector3[Int]], Int)     => sfVector3i_dV_int(tpl._1, tpl._2); ()
            case _                                   => error("This expression must be inside an inline function")

    inline def ==(rhs: Immutable[Vector3[T]]): Boolean =
        inline (_vector3.ptr, rhs.get._vector3.ptr) match
            case tpl: (Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]) => sfVector3f_eq_sfVector3f(tpl._1, tpl._2)
            case tpl: (Ptr[sfVector3[Int]], Ptr[sfVector3[Int]])     => sfVector3i_eq_sfVector3i(tpl._1, tpl._2)
            case _                                                   => error("This expression must be inside an inline function")

    inline def !=(rhs: Immutable[Vector3[T]]): Boolean =
        inline (_vector3.ptr, rhs.get._vector3.ptr) match
            case tpl: (Ptr[sfVector3[Float]], Ptr[sfVector3[Float]]) => sfVector3f_ne_sfVector3f(tpl._1, tpl._2)
            case tpl: (Ptr[sfVector3[Int]], Ptr[sfVector3[Int]])     => sfVector3i_ne_sfVector3i(tpl._1, tpl._2)
            case _                                                   => error("This expression must be inside an inline function")

    /* Scala operators */

    override def equals(obj: Any): Boolean =
        obj.asInstanceOf[Matchable] match
            case rhs: Vector3[?] =>
                ctor match
                    case Vector3.Type.Float(_) =>
                        rhs.ctor match
                            case Vector3.Type.Float(_) =>
                                this.asInstanceOf[Vector3[Float]] == Immutable(rhs.asInstanceOf[Vector3[Float]])
                            case _ => false

                    case Vector3.Type.Int(_) =>
                        rhs.ctor match
                            case Vector3.Type.Int(_) =>
                                this.asInstanceOf[Vector3[Int]] == Immutable(rhs.asInstanceOf[Vector3[Int]])
                            case _ => false

            case rhs: Immutable[?] => rhs.equals(this)
            case _                 => false

    @SuppressWarnings(Array("org.wartremover.warts.Any"))
    override def toString(): java.lang.String =
        ctor match
            case Vector3.Type.Float(_) =>
                val v = this.asInstanceOf[Vector3[Float]]
                s"Vector3(${v.x}, ${v.y}, ${v.z})"
            case Vector3.Type.Int(_) =>
                val v = this.asInstanceOf[Vector3[Int]]
                s"Vector3(${v.x}, ${v.y}, ${v.z})"

object Vector3:

    private[sfml] inline given vector3FloatToSfVector3Float: Conversion[Vector3[Float], Ptr[sfVector3[Float]]]:
        override def apply(vector3: Vector3[Float]) = vector3._vector3.ptr

    private[sfml] inline given vector3IntToSfVector3Int: Conversion[Vector3[Int], Ptr[sfVector3[Int]]]:
        override def apply(vector3: Vector3[Int]) = vector3._vector3.ptr

    /* Constructors */

    private[sfml] enum Type:
        case Float(ctor: Constructor[sfVector3[scala.Float]])
        case Int(ctor: Constructor[sfVector3[scala.Int]])

    private[sfml] type ctor = Type

    object ctor:

        def apply[T <: Float | Int](x: T, y: T, z: T): ctor =
            (x, y, z) match
                case (x: Float, y: Float, z: Float) => Type.Float(Constructor(sfVector3f_ctor(_, x, y, z)))
                case (x: Int, y: Int, z: Int)       => Type.Int(Constructor(sfVector3i_ctor(_, x, y, z)))

        def apply[T <: Float | Int]()(using ev: Numeric[T]): ctor =
            Vector3.ctor(ev.zero, ev.zero, ev.zero)

    def apply[T <: Float | Int](x: T, y: T, z: T): Vector3[T] =
        new Object with Vector3[T](using ctor(x, y, z))

    def apply[T <: Float | Int]()(using Numeric[T]): Vector3[T] =
        new Object with Vector3[T](using ctor())

    inline def unapply[T <: Float | Int](vector3: Vector3[T]): (Float, Float, Float) | (Int, Int, Int) =
        inline vector3._vector3.ptr match
            case ptr: Ptr[sfVector3[Float]] =>
                val vector3 = ptr: Ptr[sfVector3[Float]]
                (vector3._1, vector3._2, vector3._3)

            case ptr: Ptr[sfVector3[Int]] =>
                val vector3 = ptr: Ptr[sfVector3[Int]]
                (vector3._1, vector3._2, vector3._3)

            case _ => error("This expression must be inside an inline function")

    inline given vector3FloatToVector3Int: Conversion[Vector3[Float], Vector3[Int]]:
        override def apply(vector3: Vector3[Float]) = Vector3(vector3.x.toInt, vector3.y.toInt, vector3.z.toInt)

    inline given vector3IntToVector3Float: Conversion[Vector3[Int], Vector3[Float]]:
        override def apply(vector3: Vector3[Int]) = Vector3(vector3.x.toFloat, vector3.y.toFloat, vector3.z.toFloat)

    inline given tupleToVector3Float: [T: Numeric] => Conversion[(T, T, T), Vector3[Float]]:
        override def apply(tuple: (T, T, T)) = Vector3(tuple._1.toFloat, tuple._2.toFloat, tuple._3.toFloat)

    inline given tupleToVector3Int: [T: Numeric] => Conversion[(T, T, T), Vector3[Int]]:
        override def apply(tuple: (T, T, T)) = Vector3(tuple._1.toInt, tuple._2.toInt, tuple._3.toInt)

    extension (vector3: Ptr[sfVector3[Float]])
        private[sfml] def toVector3Float(): Vector3[Float] =
            Vector3(vector3._1, vector3._2, vector3._3)

    extension (vector3: Ptr[sfVector3[Int]])
        private[sfml] def toVector3Int(): Vector3[Int] =
            Vector3(vector3._1, vector3._2, vector3._3)

    /* Immutable */

    extension [T <: Float | Int](vector3: Immutable[Vector3[T]])

        transparent inline def x: Float | Int = vector3.get.x

        transparent inline def y: Float | Int = vector3.get.y

        transparent inline def z: Float | Int = vector3.get.z

        transparent inline def unary_- : Vector3[Float] | Vector3[Int] = -vector3.get

        transparent inline def +(rhs: Immutable[Vector3[T]]): Vector3[Float] | Vector3[Int] =
            vector3.get + rhs

        transparent inline def -(rhs: Immutable[Vector3[T]]): Vector3[Float] | Vector3[Int] =
            vector3.get - rhs

        transparent inline def *(rhs: T): Vector3[Float] | Vector3[Int] =
            vector3.get * rhs

        transparent inline def /(rhs: T): Vector3[Float] | Vector3[Int] =
            vector3.get / rhs

    /* Return by value handlers */

    private[sfml] def toVector3Float(
        callback: CFuncPtr0[sfVector3[Float]]
    )(): Vector3[Float] =
        ReturnTypeHandler(sfVector3f_typeHandler, callback)().toVector3Float()

    private[sfml] def toVector3Float[T1](
        callback: CFuncPtr1[Ptr[T1], sfVector3[Float]]
    )(arg1: Ptr[T1]): Vector3[Float] =
        ReturnTypeHandler(sfVector3f_typeHandler, callback)(arg1).toVector3Float()

    private[sfml] def toVector3Float[T1: Tag, T2: Tag](
        callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], sfVector3[Float]]
    )(arg1: T1, arg2: T2): Vector3[Float] =
        ReturnTypeHandler(sfVector3f_typeHandler, callback)(arg1, arg2).toVector3Float()

    private[sfml] def toVector3Float[T1: Tag, T2: Tag, T3: Tag](
        callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], sfVector3[Float]]
    )(arg1: T1, arg2: T2, arg3: T3): Vector3[Float] =
        ReturnTypeHandler(sfVector3f_typeHandler, callback)(arg1, arg2, arg3).toVector3Float()

    private[sfml] def toVector3Int(
        callback: CFuncPtr0[sfVector3[Int]]
    )(): Vector3[Int] =
        ReturnTypeHandler(sfVector3i_typeHandler, callback)().toVector3Int()

    private[sfml] def toVector3Int[T1](
        callback: CFuncPtr1[Ptr[T1], sfVector3[Int]]
    )(arg1: Ptr[T1]): Vector3[Int] =
        ReturnTypeHandler(sfVector3i_typeHandler, callback)(arg1).toVector3Int()

    private[sfml] def toVector3Int[T1: Tag, T2: Tag](
        callback: CFuncPtr1[Ptr[CStruct2[T1, T2]], sfVector3[Int]]
    )(arg1: T1, arg2: T2): Vector3[Int] =
        ReturnTypeHandler(sfVector3i_typeHandler, callback)(arg1, arg2).toVector3Int()

    private[sfml] def toVector3Int[T1: Tag, T2: Tag, T3: Tag](
        callback: CFuncPtr1[Ptr[CStruct3[T1, T2, T3]], sfVector3[Int]]
    )(arg1: T1, arg2: T2, arg3: T3): Vector3[Int] =
        ReturnTypeHandler(sfVector3i_typeHandler, callback)(arg1, arg2, arg3).toVector3Int()
