package sfml

/** Base module of SFML, defining various utilities.
  *
  * It provides vector classes, Unicode strings and conversion functions, threads and mutexes, timing classes.
  *
  * @doxygenId
  *   group__system
  */
package object system
