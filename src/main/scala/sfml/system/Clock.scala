package sfml
package system

import scala.scalanative.unsafe.*

import sfml.internal.system.Clock as Self

trait Clock(using private[sfml] ctor: Clock.ctor):

    /* Fields */

    private[sfml] val _clock: ResourcePtr[Self.sfClock] =
        ResourceBuffer(Self.dtor)(ctor)

    /* Methods */

    def elapsedTime: Time =
        Time(Self.getElapsedTime(this))

    def restart(): Time =
        Time(Self.restart(this))

object Clock:

    private[sfml] inline given Conversion[Clock, Ptr[Self.sfClock]]:
        override def apply(clock: Clock) = clock._clock.ptr

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.sfClock]

    object ctor:

        def apply(): ctor =
            Constructor(Self.ctor())

    def apply(): Clock =
        new Object with Clock(using ctor())

    private[sfml] def apply(f: Ptr[Self.sfClock] => Unit): Clock =
        new Object with Clock(using Constructor(f))

    /* Immutable */

    extension (clock: Immutable[Clock])

        def elapsedTime: Time =
            clock.get.elapsedTime
