package sfml

import scala.annotation.tailrec
import scala.quoted.*

import scala.scalanative.unsafe.*

class Constructor[T](val ctor: Ptr[T] => Unit)

object Constructor:

    private[sfml] inline given [T] => Conversion[Constructor[T], Ptr[T] => Unit]:
        override def apply(self: Constructor[T]) = self.ctor

    private inline def getTypeNameOf[T]: String = ${ getTypeNameOfImpl[T] }

    private def getTypeNameOfImpl[T: Type](using Quotes): Expr[String] =
        Expr(quotes.reflect.TypeRepr.of[T].dealiasKeepOpaques.show)

    @SuppressWarnings(Array("org.wartremover.warts.Throw"))
    inline given [T] => Constructor[T] =
        @tailrec
        def findName(l: List[String]): String =
            l match
                case Nil              => "<unknown>"
                case name :: Nil      => name
                case name :: _ :: Nil => name
                case _ :: xs          => findName(xs)

        val derivedType = findName(getTypeNameOf[T].split("\\.").toList)

        Constructor[T](_ =>
            throw new RuntimeException(
                "\n\n" +
                    "An object derives the SFML object `" + derivedType + "` that requires a constructor.\n" +
                    "  Got:      class A extends " + derivedType + "\n" +
                    "  Expected: class A extends " + derivedType + "(using " + derivedType + ".ctor(...))\n"
            )
        )
