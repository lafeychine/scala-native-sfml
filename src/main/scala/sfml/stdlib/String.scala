package sfml
package stdlib

import scala.scalanative.unsafe.*

import sfml.internal.stdlib.String as Self

trait String(using private[sfml] ctor: String.ctor):

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _string: ResourcePtr[Self.stdString] =
        ResourceBuffer(Self.dtor)(ctor)

object String:

    private[sfml] inline given Conversion[String, Ptr[Self.stdString]]:
        override def apply(string: String) = string._string.ptr

    inline given Conversion[java.lang.String, String]:
        override def apply(javaString: java.lang.String) = String(javaString)

    /* Constructors */

    private[sfml] type ctor = Constructor[Self.stdString]

    object ctor:

        def apply(javaString: java.lang.String): ctor =
            Constructor(r =>
                Zone {
                    Self.ctor(r, toCString(javaString))
                }
            )

    def apply(javaString: java.lang.String): String =
        new Object with String(using ctor(javaString))
