package sfml
package stdlib

import scala.scalanative.libc.stdlib.free
import scala.scalanative.unsafe.*
import scala.scalanative.unsigned.UnsignedRichInt

import sfml.internal.stdlib.Vector as Self

private[sfml] object Vector:

    def dtor(vector: Ptr[Self.stdVector]): Unit =
        if vector._1 == null then () else free(vector._1)
