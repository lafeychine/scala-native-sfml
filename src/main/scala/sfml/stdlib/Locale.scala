package sfml
package stdlib

import scala.scalanative.unsafe.*

import sfml.internal.stdlib.Locale as Self

trait Locale:

    /* Fields */

    @scala.annotation.threadUnsafe
    private[sfml] lazy val _locale: ResourcePtr[Self.stdLocale] =
        ResourceBuffer(Self.ctor(_))

object Locale:

    private[sfml] inline given Conversion[Locale, Ptr[Self.stdLocale]]:
        override def apply(locale: Locale) = locale._locale.ptr

    /* Constructors */

    def apply(): Locale =
        new Object with Locale
