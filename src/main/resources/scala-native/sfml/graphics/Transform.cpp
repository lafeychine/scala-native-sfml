#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

extern "C" void ___ZN2sf9TransformC2Ev(sf::Transform * result)
{
    new (result) sf::Transform();
}

extern "C" void ___ZN2sf9TransformC2Efffffffff(sf::Transform * result, float a00, float a01, float a02, float a10, float a11, float a12, float a20, float a21, float a22)
{
    new (result) sf::Transform(a00, a01, a02, a10, a11, a12, a20, a21, a22);
}

extern "C" void ___ZN2sf9TransformD2Ev(sf::Transform & self)
{
    self.~Transform();
}

extern "C" const float * ___ZNK2sf9Transform9getMatrixEv(const sf::Transform & self)
{
    return self.getMatrix();
}

extern "C" void ___ZNK2sf9Transform10getInverseEv(sf::Transform * result, const sf::Transform & self)
{
    new (result) sf::Transform(self.getInverse());
}

extern "C" void ___ZNK2sf9Transform14transformPointEff(sf::Vector2<float> * result, const sf::Transform & self, float x, float y)
{
    new (result) sf::Vector2<float>(self.transformPoint(x, y));
}

extern "C" void ___ZNK2sf9Transform14transformPointERKNS_7Vector2IfEE(sf::Vector2<float> * result, const sf::Transform & self, const sf::Vector2<float> & point)
{
    new (result) sf::Vector2<float>(self.transformPoint(point));
}

extern "C" void ___ZNK2sf9Transform13transformRectERKNS_4RectIfEE(sf::Rect<float> * result, const sf::Transform & self, const sf::Rect<float> & rectangle)
{
    new (result) sf::Rect<float>(self.transformRect(rectangle));
}

extern "C" sf::Transform & ___ZN2sf9Transform7combineERKS0_(sf::Transform & self, const sf::Transform & other)
{
    return self.combine(other);
}

extern "C" sf::Transform & ___ZN2sf9Transform9translateEff(sf::Transform & self, float x, float y)
{
    return self.translate(x, y);
}

extern "C" sf::Transform & ___ZN2sf9Transform9translateERKNS_7Vector2IfEE(sf::Transform & self, const sf::Vector2<float> & offset)
{
    return self.translate(offset);
}

extern "C" sf::Transform & ___ZN2sf9Transform6rotateEf(sf::Transform & self, float angle)
{
    return self.rotate(angle);
}

extern "C" sf::Transform & ___ZN2sf9Transform6rotateEfff(sf::Transform & self, float angle, float centerX, float centerY)
{
    return self.rotate(angle, centerX, centerY);
}

extern "C" sf::Transform & ___ZN2sf9Transform6rotateEfRKNS_7Vector2IfEE(sf::Transform & self, float angle, const sf::Vector2<float> & center)
{
    return self.rotate(angle, center);
}

extern "C" sf::Transform & ___ZN2sf9Transform5scaleEff(sf::Transform & self, float scaleX, float scaleY)
{
    return self.scale(scaleX, scaleY);
}

extern "C" sf::Transform & ___ZN2sf9Transform5scaleEffff(sf::Transform & self, float scaleX, float scaleY, float centerX, float centerY)
{
    return self.scale(scaleX, scaleY, centerX, centerY);
}

extern "C" sf::Transform & ___ZN2sf9Transform5scaleERKNS_7Vector2IfEE(sf::Transform & self, const sf::Vector2<float> & factors)
{
    return self.scale(factors);
}

extern "C" sf::Transform & ___ZN2sf9Transform5scaleERKNS_7Vector2IfEES4_(sf::Transform & self, const sf::Vector2<float> & factors, const sf::Vector2<float> & center)
{
    return self.scale(factors, center);
}

extern "C" void ___ZN2sf9Transform8IdentityE(sf::Transform * result)
{
    new (result) sf::Transform(std::move(sf::Transform::Identity));
}

extern "C" void ___ZN2sfmlERKNS_9TransformES2_(sf::Transform * result, const sf::Transform & left, const sf::Transform & right)
{
    new (result) sf::Transform(left * right);
}

extern "C" void ___ZN2sfmLERNS_9TransformERKS0_(sf::Transform & left, const sf::Transform & right)
{
    left *= right;
}

extern "C" void ___ZN2sfmlERKNS_9TransformERKNS_7Vector2IfEE(sf::Vector2<float> * result, const sf::Transform & left, const sf::Vector2<float> & right)
{
    new (result) sf::Vector2<float>(left * right);
}

extern "C" bool ___ZN2sfeqERKNS_9TransformES2_(const sf::Transform & left, const sf::Transform & right)
{
    return left == right;
}

extern "C" bool ___ZN2sfneERKNS_9TransformES2_(const sf::Transform & left, const sf::Transform & right)
{
    return left != right;
}
