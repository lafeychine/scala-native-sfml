#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

extern "C" void ___ZN2sf7TextureC2Ev(sf::Texture * result)
{
    new (result) sf::Texture();
}

// TODO: Texture(const Texture& copy)

extern "C" void ___ZN2sf7TextureD2Ev(sf::Texture & self)
{
    self.~Texture();
}

extern "C" bool ___ZN2sf7Texture6createEjj(sf::Texture & self, unsigned int width, unsigned int height)
{
    return self.create(width, height);
}

extern "C" bool ___ZN2sf7Texture12loadFromFileERKSsRKNS_4RectIiEE(sf::Texture & self, const std::string & filename, const sf::IntRect & area)
{
    return self.loadFromFile(filename, area);
}

// TODO: bool loadFromMemory(const void* data, std::size_t size, const IntRect& area)

// TODO: bool loadFromStream(InputStream& stream, const IntRect& area)

// TODO: bool loadFromImage(const Image& image, const IntRect& area)

// TODO: void getSize(sf::Vector2<unsigned int> * result) const

extern "C" void ___ZNK2sf7Texture11copyToImageEv(sf::Image * result, const sf::Texture & self)
{
    new (result) sf::Image(std::move(self.copyToImage()));
}

// TODO: void update(const Uint8* pixels)

// TODO: void update(const Uint8* pixels, unsigned int width, unsigned int height, unsigned int x, unsigned int y)

// TODO: void update(const Texture& texture)

// TODO: void update(const Texture& texture, unsigned int x, unsigned int y)

// TODO: void update(const Image& image)

// TODO: void update(const Image& image, unsigned int x, unsigned int y)

extern "C" void ___ZN2sf7Texture6updateERKNS_6WindowE(sf::Texture & self, const sf::Window & window)
{
    self.update(window);
}

extern "C" void ___ZN2sf7Texture6updateERKNS_6WindowEjj(sf::Texture & self, const sf::Window & window, unsigned int x, unsigned int y)
{
    self.update(window, x, y);
}

extern "C" void ___ZN2sf7Texture9setSmoothEb(sf::Texture & self, bool smooth)
{
    self.setSmooth(smooth);
}

extern "C" bool ___ZNK2sf7Texture8isSmoothEv(const sf::Texture & self)
{
    return self.isSmooth();
}

// TODO: void setSrgb(bool sRgb)

// TODO: bool isSrgb() const

extern "C" void ___ZN2sf7Texture11setRepeatedEb(sf::Texture & self, bool repeated)
{
    self.setRepeated(repeated);
}

extern "C" bool ___ZNK2sf7Texture10isRepeatedEv(const sf::Texture & self)
{
    return self.isRepeated();
}

// TODO: bool generateMipmap()

// TODO: Texture& operator =(const Texture& right)

// TODO: void swap(Texture& right)

// TODO: unsigned int getNativeHandle() const

// TODO: static void bind(const Texture* texture, CoordinateType coordinateType)

// TODO: static unsigned int getMaximumSize()
