#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

// TODO: RenderWindow()

extern "C" void ___ZN2sf12RenderWindowC2ENS_9VideoModeERKNS_6StringEjRKNS_15ContextSettingsE(sf::RenderWindow * result, const sf::VideoMode & mode, const sf::String & title, unsigned int style, const sf::ContextSettings & settings)
{
    new (result) sf::RenderWindow(mode, title, style, settings);
}

// TODO: RenderWindow(sf::WindowHandle handle, const sf::ContextSettings& settings)

extern "C" void ___ZN2sf12RenderWindowD2Ev(sf::RenderWindow & self)
{
    self.~RenderWindow();
}

extern "C" void ___ZNK2sf12RenderWindow7getSizeEv(sf::Vector2<unsigned int> * result, const sf::RenderWindow & self)
{
    new (result) sf::Vector2<unsigned int>(self.getSize());
}

// TODO: bool isSrgb() const

// TODO: bool setActive(bool active)

// TODO: Image capture() const
