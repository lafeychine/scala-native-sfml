#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

// TODO: Image()

extern "C" void ___ZN2sf5ImageD2Ev(sf::Image & self)
{
    self.~Image();
}

// TODO: void create(unsigned int width, unsigned int height, const Color & color)

// TODO: void create(unsigned int width, unsigned int height, const Uint8 * pixels)

// TODO: bool loadFromFile(const std::string & filename)

// TODO: bool loadFromMemory(const void * data, std::size_t size)

// TODO: bool loadFromStream(InputStream & stream)

extern "C" bool ___ZNK2sf5Image10saveToFileERKSs(const sf::Image & self, const std::string & filename)
{
    return self.saveToFile(filename);
}

// TODO: bool saveToMemory(std::vector<Uint8> & output, const std::string & format) const

// TODO: Vector2<unsigned int> getSize() const

// TODO: void createMaskFromColor(const Color & color, Uint8 alpha)

// TODO: void copy(const Image & source, unsigned int destX, unsigned int destY, const IntRect & sourceRect, bool applyAlpha)

// TODO: void setPixel(unsigned int x, unsigned int y, const Color & color)

// TODO: Color getPixel(unsigned int x, unsigned int y) const

// TODO: const Uint8 * getPixelsPtr() const

// TODO: void flipHorizontally()

// TODO: void flipVertically()
