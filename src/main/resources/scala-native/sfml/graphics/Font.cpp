#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

extern "C" void ___ZN2sf4FontC2Ev(sf::Font * result)
{
    new (result) sf::Font();
}

// TODO: Font(const Font& copy)

extern "C" void ___ZN2sf4FontD2Ev(sf::Font & self)
{
    self.~Font();
}

extern "C" bool ___ZN2sf4Font12loadFromFileERKSs(sf::Font & self, std::string const & filename)
{
    return self.loadFromFile(filename);
}

// TODO: bool loadFromMemory(const void* data, std::size_t sizeInBytes)

// TODO: bool loadFromStream(InputStream& stream)

// TODO: const Info& getInfo() const

// TODO: const sf::Glyph& getGlyph(sf::Uint32 codePoint, unsigned int characterSize, bool bold, float outlineThickness) const

// TODO: bool hasGlyph(sf::Uint32 codePoint) const

// TODO: float getKerning(sf::Uint32 first, sf::Uint32 second, unsigned int characterSize, bool bold) const

// TODO: float getLineSpacing(unsigned int characterSize) const

// TODO: float getUnderlinePosition(unsigned int characterSize) const

// TODO: float getUnderlineThickness(unsigned int characterSize) const

// TODO: const Texture& getTexture(unsigned int characterSize) const

// TODO: void setSmooth(bool smooth)

// TODO: bool isSmooth() const

// TODO: Font& operator =(const Font& right)
