#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

extern "C" void ___ZN2sf4ViewC2Ev(sf::View * result)
{
    new (result) sf::View();
}

extern "C" void ___ZN2sf4ViewC2ERKNS_4RectIfEE(sf::View * result, sf::Rect<float> const & rectangle)
{
    new (result) sf::View(rectangle);
}

extern "C" void ___ZN2sf4ViewC2ERKNS_7Vector2IfEES4_(sf::View * result, sf::Vector2<float> const & center, sf::Vector2<float> const & size)
{
    new (result) sf::View(center, size);
}

extern "C" void ___ZN2sf4ViewD2Ev(sf::View & self)
{
    self.~View();
}

extern "C" void ___ZN2sf4View9setCenterEff(sf::View & self, float x, float y)
{
    self.setCenter(x, y);
}

extern "C" void ___ZN2sf4View9setCenterERKNS_7Vector2IfEE(sf::View & self, sf::Vector2<float> const & center)
{
    self.setCenter(center);
}

extern "C" void ___ZN2sf4View7setSizeEff(sf::View & self, float x, float y)
{
    self.setSize(x, y);
}

extern "C" void ___ZN2sf4View7setSizeERKNS_7Vector2IfEE(sf::View & self, sf::Vector2<float> const & size)
{
    self.setSize(size);
}

extern "C" void ___ZN2sf4View11setRotationEf(sf::View & self, float angle)
{
    self.setRotation(angle);
}

extern "C" void ___ZN2sf4View11setViewportERKNS_4RectIfEE(sf::View & self, sf::Rect<float> const & viewport)
{
    self.setViewport(viewport);
}

extern "C" void ___ZN2sf4View5resetERKNS_4RectIfEE(sf::View & self, sf::Rect<float> const & rectangle)
{
    self.reset(rectangle);
}

extern "C" const sf::Vector2<float> & ___ZNK2sf4View9getCenterEv(const sf::View & self)
{
    return self.getCenter();
}

extern "C" const sf::Vector2<float> & ___ZNK2sf4View7getSizeEv(const sf::View & view)
{
    return view.getSize();
}

extern "C" float ___ZNK2sf4View11getRotationEv(const sf::View & view)
{
    return view.getRotation();
}

extern "C" const sf::Rect<float> & ___ZNK2sf4View11getViewportEv(const sf::View & view)
{
    return view.getViewport();
}

extern "C" void ___ZN2sf4View4moveEff(sf::View & view, float offsetX, float offsetY)
{
    view.move(offsetX, offsetY);
}

extern "C" void ___ZN2sf4View4moveERKNS_7Vector2IfEE(sf::View & view, sf::Vector2<float> const & offset)
{
    view.move(offset);
}

extern "C" void ___ZN2sf4View6rotateEf(sf::View & view, float angle)
{
    view.rotate(angle);
}

extern "C" void ___ZN2sf4View4zoomEf(sf::View & view, float factor)
{
    view.zoom(factor);
}

extern "C" const sf::Transform & ___ZNK2sf4View12getTransformEv(const sf::View & view)
{
    return view.getTransform();
}

extern "C" const sf::Transform & ___ZNK2sf4View19getInverseTransformEv(const sf::View & view)
{
    return view.getInverseTransform();
}
