#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

extern "C" void ___ZN2sf4TimeC2Ev(sf::Time * result)
{
    new (result) sf::Time();
}

extern "C" void ___ZN2sf4TimeD2Ev(sf::Time & self)
{
    self.~Time();
}

extern "C" float ___ZNK2sf4Time9asSecondsEv(sf::Time self)
{
    return self.asSeconds();
}

extern "C" int ___ZNK2sf4Time14asMillisecondsEv(sf::Time self)
{
    return self.asMilliseconds();
}

extern "C" long long ___ZNK2sf4Time14asMicrosecondsEv(sf::Time self)
{
    return self.asMicroseconds();
}

extern "C" void ___ZN2sf4Time4ZeroE(sf::Time * result)
{
    new (result) sf::Time(std::move(sf::Time::Zero));
}

extern "C" void ___ZN2sf7secondsEf(sf::Time * result, float amount)
{
    new (result) sf::Time(sf::seconds(amount));
}

extern "C" void ___ZN2sf12millisecondsEi(sf::Time * result, int amount)
{
    new (result) sf::Time(sf::milliseconds(amount));
}

extern "C" void ___ZN2sf12microsecondsEx(sf::Time * result, long long amount)
{
    new (result) sf::Time(sf::microseconds(amount));
}

extern "C" bool ___ZN2sfeqENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self == other;
}

extern "C" bool ___ZN2sfneENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self != other;
}

extern "C" bool ___ZN2sfltENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self < other;
}

extern "C" bool ___ZN2sfleENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self <= other;
}

extern "C" bool ___ZN2sfgtENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self > other;
}

extern "C" bool ___ZN2sfgeENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self >= other;
}

extern "C" void ___ZN2sfngENS_4TimeE(sf::Time * result, sf::Time self)
{
    new (result) sf::Time(-self);
}

extern "C" void ___ZN2sfplENS_4TimeES0_(sf::Time * result, sf::Time self, sf::Time other)
{
    new (result) sf::Time(self + other);
}

extern "C" void ___ZN2sfpLERNS_4TimeES0_(sf::Time & self, sf::Time other)
{
    self += other;
}

extern "C" void ___ZN2sfmiENS_4TimeES0_(sf::Time * result, sf::Time self, sf::Time other)
{
    new (result) sf::Time(self - other);
}

extern "C" void ___ZN2sfmIERNS_4TimeES0_(sf::Time & self, sf::Time other)
{
    self -= other;
}

extern "C" void ___ZN2sfmlENS_4TimeEf(sf::Time * result, sf::Time self, float amount)
{
    new (result) sf::Time(self * amount);
}

extern "C" void ___ZN2sfmlENS_4TimeEx(sf::Time * result, sf::Time self, long long amount)
{
    new (result) sf::Time(self * amount);
}

extern "C" void ___ZN2sfmLERNS_4TimeEf(sf::Time & self, float amount)
{
    self *= amount;
}

extern "C" void ___ZN2sfmLERNS_4TimeEx(sf::Time & self, long long amount)
{
    self *= amount;
}

extern "C" void ___ZN2sfdvENS_4TimeEf(sf::Time * result, sf::Time self, float amount)
{
    new (result) sf::Time(self / amount);
}

extern "C" void ___ZN2sfdvENS_4TimeEx(sf::Time * result, sf::Time self, long long amount)
{
    new (result) sf::Time(self / amount);
}

extern "C" void ___ZN2sfdVERNS_4TimeEf(sf::Time & self, float amount)
{
    self /= amount;
}

extern "C" void ___ZN2sfdVERNS_4TimeEx(sf::Time & self, long long amount)
{
    self /= amount;
}

extern "C" float ___ZN2sfdvENS_4TimeES0_(sf::Time self, sf::Time other)
{
    return self / other;
}

extern "C" void ___ZN2sfrmENS_4TimeES0_(sf::Time * result, sf::Time self, sf::Time other)
{
    new (result) sf::Time(self % other);
}

extern "C" void ___ZN2sfrMERNS_4TimeES0_(sf::Time & self, sf::Time other)
{
    self %= other;
}
