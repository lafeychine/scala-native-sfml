#include "SFML/Graphics.hpp"

#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"

extern "C" void ___ZN2sf5ClockC2Ev(sf::Clock * result)
{
    new (result) sf::Clock();
}

extern "C" void ___ZN2sf5ClockD2Ev(sf::Clock & self)
{
    self.~Clock();
}

extern "C" void ___ZNK2sf5Clock14getElapsedTimeEv(sf::Time * result, sf::Clock & self)
{
    new (result) sf::Time(self.getElapsedTime());
}

extern "C" void ___ZN2sf5Clock7restartEv(sf::Time * result, sf::Clock & self)
{
    new (result) sf::Time(self.restart());
}
