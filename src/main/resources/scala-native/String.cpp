#include <string>

namespace std
{
    /* string */
    class String
    {
      public:
        String(const char * string);
        ~String();

      private:
        basic_string<char> _string;
    };

    String::String(const char * string)
        : _string(string)
    {
    }

    String::~String()
    {
    }

} // namespace std
