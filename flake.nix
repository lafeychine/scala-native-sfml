{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-24.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      flake-utils,
      nixpkgs,
    }:
    flake-utils.lib.eachSystem [ "x86_64-darwin" "x86_64-linux" ] (
      system:
      let
        pkgs = import nixpkgs { inherit system; };

        sfml = pkgs.sfml.overrideAttrs (oldAttrs: rec {
          version = "2.6.2";

          src = pkgs.fetchFromGitHub {
            owner = "SFML";
            repo = "SFML";
            rev = version;
            sha256 = "sha256-m8FVXM56qjuRKRmkcEcRI8v6IpaJxskoUQ+sNsR1EhM=";
          };

          buildInputs = oldAttrs.buildInputs ++ [pkgs.xorg.libXcursor];
          cmakeFlags = oldAttrs.cmakeFlags ++ ["-DCMAKE_INSTALL_LIBDIR=lib"];
          patches = [];
        });

        buildInputs = [ sfml ];

        nativeBuildInputs =
          (with pkgs; [
            boehmgc
            sbt
          ])
          ++ (with pkgs.llvmPackages; [
           clang
           lld
           llvm
          ]);

        checkInputs = with pkgs; [
          doxygen
          gnumake
          xorg.xorgserver
          xorg.xset
        ];
      in
      rec {
        devShell = pkgs.mkShell {
          name = "scala-native-sfml";
          packages = buildInputs ++ checkInputs ++ nativeBuildInputs ++ [ pkgs.metals ];

          env = {
            NIX_CFLAGS_COMPILE = "-Wno-unused-command-line-argument";
          };
          hardeningDisable = [ "fortify" ];
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
