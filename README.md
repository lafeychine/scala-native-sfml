# [WIP] SFML bindings for Scala Native 3

This repository is a safe [Scala Native](https://scala-native.org/en/stable/) binding of the [SFML graphic library](https://www.sfml-dev.org/).

The [Scala-specific documentation](https://lafeychine.codeberg.page/scala-native-sfml) is a port of the [official C++ documentation](https://www.sfml-dev.org/documentation).

## Example

```scala
import sfml.graphics.*
import sfml.window.*

@main def main =
    val window = RenderWindow(VideoMode(1024, 768), "Hello world")

    val texture = Texture()
    texture.loadFromFile("cat.png")

    val sprite = Sprite(texture)

    while window.isOpen() do
        for event <- window.pollEvent() do
            event match
                case Event.Closed() => window.close()
                case _              => ()

        window.clear(Color.Black())

        window.draw(sprite)

        window.display()
```


## Installation

### Requirements

This library requires [Scala Native](https://scala-native.org/) and [Scala 3](https://www.scala-lang.org/), with the [Boehm garbage collector](https://github.com/ivmai/bdwgc).

It is recommended to use [sbt](https://www.scala-sbt.org/download.html) to build a project, by setting up `project/plugins.sbt` and `build.sbt` as follows:

```scala sc:nocompile
// project/plugins.sbt
addSbtPlugin("org.scala-native" % "sbt-scala-native" % "x.y.z") // Version of Scala Native
```

```scala sc:nocompile
// build.sbt
import scala.scalanative.build.*

name := "project"

scalaVersion := "3.y.z" // Version of Scala
version := "x.y.z"      // Version of your project

enablePlugins(ScalaNativePlugin)

nativeConfig ~= { _.withGC(GC.boehm) }
```

Please refer to [`sbt` settings and tasks](https://scala-native.org/en/stable/user/sbt.html#sbt-settings-and-tasks) for additional customisation.


### Fetching the library

The library is not published on Maven, as it is not mature yet.

Nonetheless, packages are accessible via [Codeberg Packages](https://codeberg.org/lafeychine/-/packages/maven/org.codeberg.lafeychine-scala-native-sfml_native0.4_3/).

To retrieve the library, the registry of this package must be added as a resolver and can be configured in `build.sbt`, for example:

```scala sc:nocompile
// build.sbt
resolvers += "Scala Native SFML" at "https://codeberg.org/api/packages/lafeychine/maven"
libraryDependencies += "org.codeberg.lafeychine" %%% "scala-native-sfml" % "x.y.z" // Version of Scala Native SFML
```


## Notes

The typical way to use SFML in a foreign language is via the C ABI with [CSFML](https://github.com/SFML/CSFML). However, this project makes the choice to use the C++ ABI directly.

The use of the C++ API can be motivated by the fact that both C++ and Scala revolve around an object-oriented type system; binding the two directly is in that sense more straightforward than using the C API indirection, and allows to transport more features of the library into Scala.

On the other hand, the C++ ABI is largely unstable, and this project is also a pretext to experiment with the internals of the language.

As a result, this library is, for now, only compatible with the `x86_64-linux-gnu` platform.
