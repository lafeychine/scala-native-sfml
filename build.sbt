import scala.scalanative.build.*

val root = Project(id = "scala-native-sfml", base = file("."))
    .enablePlugins(Base, Linter, Native, Publish, XorgTests)
    .settings(
        name := "Scala Native SFML",

        /* Specific compilation values */
        scalacOptions += "-language:implicitConversions",
        Test / nativeConfig := nativeConfig.value
            .withCompileOptions(Seq("-fsanitize=leak"))
            .withLinkingOptions(Seq("-fsanitize=leak")),

        /* Documentation */
        Compile / doc / scalacOptions :=
            Seq("-doc-root-content", ((Compile / scalaSource).value / "rootdoc.txt").toString) ++
                Seq("-project", name.value) ++
                Seq("-siteroot", "docs") ++
                Seq("-snippet-compiler:compile"),
        Compile / doc / target := target.value / "docs",

        /* Publish */
        organization := "org.codeberg.lafeychine",
        publishTo := Some("Codeberg" at "https://codeberg.org/api/packages/lafeychine/maven")
    )

val doxygen = Project(id = "doxygen-checker", base = file("tools/doxygen"))
    .enablePlugins(Base, Doxygen)
    .dependsOn(root)
    .settings(
        name := "Doxygen checker",
        scalacOptions += "-experimental",
        setupDoxygenChecker(root),
        libraryDependencies ++= Seq(
            "org.scala-lang" %% "scaladoc" % scalaVersion.value,
            "org.scala-lang.modules" %% "scala-xml" % getProperty("doxygen.xml.version"),
            "org.scalatest" %% "scalatest" % getProperty("doxygen.test.version") % Test
        )
    )
