def getProperty(key: String): String = {
    import com.typesafe.config.ConfigFactory

    ConfigFactory.parseFile(new File("project/build.properties")).getString(key)
}

/* Build */
addSbtPlugin("org.scala-native" % "sbt-scala-native" % getProperty("scalaNative.version"))

/* Lints */
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % getProperty("scalafix.version"))
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % getProperty("scalafmt.version"))
addSbtPlugin("org.wartremover" % "sbt-wartremover" % getProperty("wartremover.version"))
addSbtPlugin("org.wartremover" % "sbt-wartremover-contrib" % getProperty("wartremoverContrib.version"))

/* Versioning */
addSbtPlugin("com.github.sbt" % "sbt-dynver" % getProperty("dynver.version"))
