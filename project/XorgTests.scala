import collection.immutable.Map
import java.io.File
import sys.process.Process

import Base.autoImport.*
import Native.autoImport.*

import sbt.{AutoPlugin, Test, settingKey}
import sbt.Keys.{baseDirectory, envVars, parallelExecution, target, testOptions}
import sbt.Tests.{Cleanup, Setup}
import sbt.internal.util.ConsoleLogger

import scalanative.sbtplugin.ScalaNativeJUnitPlugin

object XorgTests extends AutoPlugin {
    override def requires = Base && Native && ScalaNativeJUnitPlugin

    override lazy val projectSettings = Seq(
        Test / envVars := setupEnv(target.value, targetsLinux.value),
        Test / parallelExecution := false,
        Test / testOptions += Setup(() => setupTests(baseDirectory.value, target.value, targetsLinux.value)),
        Test / testOptions += Cleanup(() => cleanupTests(target.value, targetsLinux.value))
    )

    private def setupEnv(target: File, targetsLinux: Boolean): Map[String, String] = {
        val genericEnv = Map(
            "LSAN_OPTIONS" -> "print_suppressions=false:suppressions=src/test/leak.txt",
            "SCALANATIVE_TEST_DEBUG_SIGNALS" -> "1",
            "SCALANATIVE_TEST_PREFETCH_DEBUG_INFO" -> "1",
        )

        val linuxEnv = genericEnv ++ Map(
            "DISPLAY" -> ":99",
            "GALLIUM_DRIVER" -> "llvmpipe",
            "SNSFML_SCREENSHOT_FOLDER_PATH" -> (target.getAbsolutePath() + "/screenshots")
        )

        if (targetsLinux) { linuxEnv } else { genericEnv }
    }

    private def setupTests(baseDirectory: File, target: File, targetsLinux: Boolean): Unit = {
        var attempt = 0

        ConsoleLogger().info("Building C++ tests")
        if (Process("make -C src/test/cxx", baseDirectory).! != 0) {
            throw new Exception("Failed to compile C++ test suite")
        }

        if (!targetsLinux) { return }

        if (Process("mkdir screenshots", target).! != 0) {
            if (Process("rm -rf screenshots", target).! != 0) {
                throw new Exception("Failed to remove screenshot directory")
            }

            if (Process("mkdir screenshots", target).! != 0) {
                throw new Exception("Failed to create screenshot directory")
            }
        }

        Process("Xvfb :99 -screen 0 1024x768x24 -nolisten tcp").run(devnull);

        while (Process("xset q", baseDirectory, "DISPLAY" -> ":99").!(devnull) != 0) {
            if (attempt > 30) {
                throw new Exception("Failed to start Xvfb")
            }

            Thread.sleep(100)

            attempt += 1
        }
    }

    private def cleanupTests(target: File, targetsLinux: Boolean): Unit = {
        if (!targetsLinux) { return }

        if (Process("killall Xvfb").! != 0) {
            throw new Exception("Failed to stop Xvfb")
        }

        if (Process("rm -rf screenshots", target).! != 0) {
            throw new Exception("Failed to remove screenshot directory")
        }
    }
}
