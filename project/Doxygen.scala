import java.io.File
import sys.process.Process

import sbt.*
import sbt.Keys.*
import sbt.complete.DefaultParsers.spaceDelimited

import Base.autoImport.*

object Doxygen extends AutoPlugin {
    override def requires = Base

    object autoImport {
        def setupDoxygenChecker(source: Project) =
            run := Def.inputTaskDyn {
                val doxyfile = target.value / "doxyfile"

                val TASTyFolder = (source / Compile / classDirectory).value
                val XMLFolder = target.value / "xml"

                val doxygenFolder = spaceDelimited("<arg>").parsed.headOption.map(file).getOrElse {
                    throw new Exception("The source directory of the project to call Doxygen on has to be specified")
                }

                val doxyfileContent = """
                    INPUT                  = """ + doxygenFolder.getAbsolutePath() + """
                    OUTPUT_DIRECTORY       = """ + target.value.getAbsolutePath() + """

                    FILE_PATTERNS          = *.hpp

                    GENERATE_HTML          = NO
                    GENERATE_XML           = YES

                    CLASS_GRAPH            = NO
                    RECURSIVE              = YES
                    QUIET                  = YES
                """

                IO.write(doxyfile, doxyfileContent)

                ConsoleLogger().info(f"Generating Doxygen documentation for ${doxygenFolder}")
                if (Process(f"doxygen ${doxyfile}", target.value).!(devnull) != 0) {
                    throw new Exception("Failed to generate Doxygen documentation")
                }

                Def.taskDyn {
                    (Compile / run).toTask(f" ${TASTyFolder} ${XMLFolder}")
                }
            }.evaluated
    }
}
