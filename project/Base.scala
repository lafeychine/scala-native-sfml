import java.io.File
import scala.collection.immutable.Map
import sys.process.ProcessLogger

import sbt.AutoPlugin
import sbt.Keys.{publish, scalaVersion, skip}

object Base extends AutoPlugin {
    object autoImport {
        val devnull: ProcessLogger = ProcessLogger(_ => ())

        def getProperty(key: String): String = {
            import com.typesafe.config.ConfigFactory

            ConfigFactory.parseFile(new File("project/build.properties")).getString(key)
        }
    }

    import autoImport.*

    override lazy val projectSettings = Seq(
        scalaVersion := getProperty("scala.version"),
        publish / skip := true
    )
}
