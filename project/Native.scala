import sbt.{AutoPlugin, settingKey}

import Base.autoImport.*

import java.util.Locale

import scala.scalanative.build.{GC, Mode, LTO, Sanitizer}
import scala.scalanative.sbtplugin.ScalaNativePlugin
import scala.scalanative.sbtplugin.ScalaNativePlugin.autoImport.*

object Native extends AutoPlugin {
    override def requires = Base && ScalaNativePlugin

    object autoImport {
        object BuildMode {
            sealed trait Mode
            case object Debug extends Mode
            case object Release extends Mode
        }

        val buildMode = settingKey[BuildMode.Mode]("Configuration of the build mode")
        val targetsLinux = settingKey[Boolean]("Check if the target is Linux")
        val targetsMac = settingKey[Boolean]("Check if the target is macOS")
    }

    import autoImport.*

    lazy val osUsed = System.getProperty("os.name", "unknown").toLowerCase(Locale.ROOT)

    override lazy val projectSettings = Seq(
        buildMode := BuildMode.Release,

        targetsLinux := osUsed.contains("linux"),
        targetsMac := osUsed.contains("mac"),

        nativeConfig := {
            buildMode.value match {
                case BuildMode.Debug =>
                    nativeConfig.value
                        .withGC(GC.boehm)
                        .withIncrementalCompilation(true)
                        .withLTO(LTO.none)
                        .withMode(Mode.debug)
                        .withMultithreading(Some(false))
                        .withOptimize(false)
                        .withSourceLevelDebuggingConfig(_.enableAll)

                case BuildMode.Release =>
                    nativeConfig.value
                        .withGC(GC.boehm)
                        .withIncrementalCompilation(true)
                        .withLTO(if (targetsMac.value) { LTO.full } else { LTO.thin })
                        .withMode(Mode.release)
                        .withMultithreading(Some(false))
                        .withOptimize(true)
            }
        }
    )
}
