import java.net.{HttpURLConnection, URL, URLConnection}
import java.io.{InputStream, IOException, File, FileInputStream}

import sbt.{AutoPlugin, Credentials, DirectCredentials, fileToRichFile, taskKey}
import sbt.Keys.{credentials, publish, publishMavenStyle, skip}
import sbt.Path.userHome

import org.apache.ivy.Ivy
import org.apache.ivy.util.{CopyProgressListener, FileUtil}
import org.apache.ivy.util.url.{BasicURLHandler, IvyAuthenticator, URLHandler, URLHandlerDispatcher, URLHandlerRegistry}

object Publish extends AutoPlugin {
    override def requires = Versioning

    private val authURLHandler = taskKey[Unit]("Perform auth using header credentials.")

    override lazy val projectSettings = Seq(
        authURLHandler := installAuthURLHandler(credentials.value),
        credentials ++= retrieveCredentials,
        publish := publish.dependsOn(authURLHandler).value,
        publish / skip := false,
        publishMavenStyle := true,
    )

    private def installAuthURLHandler(credentials: Seq[sbt.librarymanagement.ivy.Credentials]): Unit = {
        val credential = Credentials.allDirect(credentials).find(_.realm == "HTTP-Auth")

        if (credential.isEmpty) {
            return ()
        }

        URLHandlerRegistry.setDefault(
            new URLHandlerDispatcher {
                val customURLHandler = CustomURLHandler(credential.get)

                super.setDownloader("http", customURLHandler)
                super.setDownloader("https", customURLHandler)

                override def setDownloader(protocol: String, downloader: URLHandler): Unit = {}
            }
        )
    }

    private def retrieveCredentials: Seq[sbt.librarymanagement.ivy.Credentials] = {
        val envCredentials = for {
            realm <- sys.env.get("CI_REALM")
            host <- sys.env.get("CI_HOST")
            user <- sys.env.get("CI_USER")
            password <- sys.env.get("CI_PASSWORD")
        } yield Credentials(realm, host, user, password)

        val credentialsFile = userHome / ".sbt" / ".credentials"

        if (credentialsFile.exists) {
            Credentials(credentialsFile) +: envCredentials.toSeq
        } else {
            envCredentials.toSeq
        }
    }
}

/* Implementation of the methods below are copied directly from the BasicURLHandler.
 *
 * @see: https://github.com/apache/ant-ivy/blob/master/src/java/org/apache/ivy/util/url/BasicURLHandler.java
 */
private case class CustomURLHandler(credentials: DirectCredentials) extends BasicURLHandler {
    private val BUFFER_SIZE = 64 * 1024

    private def readResponseBody(conn: HttpURLConnection): Unit = {
        val buffer = new Array[Byte](BUFFER_SIZE)
        var inStream: InputStream = null

        try {
            inStream = conn.getInputStream
            while (inStream.read(buffer) > 0) {}
        } catch {
            case _: IOException => ()
        } finally if (inStream != null) try inStream.close() catch {
            case _: IOException => ()
        }

        val errStream = conn.getErrorStream
        if (errStream != null) {
            try while (errStream.read(buffer) > 0) {} catch {
                case _: IOException => ()
            } finally try errStream.close() catch {
                case _: IOException => ()
            }
        }
    }

    private def disconnect(conn: URLConnection): Unit = {
        conn match {
            case connection: HttpURLConnection => {
                if ("HEAD" != connection.getRequestMethod) {
                    readResponseBody(connection)
                }
                connection.disconnect()
            }

            case _ => {
                if (conn != null) try conn.getInputStream.close() catch {
                    case _: IOException => {}
                }
            }
        }
    }

    override def upload(source: File, dest: URL, listener: CopyProgressListener): Unit = {
        if (!("http" == dest.getProtocol) && !("https" == dest.getProtocol)) {
            throw new UnsupportedOperationException("URL repository only support HTTP PUT at the moment")
        }

        IvyAuthenticator.install()

        val normalizedDestURL = normalizeToURL(dest)
        val conn = normalizedDestURL.openConnection.asInstanceOf[HttpURLConnection]
        conn.setDoOutput(true)
        conn.setRequestMethod("PUT")
        conn.setRequestProperty("User-Agent", "Apache Ivy/" + Ivy.getIvyVersion)
        conn.setRequestProperty("Accept", "*/*")
        conn.setRequestProperty("Content-type", "application/octet-stream")
        conn.setRequestProperty("Content-length", source.length().toString)
        conn.setRequestProperty(credentials.userName, credentials.passwd)
        conn.setInstanceFollowRedirects(true)

        var in: FileInputStream = null
        try {
            in = new FileInputStream(source)
            val os = conn.getOutputStream
            FileUtil.copy(in, os, listener)
        } finally try in.close() catch { case _: IOException => () }

        validatePutStatusCode(normalizedDestURL, conn.getResponseCode, conn.getResponseMessage)

        disconnect(conn)
    }
}
