import sbt.AutoPlugin
import sbt.Keys.{scalacOptions, semanticdbEnabled, sourceDirectory}
import sbt.*

import Base.autoImport.*

import wartremover.WartRemover
import wartremover.WartRemover.autoImport.*

import org.wartremover.contrib.ContribWarts
import org.wartremover.contrib.ContribWarts.autoImport.*

import scalafix.sbt.ScalafixPlugin
import scalafix.sbt.ScalafixPlugin.autoImport.*

object Linter extends AutoPlugin {
    override def requires = Base && ScalafixPlugin && WartRemover && ContribWarts

    private val scalacFlags = Seq(
        "-deprecation",
        "-feature",
        "-source:future",
        "-unchecked",
        "-Werror",
        "-Wunused:all",
        "-Wunused:strict-no-implicit-warn",
        "-Wunused:unsafe-warn-patvars",
        "-Wvalue-discard"
    )

    private val builtinWarts = Warts.allBut(
        Wart.AsInstanceOf,
        Wart.DefaultArguments,
        Wart.Equals,
        Wart.ImplicitConversion,
        Wart.ImplicitParameter,
        Wart.Overloading,
        Wart.SeqApply,
        Wart.TryPartial,
        Wart.Var
    )

    private val communityWarts = Seq(
        ContribWart.MissingOverride,
        ContribWart.NoNeedForMonad,
        ContribWart.SealedCaseClass,
        ContribWart.SomeApply,
        // ContribWart.SymbolicName
    )

    override lazy val projectSettings = Seq(
        scalacOptions ++= scalacFlags,
        scalafixDependencies += "com.github.xuwei-k" %% "scalafix-rules" % getProperty("scalafixRules.version"),
        semanticdbEnabled := true,
        wartremoverErrors := builtinWarts ++ communityWarts,
        wartremoverExcluded += sourceDirectory.value / "main" / "scala" / "sfml" / "internal"
    )
}
