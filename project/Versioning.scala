import sbt.{AutoPlugin, ThisBuild}
import sbt.Keys.{version, versionScheme}

import sbtdynver.{DynVerPlugin, GitDescribeOutput}
import sbtdynver.DynVerPlugin.autoImport.*

object Versioning extends AutoPlugin {
    override def requires = DynVerPlugin

    override lazy val projectSettings = Seq(
        version := dynverGitDescribeOutput.value.mkVersion(versionFmt, "0.0.0-SNAPSHOT"),
        versionScheme := Some("early-semver")
    )

    private def versionFmt(out: GitDescribeOutput): String = {
        if (out.commitSuffix.distance == 0) {
            return out.ref.dropPrefix
        }

        out.ref.dropPrefix.split('.') match {
            case Array(major, minor, patch) => s"$major.$minor.${patch.toInt + 1}-SNAPSHOT"
        }
    }
}
