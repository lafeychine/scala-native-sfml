package tools.doxygen

import scala.util.boundary, boundary.{break, Label}
import scala.util.Try

import dotty.tools.dotc.core.Comments.Comment
import dotty.tools.scaladoc.tasty.comments.Cleaner
import xml.{Elem, Node, NodeSeq, XML}

import Doxygen.{Hash, Id, Tag}

object Checker:
    def checkDocstring(XMLFolder: String, comment: Comment): Option[String] =
        import Canonicalise.canonicaliseText

        boundary:
            val (id, scalaTag) = findDoxygenTags(comment)
                .expect("This symbol is not linked to the Doxygen documentation")

            val xml = Try(XML.loadFile(XMLFolder + "/" + id.getFile + ".xml"))
                .expect(errMsg => f"Problem reading ${errMsg}")

            val doxygenContent = retrieveDocumentTags(xml, id)
                .expect(f"Doxygen documentation not found (id: ${id})")
                .map(_.text)
                .toList
                .canonicaliseText(isScala = false)

            scalaTag match
                case Left(scalaHash) =>
                    val doxygenHash = Hash(doxygenContent)

                    (doxygenHash == scalaHash)
                        .expect(f"""
                            |Hash differs (id: ${id}):
                            |  Expected: ${doxygenHash}
                            |  Got: ${scalaHash}
                            |""".stripMargin.trim)

                case Right(scalaContent) =>
                    (doxygenContent
                        .zipAll(scalaContent.canonicaliseText(isScala = true), "*empty*", "*empty*"))
                        .find((expected, got) => expected != got)
                        .foreach((expected, got) => {
                            val doxygenHash = Hash(doxygenContent)
                            val msg = f"""
                                |Documentation differs (id: ${id}):
                                |  Expected: ${expected}
                                |  Got: ${got}
                                |
                                |Use this hash if the documentation cannot match: ${doxygenHash}
                                |""".stripMargin.trim

                            break(Some(msg))
                        })

            None

    private def findDoxygenTags(comment: Comment): Option[Tag] =
        val lines = Cleaner.clean(comment.raw).filter(_.nonEmpty)

        lines.reverse match
            case hash :: "@doxygenHash" :: id :: "@doxygenId" :: _ => Some(Id(id), Left(Hash(hash)))
            case id :: "@doxygenId" :: xs                          => Some(Id(id), Right(xs.reverse))
            case _                                                 => None

    private def retrieveDocumentTags(xml: Elem, id: Id): NodeSeq =
        val patterns =
            (xml \ "compounddef") ++
                (xml \ "compounddef" \ "sectiondef" \ "memberdef") ++
                (xml \ "compounddef" \ "sectiondef" \ "memberdef" \ "enumvalue")

        val compound = patterns.find(pattern => Id(pattern \@ "id") == id).getOrElse(NodeSeq.Empty)

        compound \@ "kind" match
            case "class" =>
                val compoundVariables = (compound \ "sectiondef" \ "memberdef")
                    .filter(member => (member \@ "kind") == "variable" && (member \@ "prot") != "private")
                    .foldLeft(NodeSeq.Empty)((seq, v) =>
                        seq ++ (v \ "name") ++ (v \ "briefdescription") ++ (v \ "detaileddescription")
                    )

                (compound \ "briefdescription") ++
                    (compound \ "detaileddescription") ++
                    compoundVariables

            case _ =>
                (compound \ "briefdescription") ++
                    (compound \ "detaileddescription")
