package tools.doxygen

import scala.util.matching, matching.Regex, matching.Regex.Match

object Canonicalise:
    val wordsToRemove = List("`", "\"", "–", "\\(\\s*\\)")
    val wordsToRemoveScaladoc = List("```scala", "sc:nocompile", "@\\w*", "^[\\s]*- ", "`") ++ wordsToRemove
    val wordsToRemoveDoxygen = List("Deprecated", "\\w+::", "(Usage )?[eE]xample:", "(pure )?virtual") ++ wordsToRemove

    val regexesScaladoc: List[(Regex, Match => String)] = List(
        (wordsToRemoveScaladoc.mkString("|").r, (_: Match) => ""),
        ("\\[\\[([^\\] ]* )([^\\]]*)\\]\\]".r, m => m.group(2)),
        ("\\[\\[([^\\]]*\\.)?([^\\]]*)\\]\\]".r, m => m.group(2)),
        ("\\[([^\\]]*)\\]\\(([^\\)]*)\\)".r, m => m.group(1))
    )
    val regexesDoxygen: List[(Regex, Match => String)] = List(
        (wordsToRemoveDoxygen.mkString("|").r, (_: Match) => ""),
        ("(^|[^[a-zA-Z]])(is|get)([A-Z])(\\w*)".r, m => m.group(1) + m.group(3).toLowerCase + m.group(4)),
        ("(^|[^[a-zA-Z]])set([A-Z])(\\w*)".r, m => m.group(1) + m.group(2).toLowerCase + m.group(3) + "_="),
        ("(a |A )?(null|NULL)( pointer)?".r, _ => "None")
    )

    extension (line: String)
        def replaceAllRegexes(regexes: List[(Regex, Match => String)]): String =
            regexes.foldLeft(line)((line, regex) => regex._1.replaceAllIn(line, regex._2))

    extension (content: List[String])
        def canonicaliseText(isScala: Boolean): List[String] =
            content
                .map(_.replaceAllRegexes(if isScala then regexesScaladoc else regexesDoxygen))
                .map(_.trim)
                .mkString(" ")
                .replaceAll("\\s+", " ")
                .split(Array('.', ':'))
                .map(_.trim)
                .filter(_.nonEmpty)
                .toList
