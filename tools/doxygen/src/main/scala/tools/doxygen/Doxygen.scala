package tools.doxygen

import java.security.MessageDigest
import java.util.HexFormat

object Doxygen:
    val hash = MessageDigest.getInstance("MD5")

    /* Id */
    opaque type Id = String

    object Id:
        def apply(id: String): Id = id.trim
    given CanEqual[Id, Id] = CanEqual.derived

    extension (id: Id) def getFile: String = id.split('_').take(3).mkString("_")

    /* Hash */
    opaque type Hash = String

    object Hash:
        def apply(hash: String): Hash = hash.trim
        def apply(lines: List[String]): Hash =
            Hash(HexFormat.of().formatHex(hash.digest(lines.mkString("$").getBytes)))
    given CanEqual[Hash, Hash] = CanEqual.derived

    /* Tag */
    type Tag = (Id, Either[Hash, List[String]])
