package tools.doxygen

import scala.util.boundary.{break, Label}
import scala.util.{Failure, Success, Try}

import xml.NodeSeq

extension (x: Boolean)
    def expect[F](f: F)(using Label[Option[F]]): Unit =
        if x then () else break(Some(f))

extension [A](x: Option[A])
    def expect[F](f: F)(using Label[Option[F]]): A =
        x.getOrElse(break(Some(f)))

extension [A](x: Try[A])
    def expect[F](f: String => F)(using Label[Option[F]]): A =
        x match
            case Success(v) => v
            case Failure(e) => break(Some(f(e.getMessage)))

extension (x: NodeSeq)
    def expect[F](f: F)(using Label[Option[F]]): NodeSeq =
        if x != NodeSeq.Empty then x else break(Some(f))
