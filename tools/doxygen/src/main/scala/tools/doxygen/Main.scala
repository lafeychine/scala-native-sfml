package tools.doxygen

import java.nio.file.{Files, Path, Paths}
import scala.collection.JavaConverters.asScalaIteratorConverter

@main def main(TASTyFolder: String, XMLFolder: String) =
    Files
        .walk(Paths.get(TASTyFolder))
        .iterator
        .asScala
        .filter(_.toString.endsWith(".tasty"))
        .iterator
        .foreach(checkTASTyFile(TASTyFolder, XMLFolder))

def checkTASTyFile(TASTyFolder: String, XMLFolder: String)(TASTyPath: Path) =
    val prettyTASTyPath = TASTyPath.toString.stripPrefix(TASTyFolder + "/")

    for (comment, symbol) <- Unpickler.parseTASTy(TASTyPath) do
        Checker
            .checkDocstring(XMLFolder, comment)
            .foreach(reportMsg(prettyTASTyPath, symbol))

def reportMsg(path: String, symbol: String)(msg: String) =
    println(f"-- In ${path} (${symbol}) " + "-" * (60 - path.length - symbol.length))
    println(msg)
    println()
