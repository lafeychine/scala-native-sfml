package tools.doxygen

import java.nio.file.{Files, Path, Paths}

import dotty.tools.dotc.core.tasty.{CommentUnpickler, TastyUnpickler}
import dotty.tools.dotc.core.Comments.Comment
import dotty.tools.dotc.core.tasty.TastyUnpickler.{NameTable, SectionUnpickler}
import dotty.tools.dotc.util.HashMap

import dotty.tools.tasty.TastyBuffer.{Addr, NameRef}
import dotty.tools.tasty.TastyFormat.{ASTsSection, CommentsSection}
import dotty.tools.tasty.TastyReader

object Unpickler:

    def parseTASTy(path: Path): List[(Comment, String)] =
        val tasty = TastyUnpickler(Files.readAllBytes(path))

        tasty
            .unpickle(CommentsUnpickler())
            .getOrElse(List.empty)
            .map((addr, comment) => (comment, tasty.unpickle(SymbolUnpickler(addr)).get))

    class CommentsUnpickler extends SectionUnpickler[List[(Addr, Comment)]](CommentsSection):
        def unpickle(reader: TastyReader, table: NameTable): List[(Addr, Comment)] =
            classOf[CommentUnpickler]
                .getMethod("comments")
                .invoke(CommentUnpickler(reader))
                .asInstanceOf[HashMap[Addr, Comment]]
                .iterator
                .toList
                .sortBy(_._1.index)
                .distinctBy(_._2)

    class SymbolUnpickler(addr: Addr) extends SectionUnpickler[String](ASTsSection):
        def unpickle(reader: TastyReader, table: NameTable): String =
            import dotty.tools.tasty.TastyFormat.*

            reader.goto(addr)

            if reader.readByte() >= firstLengthTreeTag then reader.readNat()

            table(NameRef(reader.readNat())).debugString
