import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.*

import tools.doxygen.Canonicalise

extension (line: String)
    def doxygen: String =
        import Canonicalise.replaceAllRegexes

        line.replaceAllRegexes(Canonicalise.regexesDoxygen)

class CanonicaliseDoxygenTest extends AnyFunSuite:
    test("A simple text should be untouched") {
        val text = "A simple text (should [be]) untouched"

        assert(text.doxygen == text)
    }

    test("Namespaces should be removed") {
        assert("aaa::b::Hello(    ) aaa::b::World".doxygen == "Hello World")
        assert("(aaa::b::Hello) (aaa::b::World())".doxygen == "(Hello) (World)")
    }

    test("Functions should be transformed") {
        assert("Hello World".doxygen == "Hello World")
        assert("aaa::b::Hello aaa::b::World".doxygen == "Hello World")
    }

    test("Getter should be transformed") {
        assert("getter".doxygen == "getter")
        assert("hellogetTer".doxygen == "hellogetTer")
        assert("getHello getWorld!".doxygen == "hello world!")
    }

    test("Checker should be transformed") {
        assert("is checker".doxygen == "is checker")
        assert("helloisTester".doxygen == "helloisTester")
        assert("isHello isWorld!".doxygen == "hello world!")
    }

    test("Setter should be transformed") {
        assert("setter".doxygen == "setter")
        assert("hellosetTer".doxygen == "hellosetTer")
        assert("setHello setWorld!".doxygen == "hello_= world_=!")
    }

    test("Some tokens should be removed") {
        assert("`Hello` `World`".doxygen == "Hello World")
    }

    test("NULL pointer should be renamed") {
        assert("Hello a NULL pointer".doxygen == "Hello None")
        assert("A null pointer".doxygen == "None")
        assert("null".doxygen == "None")
    }
