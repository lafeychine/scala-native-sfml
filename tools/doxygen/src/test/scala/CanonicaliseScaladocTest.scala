import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.*

import tools.doxygen.Canonicalise

extension (line: String)
    def scaladoc: String =
        import Canonicalise.replaceAllRegexes

        line.replaceAllRegexes(Canonicalise.regexesScaladoc)

class CanonicaliseScaladocTest extends AnyFunSuite:
    test("A simple text should be untouched") {
        val text = "A simple text (should [be]) untouched"

        assert(text.scaladoc == text)
    }

    test("Namespaces inside URI should be removed") {
        assert("[[Hello(   )]] [[World]]".scaladoc == "Hello World")
        assert("[[aaa.b.Hello]] [[aaa.b.World()]]".scaladoc == "Hello World")
    }

    test("Namespaces outside URI should not be transformed") {
        assert("Hello() World".scaladoc == "Hello World")
        assert("aaa.b.Hello aaa.b.World()".scaladoc == "aaa.b.Hello aaa.b.World")
    }

    test("Links URI (markdown) should be removed") {
        assert("[[ccc Hello]] [[c World]]".scaladoc == "Hello World")
        assert("[[c Hello World]]".scaladoc == "Hello World")
        assert("[[aaa.b.c Hello]] [[a.b.c World]]".scaladoc == "Hello World")
        assert("[[aaa.b.c Hello World]]".scaladoc == "Hello World")
    }

    test("Links URI (wiki) should be removed") {
        assert("[Hello](ccc) [World](c)".scaladoc == "Hello World")
        assert("[Hello World](c)".scaladoc == "Hello World")
        assert("[Hello](aaa.b.c) [World](a.b.c)".scaladoc == "Hello World")
        assert("[Hello World](a.b.c)".scaladoc == "Hello World")
    }

    test("Some tokens should be removed") {
        assert("```scala sc:nocompile `Hello` `World`".scaladoc.trim == "Hello World")
        assert(" - Hello World".scaladoc.trim == "Hello World")
        assert("@param Hello World".scaladoc.trim == "Hello World")
    }
